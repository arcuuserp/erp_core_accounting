<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="bankBalance" language="groovy" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" isFloatColumnFooter="true">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="fDateEnd" class="java.lang.String"/>
	<parameter name="dateEnd" class="java.lang.String"/>
	<parameter name="companyName" class="java.lang.String"/>
	<parameter name="sysdb_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="accounting_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="acc_shared_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_trans_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals('0')?" AND transaction_status=0":""]]></defaultValueExpression>
	</parameter>
	<parameter name="advancedFilters" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT * FROM(
		SELECT IF(ROUND(SUM(value_tab.value_cash), 2) >= 0, ROUND(SUM(value_tab.value_cash), 2), 0) AS debit
	, IF(ROUND(SUM(value_tab.value_cash), 2) < 0, ROUND(SUM(value_tab.value_cash), 2)*(-1), 0) AS credit
	, IFNULL(IF(ROUND(SUM(value_tab.value_cash_home), 2) < 0, ROUND(SUM(value_tab.value_cash), 2)*(-1), ROUND(SUM(value_tab.value_cash), 2)), 0) AS 'vl_home'
	, value_tab.id_currency , value_tab.cash_id AS id_cash, value_tab.name, value_tab.code
	, value_tab.currency , value_tab.cur_desc, value_tab.transaction_status, value_tab.transaction_date
	FROM
		(
				SELECT CASE WHEN transaction.transaction_type_id = 0 THEN transaction.total ELSE transaction.total*(-1) END AS value_cash
						, CASE WHEN transaction.transaction_type_id = 0 THEN transaction.home_cur_total ELSE transaction.home_cur_total*(-1) END AS value_cash_home
						, cash.cash_id, cash.id_currency, cash.name, cash.code, cur.name AS currency, cur.description AS cur_desc
                        , transaction.transaction_status, transaction.transaction_date
					FROM $P!{accounting_db}.cash
						LEFT JOIN $P!{accounting_db}.transaction ON transaction.subject_id = cash.cash_id AND transaction.id_account = cash.id_account  $P!{exclude_canceled_trans_cond}
							AND transaction.subject = 'cash' AND transaction.transaction_date <= $P{dateEnd}
						INNER JOIN $P!{db_sysdb}.currencies cur
							ON cash.id_currency = cur.currency_id
					WHERE cash.unit_type = 0
		) value_tab
	GROUP BY value_tab.id_currency, value_tab.cash_id ORDER BY value_tab.id_currency, value_tab.cash_id) alldata WHERE $P!{advancedFilters}]]>
	</queryString>
	<field name="debit" class="java.math.BigDecimal"/>
	<field name="credit" class="java.math.BigDecimal"/>
	<field name="vl_home" class="java.math.BigDecimal"/>
	<field name="id_currency" class="java.lang.String"/>
	<field name="transaction_status" class="java.lang.Integer"/>
	<field name="id_cash" class="java.lang.Long"/>
	<field name="cash_name" class="java.lang.String"/>
	<field name="cash_code" class="java.lang.String"/>
	<field name="currency" class="java.lang.String"/>
	<field name="cur_desc" class="java.lang.String"/>
	<variable name="dateTime" class="java.util.Date">
		<variableExpression><![CDATA[new Date()]]></variableExpression>
	</variable>
	<variable name="totDebit" class="java.math.BigDecimal" resetType="Group" resetGroup="currencyGroup" calculation="Sum">
		<variableExpression><![CDATA[$F{debit}]]></variableExpression>
	</variable>
	<variable name="totCredit" class="java.math.BigDecimal" resetType="Group" resetGroup="currencyGroup" calculation="Sum">
		<variableExpression><![CDATA[$F{credit}]]></variableExpression>
	</variable>
	<variable name="totHome" class="java.math.BigDecimal" resetType="Group" resetGroup="currencyGroup" calculation="Sum">
		<variableExpression><![CDATA[$F{vl_home}]]></variableExpression>
	</variable>
	<variable name="totReportHome" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{vl_home}]]></variableExpression>
	</variable>
	<group name="currencyGroup">
		<groupExpression><![CDATA[$F{id_currency}]]></groupExpression>
		<groupHeader>
			<band height="47">
				<textField>
					<reportElement x="0" y="27" width="64" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="64" y="27" width="194" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$F{cur_desc}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<textField pattern="#,##0.00">
					<reportElement x="258" y="0" width="98" height="20"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{totDebit}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="356" y="0" width="100" height="20"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{totCredit}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="456" y="0" width="98" height="20"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{totHome}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="64" y="0" width="194" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="81" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<textField evaluationTime="Report">
				<reportElement x="520" y="57" width="34" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="498" y="57" width="22" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[nga]]></text>
			</staticText>
			<staticText>
				<reportElement x="356" y="57" width="100" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box rightPadding="4"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Faqe]]></text>
			</staticText>
			<textField>
				<reportElement x="456" y="57" width="42" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy HH.mm.ss">
				<reportElement x="18" y="57" width="46" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{dateTime}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="57" width="18" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[dt.]]></text>
			</staticText>
			<staticText>
				<reportElement x="64" y="57" width="194" height="20"/>
				<box leftPadding="0" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Deri në datë: ]]></text>
			</staticText>
			<textField>
				<reportElement x="0" y="0" width="554" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{companyName}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="258" y="57" width="98" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{fDateEnd}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="20" width="554" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<text><![CDATA[Arka - Gjendja]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="45" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="456" y="0" width="98" height="39"/>
				<box leftPadding="2" bottomPadding="4" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom"/>
				<text><![CDATA[Monedhë Bazë]]></text>
			</staticText>
			<staticText>
				<reportElement x="356" y="19" width="100" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Kredi]]></text>
			</staticText>
			<staticText>
				<reportElement x="258" y="19" width="98" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Debi]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="0" width="64" height="39"/>
				<box leftPadding="2" bottomPadding="4" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Bottom"/>
				<text><![CDATA[Kodi]]></text>
			</staticText>
			<staticText>
				<reportElement x="64" y="0" width="194" height="39"/>
				<box leftPadding="2" bottomPadding="4" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Bottom"/>
				<text><![CDATA[Përshkrimi]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="258" y="0" width="198" height="19" backcolor="#F1EFE2"/>
				<box>
					<topPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[GJENDJE]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="0" width="258" height="20"/>
				<box>
					<topPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement x="456" y="0" width="98" height="20"/>
				<box>
					<topPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="34" splitType="Stretch">
			<textField pattern="#,##0.00">
				<reportElement x="456" y="0" width="98" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{vl_home}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="356" y="0" width="100" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{credit}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="258" y="0" width="98" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{debit}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="0" width="64" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{cash_code}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="64" y="0" width="194" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{cash_name}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="42" splitType="Stretch">
			<textField pattern="#,##0.00">
				<reportElement x="456" y="0" width="98" height="20"/>
				<box rightPadding="2">
					<topPen lineWidth="2.5" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totReportHome}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="356" y="0" width="100" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total]]></text>
			</staticText>
		</band>
	</summary>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="17" width="556" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
