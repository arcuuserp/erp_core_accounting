<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="customerPeriodPayments" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="NoDataSection" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="sysdb_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="accounting_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="acc_shared_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="dateBegin" class="java.lang.String"/>
	<parameter name="dateEnd" class="java.lang.String"/>
	<parameter name="fDateBegin" class="java.lang.String"/>
	<parameter name="fDateEnd" class="java.lang.String"/>
	<parameter name="user_name" class="java.lang.String"/>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="date" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[$P{fDateBegin}+" - "+$P{fDateEnd}]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_docs_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals('0')?" AND header.id_docstatus<>5 ":""]]></defaultValueExpression>
	</parameter>
	<parameter name="customerCondition" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT alldata.id, alldata.contact_display_name, alldata.contact_code
		, IFNULL(alldata.detyrim_periudhe, 0) AS detyrim_periudhe
		, IFNULL(alldata.shlyerje_periudhe, 0) AS shlyerje_periudhe
		, IFNULL(alldata.detyrim_before, 0) AS detyrim_before
		, IFNULL(alldata.shlyerje_before, 0) AS shlyerje_before
	FROM (

	SELECT * FROM (

	SELECT periudha.id, periudha.contact_display_name, periudha.contact_code, SUM(periudha.detyrim) AS detyrim_periudhe
		, SUM(periudha.shlyerje) AS shlyerje_periudhe
	FROM (

	SELECT cus.id, cus.contact_display_name, cus.contact_code
			, header.total_with_vat AS detyrim, 0 AS shlyerje
		FROM $P!{accounting_db}.contacts cus
			LEFT JOIN $P!{accounting_db}.salesinvoice header ON cus.id = header.id_customer $P!{exclude_canceled_docs_cond}
					AND header.document_date >= $P{dateBegin} AND header.document_date <= $P{dateEnd}
		$P!{customerCondition}

	UNION ALL

	SELECT cus.id, cus.contact_display_name, cus.contact_code
			, 0 AS detyrim, header.home_cur_total_cashed AS shlyerje
		FROM $P!{accounting_db}.contacts cus
			LEFT JOIN $P!{accounting_db}.cashreceipt header ON cus.id = header.id_customer $P!{exclude_canceled_docs_cond}
				AND header.document_date >= $P{dateBegin} AND header.document_date <= $P{dateEnd}
		$P!{customerCondition}
		) periudha
		GROUP BY periudha.id
		ORDER BY periudha.id ASC) a

	LEFT JOIN

	(SELECT beforedata.c_id, SUM(beforedata.detyrim) AS detyrim_before
		, SUM(beforedata.shlyerje) AS shlyerje_before
	FROM (

	SELECT cus.id AS c_id, header.total_with_vat AS detyrim, 0 AS shlyerje
		FROM $P!{accounting_db}.contacts cus
			LEFT  JOIN $P!{accounting_db}.salesinvoice header ON cus.id = header.id_customer $P!{exclude_canceled_docs_cond}
					AND header.document_date < $P{dateBegin}
		$P!{customerCondition}

	UNION ALL

	SELECT cus.id AS c_id, 0 AS detyrim, header.home_cur_total_cashed AS shlyerje
		FROM $P!{accounting_db}.contacts cus
			LEFT JOIN $P!{accounting_db}.cashreceipt header ON cus.id = header.id_customer $P!{exclude_canceled_docs_cond}
				AND header.document_date < $P{dateBegin}
		$P!{customerCondition}
		) beforedata

		ORDER BY beforedata.c_id ASC ) b

	ON a.id = b.c_id
	) alldata]]>
	</queryString>
	<field name="customer_id" class="java.lang.Integer"/>
	<field name="customer_name" class="java.lang.String"/>
	<field name="customer_code" class="java.lang.String"/>
	<field name="detyrim_periudhe" class="java.math.BigDecimal"/>
	<field name="shlyerje_periudhe" class="java.math.BigDecimal"/>
	<field name="detyrim_before" class="java.math.BigDecimal"/>
	<field name="shlyerje_before" class="java.math.BigDecimal"/>
	<variable name="mbeturBefore" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{detyrim_before}.subtract( $F{shlyerje_before} )]]></variableExpression>
	</variable>
	<variable name="mbeturPeriod" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{detyrim_periudhe}.subtract( $F{shlyerje_periudhe} )]]></variableExpression>
	</variable>
	<variable name="totalShlyer" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{shlyerje_before}.add( $F{shlyerje_periudhe} )]]></variableExpression>
	</variable>
	<variable name="totalMbetur" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{mbeturBefore}.add( $V{mbeturPeriod} )]]></variableExpression>
	</variable>
	<variable name="dateTime" class="java.util.Date">
		<variableExpression><![CDATA[new Date()]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="80" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="284" y="60" width="74" height="20"/>
				<box leftPadding="0" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Periudha: ]]></text>
			</staticText>
			<textField>
				<reportElement x="654" y="40" width="148" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{user_name}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="752" y="60" width="50" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="60" width="80" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[dt.]]></text>
			</staticText>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy HH.mm.ss">
				<reportElement x="80" y="60" width="130" height="20"/>
				<box leftPadding="2" rightPadding="0"/>
				<textElement textAlignment="Left" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{dateTime}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="654" y="60" width="48" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box rightPadding="4"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Faqe]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="20" width="802" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<text><![CDATA[Klientë: Shlyerje periudhe]]></text>
			</staticText>
			<staticText>
				<reportElement x="728" y="60" width="24" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[nga]]></text>
			</staticText>
			<textField>
				<reportElement x="0" y="0" width="802" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{company_name}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="702" y="60" width="26" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="358" y="60" width="222" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{date}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="40" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="0" y="0" width="80" height="40"/>
				<box leftPadding="2" bottomPadding="4">
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font size="9"/>
				</textElement>
				<text><![CDATA[Klient]]></text>
			</staticText>
			<staticText>
				<reportElement x="80" y="0" width="130" height="40"/>
				<box leftPadding="2" bottomPadding="4">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font size="9"/>
				</textElement>
				<text><![CDATA[Përshkrim]]></text>
			</staticText>
			<staticText>
				<reportElement x="210" y="20" width="74" height="20"/>
				<box rightPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<text><![CDATA[Detyrim]]></text>
			</staticText>
			<staticText>
				<reportElement x="284" y="20" width="74" height="20"/>
				<box rightPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<text><![CDATA[Shlyerje]]></text>
			</staticText>
			<staticText>
				<reportElement x="358" y="20" width="74" height="20"/>
				<box rightPadding="2">
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<text><![CDATA[Mbetje]]></text>
			</staticText>
			<staticText>
				<reportElement x="432" y="20" width="74" height="20"/>
				<box rightPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<text><![CDATA[Detyrim i ri]]></text>
			</staticText>
			<staticText>
				<reportElement x="506" y="20" width="74" height="20"/>
				<box rightPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<text><![CDATA[Shlyerje]]></text>
			</staticText>
			<staticText>
				<reportElement x="580" y="20" width="74" height="20"/>
				<box rightPadding="2">
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<text><![CDATA[Mbetje]]></text>
			</staticText>
			<staticText>
				<reportElement x="654" y="0" width="74" height="40"/>
				<box topPadding="5" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[Total Shlyer]]></text>
			</staticText>
			<staticText>
				<reportElement x="728" y="0" width="74" height="40"/>
				<box topPadding="5" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[Mbetur]]></text>
			</staticText>
			<staticText>
				<reportElement x="654" y="20" width="148" height="20"/>
				<box>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Top">
					<font size="9"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement x="432" y="0" width="222" height="20"/>
				<box>
					<topPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[Gjatë periudhës]]></text>
			</staticText>
			<staticText>
				<reportElement x="210" y="0" width="222" height="20"/>
				<box>
					<topPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="9" isBold="true"/>
				</textElement>
				<text><![CDATA[Të kaluara]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="20" width="210" height="20"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement x="0" y="0" width="80" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customer_code}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="80" y="0" width="130" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customer_name}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="210" y="0" width="74" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{detyrim_before}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="284" y="0" width="74" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{shlyerje_before}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="432" y="0" width="74" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{detyrim_periudhe}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="506" y="0" width="74" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{shlyerje_periudhe}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="358" y="0" width="74" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{mbeturBefore}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="580" y="0" width="74" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{mbeturPeriod}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="654" y="0" width="74" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalShlyer}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="728" y="0" width="74" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalMbetur}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="54" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
		</band>
	</pageFooter>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="13" width="802" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
