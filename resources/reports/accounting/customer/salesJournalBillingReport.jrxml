<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="purchasesJournalBillingReport" language="groovy" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="acc_name" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="dateBegin" class="java.lang.String"/>
	<parameter name="dateEnd" class="java.lang.String"/>
	<parameter name="SUBREPORT_FOOTER" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["repo:/subr/footer"]]></defaultValueExpression>
	</parameter>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="report_title" class="java.lang.String"/>
	<parameter name="name_surname" class="java.lang.String"/>
	<parameter name="month_human" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateBegin}).toString()+" - "+new SimpleDateFormat("yyyy-MM-dd").parse($P{dateEnd}).toString()]]></defaultValueExpression>
	</parameter>
	<parameter name="bottom_message" class="java.lang.String"/>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_docs_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals("true") ? "":"header.id_docstatus<>5 AND "]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT * FROM (
SELECT header.id, header.`document_number`, header.`serial_number`, header.`document_date`
	, header.`description`, header.`customer_code`, header.`currency`
	, c.`contact_display_name`
	, sb.`item_code`,sb.`item_description`,sb.`quantity`
	, sb.unit_name
	, sb.`price`,sb.`home_cur_subtotal_no_vat`,sb.`home_cur_subtotal_vat`, sb.`home_cur_subtotal_with_vat`
	, sb.`subtotal_no_vat`,sb.`subtotal_vat`,sb.`subtotal_with_vat`, CONCAT(w.`code`, "-", w.`name`) AS warehouse
	, header.body_type
	FROM $P!{acc_name}.`salesinvoice` header
		INNER JOIN $P!{acc_name}.`salesinvoicebody` sb ON header.`id` = sb.`id_saleinvoice`
		INNER JOIN $P!{acc_name}.contacts c ON header.`id_contact` = c.id
		LEFT JOIN $P!{acc_name}.warehouse w ON w.`id` = header.`idwarehouse_exit` AND w.deleted = 0
	WHERE $P!{exclude_canceled_docs_cond} header.`document_date` >= $P{dateBegin} AND header.`document_date` <= $P{dateEnd} AND header.body_type = 'items'


UNION ALL

SELECT header.id, header.`document_number`, header.`serial_number`, header.`document_date`
	, header.`description`, header.`customer_code`, header.`currency`
	, c.`contact_display_name`
	, sb.`account_code` AS item_code, sb.`description` AS item_description, 0 AS `quantity`
	, CASE WHEN sb.transaction_type_name = 'credit' THEN 'Kredi' ELSE 'Debi' END AS unit_name
	, 0 AS `price`,sb.`home_cur_subtotal_no_vat`,sb.`home_cur_subtotal_vat`, sb.`home_cur_subtotal_with_vat`
	, sb.`subtotal_no_vat`,sb.`subtotal_vat`,sb.`subtotal_with_vat`, IFNULL(CONCAT(w.`code`, "-", w.`name`),'-') AS warehouse
	, header.body_type
	FROM $P!{acc_name}.salesinvoice header
		INNER JOIN $P!{acc_name}.salesinvoicebodyaccounts sb ON header.`id` = sb.`id_saleinvoice`
		INNER JOIN $P!{acc_name}.contacts c ON header.`id_contact` = c.id
		LEFT JOIN $P!{acc_name}.warehouse w ON w.`id` = header.`idwarehouse_exit` AND w.deleted = 0
	WHERE $P!{exclude_canceled_docs_cond} header.`document_date` >= $P{dateBegin} AND header.`document_date` <= $P{dateEnd} AND header.body_type = 'accounts'
) alldt
ORDER BY alldt.`document_date`, alldt.`document_number`]]>
	</queryString>
	<field name="saleinvoiceheader_id" class="java.lang.Integer"/>
	<field name="document_number" class="java.lang.String"/>
	<field name="serial_number" class="java.lang.String"/>
	<field name="document_date" class="java.sql.Timestamp"/>
	<field name="description" class="java.lang.String"/>
	<field name="customer_code" class="java.lang.String"/>
	<field name="currency" class="java.lang.String"/>
	<field name="company_nipt" class="java.lang.String"/>
	<field name="item_code" class="java.lang.String"/>
	<field name="item_description" class="java.lang.String"/>
	<field name="quantity" class="java.math.BigDecimal"/>
	<field name="unit_name" class="java.lang.String"/>
	<field name="price" class="java.math.BigDecimal"/>
	<field name="home_cur_subtotal_no_vat" class="java.math.BigDecimal"/>
	<field name="home_cur_subtotal_vat" class="java.math.BigDecimal"/>
	<field name="home_cur_subtotal_with_vat" class="java.math.BigDecimal"/>
	<field name="subtotal_no_vat" class="java.math.BigDecimal"/>
	<field name="subtotal_vat" class="java.math.BigDecimal"/>
	<field name="subtotal_with_vat" class="java.math.BigDecimal"/>
	<field name="warehouse" class="java.lang.String"/>
	<field name="body_type" class="java.lang.String"/>
	<variable name="subtotal_no_vat_1" class="java.math.BigDecimal" resetType="Group" resetGroup="saleinvoiceheader_id" calculation="Sum">
		<variableExpression><![CDATA[$F{subtotal_no_vat}]]></variableExpression>
	</variable>
	<variable name="subtotal_vat_1" class="java.math.BigDecimal" resetType="Group" resetGroup="saleinvoiceheader_id" calculation="Sum">
		<variableExpression><![CDATA[$F{subtotal_vat}]]></variableExpression>
	</variable>
	<variable name="subtotal_with_vat_1" class="java.math.BigDecimal" resetType="Group" resetGroup="saleinvoiceheader_id" calculation="Sum">
		<variableExpression><![CDATA[$F{subtotal_with_vat}]]></variableExpression>
	</variable>
	<variable name="total_rows" class="java.lang.Integer" resetType="Group" resetGroup="saleinvoiceheader_id" calculation="Count">
		<variableExpression><![CDATA[$V{REPORT_COUNT}]]></variableExpression>
		<initialValueExpression><![CDATA[]]></initialValueExpression>
	</variable>
	<variable name="variable1" class="java.lang.String"/>
	<group name="saleinvoiceheader_id">
		<groupExpression><![CDATA[$F{saleinvoiceheader_id}]]></groupExpression>
		<groupHeader>
			<band height="12">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="42" height="12"/>
					<box leftPadding="2"/>
					<textElement>
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{document_number}]]></textFieldExpression>
				</textField>
				<textField pattern="dd.MM.yyyy" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="42" y="0" width="44" height="12"/>
					<box leftPadding="2"/>
					<textElement>
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{document_date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="86" y="0" width="84" height="12"/>
					<box leftPadding="2"/>
					<textElement>
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{description}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="274" y="0" width="52" height="12"/>
					<box leftPadding="2"/>
					<textElement>
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{company_nipt}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="326" y="0" width="76" height="12"/>
					<box leftPadding="2">
						<pen lineStyle="Dashed"/>
						<topPen lineWidth="0.5" lineStyle="Dashed"/>
						<leftPen lineWidth="0.5" lineStyle="Dashed"/>
						<bottomPen lineWidth="0.5" lineStyle="Dashed"/>
						<rightPen lineStyle="Dashed"/>
					</box>
					<textElement verticalAlignment="Top">
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{serial_number}]]></textFieldExpression>
				</textField>
				<textField pattern="dd.MM.yyyy" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="402" y="0" width="40" height="12"/>
					<box leftPadding="2">
						<topPen lineWidth="0.5" lineStyle="Dashed"/>
						<bottomPen lineWidth="0.5" lineStyle="Dashed"/>
					</box>
					<textElement>
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{document_date}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="442" y="0" width="112" height="12"/>
					<box leftPadding="2">
						<topPen lineWidth="0.5" lineStyle="Dashed"/>
						<bottomPen lineWidth="0.5" lineStyle="Dashed"/>
						<rightPen lineWidth="0.5" lineStyle="Dashed"/>
					</box>
					<textElement>
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{warehouse}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="170" y="0" width="104" height="12"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right">
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{customer_code}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="16">
				<staticText>
					<reportElement x="0" y="0" width="42" height="16"/>
					<box>
						<topPen lineWidth="1.0" lineStyle="Dashed"/>
					</box>
					<textElement verticalAlignment="Top">
						<font size="7"/>
					</textElement>
					<text><![CDATA[Gjithsej]]></text>
				</staticText>
				<textField evaluationTime="Group" evaluationGroup="saleinvoiceheader_id" isBlankWhenNull="true">
					<reportElement x="42" y="0" width="44" height="16"/>
					<box leftPadding="2">
						<topPen lineWidth="1.0" lineStyle="Dashed"/>
					</box>
					<textElement verticalAlignment="Top">
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_rows}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="326" y="0" width="76" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right">
						<font size="7" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{subtotal_no_vat_1}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="402" y="0" width="76" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right">
						<font size="7" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{subtotal_vat_1}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="478" y="0" width="76" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right">
						<font size="7" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{subtotal_with_vat_1}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="114" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="506" y="0" width="18" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[nga]]></text>
			</staticText>
			<staticText>
				<reportElement x="442" y="0" width="36" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Faqe       ]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="478" y="0" width="28" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" isBlankWhenNull="true">
				<reportElement x="524" y="0" width="30" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="60" width="250" height="20"/>
				<box leftPadding="0" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Periudha:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="0" width="442" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{company_name}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="80" width="554" height="20"/>
				<box rightPadding="20"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{name_surname}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="40" width="554" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{report_title}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="dd/MM/yyyy">
				<reportElement x="402" y="60" width="76" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateEnd})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="326" y="60" width="76" height="20"/>
				<box bottomPadding="7"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[-]]></text>
			</staticText>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy">
				<reportElement x="250" y="60" width="76" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateBegin})]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="42" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="0" y="20" width="42" height="20"/>
				<box leftPadding="2">
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Artikull]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="0" width="42" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Nr.]]></text>
			</staticText>
			<staticText>
				<reportElement x="42" y="0" width="44" height="20"/>
				<box>
					<topPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Data]]></text>
			</staticText>
			<staticText>
				<reportElement x="42" y="20" width="128" height="20"/>
				<box leftPadding="2">
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Pershkrim]]></text>
			</staticText>
			<staticText>
				<reportElement x="274" y="20" width="52" height="20"/>
				<box>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Cmimi]]></text>
			</staticText>
			<staticText>
				<reportElement x="274" y="0" width="52" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Nipt]]></text>
			</staticText>
			<staticText>
				<reportElement x="326" y="20" width="76" height="20"/>
				<box>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Vl. pa t.v.header.]]></text>
			</staticText>
			<staticText>
				<reportElement x="402" y="20" width="76" height="20"/>
				<box>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[t.v.header.]]></text>
			</staticText>
			<staticText>
				<reportElement x="478" y="20" width="76" height="20"/>
				<box>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Vl. me t.v.header.]]></text>
			</staticText>
			<staticText>
				<reportElement x="326" y="0" width="76" height="20"/>
				<box>
					<topPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[nr, dt. fature]]></text>
			</staticText>
			<staticText>
				<reportElement x="402" y="0" width="152" height="20"/>
				<box>
					<topPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Magazina]]></text>
			</staticText>
			<staticText>
				<reportElement x="86" y="0" width="84" height="20"/>
				<box>
					<topPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement x="170" y="20" width="54" height="20"/>
				<box rightPadding="2">
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Sasi]]></text>
			</staticText>
			<staticText>
				<reportElement x="170" y="0" width="104" height="20"/>
				<box rightPadding="2">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Klienti]]></text>
			</staticText>
			<staticText>
				<reportElement x="224" y="20" width="50" height="20"/>
				<box rightPadding="2">
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Njesi/Veprimi]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="16" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="86" height="16"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Bottom">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{item_code}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="86" y="0" width="84" height="16"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Bottom">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{item_description}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="170" y="0" width="54" height="16">
					<printWhenExpression><![CDATA[$F{body_type}.equals( "items" )]]></printWhenExpression>
				</reportElement>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{quantity}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="224" y="0" width="50" height="16"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Bottom">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{unit_name}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="274" y="0" width="52" height="16">
					<printWhenExpression><![CDATA[$F{body_type}.equals( "items" )]]></printWhenExpression>
				</reportElement>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{price}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="326" y="0" width="76" height="16"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_no_vat}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="402" y="0" width="76" height="16"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_vat}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="478" y="0" width="76" height="16"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_with_vat}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="60" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
			<subreport>
				<reportElement x="-20" y="0" width="596" height="30"/>
				<subreportParameter name="bottom_message">
					<subreportParameterExpression><![CDATA[$P{bottom_message}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_FOOTER}]]></subreportExpression>
			</subreport>
		</band>
	</pageFooter>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="16" width="554" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
