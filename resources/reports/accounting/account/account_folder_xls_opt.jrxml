<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Kartelat e Llogarive 2" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="NoDataSection" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="core_name" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="acc_name" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="name_surname" class="java.lang.String"/>
	<parameter name="bottom_message" class="java.lang.String"/>
	<parameter name="report_title" class="java.lang.String"/>
	<parameter name="dateBegin" class="java.lang.String">
		<defaultValueExpression><![CDATA["2012-01-01"]]></defaultValueExpression>
	</parameter>
	<parameter name="dateEnd" class="java.lang.String">
		<defaultValueExpression><![CDATA["2012-12-30"]]></defaultValueExpression>
	</parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String"><defaultValueExpression><![CDATA["false"]]></defaultValueExpression></parameter>
	<parameter name="exclude_canceled_trans_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[ $P{show_canceled}.equals("true") ? "":"atrans.transaction_status=0 AND "]]></defaultValueExpression>
	</parameter>
	<parameter name="advancedFilters" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[
SELECT atrans.`transaction_status`, atrans.`id` AS 'Nr.Ref', DATE_FORMAT(atrans.`transaction_system_date`,'%d/%m/%Y %h:%i %p ') AS 'Date Regjistrimi',
atrans.description AS 'Pershkrimi',atrans.`document_number` AS 'Nr.Dok', DATE_FORMAT(atrans.`transaction_date`,'%d/%m/%Y') AS 'Dt.Dok',
atrans.`transaction_type_name` AS 'Lloji',atrans.`currency` AS 'Monedha',atrans.`code` AS 'Nr.Llog',IFNULL(suser.`firstname`,'-') AS 'Emri',
atrans.`transaction_type_id`, atrans.`account_currency` AS 'Monedha e llogarise',atrans.`name` AS 'Pershkrimi i llogarise',
CASE WHEN atrans.`transaction_type_id`=1 THEN CAST(atrans.`home_cur_total` AS DECIMAL(20,2)) ELSE 0 END AS 'Vlefta Kredi',
CASE WHEN atrans.`transaction_type_id`=0 THEN CAST(atrans.`home_cur_total` AS DECIMAL(20,2)) ELSE 0 END AS 'Vlefta Debi',
CASE WHEN atrans.`transaction_type_id`=1 THEN CAST(atrans.`total` AS DECIMAL(20,2)) ELSE 0 END AS 'Vlefta Kredi(trans)',
CASE WHEN atrans.`transaction_type_id`=0 THEN CAST(atrans.`total` AS DECIMAL(20,2)) ELSE 0 END AS 'Vlefta Debi(trans)',
CASE WHEN atrans.`transaction_type_id`=1 THEN CAST(atrans.`account_cur_total` AS DECIMAL(20,2)) ELSE 0 END AS 'Vlefta Kredi(account)',
CASE WHEN atrans.`transaction_type_id`=0 THEN CAST(atrans.`account_cur_total` AS DECIMAL(20,2)) ELSE 0 END AS 'Vlefta Debi(account)'
FROM $P!{acc_name}.`transaction` AS atrans
LEFT JOIN $P!{core_name}.`users` AS suser
ON atrans.`id_user`=suser.`id`
INNER JOIN $P!{acc_name}.documenttypes dt ON
atrans.documenttype_name = dt.system_name
WHERE $P!{advancedFilters}  $P!{exclude_canceled_trans_cond} atrans.`transaction_date` >= $P{dateBegin} AND
atrans.`transaction_date`<= $P{dateEnd}
ORDER BY atrans.`code`, atrans.`transaction_date`, atrans.`id`]]>
	</queryString>
	<field name="Nr.Ref" class="java.lang.Long"/>
	<field name="Date Regjistrimi" class="java.lang.String"/>
	<field name="Pershkrimi" class="java.lang.String"/>
	<field name="Nr.Dok" class="java.lang.String"/>
	<field name="Dt.Dok" class="java.lang.String"/>
	<field name="Lloji" class="java.lang.String"/>
	<field name="Monedha" class="java.lang.String"/>
	<field name="Nr.Llog" class="java.lang.String"/>
	<field name="Emri" class="java.lang.String"/>
	<field name="transaction_type_id" class="java.lang.Integer"/>
	<field name="transaction_status" class="java.lang.Integer"/>
	<field name="Monedha e llogarise" class="java.lang.String"/>
	<field name="Pershkrimi i llogarise" class="java.lang.String"/>
	<field name="Vlefta Kredi" class="java.math.BigDecimal"/>
	<field name="Vlefta Debi" class="java.math.BigDecimal"/>
	<field name="Vlefta Kredi(trans)" class="java.math.BigDecimal"/>
	<field name="Vlefta Debi(trans)" class="java.math.BigDecimal"/>
	<field name="Vlefta Kredi(account)" class="java.math.BigDecimal"/>
	<field name="Vlefta Debi(account)" class="java.math.BigDecimal"/>
	<variable name="Vlefta Kredi_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Llogaria" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Kredi} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Debi_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Llogaria" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Debi} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Kredi_2" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Kredi} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Debi_2" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Debi} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Kredi_3" class="java.math.BigDecimal" resetType="Group" resetGroup="Monedha" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Kredi} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Debi_3" class="java.math.BigDecimal" resetType="Group" resetGroup="Monedha" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Debi} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Kredi(trans)_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Llogaria" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Kredi(trans)} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Debi(trans)_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Llogaria" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Debi(trans)} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Kredi(trans)_2" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Kredi(trans)} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Debi(trans)_2" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Debi(trans)} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Kredi(trans)_3" class="java.math.BigDecimal" resetType="Group" resetGroup="Monedha" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Kredi(trans)} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Debi(trans)_3" class="java.math.BigDecimal" resetType="Group" resetGroup="Monedha" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ?$F{Vlefta Debi(trans)} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Kredi(account)_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Llogaria" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Kredi(account)} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Debi(account)_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Llogaria" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Debi(account)} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Kredi(account)_2" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Kredi(account)} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Debi(account)_2" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Debi(account)} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Kredi(account)_3" class="java.math.BigDecimal" resetType="Group" resetGroup="Monedha" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Kredi(account)} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Debi(account)_3" class="java.math.BigDecimal" resetType="Group" resetGroup="Monedha" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Debi(account)} : 0]]></variableExpression>
	</variable>
	<group name="Llogaria">
		<groupExpression><![CDATA[$F{Nr.Llog}]]></groupExpression>
		<groupHeader>
			<band height="78">
				<staticText>
					<reportElement x="0" y="58" width="44" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Nr.Ref]]></text>
				</staticText>
				<staticText>
					<reportElement x="44" y="58" width="61" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Date Regjistrimi]]></text>
				</staticText>
				<staticText>
					<reportElement x="105" y="58" width="32" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Lloji]]></text>
				</staticText>
				<staticText>
					<reportElement x="137" y="58" width="51" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Nr.Dok]]></text>
				</staticText>
				<staticText>
					<reportElement x="188" y="58" width="64" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Dt.Dok]]></text>
				</staticText>
				<staticText>
					<reportElement x="252" y="58" width="100" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Pershkrimi]]></text>
				</staticText>
				<staticText>
					<reportElement x="352" y="58" width="75" height="20"/>
					<box leftPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Vlefta Debi(trans)]]></text>
				</staticText>
				<staticText>
					<reportElement x="502" y="58" width="75" height="20"/>
					<box leftPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Vlefta Debi(trans)]]></text>
				</staticText>
				<staticText>
					<reportElement x="652" y="58" width="75" height="20"/>
					<box leftPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Vlefta Debi(account)]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="18" width="105" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Nr. Llogarie]]></text>
				</staticText>
				<textField>
					<reportElement x="105" y="38" width="83" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Monedha e llogarise}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="38" width="105" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Monedha e llogarise]]></text>
				</staticText>
				<textField>
					<reportElement x="105" y="18" width="83" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Nr.Llog}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="352" y="38" width="150" height="20"/>
					<box leftPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="10" isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Monedhe Baze ]]></text>
				</staticText>
				<staticText>
					<reportElement x="502" y="38" width="150" height="20"/>
					<box leftPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="10" isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Monedhe Transaksioni]]></text>
				</staticText>
				<staticText>
					<reportElement x="652" y="38" width="150" height="20"/>
					<box leftPadding="2"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="10" isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Monedhe Llogarie]]></text>
				</staticText>
				<staticText>
					<reportElement x="188" y="18" width="164" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<text><![CDATA[Pershkrimi i llogarise:]]></text>
				</staticText>
				<textField>
					<reportElement x="352" y="18" width="225" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Pershkrimi i llogarise}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="427" y="58" width="75" height="20"/>
					<box leftPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Vlefta Kredi]]></text>
				</staticText>
				<staticText>
					<reportElement x="577" y="58" width="75" height="20"/>
					<box leftPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Vlefta Kredi(trans)]]></text>
				</staticText>
				<staticText>
					<reportElement x="727" y="58" width="75" height="20"/>
					<box leftPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true" isItalic="false"/>
					</textElement>
					<text><![CDATA[Vlefta Kredi(account)]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<textField evaluationTime="Group" evaluationGroup="Llogaria" pattern="#,##0.00">
					<reportElement mode="Opaque" x="427" y="0" width="75" height="20" backcolor="#99CC00"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Vlefta Kredi_1}]]></textFieldExpression>
				</textField>
				<textField evaluationTime="Group" evaluationGroup="Llogaria" pattern="#,##0.00">
					<reportElement mode="Opaque" x="352" y="0" width="75" height="20" backcolor="#99CC00"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Vlefta Debi_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement mode="Opaque" x="577" y="0" width="75" height="20" backcolor="#99CC00"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Vlefta Kredi(trans)_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement mode="Opaque" x="502" y="0" width="75" height="20" backcolor="#99CC00"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Vlefta Debi(trans)_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement mode="Opaque" x="727" y="0" width="75" height="20" backcolor="#99CC00"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Vlefta Kredi(account)_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement mode="Opaque" x="652" y="0" width="75" height="20" backcolor="#99CC00"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Vlefta Debi(account)_1}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="252" y="0" width="100" height="20"/>
					<box leftPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right">
						<font isBold="true"/>
					</textElement>
					<text><![CDATA[Totali]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<group name="Monedha">
		<groupExpression><![CDATA[$F{Monedha}]]></groupExpression>
		<groupHeader>
			<band height="20">
				<staticText>
					<reportElement x="0" y="0" width="137" height="20"/>
					<textElement/>
					<text><![CDATA[Monedha e transaksionit]]></text>
				</staticText>
				<textField>
					<reportElement x="137" y="0" width="115" height="20"/>
					<textElement/>
					<textFieldExpression><![CDATA[$F{Monedha}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<textField pattern="#,##0.00">
					<reportElement mode="Opaque" x="502" y="0" width="75" height="20" backcolor="#FF9966"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Vlefta Debi(trans)_3}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement mode="Opaque" x="727" y="0" width="75" height="20" backcolor="#FF9966"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Vlefta Kredi(account)_3}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement mode="Opaque" x="652" y="0" width="75" height="20" backcolor="#FF9966"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Vlefta Debi(account)_3}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement mode="Opaque" x="352" y="0" width="75" height="20" backcolor="#FF9966"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Vlefta Debi_3}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement mode="Opaque" x="427" y="0" width="75" height="20" backcolor="#FF9966"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Vlefta Kredi_3}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement mode="Opaque" x="577" y="0" width="75" height="20" backcolor="#FF9966"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Vlefta Kredi(trans)_3}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="252" y="0" width="100" height="20"/>
					<box leftPadding="2" rightPadding="2"/>
					<textElement textAlignment="Right">
						<font isBold="true"/>
					</textElement>
					<text><![CDATA[Nentotali]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="68" splitType="Stretch">
			<textField>
				<reportElement x="652" y="0" width="40" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="0" width="577" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[$P{company_name}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="577" y="0" width="75" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Right"/>
				<text><![CDATA[Faqe]]></text>
			</staticText>
			<textField evaluationTime="Report">
				<reportElement x="727" y="0" width="75" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="692" y="0" width="35" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center"/>
				<text><![CDATA[nga]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="48" width="803" height="20"/>
				<textElement textAlignment="Center">
					<font size="11" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{report_title}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="dd/MM/yyyy">
				<reportElement x="137" y="20" width="115" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateEnd})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="105" y="20" width="32" height="20"/>
				<box bottomPadding="7"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[-]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="20" width="44" height="20"/>
				<textElement/>
				<text><![CDATA[Periudha]]></text>
			</staticText>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy">
				<reportElement x="44" y="20" width="61" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateBegin})]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band height="25" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<textField>
				<reportElement x="652" y="0" width="150" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{name_surname}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="577" y="0" width="75" height="20"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Printoi:]]></text>
			</staticText>
		</band>
	</pageHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<rectangle>
				<reportElement stretchType="RelativeToTallestObject" x="1" y="0" width="802" height="18" forecolor="#FFD7DA" backcolor="#FFD7DA">
					<printWhenExpression><![CDATA[$F{transaction_status}.compareTo( 0 ) > 0]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="44" height="20"/>
				<box>
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Nr.Ref}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="44" y="0" width="61" height="20"/>
				<box>
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Date Regjistrimi}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="105" y="0" width="32" height="20"/>
				<box>
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Lloji}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="137" y="0" width="51" height="20"/>
				<box>
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Nr.Dok}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="188" y="0" width="64" height="20"/>
				<box>
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Dt.Dok}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement stretchType="RelativeToTallestObject" x="252" y="0" width="100" height="20"/>
				<box>
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Pershkrimi}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement stretchType="RelativeToTallestObject" x="577" y="0" width="75" height="20" backcolor="#99CCFF"/>
				<box>
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Vlefta Kredi(trans)}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement stretchType="RelativeToTallestObject" x="727" y="0" width="75" height="20" backcolor="#99CCFF"/>
				<box>
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Vlefta Kredi(account)}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement stretchType="RelativeToTallestObject" x="352" y="0" width="75" height="20" forecolor="#9999FF"/>
				<box rightPadding="0">
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Vlefta Debi}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement stretchType="RelativeToTallestObject" x="427" y="0" width="75" height="20" backcolor="#99CCFF"/>
				<box>
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Vlefta Kredi}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement stretchType="RelativeToTallestObject" x="502" y="0" width="75" height="20" forecolor="#9999FF"/>
				<box rightPadding="0">
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Vlefta Debi(trans)}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement stretchType="RelativeToTallestObject" x="652" y="0" width="75" height="20" forecolor="#9999FF"/>
				<box rightPadding="0">
					<leftPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Vlefta Debi(account)}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="35">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
			<textField>
				<reportElement x="0" y="15" width="802" height="20"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bottom_message}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="20" splitType="Stretch">
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="427" y="0" width="75" height="20" backcolor="#CC0000"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Vlefta Kredi_2}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="352" y="0" width="75" height="20" backcolor="#CC0000"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Vlefta Debi_2}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="577" y="0" width="75" height="20" backcolor="#CC0000"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Vlefta Kredi(trans)_2}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="502" y="0" width="75" height="20" backcolor="#CC0000"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Vlefta Debi(trans)_2}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="727" y="0" width="75" height="20" backcolor="#CC0000"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Vlefta Kredi(account)_2}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="652" y="0" width="75" height="20" backcolor="#CC0000"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Vlefta Debi(account)_2}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="252" y="0" width="100" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Totali]]></text>
			</staticText>
		</band>
	</summary>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="16" width="802" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
