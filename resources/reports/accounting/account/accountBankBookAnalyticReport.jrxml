<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="accountCashBookAnalyticReport" language="groovy" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="acc_name" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="dateBegin" class="java.lang.String"><defaultValueExpression><![CDATA["2010-01-01"]]></defaultValueExpression></parameter>
	<parameter name="dateEnd" class="java.lang.String"><defaultValueExpression><![CDATA["2020-12-31"]]></defaultValueExpression></parameter>
	<parameter name="SUBREPORT_FOOTER" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["repo:/subr/footer"]]></defaultValueExpression>
	</parameter>
	<parameter name="bottom_message" class="java.lang.String"/>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="report_title" class="java.lang.String"/>
	<parameter name="name_surname" class="java.lang.String"/>
	<parameter name="month_human" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String"><defaultValueExpression><![CDATA["false"]]></defaultValueExpression></parameter>
	<parameter name="exclude_canceled_docs_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[ $P{show_canceled}.equals("true") ? "":"header.id_docstatus<>5 AND "]]></defaultValueExpression>
	</parameter>
	<parameter name="advancedFilters" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT c.`code`, c.`name` ,c.`currency`,c.`id_currency`,c.id_account AS idaccount,c.account AS cash_account, acc.code AS cash_accountcode, alldata.*
FROM $P!{acc_name}.cash c
LEFT JOIN
(SELECT header.id_docstatus, a.`code`, t.id_account, t.`id_currency`,t.`account_currency`, header.id_cash, t.`documenttype_name`, t.id_documenttype, t.`transaction_date`, dt.`symbol`,
header.`description`,header.`document_number`, header.`document_date`, t.currency  AS tCurrency, 0 AS acc_debit, 0 AS debit,
IFNULL(t.`home_cur_total`/t.`account_cur_exchange_rate`,0)  AS acc_credit, IFNULL(t.`total`,0)AS credit
FROM $P!{acc_name}.`transaction` t
INNER JOIN $P!{acc_name}.cashreceipt header
ON header.`document_number` = t.`document_number`
INNER JOIN $P!{acc_name}.documenttypes dt
ON t.`id_documenttype` = dt.id
INNER JOIN $P!{acc_name}.accounts a
ON a.`id` = t.`id_account`
WHERE $P!{advancedFilters} t.id_documenttype IN(20,43) AND t.transaction_type_id = 1 AND $P!{exclude_canceled_docs_cond} t.transaction_date >= $P{dateBegin} AND t.transaction_date <= $P{dateEnd}

UNION ALL

SELECT header.id_docstatus, a.`code`, t.id_account, t.`id_currency`,t.`account_currency`, header.id_cash, t.`documenttype_name`, t.id_documenttype, t.`transaction_date`, dt.`symbol`,
header.`description`,header.`document_number`, header.`document_date`,t.currency AS tCurrency,
IFNULL(t.`home_cur_total`/t.`account_cur_exchange_rate`,0)  AS acc_debit, IFNULL(t.`total`,0) AS debit,
0 AS acc_credit, 0 AS credit
FROM $P!{acc_name}.`transaction` t
INNER JOIN $P!{acc_name}.`cashpayment` header
ON header.`document_number` = t.`document_number`
INNER JOIN $P!{acc_name}.documenttypes dt
ON t.`id_documenttype` = dt.id
INNER JOIN $P!{acc_name}.accounts a
ON a.`id` = t.`id_account`
WHERE $P!{advancedFilters} t.id_documenttype IN(22,45) AND t.transaction_type_id = 0 AND $P!{exclude_canceled_docs_cond} t.transaction_date >= $P{dateBegin} AND t.transaction_date <= $P{dateEnd}) alldata
ON c.`id` = alldata.id_cash
INNER JOIN $P!{acc_name}.accounts acc
ON acc.id = c.id_account
WHERE c.`unit_type`= 1

ORDER BY c.`id_currency`,c.`code`, alldata.`document_date`]]>
	</queryString>
	<field name="cash_code" class="java.lang.String"/>
	<field name="cash_name" class="java.lang.String"/>
	<field name="currency" class="java.lang.String"/>
	<field name="id_currency" class="java.lang.String"/>
	<field name="cash_idaccount" class="java.lang.String"/>
	<field name="cash_account" class="java.lang.String"/>
	<field name="cash_accountcode" class="java.lang.String"/>
	<field name="code" class="java.lang.String"/>
	<field name="id_account" class="java.lang.Long"/>
	<field name="account_id_currency" class="java.lang.Integer"/>
	<field name="id_docstatus" class="java.lang.Integer"/>
	<field name="account_currency" class="java.lang.String"/>
	<field name="id_cash" class="java.lang.Integer"/>
	<field name="documenttype_name" class="java.lang.String"/>
	<field name="id_documenttype" class="java.lang.Long"/>
	<field name="transaction_date" class="java.sql.Timestamp"/>
	<field name="symbol" class="java.lang.String"/>
	<field name="description" class="java.lang.String"/>
	<field name="document_number" class="java.lang.String"/>
	<field name="document_date" class="java.sql.Timestamp"/>
	<field name="tCurrency" class="java.lang.String"/>
	<field name="acc_debit" class="java.math.BigDecimal"/>
	<field name="debit" class="java.math.BigDecimal"/>
	<field name="acc_credit" class="java.math.BigDecimal"/>
	<field name="credit" class="java.math.BigDecimal"/>
	<variable name="ref_number" class="java.lang.String">
		<variableExpression><![CDATA["B "+$F{cash_code}]]></variableExpression>
	</variable>
	<group name="currency">
		<groupExpression><![CDATA[$F{id_currency}]]></groupExpression>
		<groupHeader>
			<band height="24">
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="3" width="239" height="20"/>
					<box leftPadding="102">
						<bottomPen lineWidth="0.75"/>
					</box>
					<textElement>
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<group name="cash">
		<groupExpression><![CDATA[$F{cash_code}]]></groupExpression>
		<groupHeader>
			<band height="28">
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="7" width="68" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="0.75"/>
					</box>
					<textElement>
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{cash_accountcode}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="68" y="7" width="171" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="0.75"/>
					</box>
					<textElement>
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{cash_account}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="101" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="497" y="0" width="23" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[nga]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="60" width="239" height="20"/>
				<box leftPadding="0" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Periudha:]]></text>
			</staticText>
			<textField>
				<reportElement x="0" y="80" width="555" height="20"/>
				<box rightPadding="20"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{name_surname}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="40" width="555" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{report_title}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="454" y="0" width="43" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="0" width="354" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{company_name}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="354" y="0" width="100" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Faqe       ]]></text>
			</staticText>
			<textField>
				<reportElement x="239" y="60" width="316" height="20"/>
				<box leftPadding="2" rightPadding="0"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{month_human}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" isBlankWhenNull="true">
				<reportElement x="520" y="0" width="35" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="37" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="354" y="2" width="100" height="34"/>
				<box bottomPadding="5" rightPadding="2">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom"/>
				<text><![CDATA[Debi]]></text>
			</staticText>
			<staticText>
				<reportElement x="454" y="2" width="101" height="34"/>
				<box bottomPadding="5" rightPadding="2">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom"/>
				<text><![CDATA[Kredi]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="2" width="68" height="34"/>
				<box leftPadding="2" bottomPadding="5">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Bottom"/>
				<text><![CDATA[Llogari]]></text>
			</staticText>
			<staticText>
				<reportElement x="239" y="2" width="52" height="34"/>
				<box leftPadding="2" bottomPadding="5">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Bottom"/>
				<text><![CDATA[Reference]]></text>
			</staticText>
			<staticText>
				<reportElement x="68" y="2" width="171" height="17"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
				</box>
				<textElement/>
				<text><![CDATA[Pershkrim]]></text>
			</staticText>
			<staticText>
				<reportElement x="291" y="2" width="63" height="34"/>
				<box leftPadding="2" bottomPadding="5">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Bottom"/>
				<text><![CDATA[Dok / numer dokumenti]]></text>
			</staticText>
			<staticText>
				<reportElement x="122" y="19" width="117" height="17"/>
				<box leftPadding="2">
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement/>
				<text><![CDATA[Koment]]></text>
			</staticText>
			<staticText>
				<reportElement x="68" y="19" width="54" height="17"/>
				<box leftPadding="2">
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement/>
				<text><![CDATA[Date]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="16" splitType="Stretch">
			<rectangle>
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="555" height="15" forecolor="#FFD7DA" backcolor="#FFD7DA">
					<printWhenExpression><![CDATA[$F{id_docstatus}.compareTo( 5 ) == 0]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="354" y="0" width="87" height="16">
					<printWhenExpression><![CDATA[$F{debit}!= 0]]></printWhenExpression>
				</reportElement>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{debit}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="454" y="0" width="88" height="16">
					<printWhenExpression><![CDATA[$F{credit} != 0]]></printWhenExpression>
				</reportElement>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{credit}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="441" y="0" width="13" height="16">
					<printWhenExpression><![CDATA[$F{debit}!= 0]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{tCurrency}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="542" y="0" width="13" height="16">
					<printWhenExpression><![CDATA[$F{credit}!= 0]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{tCurrency}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="68" height="16"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{code}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="239" y="0" width="52" height="16"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{ref_number}]]></textFieldExpression>
			</textField>
			<textField pattern="dd.MM.yyyy" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="68" y="0" width="54" height="16"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{document_date}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="291" y="0" width="63" height="16"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{document_number}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="122" y="0" width="117" height="16"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{description}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="65" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
			<subreport>
				<reportElement x="-22" y="0" width="598" height="31"/>
				<subreportParameter name="bottom_message">
					<subreportParameterExpression><![CDATA[$P{bottom_message}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_FOOTER}]]></subreportExpression>
			</subreport>
		</band>
	</pageFooter>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="15" width="555" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
