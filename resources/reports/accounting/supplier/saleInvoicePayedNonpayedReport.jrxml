<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="supplierInvoicePayedNonpayedReport" language="groovy" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="acc_name" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="dateBegin" class="java.lang.String"/>
	<parameter name="dateEnd" class="java.lang.String"/>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="report_title" class="java.lang.String"/>
	<parameter name="name_surname" class="java.lang.String"/>
	<parameter name="month_human" class="java.lang.String"/>
	<parameter name="bottom_message" class="java.lang.String"/>
	<parameter name="SUBREPORT_HEADER" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["repo:/subr/header"]]></defaultValueExpression>
	</parameter>
	<parameter name="SUBREPORT_FOOTER" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["repo:/subr/footer"]]></defaultValueExpression>
	</parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT nje.customer_id, nje.customer_code, nje.customer_id, nje.id_currency, nje.contact_display_name, nje.customer_currency, nje.`saleinvoiceheader_id`, nje.serial_number, nje.document_number, nje.document_date, nje.`description`, nje.dueDate, nje.home_cur_total_with_vat, nje.curr_value, DATEDIFF(nje.dueDate,NOW()) AS days_till, DATEDIFF(NOW(),nje.dueDate) AS days_after, IFNULL(dy.payed,0) AS payed, IFNULL(dy. payed_home_curr,0) AS payed_home_curr
FROM
	(SELECT c.customer_id, c.customer_code, c.contact_display_name,c.id_currency, c.currency AS customer_currency,
	sh.`saleinvoiceheader_id`, sh.`serial_number`, sh.`document_number`, sh.`document_date`, sh.description, accounting_test.getDocumentDueDate(sh.`document_date`,sh.`id_commercialterms`) AS dueDate,
	IFNULL(sh.`home_cur_total_with_vat`,0) AS home_cur_total_with_vat,
	IFNULL(sh.`home_cur_total_with_vat`/sh.`customer_cur_exchange_rate`,0) AS curr_value
	FROM $P!{acc_name}.customer c
	LEFT JOIN
	$P!{acc_name}.saleinvoiceheader sh
	ON sh.`id_contact` = c.customer_id AND LEFT(sh.`document_date`,10) >= $P{dateBegin}
	AND LEFT(sh.`document_date`,10) <= $P{dateEnd}
	WHERE c.deleted = 0
	) nje
LEFT JOIN
	(SELECT rp.`subject_id`, SUM(IFNULL((rp.home_cur_amount_payed/rp.receipt_currency_exchange_rate),0)) AS payed, SUM(IFNULL(rp.home_cur_amount_payed,0)) AS payed_home_curr
	FROM  $P!{acc_name}.r_payment rp
	WHERE rp.deleted = 0 AND rp.`subject` = "saleinvoiceheader" AND LEFT(rp.`payment_date`,10) >= $P{dateBegin}  AND LEFT(rp.`payment_date`,10) <= $P{dateEnd}
	GROUP BY rp.`subject_id`) dy
ON nje.saleinvoiceheader_id = dy.subject_id
ORDER BY nje.id_currency, nje.customer_code, nje.document_date,nje.document_number]]>
	</queryString>
	<field name="customer_id" class="java.lang.Integer"/>
	<field name="customer_code" class="java.lang.String"/>
	<field name="id_currency" class="java.lang.Integer"/>
	<field name="contact_display_name" class="java.lang.String"/>
	<field name="customer_currency" class="java.lang.String"/>
	<field name="saleinvoiceheader_id" class="java.lang.Integer"/>
	<field name="serial_number" class="java.lang.String"/>
	<field name="document_number" class="java.lang.String"/>
	<field name="document_date" class="java.sql.Timestamp"/>
	<field name="description" class="java.lang.String"/>
	<field name="dueDate" class="java.sql.Date"/>
	<field name="home_cur_total_with_vat" class="java.math.BigDecimal"/>
	<field name="curr_value" class="java.math.BigDecimal"/>
	<field name="days_till" class="java.lang.Integer"/>
	<field name="days_after" class="java.lang.Integer"/>
	<field name="payed" class="java.math.BigDecimal"/>
	<field name="payed_home_curr" class="java.math.BigDecimal"/>
	<variable name="notpayed" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{curr_value}-$F{payed}]]></variableExpression>
	</variable>
	<variable name="curr_value_1" class="java.math.BigDecimal" resetType="Group" resetGroup="customer" calculation="Sum">
		<variableExpression><![CDATA[$F{curr_value}]]></variableExpression>
	</variable>
	<variable name="payed_1" class="java.math.BigDecimal" resetType="Group" resetGroup="customer" calculation="Sum">
		<variableExpression><![CDATA[$F{payed}]]></variableExpression>
	</variable>
	<variable name="notpayed_1" class="java.math.BigDecimal" resetType="Group" resetGroup="customer">
		<variableExpression><![CDATA[$V{curr_value_1}-$V{payed_1}]]></variableExpression>
	</variable>
	<group name="currency">
		<groupExpression><![CDATA[$F{id_currency}]]></groupExpression>
		<groupHeader>
			<band height="22">
				<staticText>
					<reportElement x="0" y="2" width="50" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="0.0"/>
					</box>
					<textElement>
						<font size="8"/>
					</textElement>
					<text><![CDATA[]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement x="50" y="2" width="88" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{customer_currency}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<group name="customer">
		<groupExpression><![CDATA[$F{customer_id}]]></groupExpression>
		<groupHeader>
			<band height="20">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="50" height="16"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{customer_code}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="50" y="0" width="88" height="16"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{contact_display_name}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="16">
				<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="256" y="0" width="100" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{curr_value_1}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="356" y="0" width="100" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{payed_1}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="456" y="0" width="99" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{notpayed_1}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="138" y="0" width="118" height="16"/>
					<box leftPadding="0" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{customer_code}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="35" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<subreport>
				<reportElement x="-21" y="0" width="597" height="35"/>
				<subreportParameter name="company_name">
					<subreportParameterExpression><![CDATA[$P{company_name}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="name_surname">
					<subreportParameterExpression><![CDATA[$P{name_surname}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="report_title">
					<subreportParameterExpression><![CDATA[$P{report_title}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="month_human">
					<subreportParameterExpression><![CDATA[$P{month_human}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_HEADER}]]></subreportExpression>
			</subreport>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="53" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="50" y="16" width="44" height="16"/>
				<box leftPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Date Fature]]></text>
			</staticText>
			<staticText>
				<reportElement x="94" y="16" width="44" height="16"/>
				<box leftPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Date Afat]]></text>
			</staticText>
			<staticText>
				<reportElement x="138" y="16" width="44" height="16"/>
				<box leftPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Dite Mbetur]]></text>
			</staticText>
			<staticText>
				<reportElement x="256" y="16" width="100" height="16"/>
				<box rightPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Detyrim]]></text>
			</staticText>
			<staticText>
				<reportElement x="356" y="16" width="100" height="16"/>
				<box rightPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Paguar]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="0" width="50" height="16"/>
				<box leftPadding="2">
					<topPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Klient]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="16" width="50" height="16"/>
				<box leftPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[fatura]]></text>
			</staticText>
			<staticText>
				<reportElement x="50" y="0" width="44" height="16"/>
				<box leftPadding="2">
					<topPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Pershkrim]]></text>
			</staticText>
			<staticText>
				<reportElement x="456" y="16" width="99" height="16"/>
				<box rightPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Pa Paguar]]></text>
			</staticText>
			<staticText>
				<reportElement x="138" y="0" width="118" height="16"/>
				<box>
					<topPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[(nga date afat)]]></text>
			</staticText>
			<staticText>
				<reportElement x="256" y="0" width="299" height="16"/>
				<box>
					<topPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement x="182" y="16" width="74" height="16"/>
				<box leftPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Dite Kaluar]]></text>
			</staticText>
			<staticText>
				<reportElement x="94" y="0" width="44" height="16"/>
				<box>
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="0.0" lineStyle="Dotted"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="16" splitType="Stretch">
			<printWhenExpression><![CDATA[$F{saleinvoiceheader_id}!= null]]></printWhenExpression>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="50" height="16"/>
				<box leftPadding="2"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serial_number}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="182" y="0" width="74" height="16">
					<printWhenExpression><![CDATA[$F{days_after}>0]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2"/>
				<textElement textAlignment="Center">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{days_after}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="138" y="0" width="44" height="16">
					<printWhenExpression><![CDATA[$F{days_till}>0]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2"/>
				<textElement textAlignment="Center">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{days_till}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="356" y="0" width="100" height="16"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{payed}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="256" y="0" width="100" height="16"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{curr_value}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="456" y="0" width="99" height="16"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{notpayed}]]></textFieldExpression>
			</textField>
			<textField pattern="dd.MM.yyyy" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="94" y="0" width="44" height="16"/>
				<box leftPadding="2"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{dueDate}]]></textFieldExpression>
			</textField>
			<textField pattern="dd.MM.yyyy" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="50" y="0" width="44" height="16"/>
				<box leftPadding="2"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{document_date}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="62" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
			<subreport>
				<reportElement x="-21" y="0" width="597" height="33"/>
				<subreportParameter name="bottom_message">
					<subreportParameterExpression><![CDATA[$P{bottom_message}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_FOOTER}]]></subreportExpression>
			</subreport>
		</band>
	</pageFooter>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="6" width="555" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
