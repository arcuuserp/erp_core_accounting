<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="bankDiary" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="dateBegin" class="java.lang.String"/>
	<parameter name="dateEnd" class="java.lang.String"/>
	<parameter name="companyName" class="java.lang.String"/>
	<parameter name="fDateBegin" class="java.lang.String"/>
	<parameter name="fDateEnd" class="java.lang.String"/>
	<parameter name="acc_name" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="core_name" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="id_cash" class="java.lang.String">
		<defaultValueExpression><![CDATA["cash.cash_id"]]></defaultValueExpression>
	</parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="date" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[$P{fDateBegin}+" - "+$P{fDateEnd}]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_trans_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[ $P{show_canceled}.equals("true") ? "":" AND t.transaction_status=0 "]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_docs_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals("true") ? "":"header.id_docstatus<>5 AND "]]></defaultValueExpression>
	</parameter>
	<parameter name="advancedFilters" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT *
FROM (
SELECT * FROM(
	SELECT header.id_docstatus, header.serial_number, header.document_number, header.document_date, header.description, header.customer_cur_exchange_rate AS 'exchange_rate'
				, header.total_cashed AS 'arketim', 0 AS 'pagese', cash.code, cash.name, cash.cash_id AS c_id, header.id_currency
				, cur.description AS cur_desc, cur.name AS currency, 'MA' AS doctype
				FROM $P!{acc_name}.cashreceipt header
					INNER JOIN $P!{acc_name}.cash
						ON header.id_cash = cash.cash_id
					INNER JOIN $P!{core_name}.currencies cur
						ON header.id_currency = cur.currency_id
				WHERE cash.cash_id = $P!{id_cash} AND cash.unit_type = 0 AND $P!{exclude_canceled_docs_cond} header.document_date >= $P{dateBegin} AND header.document_date <= $P{dateEnd}

		UNION ALL

	SELECT header.id_docstatus, header.serial_number, header.document_number, header.document_date, header.description, header.supplier_cur_exchange_rate AS 'exchange_rate'
				, 0 AS 'arketim', header.total_amount_payed AS 'pagese', cash.code, cash.name, cash.cash_id AS c_id
				, header.id_currency, cur.description AS cur_desc, cur.name AS currency, 'MP' AS doctype
				FROM $P!{acc_name}.cashpaymentheader header
					INNER JOIN $P!{acc_name}.cash
						ON header.id_cash = cash.cash_id
					INNER JOIN $P!{core_name}.currencies cur
						ON header.id_currency = cur.currency_id
				WHERE cash.cash_id = $P!{id_cash} AND cash.unit_type = 0 AND $P!{exclude_canceled_docs_cond} header.document_date >= $P{dateBegin} AND header.document_date <= $P{dateEnd}
) a

LEFT JOIN

(
	SELECT
		IFNULL(ROUND(SUM(value_tab.value_cash), 2), 0) AS cash_first_value
		, value_tab.cash_id
	FROM
		(SELECT CASE WHEN transaction.transaction_type_id = 0 THEN transaction.total ELSE transaction.total*(-1) END AS value_cash
				, cash.cash_id, cash.id_currency
			FROM $P!{acc_name}.cash
				LEFT JOIN $P!{acc_name}.transaction ON transaction.subject_id = cash.cash_id $P!{exclude_canceled_trans_cond}
					AND transaction.subject = 'cash' AND transaction.transaction_date < $P{dateBegin}
			WHERE cash.cash_id = $P!{id_cash} AND cash.unit_type = 0
		) value_tab
	GROUP BY value_tab.id_currency, value_tab.cash_id ORDER BY value_tab.id_currency, value_tab.cash_id
) b

ON a.c_id = b.cash_id
) AS alldata WHERE $P!{advancedFilters} ORDER BY alldata.id_currency ASC, alldata.cash_id ASC, alldata.document_date ASC, alldata.document_number]]>
	</queryString>
	<field name="serial_number" class="java.lang.String"/>
	<field name="document_number" class="java.lang.String"/>
	<field name="document_date" class="java.sql.Timestamp"/>
	<field name="description" class="java.lang.String"/>
	<field name="exchange_rate" class="java.math.BigDecimal"/>
	<field name="arketim" class="java.math.BigDecimal"/>
	<field name="pagese" class="java.math.BigDecimal"/>
	<field name="cash_code" class="java.lang.String"/>
	<field name="cash_name" class="java.lang.String"/>
	<field name="c_id" class="java.lang.Long"/>
	<field name="id_currency" class="java.lang.Integer"/>
	<field name="id_docstatus" class="java.lang.Integer"/>
	<field name="cur_desc" class="java.lang.String"/>
	<field name="currency" class="java.lang.String"/>
	<field name="doctype" class="java.lang.String"/>
	<field name="cash_first_value" class="java.math.BigDecimal"/>
	<field name="cash_id" class="java.lang.Long"/>
	<variable name="currentValue" class="java.math.BigDecimal" resetType="Group" resetGroup="bankGroup">
		<variableExpression><![CDATA[$V{currentValue}.add( $F{arketim} ).subtract( $F{pagese} )]]></variableExpression>
		<initialValueExpression><![CDATA[$F{cash_first_value}]]></initialValueExpression>
	</variable>
	<variable name="dateTime" class="java.util.Date">
		<variableExpression><![CDATA[new Date()]]></variableExpression>
	</variable>
	<group name="currencyGroup">
		<groupExpression><![CDATA[$F{id_currency}]]></groupExpression>
		<groupHeader>
			<band height="46">
				<textField>
					<reportElement x="0" y="26" width="150" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="150" y="26" width="166" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{cur_desc}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<group name="bankGroup">
		<groupExpression><![CDATA[$F{cash_id}]]></groupExpression>
		<groupHeader>
			<band height="25">
				<textField>
					<reportElement x="0" y="0" width="150" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="9" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{cash_code}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="150" y="0" width="166" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="9" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{cash_name}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="476" y="0" width="79" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{cash_first_value}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="77" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<textField>
				<reportElement x="476" y="57" width="30" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="396" y="57" width="80" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box rightPadding="4"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Faqe]]></text>
			</staticText>
			<staticText>
				<reportElement x="506" y="57" width="22" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[nga]]></text>
			</staticText>
			<textField>
				<reportElement x="0" y="0" width="555" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{companyName}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="150" y="57" width="96" height="20"/>
				<box leftPadding="0" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Periudha: ]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="20" width="555" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<text><![CDATA[Arka - Ditar]]></text>
			</staticText>
			<textField evaluationTime="Report">
				<reportElement x="528" y="57" width="27" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy HH.mm.ss">
				<reportElement x="26" y="57" width="124" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{dateTime}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="57" width="26" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[dt.]]></text>
			</staticText>
			<textField>
				<reportElement x="246" y="57" width="150" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{date}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="42" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="0" y="1" width="150" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Banka ]]></text>
			</staticText>
			<staticText>
				<reportElement x="150" y="1" width="166" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Përshkrim ]]></text>
			</staticText>
			<staticText>
				<reportElement x="316" y="1" width="80" height="40"/>
				<box topPadding="4" leftPadding="0" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top"/>
				<text><![CDATA[Arkëtime]]></text>
			</staticText>
			<staticText>
				<reportElement x="396" y="1" width="80" height="40"/>
				<box topPadding="4" leftPadding="0" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top"/>
				<text><![CDATA[Pagesa]]></text>
			</staticText>
			<staticText>
				<reportElement x="476" y="1" width="79" height="40"/>
				<box topPadding="4" leftPadding="0" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top"/>
				<text><![CDATA[Gjendje]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="21" width="150" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Dokument]]></text>
			</staticText>
			<staticText>
				<reportElement x="150" y="21" width="166" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Koment]]></text>
			</staticText>
			<staticText>
				<reportElement x="316" y="21" width="239" height="20"/>
				<box leftPadding="2">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<rectangle>
				<reportElement stretchType="RelativeToTallestObject" x="1" y="0" width="555" height="19" forecolor="#FFD7DA" backcolor="#FFD7DA">
					<printWhenExpression><![CDATA[$F{id_docstatus}.compareTo( 5 ) == 0]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField>
				<reportElement x="26" y="0" width="68" height="20"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serial_number}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement x="94" y="0" width="56" height="20"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{document_date}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="150" y="0" width="166" height="20"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{description}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="316" y="0" width="80" height="20"/>
				<box rightPadding="2">
					<topPen lineWidth="0.5" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{arketim}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="396" y="0" width="80" height="20"/>
				<box rightPadding="2">
					<topPen lineWidth="0.5" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{pagese}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="0" width="26" height="20"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Dashed"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{doctype}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="476" y="0" width="79" height="20"/>
				<box rightPadding="2">
					<topPen lineWidth="0.5" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{currentValue}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="14" width="556" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
