<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="journal_date_state" language="groovy" pageWidth="555" pageHeight="802" whenNoDataType="BlankPage" columnWidth="555" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="dateBegin" class="java.lang.String"/>
	<parameter name="warehouse_code" class="java.lang.String"/>
	<parameter name="item_code" class="java.lang.String"/>
	<parameter name="acc_name" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="core_name" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_docs_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[ $P{show_canceled}.equals("true") ? "":"header.status<>5 AND "]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT SUM(w.quantity/w.coefficient) AS quantity_state,SUM(w.quantity*w.cost/w.coefficient) AS value_state
FROM 	(SELECT header.`id`,header.`warehouse_code`,header.`warehouse_name`,wh.`description`, i.`name`, i.`code`, i.`sale_description`, header.`documenttype_name`, header.`document_number`, header.`document_date`,DATE_FORMAT(header.`document_date`,'%Y-%m-%d')AS dateDoc,header.`serial_number`,d.`description` AS document_description, IFNULL(enb.`cost`,0) AS cost, IFNULL(uc.`coefficient`,0) AS coefficient, IFNULL(enb.`quantity`,0) AS quantity
	FROM $P!{acc_name}.`entrywarehouse` header
	INNER JOIN $P!{acc_name}.`entrywarehousebody` enb
	ON enb.`id_entrywarehouse` = header.`id`
	INNER JOIN $P!{acc_name}.`warehouse` wh
	ON wh.`id` = header.`id_warehouse`
	LEFT JOIN $P!{acc_name}.items i
	ON i.`id`=enb.`id_item`
	LEFT JOIN $P!{core_name}.`unitconversions` uc
	ON uc.`idunitentry` = i.`id_unit` AND uc.`idunitexit`=enb.`id_unit`
	LEFT JOIN $P!{acc_name}.`documenttypes` d
	ON d.`id` = header.`id_documenttype`
	WHERE header.`deleted` = 0 AND $P!{exclude_canceled_docs_cond} header.`document_date`<$P{dateBegin} AND header.`warehouse_code` = $P{warehouse_code}
	UNION ALL
	SELECT header.id, header.`warehouse_code`,header.`warehouse_name`, wh.`description`, i.`name`, i.`code`, i.`sale_description`, header.`documenttype_name`, header.`document_number`, header.`document_date`, DATE_FORMAT(header.`document_date`,'%Y-%m-%d')AS dateDoc, header.`serial_number`, d.`description` AS document_description, IFNULL(exb.cost,0) AS cost, IFNULL(uc.`coefficient`,0) AS coefficient,IFNULL(-1*exb.`quantity`,0) AS quantity
	FROM $P!{acc_name}.`exitwarehouse` header
	INNER JOIN $P!{acc_name}.`exitwarehousebody` exb
	ON exb.`id_exitwarehouse` = header.id
	INNER JOIN $P!{acc_name}.`warehouse` wh
	ON wh.`id` = header.`id_warehouse`
	LEFT JOIN $P!{acc_name}.items i
	ON i.`id`=exb.`id_item`
	LEFT JOIN $P!{core_name}.`unitconversions` uc
	ON uc.`idunitentry` = i.`id_unit` AND uc.`idunitexit`=exb.`id_unit`
	LEFT JOIN $P!{acc_name}.`documenttypes` d
	ON d.`id` = header.`id_documenttype`
	WHERE header.deleted = 0 AND $P!{exclude_canceled_docs_cond} header.`document_date`<$P{dateBegin} AND header.`warehouse_code` = $P{warehouse_code} )w
ORDER BY w.warehouse_code, w.code]]>
	</queryString>
	<field name="quantity_state" class="java.math.BigDecimal"/>
	<field name="value_state" class="java.math.BigDecimal"/>
	<variable name="value/unit" class="java.math.BigDecimal">
		<variableExpression><![CDATA[($V{quantity_state}==0)? $V{value_state}:$V{value_state}/$V{quantity_state}]]></variableExpression>
	</variable>
	<variable name="quantity_state" class="java.math.BigDecimal">
		<variableExpression><![CDATA[($F{quantity_state}==null)? 0:$F{quantity_state}]]></variableExpression>
	</variable>
	<variable name="value_state" class="java.math.BigDecimal">
		<variableExpression><![CDATA[($V{value_state}==null)? 0:$V{value_state}]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<detail>
		<band height="21" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="1" width="94" height="20" backcolor="#FACCCC"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[gjendja fillestare]]></text>
			</staticText>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="266" y="1" width="68" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{value_state}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="206" y="1" width="60" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{value_state}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
