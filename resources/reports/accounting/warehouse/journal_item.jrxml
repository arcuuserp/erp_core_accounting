<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="journal_item" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="NoDataSection" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="291"/>
	<property name="ireport.y" value="0"/>
	<parameter name="SUBREPORT_HEADER" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["repo:/subr/header"]]></defaultValueExpression>
	</parameter>
	<parameter name="SUBREPORT_DIR_2" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["/C://reports/accounting/warehouse/"]]></defaultValueExpression>
	</parameter>
	<parameter name="dateBegin" class="java.lang.String">
		<defaultValueExpression><![CDATA["2010-01-01"]]></defaultValueExpression>
	</parameter>
	<parameter name="dateEnd" class="java.lang.String">
		<defaultValueExpression><![CDATA["2020-12-31"]]></defaultValueExpression>
	</parameter>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="report_title" class="java.lang.String"/>
	<parameter name="bottom_message" class="java.lang.String"/>
	<parameter name="name_surname" class="java.lang.String"/>
	<parameter name="acc_name" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="core_name" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>	
	<parameter name="id_item" class="java.lang.String">
		<defaultValueExpression><![CDATA["enb.id_item"]]></defaultValueExpression>
	</parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_docs_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[ $P{show_canceled}.equals("true") ? "":"header.status<>5 AND "]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT w.*
FROM 	(SELECT header.status, 0 AS entryexit, header.`id`,header.`warehouse_code`,header.`warehouse_name`,wh.`description`, i.`name`, i.`code`, i.`sale_description`, d.document_name AS documenttype_name, header.`document_number`, header.`document_date`,header.`serial_number`,d.`description` AS document_description,  IFNULL(enb.`cost`,0) AS cost,IFNULL(uc.`coefficient`,0) AS coefficient, IFNULL(enb.`quantity`, 0) AS quantity
	FROM $P!{acc_name}.`entrywarehouse` header
	INNER JOIN $P!{acc_name}.`entrywarehousebody` enb
	ON enb.`id_entrywarehouse` = header.`id`
	INNER JOIN $P!{acc_name}.`warehouse` wh
	ON wh.`id` = header.`id_warehouse`
	LEFT JOIN $P!{acc_name}.items i
	ON i.`id`=enb.`id_item`
	LEFT JOIN $P!{core_name}.`unitconversions` uc
	ON uc.`idunitentry` = i.`id_unit` AND uc.`idunitexit`=enb.`id_unit`
	LEFT JOIN $P!{acc_name}.`documenttypes` d
	ON d.`id` = header.`id_documenttype`
	WHERE header.`deleted` = 0 AND $P!{exclude_canceled_docs_cond} enb.id_item = $P!{id_item} AND header.`document_date`>=$P{dateBegin} AND header.`document_date`<=$P{dateEnd}
	UNION ALL
	SELECT header.status, 1 AS entryexit, header.id, header.`warehouse_code`,header.`warehouse_name`, wh.`description`, i.`name`, i.`code`, i.`sale_description`, d.document_name AS documenttype_name, header.`document_number`, header.`document_date`,header.`serial_number`, d.`description` AS document_description, IFNULL(enb.cost, 0) AS cost,IFNULL(uc.`coefficient`,0) AS coefficient ,IFNULL(-1*enb.`quantity`,0) AS quantity
	FROM $P!{acc_name}.`exitwarehouse` header
	INNER JOIN $P!{acc_name}.`exitwarehousebody` enb
	ON enb.`id_exitwarehouse` = header.id
	INNER JOIN $P!{acc_name}.`warehouse` wh
	ON wh.`id` = header.`id_warehouse`
	LEFT JOIN $P!{acc_name}.items i
	ON i.`id`=enb.`id_item`
	LEFT JOIN $P!{core_name}.`unitconversionss` uc
	ON uc.`idunitentry` = i.`id_unit` AND uc.`idunitexit`=enb.`id_unit`
	LEFT JOIN $P!{acc_name}.`documenttypes` d
	ON d.`id` = header.`id_documenttype`
	WHERE header.deleted = 0 AND $P!{exclude_canceled_docs_cond} enb.id_item = $P!{id_item} AND header.`document_date`>=$P{dateBegin} AND header.`document_date`<=$P{dateEnd})w
ORDER BY w.warehouse_code, w.code, w.document_date, w.entryexit ASC]]>
	</queryString>
	<field name="entrywarehouseheader_id" class="java.lang.Integer"/>
	<field name="status" class="java.lang.Integer"/>
	<field name="warehouse_code" class="java.lang.String"/>
	<field name="warehouse_name" class="java.lang.String"/>
	<field name="description" class="java.lang.String"/>
	<field name="item_name" class="java.lang.String"/>
	<field name="item_code" class="java.lang.String"/>
	<field name="sale_description" class="java.lang.String"/>
	<field name="documenttype_name" class="java.lang.String"/>
	<field name="document_number" class="java.lang.String"/>
	<field name="document_date" class="java.sql.Timestamp"/>
	<field name="serial_number" class="java.lang.String"/>
	<field name="document_description" class="java.lang.String"/>
	<field name="cost" class="java.math.BigDecimal"/>
	<field name="coefficient" class="java.math.BigDecimal"/>
	<field name="quantity" class="java.math.BigDecimal"/>
	<variable name="total_value" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{cost}* $F{quantity}]]></variableExpression>
	</variable>
	<variable name="quantity_state" class="java.math.BigDecimal" calculation="System"/>
	<variable name="value_state" class="java.math.BigDecimal" calculation="System"/>
	<variable name="progressive_quantity(without state)" class="java.math.BigDecimal" resetType="Group" resetGroup="item">
		<variableExpression><![CDATA[$V{progressive_quantity(without state)}.add($V{quantity_unit})]]></variableExpression>
		<initialValueExpression><![CDATA[0]]></initialValueExpression>
	</variable>
	<variable name="progressive_quantity" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{progressive_quantity(without state)}+$V{quantity_state}]]></variableExpression>
	</variable>
	<variable name="progressive_value(without state)" class="java.math.BigDecimal" resetType="Group" resetGroup="item">
		<variableExpression><![CDATA[$V{progressive_value(without state)}+$V{total_value}]]></variableExpression>
		<initialValueExpression><![CDATA[0]]></initialValueExpression>
	</variable>
	<variable name="progressive_value" class="java.math.BigDecimal" resetType="Group" resetGroup="item">
		<variableExpression><![CDATA[$V{progressive_value(without state)}+$V{value_state}]]></variableExpression>
	</variable>
	<variable name="quantity_item(without state)" class="java.math.BigDecimal" resetType="Group" resetGroup="item" calculation="Sum">
		<variableExpression><![CDATA[($F{status}.compareTo( 5 ) == 0) ? 0 : $V{quantity_unit}]]></variableExpression>
	</variable>
	<variable name="quantity_item" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{quantity_item(without state)}+$V{quantity_state}]]></variableExpression>
	</variable>
	<variable name="total_value_item(without state)" class="java.math.BigDecimal" resetType="Group" resetGroup="item" calculation="Sum">
		<variableExpression><![CDATA[($F{status}.compareTo( 5 ) == 0) ? 0 : $V{total_value}]]></variableExpression>
	</variable>
	<variable name="total_value_item" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{total_value_item(without state)}+$V{value_state}]]></variableExpression>
	</variable>
	<variable name="total_value_warehouse" class="java.math.BigDecimal" resetType="Group" resetGroup="warehouse" calculation="Sum">
		<variableExpression><![CDATA[$V{total_value}]]></variableExpression>
	</variable>
	<variable name="count_trans_item" class="java.lang.Integer" resetType="Group" resetGroup="item" calculation="Count">
		<variableExpression><![CDATA[$V{item_COUNT}]]></variableExpression>
	</variable>
	<variable name="date" class="java.lang.String">
		<variableExpression><![CDATA[$P{dateBegin}.concat( " - " ).concat($P{dateEnd})]]></variableExpression>
	</variable>
	<variable name="quantity_unit" class="java.math.BigDecimal" resetType="None">
		<variableExpression><![CDATA[($F{coefficient}==0)? $F{quantity}:$F{quantity}/$F{coefficient}]]></variableExpression>
	</variable>
	<variable name="value/unit" class="java.math.BigDecimal">
		<variableExpression><![CDATA[(($F{quantity}==0))? $F{cost}:$F{cost}*$V{quantity_unit}/$F{quantity}]]></variableExpression>
	</variable>
	<group name="warehouse">
		<groupExpression><![CDATA[$F{warehouse_code}]]></groupExpression>
		<groupHeader>
			<band height="49">
				<staticText>
					<reportElement x="550" y="29" width="62" height="20"/>
					<box leftPadding="0" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Vlere/njesi]]></text>
				</staticText>
				<staticText>
					<reportElement x="262" y="29" width="80" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Seria]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement x="132" y="0" width="66" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="9" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{warehouse_code}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="198" y="0" width="144" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="9" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{description}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="672" y="29" width="62" height="20"/>
					<box leftPadding="0" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Sasia Progresive]]></text>
				</staticText>
				<staticText>
					<reportElement x="198" y="29" width="64" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Dt. Dokumentit]]></text>
				</staticText>
				<staticText>
					<reportElement x="32" y="29" width="100" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Lloj Dokumenti]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="0" width="132" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="9" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{warehouse_name}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="132" y="29" width="66" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Nr. Dokument]]></text>
				</staticText>
				<staticText>
					<reportElement x="612" y="29" width="60" height="20"/>
					<box leftPadding="0" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Vlera totale]]></text>
				</staticText>
				<staticText>
					<reportElement x="342" y="29" width="64" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Dt. regjistrimi]]></text>
				</staticText>
				<staticText>
					<reportElement x="500" y="29" width="50" height="20"/>
					<box leftPadding="0" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Sasia]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="29" width="32" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Nr. Ref]]></text>
				</staticText>
				<staticText>
					<reportElement x="406" y="29" width="94" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Pershkrimi i dokumentit]]></text>
				</staticText>
				<staticText>
					<reportElement x="734" y="29" width="68" height="20"/>
					<box leftPadding="0" rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Vlera progresive]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="612" y="0" width="60" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_value_warehouse}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="406" y="0" width="94" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Totali Magazines]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<group name="item">
		<groupExpression><![CDATA[$F{item_code}]]></groupExpression>
		<groupHeader>
			<band height="20">
				<subreport>
					<reportElement x="406" y="0" width="396" height="20"/>
					<subreportParameter name="core_name">
						<subreportParameterExpression><![CDATA[$P{core_name}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter name="item_code">
						<subreportParameterExpression><![CDATA[$F{item_code}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter name="dateBegin">
						<subreportParameterExpression><![CDATA[$P{dateBegin}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter name="warehouse_code">
						<subreportParameterExpression><![CDATA[$F{warehouse_code}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter name="acc_name">
						<subreportParameterExpression><![CDATA[$P{acc_name}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<returnValue subreportVariable="quantity_state" toVariable="quantity_state"/>
					<returnValue subreportVariable="value_state" toVariable="value_state"/>
					<subreportExpression><![CDATA[$P{SUBREPORT_DIR_2} + "journal_item_state.jasper"]]></subreportExpression>
				</subreport>
				<textField isBlankWhenNull="true">
					<reportElement x="198" y="0" width="208" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true" isItalic="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{sale_description}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="132" y="0" width="66" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true" isItalic="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{item_code}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="0" width="132" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true" isItalic="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{item_name}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="500" y="0" width="50" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{quantity_item}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="612" y="0" width="60" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_value_item}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00" isBlankWhenNull="false">
					<reportElement x="672" y="0" width="62" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{quantity_item}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="734" y="0" width="68" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_value_item}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="406" y="0" width="94" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Nentotal Artikulli]]></text>
				</staticText>
				<line>
					<reportElement x="0" y="19" width="802" height="1"/>
					<graphicElement>
						<pen lineWidth="0.25" lineStyle="Dashed"/>
					</graphicElement>
				</line>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="100">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals( "true" )||$V{PAGE_NUMBER}==1]]></printWhenExpression>
			<subreport>
				<reportElement x="-20" y="0" width="822" height="100"/>
				<subreportParameter name="company_name">
					<subreportParameterExpression><![CDATA[$P{company_name}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="name_surname">
					<subreportParameterExpression><![CDATA[$P{name_surname}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="report_title">
					<subreportParameterExpression><![CDATA[$P{report_title}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="dateEnd">
					<subreportParameterExpression><![CDATA[$P{dateEnd}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="dateBegin">
					<subreportParameterExpression><![CDATA[$P{dateBegin}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_HEADER}]]></subreportExpression>
			</subreport>
			<staticText>
				<reportElement x="612" y="0" width="50" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Faqe       ]]></text>
			</staticText>
			<staticText>
				<reportElement x="706" y="0" width="46" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[nga]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="662" y="0" width="44" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" isBlankWhenNull="true">
				<reportElement x="752" y="0" width="50" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<rectangle>
				<reportElement stretchType="RelativeToTallestObject" x="1" y="1" width="801" height="19" forecolor="#FFD7DA" backcolor="#FFD7DA">
					<printWhenExpression><![CDATA[$F{status}.compareTo( 5 ) == 0]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="672" y="0" width="62" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{progressive_quantity}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="612" y="0" width="60" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{total_value}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="550" y="0" width="62" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{value/unit}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement x="198" y="0" width="64" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{document_date}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="262" y="0" width="80" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serial_number}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="0" width="32" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{count_trans_item}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="132" y="0" width="66" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{document_number}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement x="342" y="0" width="64" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{document_date}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="32" y="0" width="100" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{documenttype_name}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="500" y="0" width="50" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{quantity_unit}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="406" y="0" width="94" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{document_description}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="734" y="0" width="68" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{progressive_value}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="31" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals( "true" )]]></printWhenExpression>
			<textField>
				<reportElement x="0" y="11" width="802" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[$P{bottom_message}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="18" width="802" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
