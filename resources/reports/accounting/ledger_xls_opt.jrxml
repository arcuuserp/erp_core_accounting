<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Ditari i kontabilitetit (per excel)" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="NoDataSection" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="core_name" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="acc_name" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="currency" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["ALL"]]></defaultValueExpression>
	</parameter>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="name_surname" class="java.lang.String"/>
	<parameter name="bottom_message" class="java.lang.String"/>
	<parameter name="id_currency" class="java.lang.String"/>
	<parameter name="report_title" class="java.lang.String"/>
	<parameter name="dateBegin" class="java.lang.String">
		<defaultValueExpression><![CDATA["2010-01-01"]]></defaultValueExpression>
	</parameter>
	<parameter name="dateEnd" class="java.lang.String">
		<defaultValueExpression><![CDATA["2020-12-31"]]></defaultValueExpression>
	</parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_trans_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[ $P{show_canceled}.equals("true") ? "":"atrans.transaction_status=0 AND "]]></defaultValueExpression>
	</parameter>
	<parameter name="advancedFilters" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT atrans.transaction_status, CONCAT(atrans.`id_documenttype`,atrans.`document_id`) AS 'Nr.Ref', 
DATE_FORMAT(atrans.`transaction_system_date`,'%d/%m/%Y %h:%i %p ') AS 'Date Regjistrimi', atrans.description AS 'Pershkrimi',
atrans.`document_number` AS 'Nr.Dok', DATE_FORMAT(atrans.`transaction_date`,'%d/%m/%Y') AS 'Dt.Dok',
atrans.`transaction_type_name` AS 'Lloji',atrans.`currency` AS 'Monedha',atrans.`code` AS 'Nr.Llog',suser.`username` AS 'Emri',
atrans.`transaction_type_id`,
CASE WHEN atrans.`transaction_type_id`=1 THEN atrans.`home_cur_total` ELSE 0 END AS 'Vlefta Kredi',
CASE WHEN atrans.`transaction_type_id`=0 THEN atrans.`home_cur_total` ELSE 0 END AS 'Vlefta Debi',
CASE WHEN atrans.`transaction_type_id`=1 THEN atrans.`total` ELSE 0 END AS 'Vlefta Kredi(trans)',
CASE WHEN atrans.`transaction_type_id`=0 THEN atrans.`total` ELSE 0 END AS 'Vlefta Debi(trans)'
FROM $P!{acc_name}.`transaction` AS atrans
INNER JOIN $P!{core_name}.`users` AS suser
ON atrans.`id_user`=suser.`id`
INNER JOIN $P!{acc_name}.documenttypes dt ON
atrans.documenttype_name = dt.system_name
WHERE $P!{advancedFilters} $P!{exclude_canceled_trans_cond} atrans.`transaction_date` >= $P{dateBegin} AND
atrans.`transaction_date`<= $P{dateEnd}]]>
	</queryString>
	<field name="Nr.Ref" class="java.lang.Long"/>
	<field name="Date Regjistrimi" class="java.lang.String"/>
	<field name="Pershkrimi" class="java.lang.String"/>
	<field name="Nr.Dok" class="java.lang.String"/>
	<field name="Dt.Dok" class="java.lang.String"/>
	<field name="Lloji" class="java.lang.String"/>
	<field name="Monedha" class="java.lang.String"/>
	<field name="Nr.Llog" class="java.lang.String"/>
	<field name="Emri" class="java.lang.String"/>
	<field name="transaction_type_id" class="java.lang.Integer"/>
	<field name="transaction_status" class="java.lang.Integer"/>
	<field name="Vlefta Kredi" class="java.math.BigDecimal"/>
	<field name="Vlefta Debi" class="java.math.BigDecimal"/>
	<field name="Vlefta Kredi(trans)" class="java.math.BigDecimal"/>
	<field name="Vlefta Debi(trans)" class="java.math.BigDecimal"/>
	<variable name="Vlefta Kredi_1" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Kredi} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Debi_1" class="java.math.BigDecimal" resetType="Column" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Debi} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Kredi_2" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Kredi} : 0]]></variableExpression>
	</variable>
	<variable name="Vlefta Debi_2" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{Vlefta Debi} : 0]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="102" splitType="Stretch">
			<textField evaluationTime="Report">
				<reportElement x="753" y="0" width="49" height="20"/>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="644" y="0" width="52" height="20"/>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="584" y="0" width="60" height="20"/>
				<textElement textAlignment="Right"/>
				<text><![CDATA[Faqe]]></text>
			</staticText>
			<staticText>
				<reportElement x="696" y="0" width="57" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[nga]]></text>
			</staticText>
			<textField>
				<reportElement x="0" y="0" width="584" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[$P{company_name}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="52" width="803" height="20"/>
				<textElement textAlignment="Center">
					<font size="11" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{report_title}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="20" width="187" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Monedha e shfaqjes se ditarit]]></text>
			</staticText>
			<textField>
				<reportElement x="187" y="20" width="88" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{currency}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="1" y="72" width="274" height="20"/>
				<box leftPadding="0" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Periudha:]]></text>
			</staticText>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy">
				<reportElement x="275" y="72" width="89" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateBegin})]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="dd/MM/yyyy">
				<reportElement x="469" y="72" width="334" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateEnd})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="364" y="72" width="105" height="20"/>
				<box bottomPadding="7"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[-]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band height="24" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<textField>
				<reportElement x="644" y="0" width="158" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{name_surname}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="584" y="0" width="60" height="20"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Printoi:]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="40" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="0" y="20" width="33" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Nr.Ref]]></text>
			</staticText>
			<staticText>
				<reportElement x="33" y="20" width="81" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Date Regjistrimi]]></text>
			</staticText>
			<staticText>
				<reportElement x="114" y="20" width="28" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Lloji]]></text>
			</staticText>
			<staticText>
				<reportElement x="187" y="20" width="88" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Emri]]></text>
			</staticText>
			<staticText>
				<reportElement x="364" y="20" width="105" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Pershkrimi]]></text>
			</staticText>
			<staticText>
				<reportElement x="469" y="20" width="58" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Nr.Llog]]></text>
			</staticText>
			<staticText>
				<reportElement x="644" y="20" width="52" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Vlefta Debi(trans)]]></text>
			</staticText>
			<staticText>
				<reportElement x="527" y="0" width="117" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Monedhe Baze ]]></text>
			</staticText>
			<staticText>
				<reportElement x="644" y="0" width="109" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Monedhe Transaksioni]]></text>
			</staticText>
			<staticText>
				<reportElement x="753" y="20" width="49" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Monedha]]></text>
			</staticText>
			<staticText>
				<reportElement x="275" y="20" width="89" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Dt.Dok]]></text>
			</staticText>
			<staticText>
				<reportElement x="142" y="20" width="45" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Nr.Dok]]></text>
			</staticText>
			<staticText>
				<reportElement x="527" y="20" width="57" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Vlefta Debi]]></text>
			</staticText>
			<staticText>
				<reportElement x="584" y="20" width="60" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Vlefta Kredi]]></text>
			</staticText>
			<staticText>
				<reportElement x="696" y="20" width="57" height="20"/>
				<box topPadding="0" leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Vlefta Kredi(trans)]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<rectangle>
				<reportElement stretchType="RelativeToTallestObject" x="1" y="0" width="802" height="19" forecolor="#FFD7DA" backcolor="#FFD7DA">
					<printWhenExpression><![CDATA[$F{transaction_status}.compareTo( 0 ) > 0]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField>
				<reportElement mode="Transparent" x="0" y="0" width="33" height="20" backcolor="#FFFFCC"/>
				<box topPadding="0" leftPadding="2">
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Nr.Ref}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy">
				<reportElement mode="Transparent" x="33" y="0" width="81" height="20" backcolor="#FFFFCC"/>
				<box topPadding="0" leftPadding="2">
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Date Regjistrimi}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Transparent" x="114" y="0" width="28" height="20" backcolor="#FFFFCC"/>
				<box topPadding="0" leftPadding="2">
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Lloji}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Transparent" x="187" y="0" width="88" height="20" backcolor="#FFFFCC"/>
				<box topPadding="0" leftPadding="2">
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Emri}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Transparent" x="275" y="0" width="89" height="20" backcolor="#FFFFCC"/>
				<box topPadding="0" leftPadding="2">
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Dt.Dok}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Transparent" x="364" y="0" width="105" height="20" backcolor="#FFFFCC"/>
				<box topPadding="0" leftPadding="2">
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Pershkrimi}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Transparent" x="142" y="0" width="45" height="20" backcolor="#FFFFCC"/>
				<box topPadding="0" leftPadding="2">
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Nr.Dok}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Transparent" x="469" y="0" width="58" height="20" backcolor="#FFFFCC"/>
				<box topPadding="0" leftPadding="2">
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Nr.Llog}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Transparent" x="584" y="0" width="60" height="20" backcolor="#FFFFCC"/>
				<box topPadding="0" leftPadding="2">
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Vlefta Kredi}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Transparent" x="527" y="0" width="57" height="20" backcolor="#FFFFCC"/>
				<box topPadding="0" leftPadding="2">
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Vlefta Debi}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Transparent" x="696" y="0" width="57" height="20" backcolor="#FFFFCC"/>
				<box topPadding="0" leftPadding="2">
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Vlefta Kredi(trans)}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Transparent" x="644" y="0" width="52" height="20" backcolor="#FFFFCC"/>
				<box topPadding="0" leftPadding="2">
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Vlefta Debi(trans)}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Transparent" x="753" y="0" width="49" height="20" backcolor="#FFFFCC"/>
				<box topPadding="0" leftPadding="2">
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Monedha}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band height="40" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="584" y="0" width="60" height="20" backcolor="#FFFFCC"/>
				<box leftPadding="2">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Vlefta Kredi_1}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="527" y="0" width="57" height="20" backcolor="#FFFFCC"/>
				<box leftPadding="2">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Vlefta Debi_1}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="584" y="20" width="60" height="20" backcolor="#FFFFCC"/>
				<box leftPadding="2">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Vlefta Kredi_2}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="527" y="20" width="57" height="20" backcolor="#FFFFCC"/>
				<box leftPadding="2">
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<leftPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<bottomPen lineWidth="1.0" lineColor="#FFFFFF"/>
					<rightPen lineWidth="1.0" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Vlefta Debi_2}]]></textFieldExpression>
			</textField>
		</band>
	</columnFooter>
	<pageFooter>
		<band height="38" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
			<textField>
				<reportElement x="0" y="18" width="802" height="20"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bottom_message}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="0" width="802" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
