<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="furnitori_gjendja" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="BlankPage" columnWidth="842" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="291"/>
	<property name="ireport.y" value="0"/>
	<parameter name="accounting_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="date_begin" class="java.lang.String"/>
	<parameter name="supplier_code" class="java.lang.String"/>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_trans_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals('0')?" AND transaction_status=0":""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT supplier_code, exchange_rate, currency,
		CASE WHEN ((gjendja_debi - gjendja_kredi) > 0) THEN (gjendja_debi-gjendja_kredi) ELSE 0 END AS 'gjendja_debi',
		CASE WHEN ((gjendja_kredi - gjendja_debi) > 0) THEN (gjendja_kredi - gjendja_debi) ELSE 0 END AS 'gjendja_kredi',
		CASE WHEN ((gjendja_debi_llog - gjendja_kredi_llog) >0) THEN (gjendja_debi_llog - gjendja_kredi_llog) ELSE 0 END AS 'gjendja_debi_llog',
		CASE WHEN ((gjendja_kredi_llog - gjendja_debi_llog) > 0) THEN (gjendja_kredi_llog - gjendja_debi_llog) ELSE 0 END AS 'gjendja_kredi_llog'
FROM (
SELECT * FROM (
SELECT s.`supplier_code`,t.`exchange_rate`,t.currency,
SUM(CASE WHEN (t.`transaction_type_id`=0) THEN t.`home_cur_total` ELSE 0 END)  AS 'gjendja_debi',
SUM(CASE WHEN (t.`transaction_type_id`=1) THEN t.`home_cur_total` ELSE 0 END)  AS 'gjendja_kredi',
SUM(CASE WHEN (t.`transaction_type_id`=0) THEN t.`account_cur_total` ELSE 0 END) AS 'gjendja_debi_llog',
SUM(CASE WHEN (t.`transaction_type_id`=1) THEN t.`account_cur_total` ELSE 0 END )AS 'gjendja_kredi_llog'
FROM $P!{accounting_db}.`supplier` AS s
INNER JOIN $P!{accounting_db}.`transaction` AS t
ON s.`supplier_id`=t.`subject_id` AND t.subject = 'supplier' $P!{exclude_canceled_trans_cond}
WHERE t.`transaction_date` < $P{date_begin}
AND s.supplier_code = $P{supplier_code}
AND s.`deleted`=0
) AS d
GROUP BY 1
ORDER BY 1
) a]]>
	</queryString>
	<field name="supplier_code" class="java.lang.String"/>
	<field name="exchange_rate" class="java.math.BigDecimal"/>
	<field name="currency" class="java.lang.String"/>
	<field name="gjendja_debi" class="java.math.BigDecimal"/>
	<field name="gjendja_kredi" class="java.math.BigDecimal"/>
	<field name="gjendja_debi_llog" class="java.math.BigDecimal"/>
	<field name="gjendja_kredi_llog" class="java.math.BigDecimal"/>
	<variable name="gjendja_debi_1" class="java.math.BigDecimal" resetType="Group" resetGroup="furnitori" calculation="Sum">
		<variableExpression><![CDATA[$F{gjendja_debi}]]></variableExpression>
	</variable>
	<variable name="gjendja_kredi_1" class="java.math.BigDecimal" resetType="Group" resetGroup="furnitori" calculation="Sum">
		<variableExpression><![CDATA[$F{gjendja_kredi}]]></variableExpression>
	</variable>
	<variable name="gejndja_debi_trans" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{gjendja_debi_llog}.multiply($F{exchange_rate})]]></variableExpression>
	</variable>
	<variable name="gjendja_kredi_trans" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{gjendja_kredi_llog}.multiply($F{exchange_rate})]]></variableExpression>
	</variable>
	<variable name="gjendja_debi_trans_1" class="java.math.BigDecimal" resetType="Group" resetGroup="furnitori" calculation="Sum">
		<variableExpression><![CDATA[$V{gjendja_kredi_trans}]]></variableExpression>
	</variable>
	<variable name="gjendja_kredi_trans_1" class="java.math.BigDecimal" resetType="Group" resetGroup="furnitori" calculation="Sum">
		<variableExpression><![CDATA[$V{gjendja_kredi_trans}]]></variableExpression>
	</variable>
	<variable name="debitHomeCur" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{gjendja_debi_1}]]></variableExpression>
	</variable>
	<variable name="creditHomeCur" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{gjendja_kredi_1}]]></variableExpression>
	</variable>
	<variable name="progresiviLlogSub" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{gjendja_debi_llog}.subtract( $F{gjendja_kredi_llog} )]]></variableExpression>
	</variable>
	<variable name="progresiviSub" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{gjendja_debi}.subtract( $F{gjendja_kredi} )]]></variableExpression>
	</variable>
	<group name="supplier_code">
		<groupExpression><![CDATA[$F{supplier_code}]]></groupExpression>
	</group>
	<group name="furnitori">
		<groupExpression><![CDATA[$F{supplier_code}]]></groupExpression>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<detail>
		<band height="20" splitType="Stretch">
			<staticText>
				<reportElement mode="Opaque" x="354" y="0" width="72" height="20" backcolor="#FCFCC8"/>
				<box leftPadding="2">
					<pen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Gjendja Fillestare]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="542" y="0" width="58" height="20" backcolor="#FCFCC8"/>
				<box rightPadding="2">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="716" y="0" width="58" height="20" backcolor="#FCFCC8"/>
				<box>
					<pen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement/>
				<text><![CDATA[]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement mode="Opaque" x="774" y="0" width="28" height="20" backcolor="#FCFCC8"/>
				<box leftPadding="2">
					<pen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Opaque" x="0" y="0" width="354" height="20" backcolor="#FCFCC8"/>
				<box>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement/>
				<text><![CDATA[]]></text>
			</staticText>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Opaque" x="426" y="0" width="58" height="20" backcolor="#FCFCC8"/>
				<box rightPadding="2">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{gjendja_debi}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Opaque" x="484" y="0" width="58" height="20" backcolor="#FCFCC8"/>
				<box rightPadding="2">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{gjendja_kredi}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Opaque" x="600" y="0" width="58" height="20" backcolor="#FCFCC8"/>
				<box rightPadding="2">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{gjendja_debi_llog}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Opaque" x="658" y="0" width="58" height="20" backcolor="#FCFCC8"/>
				<box rightPadding="2">
					<pen lineWidth="0.25"/>
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{gjendja_kredi_llog}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="542" y="0" width="58" height="20" backcolor="#FCFCC8"/>
				<box rightPadding="2">
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{progresiviSub}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement mode="Opaque" x="716" y="0" width="58" height="20" backcolor="#FCFCC8"/>
				<box rightPadding="2">
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{progresiviLlogSub}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
