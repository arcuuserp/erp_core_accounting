<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="supplierAnalyticActionsReport" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="NoDataSection" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="accounting_db" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="dateBegin" class="java.lang.String"/>
	<parameter name="dateEnd" class="java.lang.String"/>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="report_title" class="java.lang.String"/>
	<parameter name="name_surname" class="java.lang.String"/>
	<parameter name="bottom_message" class="java.lang.String"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["C://reports/MyReports/accounting/"]]></defaultValueExpression>
	</parameter>
	<parameter name="month_human" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[$P{dateBegin}+" - "+$P{dateEnd}]]></defaultValueExpression>
	</parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_docs_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals('0')?"header.id_docstatus<>5 AND ":""]]></defaultValueExpression>
	</parameter>
	<parameter name="supplierCondition" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT s.supplier_id, s.supplier_code, s.supplier_name, s.id_currency, s.currency AS supplier_currency, nje.*
FROM $P!{accounting_db}.supplier s
LEFT JOIN(
	SELECT header.id_docstatus, header.id_supplier, header.purchaseinvoiceheader_id As id, header.`serial_number`, header.`document_number`, header.`document_date`, header.description,
	IFNULL(header.`home_cur_total_with_vat`,0) AS home_value,
	IFNULL(header.`home_cur_total_with_vat`/header.`supplier_cur_exchange_rate`,0) AS curr_value, 0 AS payed, 0 As payed_home_curr
	FROM $P!{accounting_db}.purchaseinvoiceheader header
	WHERE $P!{exclude_canceled_docs_cond} LEFT(header.`document_date`,10) >= $P{dateBegin}   AND LEFT(header.`document_date`,10) <= $P{dateEnd}

	UNION ALL
	SELECT header.id_docstatus, header.id_supplier, pp.`subject_id` As id, header.`serial_number`, header.`document_number`, header.`document_date`, header.description, 0 As home_value, 0 As curr_value, IFNULL(pp.home_cur_amount_payed/pp.receipt_currency_exchange_rate,0) AS payed, pp.home_cur_amount_payed AS payed_home_curr
	FROM $P!{accounting_db}.p_payment pp
	INNER JOIN $P!{accounting_db}.cashpaymentheader header
	ON $P!{exclude_canceled_docs_cond} header.cashpaymentheader_id = pp.id_cashpaymentheader
	WHERE pp.deleted = 0 AND pp.`subject` = "purchaseinvoiceheader" AND LEFT(pp.`payment_date`,10) >= $P{dateBegin} AND LEFT(pp.`payment_date`,10) <= $P{dateEnd} ) nje
ON s.`supplier_id` = nje.id_supplier
$P!{supplierCondition}
ORDER BY s.`supplier_code`, nje.document_date, nje.document_number]]>
	</queryString>
	<field name="supplier_id" class="java.lang.Integer"/>
	<field name="id_docstatus" class="java.lang.Integer"/>
	<field name="supplier_code" class="java.lang.String"/>
	<field name="supplier_name" class="java.lang.String"/>
	<field name="id_currency" class="java.lang.String"/>
	<field name="supplier_currency" class="java.lang.String"/>
	<field name="id_supplier" class="java.lang.Long"/>
	<field name="id" class="java.lang.Long"/>
	<field name="serial_number" class="java.lang.String"/>
	<field name="document_number" class="java.lang.String"/>
	<field name="document_date" class="java.sql.Timestamp"/>
	<field name="description" class="java.lang.String"/>
	<field name="home_value" class="java.math.BigDecimal"/>
	<field name="curr_value" class="java.math.BigDecimal"/>
	<field name="payed" class="java.math.BigDecimal"/>
	<field name="payed_home_curr" class="java.math.BigDecimal"/>
	<variable name="notpayed" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{home_value} - $F{payed_home_curr}]]></variableExpression>
	</variable>
	<variable name="home_value_1" class="java.math.BigDecimal" resetType="Group" resetGroup="supplier" calculation="Sum">
		<variableExpression><![CDATA[($F{id_docstatus}.compareTo( 5 ) == 0) ? 0 : $F{home_value}]]></variableExpression>
	</variable>
	<variable name="payed_home_curr_1" class="java.math.BigDecimal" resetType="Group" resetGroup="supplier" calculation="Sum">
		<variableExpression><![CDATA[($F{id_docstatus}.compareTo( 5 ) == 0) ? 0 : $F{payed_home_curr}]]></variableExpression>
	</variable>
	<variable name="subtotalnotpayed" class="java.math.BigDecimal" resetType="Group" resetGroup="supplier" calculation="Sum">
		<variableExpression><![CDATA[($F{id_docstatus}.compareTo( 5 ) == 0) ? 0 : $V{notpayed}]]></variableExpression>
		<initialValueExpression><![CDATA[]]></initialValueExpression>
	</variable>
	<variable name="home_value_2" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[($F{id_docstatus}.compareTo( 5 ) == 0) ? 0 : $F{home_value}]]></variableExpression>
	</variable>
	<variable name="payed_home_curr_2" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[($F{id_docstatus}.compareTo( 5 ) == 0) ? 0 : $F{payed_home_curr}]]></variableExpression>
	</variable>
	<variable name="totalnotpayed" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$V{notpayed}]]></variableExpression>
	</variable>
	<group name="supplier">
		<groupExpression><![CDATA[$F{supplier_id}]]></groupExpression>
		<groupHeader>
			<band height="27">
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="3" width="80" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="0.75"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA[$F{supplier_code}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="80" y="3" width="190" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="0.75"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA[$F{supplier_name}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement x="0" y="26" width="802" height="1"/>
					<graphicElement>
						<pen lineWidth="0.75" lineStyle="Dashed"/>
					</graphicElement>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="16">
				<printWhenExpression><![CDATA[$F{serial_number}!= null && $F{home_value}!= null]]></printWhenExpression>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="368" y="0" width="104" height="16"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{supplier_code}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="472" y="0" width="110" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="0.75"/>
					</box>
					<textElement textAlignment="Right">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{home_value_1}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="582" y="0" width="110" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="0.75"/>
					</box>
					<textElement textAlignment="Right">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{payed_home_curr_1}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="692" y="0" width="110" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="0.75"/>
					</box>
					<textElement textAlignment="Right">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{subtotalnotpayed}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="103" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="1" width="692" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{company_name}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="42" width="802" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{report_title}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="82" width="802" height="20"/>
				<box rightPadding="20"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{name_surname}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="62" width="368" height="20"/>
				<box leftPadding="0" rightPadding="40"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Periudha:]]></text>
			</staticText>
			<textField evaluationTime="Report" isBlankWhenNull="true">
				<reportElement x="773" y="1" width="29" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="743" y="1" width="30" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[nga]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="713" y="1" width="30" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="692" y="1" width="21" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Faqe       ]]></text>
			</staticText>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy">
				<reportElement x="368" y="62" width="104" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateBegin})]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="dd/MM/yyyy">
				<reportElement x="582" y="62" width="110" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateEnd})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="472" y="62" width="110" height="20"/>
				<box bottomPadding="7"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[-]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="40" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="0" y="20" width="80" height="20"/>
				<box>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement/>
				<text><![CDATA[Fatura]]></text>
			</staticText>
			<staticText>
				<reportElement x="80" y="20" width="392" height="20"/>
				<box>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement/>
				<text><![CDATA[Pershkrim]]></text>
			</staticText>
			<staticText>
				<reportElement x="472" y="20" width="110" height="20"/>
				<box rightPadding="2">
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="false"/>
				</textElement>
				<text><![CDATA[Detyrim fature]]></text>
			</staticText>
			<staticText>
				<reportElement x="582" y="20" width="110" height="20"/>
				<box rightPadding="2">
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="false"/>
				</textElement>
				<text><![CDATA[Shlyer fature]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="0" width="802" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
				</box>
				<textElement/>
				<text><![CDATA[Furnitor]]></text>
			</staticText>
			<staticText>
				<reportElement x="692" y="20" width="110" height="20"/>
				<box rightPadding="2">
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="false"/>
				</textElement>
				<text><![CDATA[Mbetur pa paguar]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="16" splitType="Stretch">
			<printWhenExpression><![CDATA[$F{serial_number} != null && $F{home_value}!= null]]></printWhenExpression>
			<rectangle>
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="801" height="15" forecolor="#FFD7DA" backcolor="#FFD7DA">
					<printWhenExpression><![CDATA[$F{id_docstatus}.compareTo( 5 ) == 0]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="80" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA[$F{serial_number}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd.MM.yyyy" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="80" y="0" width="54" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA[$F{document_date}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="134" y="0" width="82" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA[$F{document_number}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd.MM.yyyy" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="216" y="0" width="54" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA[$F{document_date}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="368" y="0" width="104" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA[$F{description}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="270" y="0" width="98" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA[$F{supplier_name}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="472" y="0" width="110" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{home_value}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="582" y="0" width="110" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{payed_home_curr}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="692" y="0" width="110" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Dashed"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{notpayed}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="61" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
			<subreport>
				<reportElement x="-20" y="0" width="842" height="28"/>
				<subreportParameter name="bottom_message">
					<subreportParameterExpression><![CDATA[$P{bottom_message}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "footer.jasper"]]></subreportExpression>
			</subreport>
		</band>
	</pageFooter>
	<summary>
		<band height="16">
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="472" y="0" width="110" height="16"/>
				<box rightPadding="2">
					<topPen lineWidth="0.75" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{home_value_2}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="582" y="0" width="110" height="16"/>
				<box rightPadding="2">
					<topPen lineWidth="0.75" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{payed_home_curr_2}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="692" y="0" width="110" height="16"/>
				<box rightPadding="2">
					<topPen lineWidth="0.75" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalnotpayed}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="368" y="0" width="104" height="16"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total]]></text>
			</staticText>
		</band>
	</summary>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="1" y="0" width="801" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
