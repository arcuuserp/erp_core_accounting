<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="cashAnalytic" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="NoDataSection" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="user_name" class="java.lang.String"/>
	<parameter name="accounting_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="fDateEnd" class="java.lang.String"/>
	<parameter name="fDateBegin" class="java.lang.String"/>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="dateEnd" class="java.lang.String"/>
	<parameter name="dateBegin" class="java.lang.String"/>
	<parameter name="id_cash" class="java.lang.String">
		<defaultValueExpression><![CDATA["cash.cash_id"]]></defaultValueExpression>
	</parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="date" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[$P{fDateBegin}+" - "+$P{fDateEnd}]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_docs_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals('0')?"header.id_docstatus<>5 AND ":""]]></defaultValueExpression>
	</parameter>
	<parameter name="advancedFilters" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT * FROM (

SELECT a.*,accounts.code AS fat_cuse_code, accounts.name AS fat_cust FROM (
(SELECT header.id_docstatus, header.id AS doc_id, header.id_cash, header.id_currency, header.currency, header.id_customer AS id_sub
		, header.serial_number, header.document_number
		, header.document_date, 0 AS pagese, header.total_cashed AS arketim
		, header.home_cur_total_cashed AS arketim_home_cur, cash.name AS cash_name, cash.code AS cash_code, cash.id_account
		, contacts.contact_code AS sub_code, contacts.contact_firstname AS sub_name
		, rp.subject_id, rp.subject
		, '' AS fat_nr, '' AS fat_date, rp.currency AS fat_cur
		, rp.amount_payed AS pagese_fature, 0 AS arketim_fature, rp.home_cur_amount_payed AS pagese_fature_home
		, 0 AS arketim_fature_home
		,accounts.code, acc.id_account AS cpbodyacc
		FROM $P!{accounting_db}.cashreceipt header
			INNER JOIN $P!{accounting_db}.cash ON header.id_cash = cash.id AND cash.unit_type = 1
			INNER JOIN $P!{accounting_db}.contacts ON header.id_customer = contacts.id
			INNER JOIN $P!{accounting_db}.r_payment rp ON header.id = rp.id_cashreceipt AND header.id_cash = rp.id_cash AND rp.deleted = 0 AND rp.subject = 'account'
			INNER JOIN $P!{accounting_db}.cashreceiptbodyaccounts acc ON rp.id = acc.id_r_payment
			INNER JOIN $P!{accounting_db}.accounts ON cash.id_account = accounts.id
			WHERE cash.id = $P!{id_cash} AND $P!{exclude_canceled_docs_cond}
				header.document_date >= $P{dateBegin} AND header.document_date <= $P{dateEnd}) a

	INNER JOIN $P!{accounting_db}.accounts ON a.cpbodyacc = accounts.id)

UNION ALL

SELECT header.id_docstatus, header.id AS doc_id, header.id_cash, header.id_currency, header.currency, header.id_customer AS id_sub
		, header.serial_number, header.document_number
		, header.document_date, 0 AS pagese, header.total_cashed AS arketim
		, header.home_cur_total_cashed AS arketim_home_cur, cash.name AS cash_name, cash.code AS cash_code, cash.id_account
		, contacts.contact_code AS sub_code, contacts.contact_firstname AS sub_name
		, rp.subject_id, rp.subject
		, sh.document_number AS fat_nr, DATE_FORMAT(sh.document_date, '%d/%m/%Y') AS fat_date, sh.currency AS fat_cur
		, rp.amount_payed AS pagese_fature, 0 AS arketim_fature, rp.home_cur_amount_payed AS pagese_fature_home
		, 0 AS arketim_fature_home
		,accounts.code, 0 cpbodyacc, sh.customer_name AS fat_cust, sh.customer_code AS fat_cuse_code
		FROM $P!{accounting_db}.cashreceipt header
			INNER JOIN $P!{accounting_db}.cash ON header.id_cash = cash.id AND cash.unit_type = 1
			INNER JOIN $P!{accounting_db}.contacts ON header.id_customer = contacts.id
			INNER JOIN $P!{accounting_db}.r_payment rp ON header.id = rp.id_cashreceipt AND header.id_cash = rp.id_cash AND rp.deleted = 0 AND rp.subject = 'salesinvoice'
			INNER JOIN $P!{accounting_db}.salesinvoice sh ON rp.subject_id = sh.id
			INNER JOIN $P!{accounting_db}.accounts ON cash.id_account = accounts.id
			WHERE cash.id = $P!{id_cash} AND $P!{exclude_canceled_docs_cond}
				header.document_date >= $P{dateBegin} AND header.document_date <= $P{dateEnd}

UNION ALL

SELECT b.*,accounts.code AS fat_cuse_code, accounts.name AS fat_cust FROM (
(SELECT header.id_docstatus, header.id AS doc_id, header.id_cash, header.id_currency, header.currency, header.id_supplier AS id_sub
		, header.serial_number, header.document_number
		, header.document_date, header.total_amount_payed AS pagese, 0  AS arketim
		, header.home_cur_total_amount_payed * (-1) AS arketim_home_cur, cash.name AS cash_name, cash.code AS cash_code, cash.id_account
		, contact_code AS sub_code, contacts.contact_firstname AS sub_name
		, pp.subject_id, pp.subject
		, '' AS fat_nr, '' AS fat_date, pp.currency AS fat_cur
		, 0 AS pagese_fature, pp.amount_payed AS arketim_fature, 0 AS pagese_fature_home
		, pp.home_cur_amount_payed AS arketim_fature_home
		,accounts.code, acc.id_account AS cpbodyacc
		FROM $P!{accounting_db}.cashpayment header
			INNER JOIN $P!{accounting_db}.cash ON header.id_cash = cash.id AND cash.unit_type = 1
			INNER JOIN $P!{accounting_db}.contacts ON header.id_supplier = contacts.id
			INNER JOIN $P!{accounting_db}.p_payment pp ON header.id = pp.id_cashpayment AND header.id_cash = pp.id_cash AND pp.deleted = 0 AND pp.subject = 'account'
			INNER JOIN $P!{accounting_db}.cashpaymentbodyaccounts acc ON pp.id = acc.id_p_payment
			INNER JOIN $P!{accounting_db}.accounts ON cash.id_account = accounts.id
			WHERE cash.id = $P!{id_cash} AND $P!{exclude_canceled_docs_cond}
				header.document_date >= $P{dateBegin} AND header.document_date <= $P{dateEnd}) b

	INNER JOIN $P!{accounting_db}.accounts ON b.cpbodyacc = accounts.id)

UNION ALL

SELECT header.id_docstatus, header.id AS doc_id, header.id_cash, header.id_currency, header.currency, header.id_supplier AS id_sub
		, header.serial_number, header.document_number
		, header.document_date, header.total_amount_payed AS pagese, 0  AS arketim
		, header.home_cur_total_amount_payed * (-1) AS arketim_home_cur,cash.name AS cash_name, cash.code AS cash_code, cash.id_account
		, contact_code AS sub_code, contacts.contact_firstname AS sub_name
		, pp.subject_id, pp.subject
		, ph.document_number AS fat_nr, DATE_FORMAT(ph.document_date, '%d/%m/%Y') AS fat_date, ph.currency AS fat_cur
		, 0 AS pagese_fature, pp.amount_payed AS arketim_fature, 0 AS pagese_fature_home
		, pp.home_cur_amount_payed AS arketim_fature_home
		,accounts.code, 0 cpbodyacc, ph.supplier_name AS fat_cust, ph.supplier_code AS fat_cuse_code
		FROM $P!{accounting_db}.cashpayment header
			INNER JOIN $P!{accounting_db}.cash ON header.id_cash = cash.id AND cash.unit_type = 1
			INNER JOIN $P!{accounting_db}.contacts ON header.id_supplier = contacts.id
			INNER JOIN $P!{accounting_db}.p_payment pp ON header.id = pp.id_cashpayment AND header.id_cash = pp.id_cash AND pp.deleted = 0 AND pp.subject = 'purchaseinvoice'
			INNER JOIN $P!{accounting_db}.purchaseinvoice ph ON pp.subject_id = ph.id
			INNER JOIN $P!{accounting_db}.accounts ON cash.id_account = accounts.id
			WHERE cash.id = $P!{id_cash} AND $P!{exclude_canceled_docs_cond}
				header.document_date >= $P{dateBegin} AND header.document_date <= $P{dateEnd}
) alldata WHERE $P!{advancedFilters}
	ORDER BY alldata.id_cash, alldata.doc_id, alldata.document_date, alldata.document_number]]>
	</queryString>
	<field name="doc_id" class="java.lang.Integer"/>
	<field name="id_cash" class="java.lang.Integer"/>
	<field name="id_docstatus" class="java.lang.Integer"/>
	<field name="currency" class="java.lang.String"/>
	<field name="id_sub" class="java.lang.Long"/>
	<field name="serial_number" class="java.lang.String"/>
	<field name="document_number" class="java.lang.String"/>
	<field name="document_date" class="java.util.Date"/>
	<field name="pagese" class="java.math.BigDecimal"/>
	<field name="arketim" class="java.math.BigDecimal"/>
	<field name="arketim_home_cur" class="java.math.BigDecimal"/>
	<field name="cash_name" class="java.lang.String"/>
	<field name="cash_code" class="java.lang.String"/>
	<field name="id_account" class="java.lang.String"/>
	<field name="sub_code" class="java.lang.String"/>
	<field name="sub_name" class="java.lang.String"/>
	<field name="subject_id" class="java.lang.Long"/>
	<field name="subject" class="java.lang.String"/>
	<field name="fat_nr" class="java.lang.String"/>
	<field name="fat_date" class="java.lang.String"/>
	<field name="fat_cur" class="java.lang.String"/>
	<field name="pagese_fature" class="java.math.BigDecimal"/>
	<field name="arketim_fature" class="java.math.BigDecimal"/>
	<field name="pagese_fature_home" class="java.math.BigDecimal"/>
	<field name="arketim_fature_home" class="java.math.BigDecimal"/>
	<field name="fat_cust" class="java.lang.String"/>
	<field name="fat_cuse_code" class="java.lang.String"/>
	<field name="code" class="java.lang.String"/>
	<variable name="totMonedhaBaze" class="java.math.BigDecimal" resetType="Group" resetGroup="subjectGroup">
		<variableExpression><![CDATA[$V{totMonedhaBaze}.add( $F{arketim_fature_home} ).subtract($F{pagese_fature_home})]]></variableExpression>
		<initialValueExpression><![CDATA[0]]></initialValueExpression>
	</variable>
	<variable name="totMonBazeReal" class="java.math.BigDecimal" resetType="Group" resetGroup="subjectGroup">
		<variableExpression><![CDATA[$F{arketim_home_cur}.add( $V{totMonedhaBaze} )]]></variableExpression>
	</variable>
	<variable name="dateTime" class="java.util.Date">
		<variableExpression><![CDATA[new Date()]]></variableExpression>
	</variable>
	<group name="cashGroup">
		<groupExpression><![CDATA[$F{id_cash}]]></groupExpression>
		<groupHeader>
			<band height="27">
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="8" width="100" height="15"/>
					<box leftPadding="2" rightPadding="2">
						<bottomPen lineWidth="1.75"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{cash_code}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="100" y="8" width="219" height="15"/>
					<box leftPadding="2" rightPadding="2">
						<bottomPen lineWidth="1.75"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{cash_name}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<group name="docGroup">
		<groupExpression><![CDATA[$F{doc_id}]]></groupExpression>
		<groupHeader>
			<band height="16">
				<frame>
					<reportElement mode="Opaque" x="0" y="0" width="802" height="15" backcolor="#ECE9D8"/>
					<textField isBlankWhenNull="true">
						<reportElement mode="Opaque" x="100" y="0" width="219" height="15" forecolor="#000000" backcolor="#ECE9D8"/>
						<box leftPadding="2"/>
						<textElement verticalAlignment="Middle">
							<font size="9"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{sub_name}]]></textFieldExpression>
					</textField>
					<textField isBlankWhenNull="true">
						<reportElement mode="Opaque" x="319" y="0" width="83" height="15" forecolor="#000000" backcolor="#ECE9D8"/>
						<box leftPadding="2"/>
						<textElement verticalAlignment="Middle">
							<font size="9"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{document_number}]]></textFieldExpression>
					</textField>
					<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
						<reportElement mode="Opaque" x="402" y="0" width="86" height="15" forecolor="#000000" backcolor="#ECE9D8"/>
						<box leftPadding="2"/>
						<textElement verticalAlignment="Middle">
							<font size="9"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{document_date}]]></textFieldExpression>
					</textField>
					<textField pattern="#,##0.00" isBlankWhenNull="true">
						<reportElement mode="Opaque" x="488" y="0" width="86" height="15" forecolor="#000000" backcolor="#ECE9D8">
							<printWhenExpression><![CDATA[$F{arketim}.compareTo( 0 ) != 0]]></printWhenExpression>
						</reportElement>
						<box rightPadding="2"/>
						<textElement textAlignment="Right" verticalAlignment="Middle">
							<font size="9"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{arketim}]]></textFieldExpression>
					</textField>
					<textField isBlankWhenNull="true">
						<reportElement mode="Opaque" x="574" y="0" width="20" height="15" forecolor="#000000" backcolor="#ECE9D8">
							<printWhenExpression><![CDATA[$F{arketim}.compareTo( 0 ) != 0]]></printWhenExpression>
						</reportElement>
						<box leftPadding="2"/>
						<textElement verticalAlignment="Middle">
							<font size="8"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
					</textField>
					<textField pattern="#,##0.00" isBlankWhenNull="true">
						<reportElement mode="Opaque" x="594" y="0" width="92" height="15" forecolor="#000000" backcolor="#ECE9D8">
							<printWhenExpression><![CDATA[$F{pagese}.compareTo( 0 ) != 0]]></printWhenExpression>
						</reportElement>
						<box rightPadding="2"/>
						<textElement textAlignment="Right" verticalAlignment="Middle">
							<font size="9"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{pagese}]]></textFieldExpression>
					</textField>
					<textField isBlankWhenNull="true">
						<reportElement mode="Opaque" x="686" y="0" width="20" height="15" forecolor="#000000" backcolor="#ECE9D8">
							<printWhenExpression><![CDATA[$F{pagese}.compareTo( 0 ) != 0]]></printWhenExpression>
						</reportElement>
						<box leftPadding="2"/>
						<textElement verticalAlignment="Middle">
							<font size="8"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
					</textField>
					<textField pattern="#,##0.00" isBlankWhenNull="true">
						<reportElement mode="Opaque" x="706" y="0" width="96" height="15" forecolor="#000000" backcolor="#ECE9D8"/>
						<box rightPadding="2"/>
						<textElement textAlignment="Right" verticalAlignment="Middle">
							<font size="9"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{arketim_home_cur}]]></textFieldExpression>
					</textField>
				</frame>
				<textField isBlankWhenNull="true">
					<reportElement mode="Opaque" x="0" y="0" width="100" height="15" forecolor="#000000" backcolor="#ECE9D8"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle">
						<font size="9"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{code}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<group name="subjectGroup">
		<groupExpression><![CDATA[$F{subject}]]></groupExpression>
		<groupHeader>
			<band height="1"/>
		</groupHeader>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="87" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<textField evaluationTime="Report">
				<reportElement x="731" y="60" width="71" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="594" y="40" width="208" height="20"/>
				<box rightPadding="5"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{user_name}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="60" width="18" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[dt.]]></text>
			</staticText>
			<staticText>
				<reportElement x="594" y="60" width="92" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box rightPadding="4"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Faqe]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="20" width="802" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<text><![CDATA[Banka - Analitik]]></text>
			</staticText>
			<staticText>
				<reportElement x="100" y="60" width="219" height="20"/>
				<box leftPadding="0" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Periudha: ]]></text>
			</staticText>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy HH.mm.ss" isBlankWhenNull="true">
				<reportElement x="18" y="60" width="82" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{dateTime}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="706" y="60" width="25" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[nga]]></text>
			</staticText>
			<textField>
				<reportElement x="686" y="60" width="20" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="0" width="802" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{company_name}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="319" y="60" width="169" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{date}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="33" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="0" y="2" width="100" height="30"/>
				<box leftPadding="2" bottomPadding="4" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Bottom"/>
				<text><![CDATA[Kod]]></text>
			</staticText>
			<staticText>
				<reportElement x="319" y="2" width="169" height="15"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
				</box>
				<textElement/>
				<text><![CDATA[Dokumenti]]></text>
			</staticText>
			<staticText>
				<reportElement x="100" y="2" width="219" height="30"/>
				<box leftPadding="2" bottomPadding="4" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Bottom"/>
				<text><![CDATA[Përshkrim]]></text>
			</staticText>
			<staticText>
				<reportElement x="488" y="2" width="106" height="30"/>
				<box leftPadding="2" bottomPadding="4" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom"/>
				<text><![CDATA[Arkëtim]]></text>
			</staticText>
			<staticText>
				<reportElement x="594" y="2" width="112" height="30"/>
				<box leftPadding="2" bottomPadding="4" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom"/>
				<text><![CDATA[Pagesë]]></text>
			</staticText>
			<staticText>
				<reportElement x="706" y="2" width="96" height="30"/>
				<box leftPadding="2" bottomPadding="4" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom"/>
				<text><![CDATA[Monedhe bazë]]></text>
			</staticText>
			<staticText>
				<reportElement x="319" y="17" width="169" height="15"/>
				<box leftPadding="2" rightPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement/>
				<text><![CDATA[Faturë]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="15" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement x="319" y="0" width="83" height="15"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{fat_nr}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="402" y="0" width="86" height="15"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{fat_date}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="0" width="100" height="15"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{fat_cuse_code}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="100" y="0" width="219" height="15"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{fat_cust}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="488" y="0" width="86" height="15">
					<printWhenExpression><![CDATA[$F{fat_cust} != null]]></printWhenExpression>
				</reportElement>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{arketim_fature}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="594" y="0" width="92" height="15">
					<printWhenExpression><![CDATA[$F{fat_cust} != null]]></printWhenExpression>
				</reportElement>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{pagese_fature}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="574" y="0" width="20" height="15"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{fat_cur}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="686" y="0" width="20" height="15"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{fat_cur}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="706" y="0" width="96" height="15"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totMonBazeReal}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="22" width="802" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
