<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Gjendje Kliente" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="NoDataSection" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="accounting_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="name_surname" class="java.lang.String"/>
	<parameter name="bottom_message" class="java.lang.String"/>
	<parameter name="report_title" class="java.lang.String"/>
	<parameter name="dateBegin" class="java.lang.String">
		<defaultValueExpression><![CDATA["2012-12-03"]]></defaultValueExpression>
	</parameter>
	<parameter name="dateEnd" class="java.lang.String">
		<defaultValueExpression><![CDATA["2013-12-30"]]></defaultValueExpression>
	</parameter>
	<parameter name="include_inactives" class="java.lang.String">
		<defaultValueExpression><![CDATA["YES"]]></defaultValueExpression>
	</parameter>
	<parameter name="join_type" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{include_inactives}.equals("YES")?"LEFT":"INNER"]]></defaultValueExpression>
	</parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_trans_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals('0')?" AND transaction_status=0":""]]></defaultValueExpression>
	</parameter>
	<parameter name="customerCondition" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT c.`contact_code`, c.`contact_display_name` ,c.`id_account`,  t.`account_currency`,a.code,
SUM(CASE WHEN (t.`transaction_date`<=$P{dateBegin})
THEN CASE WHEN (t.`transaction_type_id`=0) THEN t.`home_cur_total` ELSE 0 END ELSE 0 END ) AS 'gjendja_debit',SUM(CASE WHEN (t.`transaction_date`<=$P{dateBegin})
THEN CASE WHEN (t.`transaction_type_id`=1) THEN t.`home_cur_total` ELSE 0 END ELSE 0 END ) AS 'gjendja_kredit', SUM(CASE WHEN (t.`transaction_date`>$P{dateBegin} AND t.`transaction_date`<=$P{dateEnd})
THEN CASE WHEN (t.`transaction_type_id`=0) THEN t.`home_cur_total` ELSE 0 END ELSE 0 END ) AS 'debit',SUM(CASE WHEN (t.`transaction_date`>$P{dateBegin} AND t.`transaction_date`<=$P{dateEnd})
THEN CASE WHEN (t.`transaction_type_id`=1) THEN t.`home_cur_total` ELSE 0 END ELSE 0 END ) AS 'kredit'
FROM $P!{accounting_db}.`contacts` AS c
INNER JOIN $P!{accounting_db}.accounts a
ON c.id_account = a.id
$P!{join_type} JOIN $P!{accounting_db}.`transaction` AS t
ON t.subject="customer" AND c.id = t.subject_id $P!{exclude_canceled_trans_cond}
WHERE c.`deleted`=0 $P!{customerCondition}
AND t.`transaction_date` >= $P{dateBegin} AND
t.`transaction_date`<= $P{dateEnd}
GROUP BY c.`contact_code`
ORDER BY c.`contact_display_name`]]>
	</queryString>
	<field name="customer_code" class="java.lang.String"/>
	<field name="customer_name" class="java.lang.String"/>
	<field name="id_account" class="java.lang.Integer"/>
	<field name="account_currency" class="java.lang.String"/>
	<field name="code" class="java.lang.String"/>
	<field name="gjendja_debit" class="java.math.BigDecimal"/>
	<field name="gjendja_kredit" class="java.math.BigDecimal"/>
	<field name="debit" class="java.math.BigDecimal"/>
	<field name="kredit" class="java.math.BigDecimal"/>
	<variable name="gjendja_fillestare" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{gjendja_debit}.subtract($F{gjendja_kredit})]]></variableExpression>
	</variable>
	<variable name="gjendja_aktuale" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{gjendja_fillestare}.add($F{debit}).subtract($F{kredit})]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="111" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<textField evaluationTime="Report">
				<reportElement x="725" y="0" width="74" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="46" width="799" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="13" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{report_title}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="0" width="595" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{company_name}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="595" y="80" width="102" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Printoi:]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="20" width="100" height="20"/>
				<textElement/>
				<text><![CDATA[Periudha]]></text>
			</staticText>
			<staticText>
				<reportElement x="178" y="20" width="36" height="20"/>
				<box bottomPadding="7"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[-]]></text>
			</staticText>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy">
				<reportElement x="100" y="20" width="78" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateBegin})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="697" y="80" width="102" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{name_surname}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="659" y="0" width="38" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="595" y="0" width="64" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Faqe]]></text>
			</staticText>
			<staticText>
				<reportElement x="697" y="0" width="28" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[nga]]></text>
			</staticText>
			<textField evaluationTime="Auto" pattern="dd/MM/yyyy">
				<reportElement x="214" y="20" width="89" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateEnd})]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="20" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement mode="Opaque" x="0" y="0" width="100" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Kodi i Klientit]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="100" y="0" width="203" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Emri i Klientit]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="389" y="0" width="104" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Gjendja Fillestare]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="493" y="0" width="102" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Debit]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="303" y="0" width="86" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Nr i LLogarise]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="697" y="0" width="102" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Gjendja aktuale]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="595" y="0" width="102" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Kredit]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement mode="Opaque" x="0" y="0" width="100" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{customer_code}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement mode="Opaque" x="100" y="0" width="203" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{customer_name}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Opaque" x="493" y="0" width="102" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$F{debit}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Opaque" x="595" y="0" width="102" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$F{kredit}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement mode="Opaque" x="303" y="0" width="86" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{code}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Opaque" x="389" y="0" width="104" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$V{gjendja_fillestare}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Opaque" x="697" y="0" width="101" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$V{gjendja_aktuale}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="44">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
			<textField>
				<reportElement x="0" y="24" width="799" height="20"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bottom_message}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="18" width="799" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
