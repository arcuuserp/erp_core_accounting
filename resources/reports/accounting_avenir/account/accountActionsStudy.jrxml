<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="accountActionsStudy" language="groovy" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="dateBegin" class="java.lang.String"/>
	<parameter name="dateEnd" class="java.lang.String"/>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="fDateBegin" class="java.lang.String"/>
	<parameter name="fDateEnd" class="java.lang.String"/>
	<parameter name="sysdb_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="accounting_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="acc_shared_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="user_name" class="java.lang.String"/>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="date" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[$P{fDateBegin}+" - "+$P{fDateEnd}]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_trans_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals('0')?"t.transaction_status=0 AND ":""]]></defaultValueExpression>
	</parameter>
	<parameter name="advancedFilters" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT t.transaction_status, t.document_number, t.description, t.id_account, acc.code AS acc_code, 
acc.name AS acc_name, t.account_id_currency, c.name AS currency, c.description AS cur_desc, 
DATE_FORMAT(t.transaction_date, '%d/%m/%Y') AS transaction_date
		, CASE WHEN (t.transaction_type_name = 'debit') THEN t.total ELSE 0 END AS debit
		, CASE WHEN (t.transaction_type_name = 'debit') THEN t.home_cur_total ELSE 0 END AS debit_home
		, CASE WHEN (t.transaction_type_name = 'debit') THEN t.account_cur_total ELSE 0 END AS debit_account
		, CASE WHEN (t.transaction_type_name = 'credit') THEN t.total ELSE 0 END AS credit
		, CASE WHEN (t.transaction_type_name = 'credit') THEN t.home_cur_total ELSE 0 END AS credit_home
		, CASE WHEN (t.transaction_type_name = 'credit') THEN t.account_cur_total ELSE 0 END AS credit_account
	FROM $P!{accounting_db}.`transaction` t
		INNER JOIN $P!{db_sysdb}.currencies c ON t.account_id_currency = c.id
		INNER JOIN $P!{accounting_db}.accounts acc ON t.id_account = acc.id
WHERE $P!{advancedFilters} AND $P!{exclude_canceled_trans_cond} t.transaction_date >= $P{dateBegin} AND t.transaction_date <= $P{dateEnd}
		ORDER BY t.account_id_currency, t.id_account]]>
	</queryString>
	<field name="document_number" class="java.lang.String"/>
	<field name="description" class="java.lang.String"/>
	<field name="id_account" class="java.lang.Long"/>
	<field name="acc_code" class="java.lang.String"/>
	<field name="acc_name" class="java.lang.String"/>
	<field name="account_id_currency" class="java.lang.Integer"/>
	<field name="transaction_status" class="java.lang.Integer"/>
	<field name="currency" class="java.lang.String"/>
	<field name="cur_desc" class="java.lang.String"/>
	<field name="transaction_date" class="java.lang.String"/>
	<field name="debit" class="java.math.BigDecimal"/>
	<field name="debit_home" class="java.math.BigDecimal"/>
	<field name="debit_account" class="java.math.BigDecimal"/>
	<field name="credit" class="java.math.BigDecimal"/>
	<field name="credit_home" class="java.math.BigDecimal"/>
	<field name="credit_account" class="java.math.BigDecimal"/>
	<variable name="HomeCurDifference" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{debit_account}.subtract( $F{credit_account} )]]></variableExpression>
	</variable>
	<variable name="accountDebitTotal" class="java.math.BigDecimal" resetType="Group" resetGroup="accountGroup" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{debit_account} : 0]]></variableExpression>
	</variable>
	<variable name="accountCreditTotal" class="java.math.BigDecimal" resetType="Group" resetGroup="accountGroup" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{credit_account} : 0]]></variableExpression>
	</variable>
	<variable name="dateTime" class="java.util.Date">
		<variableExpression><![CDATA[new Date()]]></variableExpression>
	</variable>
	<group name="currencyGroup">
		<groupExpression><![CDATA[$F{account_id_currency}]]></groupExpression>
		<groupHeader>
			<band height="26">
				<textField>
					<reportElement x="0" y="6" width="113" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="113" y="6" width="244" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{cur_desc}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<group name="accountGroup">
		<groupExpression><![CDATA[$F{id_account}]]></groupExpression>
		<groupHeader>
			<band height="27">
				<textField>
					<reportElement x="0" y="7" width="113" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{acc_code}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="113" y="7" width="244" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{acc_name}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<textField pattern="#,##0.00">
					<reportElement x="258" y="0" width="99" height="20"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{accountDebitTotal}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="357" y="0" width="99" height="20"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{accountCreditTotal}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="113" y="0" width="145" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{acc_code}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="84" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="456" y="60" width="42" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy HH.mm.ss">
				<reportElement stretchType="RelativeToTallestObject" x="18" y="60" width="95" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{dateTime}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="20" width="555" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<text><![CDATA[Studim veprime llogari]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" x="357" y="60" width="99" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box rightPadding="4"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Faqe]]></text>
			</staticText>
			<textField evaluationTime="Report">
				<reportElement stretchType="RelativeToTallestObject" x="520" y="60" width="35" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="0" width="555" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{company_name}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" x="113" y="60" width="78" height="20"/>
				<box leftPadding="0" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Periudha: ]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" x="0" y="60" width="18" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[dt.]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" x="498" y="60" width="22" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[nga]]></text>
			</staticText>
			<textField>
				<reportElement x="456" y="40" width="99" height="20"/>
				<box rightPadding="5"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{user_name}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="113" y="40" width="343" height="20"/>
				<box leftPadding="0" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[(Periudha në Raport me Dokument të pakaluar në LM)]]></text>
			</staticText>
			<textField isStretchWithOverflow="true">
				<reportElement stretchType="RelativeToTallestObject" x="191" y="60" width="166" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{date}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="22" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="456" y="1" width="99" height="20"/>
				<box rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Monedhë vendi]]></text>
			</staticText>
			<staticText>
				<reportElement x="113" y="1" width="145" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Përshkrimi]]></text>
			</staticText>
			<staticText>
				<reportElement x="258" y="1" width="99" height="20"/>
				<box rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Debi]]></text>
			</staticText>
			<staticText>
				<reportElement x="357" y="1" width="99" height="20"/>
				<box rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Kredi]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="1" width="113" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Kodi]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<rectangle>
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="555" height="19" forecolor="#FFD7DA" backcolor="#FFD7DA">
					<printWhenExpression><![CDATA[$F{transaction_status}.compareTo( 0 ) > 0]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="54" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{document_number}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="54" y="0" width="59" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{transaction_date}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement stretchType="RelativeToTallestObject" x="113" y="0" width="145" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{description}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement stretchType="RelativeToTallestObject" x="258" y="0" width="99" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{debit_account}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement stretchType="RelativeToTallestObject" x="357" y="0" width="99" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{credit_account}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement stretchType="RelativeToTallestObject" x="456" y="0" width="99" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{HomeCurDifference}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="54" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
		</band>
	</pageFooter>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="18" width="555" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
