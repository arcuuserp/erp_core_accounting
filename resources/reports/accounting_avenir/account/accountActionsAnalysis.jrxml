<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="accountActionsAnalysis" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="NoDataSection" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="user_name" class="java.lang.String"/>
	<parameter name="acc_shared_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="accounting_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="sysdb_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="fDateEnd" class="java.lang.String"/>
	<parameter name="fDateBegin" class="java.lang.String"/>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="dateEnd" class="java.lang.String"/>
	<parameter name="dateBegin" class="java.lang.String"/>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="date" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[$P{fDateBegin}+" - "+$P{fDateEnd}]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_trans_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals('0')?" AND transaction_status=0 AND ":""]]></defaultValueExpression>
	</parameter>
	<parameter name="advancedFilters" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[CALL $P!{accounting_db}.report_accountActionsAnalysis($P{acc_shared_db}, $P{dateBegin}, $P{dateEnd}, $P{exclude_canceled_trans_cond}, $P{advancedFilters});]]>
	</queryString>
	<field name="description" class="java.lang.String"/>
	<field name="document_number" class="java.lang.String"/>
	<field name="id_account" class="java.lang.Long"/>
	<field name="acc_code" class="java.lang.String"/>
	<field name="acc_name" class="java.lang.String"/>
	<field name="account_id_currency" class="java.lang.Integer"/>
	<field name="transaction_status" class="java.lang.Integer"/>
	<field name="currency" class="java.lang.String"/>
	<field name="cur_desc" class="java.lang.String"/>
	<field name="doc_date" class="java.util.Date"/>
	<field name="debit" class="java.math.BigDecimal"/>
	<field name="credit" class="java.math.BigDecimal"/>
	<field name="gjendja" class="java.math.BigDecimal"/>
	<variable name="gjendjaProgresive" class="java.math.BigDecimal" resetType="Group" resetGroup="accountGroup">
		<variableExpression><![CDATA[$V{gjendjaProgresive}.add($F{debit}).subtract( $F{credit} )]]></variableExpression>
		<initialValueExpression><![CDATA[$F{gjendja}]]></initialValueExpression>
	</variable>
	<variable name="debitTotal" class="java.math.BigDecimal" resetType="Group" resetGroup="accountGroup" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{debit} : 0]]></variableExpression>
	</variable>
	<variable name="creditTotal" class="java.math.BigDecimal" resetType="Group" resetGroup="accountGroup" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0) ? $F{credit} : 0]]></variableExpression>
	</variable>
	<variable name="gjendjeTotal" class="java.math.BigDecimal" resetType="Group" resetGroup="accountGroup">
		<variableExpression><![CDATA[$F{gjendja}.add( $V{debitTotal} ).subtract($V{creditTotal})]]></variableExpression>
	</variable>
	<variable name="dateTime" class="java.util.Date">
		<variableExpression><![CDATA[new Date()]]></variableExpression>
	</variable>
	<group name="currencyGroup">
		<groupExpression><![CDATA[$F{account_id_currency}]]></groupExpression>
		<groupHeader>
			<band height="32">
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="11" width="100" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="2.5" lineStyle="Double"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="100" y="11" width="219" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="2.5" lineStyle="Double"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{cur_desc}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<group name="accountGroup">
		<groupExpression><![CDATA[$F{id_account}]]></groupExpression>
		<groupHeader>
			<band height="52">
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="10" width="100" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{acc_code}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="100" y="10" width="219" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{acc_name}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="100" y="32" width="219" height="20"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Middle"/>
					<text><![CDATA[Fillim periudhe]]></text>
				</staticText>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="702" y="32" width="100" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{gjendja}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="21">
				<textField isBlankWhenNull="true">
					<reportElement x="319" y="0" width="183" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{acc_code}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="502" y="0" width="100" height="20"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{debitTotal}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="602" y="0" width="100" height="20"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{creditTotal}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="702" y="0" width="100" height="20"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{gjendjeTotal}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="83" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="0" y="60" width="18" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[dt.]]></text>
			</staticText>
			<staticText>
				<reportElement x="602" y="60" width="100" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box rightPadding="4"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Faqe]]></text>
			</staticText>
			<textField>
				<reportElement x="0" y="0" width="802" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{company_name}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="767" y="60" width="35" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="20" width="802" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<text><![CDATA[Studim veprime llogari]]></text>
			</staticText>
			<textField>
				<reportElement x="702" y="60" width="43" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="188" y="40" width="414" height="20"/>
				<box leftPadding="0" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[(Periudha në Raport me Dokument të pakaluar në LM)]]></text>
			</staticText>
			<staticText>
				<reportElement x="745" y="60" width="22" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[nga]]></text>
			</staticText>
			<staticText>
				<reportElement x="100" y="60" width="255" height="20"/>
				<box leftPadding="0" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Periudha: ]]></text>
			</staticText>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy HH.mm.ss">
				<reportElement x="18" y="60" width="82" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{dateTime}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="602" y="40" width="200" height="20"/>
				<box rightPadding="5"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{user_name}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="355" y="60" width="247" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{date}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="27" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="100" y="1" width="219" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Pershkrim]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="1" width="100" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Dokumenti]]></text>
			</staticText>
			<staticText>
				<reportElement x="319" y="1" width="83" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Koment]]></text>
			</staticText>
			<staticText>
				<reportElement x="602" y="1" width="100" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Kredi]]></text>
			</staticText>
			<staticText>
				<reportElement x="502" y="1" width="100" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Debi]]></text>
			</staticText>
			<staticText>
				<reportElement x="702" y="1" width="100" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Gjendje]]></text>
			</staticText>
			<staticText>
				<reportElement x="402" y="1" width="100" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Dokument referues]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<rectangle>
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="801" height="19" forecolor="#FFD7DA" backcolor="#FFD7DA">
					<printWhenExpression><![CDATA[$F{transaction_status}.compareTo( 0 ) > 0]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="100" y="0" width="219" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{description}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="100" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{document_number}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="402" y="0" width="100" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{doc_date}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="602" y="0" width="100" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{credit}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="502" y="0" width="100" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{debit}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="702" y="0" width="100" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{gjendjaProgresive}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="16" width="802" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
