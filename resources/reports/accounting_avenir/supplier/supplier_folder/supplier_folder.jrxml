<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="supplier_folder" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="NoDataSection" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="accounting_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="dateBegin" class="java.lang.String">
		<defaultValueExpression><![CDATA["2010-01-01"]]></defaultValueExpression>
	</parameter>
	<parameter name="dateEnd" class="java.lang.String">
		<defaultValueExpression><![CDATA["2020-12-31"]]></defaultValueExpression>
	</parameter>
	<parameter name="report_title" class="java.lang.String"/>
	<parameter name="name_surname" class="java.lang.String"/>
	<parameter name="bottom_message" class="java.lang.String"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["C://reports/MyReports/accounting/supplier/supplier_folder/"]]></defaultValueExpression>
	</parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_trans_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals('0')?" AND transaction_status=0":""]]></defaultValueExpression>
	</parameter>
	<parameter name="supplierCondition" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT t.transaction_status, s.`contact_display_name` AS EMRI, s.`supplier_code`, s.`id_account`, s.`company_nipt`,  t.`account_currency`,  t.`transaction_id`,
 DATE_FORMAT(t.`transaction_system_date`,'%d-%m-%Y') AS transaction_system_date
, DATE_FORMAT(t.`transaction_date`,'%d-%m-%Y') AS transaction_date, dt.symbol as `documenttype_name`, t.`document_number`
, t.`description` AS 'Pershkrimi',t.id_user, 1 AS 'Order', t.currency,t.`exchange_rate`,
CASE WHEN (t.`transaction_type_id`=0) THEN t.`home_cur_total` ELSE 0 END  AS 'debi',
CASE WHEN (t.`transaction_type_id`=1) THEN t.`home_cur_total` ELSE 0 END  AS 'kredi',
CASE WHEN (t.`transaction_type_id`=0) THEN t.`account_cur_total` ELSE 0 END AS 'debi_llog',
CASE WHEN (t.`transaction_type_id`=1) THEN t.`account_cur_total` ELSE 0 END AS 'kredi_llog',
NOW() AS 'date'
FROM $P!{accounting_db}.supplier s
INNER JOIN $P!{accounting_db}.`transaction` t
ON s.`supplier_id`=t.`subject_id` $P!{exclude_canceled_trans_cond}
inner join $P!{accounting_db}.documenttype dt on
t.documenttype_name = dt.system_name
WHERE t.`transaction_date` >= $P{dateBegin} AND  t.`transaction_date`<= $P{dateEnd} AND t.`subject`='supplier'
AND s.`deleted`=0 $P!{supplierCondition}]]>
	</queryString>
	<field name="EMRI" class="java.lang.String"/>
	<field name="supplier_code" class="java.lang.String"/>
	<field name="transaction_status" class="java.lang.Integer"/>
	<field name="id_account" class="java.lang.String"/>
	<field name="company_nipt" class="java.lang.String"/>
	<field name="account_currency" class="java.lang.String"/>
	<field name="transaction_id" class="java.lang.Long"/>
	<field name="transaction_system_date" class="java.lang.String"/>
	<field name="transaction_date" class="java.lang.String"/>
	<field name="documenttype_name" class="java.lang.String"/>
	<field name="document_number" class="java.lang.String"/>
	<field name="Pershkrimi" class="java.lang.String"/>
	<field name="id_user" class="java.lang.Long"/>
	<field name="Order" class="java.lang.Long"/>
	<field name="currency" class="java.lang.String"/>
	<field name="exchange_rate" class="java.math.BigDecimal"/>
	<field name="debi" class="java.math.BigDecimal"/>
	<field name="kredi" class="java.math.BigDecimal"/>
	<field name="debi_llog" class="java.math.BigDecimal"/>
	<field name="kredi_llog" class="java.math.BigDecimal"/>
	<field name="date" class="java.sql.Timestamp"/>
	<variable name="progresivi" class="java.math.BigDecimal" resetType="Group" resetGroup="order">
		<variableExpression><![CDATA[$V{progresivi}.add($F{debi}).subtract($F{kredi})]]></variableExpression>
		<initialValueExpression><![CDATA[0]]></initialValueExpression>
	</variable>
	<variable name="progresivi_llog" class="java.math.BigDecimal" resetType="Group" resetGroup="order">
		<variableExpression><![CDATA[($V{progresivi_llog}.add($F{debi_llog})).subtract($F{kredi_llog})]]></variableExpression>
		<initialValueExpression><![CDATA[0]]></initialValueExpression>
	</variable>
	<variable name="debi_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Furnitori" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0)?$F{debi}:0]]></variableExpression>
	</variable>
	<variable name="kredi_1" class="java.math.BigDecimal" resetType="Group" resetGroup="Furnitori" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0)?$F{kredi}:0]]></variableExpression>
	</variable>
	<variable name="debi_llog_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0)?$F{debi_llog}:0]]></variableExpression>
	</variable>
	<variable name="kredi_llog_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0)?$F{kredi_llog}:0]]></variableExpression>
	</variable>
	<variable name="diff_debi" class="java.math.BigDecimal" resetType="Group" resetGroup="Furnitori">
		<variableExpression><![CDATA[$V{debi_1}.subtract( $V{kredi_1} )]]></variableExpression>
		<initialValueExpression><![CDATA[]]></initialValueExpression>
	</variable>
	<variable name="diff_kredi" class="java.math.BigDecimal" resetType="None">
		<variableExpression><![CDATA[$V{totalCreditHomeCur}-$V{totalDebitHomeCur}]]></variableExpression>
		<initialValueExpression><![CDATA[]]></initialValueExpression>
	</variable>
	<variable name="diff_debi_llog" class="java.lang.String">
		<variableExpression><![CDATA[$V{debi_llog_1}.subtract($V{kredi_llog_1})]]></variableExpression>
	</variable>
	<variable name="diff_kredi_llog" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{kredi_llog_1}.subtract($V{debi_llog_1})]]></variableExpression>
	</variable>
	<variable name="debitHomeCurSubreport" class="java.math.BigDecimal" calculation="System"/>
	<variable name="creditHomeCurSubreport" class="java.math.BigDecimal" calculation="System"/>
	<variable name="debitAccCurSubreport" class="java.math.BigDecimal" calculation="System"/>
	<variable name="creditAccCurSubreport" class="java.math.BigDecimal" calculation="System"/>
	<variable name="totalDebitHomeCur" class="java.math.BigDecimal" resetType="None">
		<variableExpression><![CDATA[$V{debi_1}.add( $V{debitHomeCurSubreport})]]></variableExpression>
		<initialValueExpression><![CDATA[]]></initialValueExpression>
	</variable>
	<variable name="totalCreditHomeCur" class="java.math.BigDecimal" resetType="None">
		<variableExpression><![CDATA[$V{creditHomeCurSubreport}.add($V{kredi_1})]]></variableExpression>
		<initialValueExpression><![CDATA[]]></initialValueExpression>
	</variable>
	<variable name="totalDebitAccCur" class="java.math.BigDecimal" resetType="None">
		<variableExpression><![CDATA[$V{debi_llog_1}.add($V{debitAccCurSubreport})]]></variableExpression>
	</variable>
	<variable name="totalCreditAccCur" class="java.math.BigDecimal" resetType="None">
		<variableExpression><![CDATA[$V{kredi_llog_1}.add($V{creditAccCurSubreport})]]></variableExpression>
	</variable>
	<variable name="progressivReal" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{progresivi}.add( $V{debitHomeCurSubreport} ).subtract( $V{creditHomeCurSubreport} )]]></variableExpression>
	</variable>
	<variable name="progressivReal_llog" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{progresivi_llog}.add($V{debitAccCurSubreport}).subtract( $V{creditAccCurSubreport} )]]></variableExpression>
	</variable>
	<variable name="debi_2" class="java.math.BigDecimal" resetType="Group" resetGroup="Furnitori" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0)?$F{debi}:0]]></variableExpression>
	</variable>
	<variable name="kredi_2" class="java.math.BigDecimal" resetType="Group" resetGroup="Furnitori" calculation="Sum">
		<variableExpression><![CDATA[($F{transaction_status}.compareTo( 0 ) == 0)?$F{kredi}:0]]></variableExpression>
	</variable>
	<variable name="TotalDebitHome" class="java.math.BigDecimal" resetType="None">
		<variableExpression><![CDATA[$V{totalDebitHomeCur}.subtract($V{totalCreditHomeCur} )]]></variableExpression>
	</variable>
	<variable name="TotalCreditHome" class="java.math.BigDecimal" resetType="None">
		<variableExpression><![CDATA[$V{totalCreditHomeCur}.subtract( $V{totalDebitHomeCur} )]]></variableExpression>
	</variable>
	<variable name="TotalDebitAcc" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{totalDebitAccCur}.subtract( $V{totalCreditAccCur} )]]></variableExpression>
	</variable>
	<variable name="TotalCreditAcc" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{totalCreditAccCur}.subtract( $V{totalDebitAccCur} )]]></variableExpression>
	</variable>
	<group name="Furnitori" isStartNewPage="true">
		<groupExpression><![CDATA[$F{supplier_code}]]></groupExpression>
		<groupHeader>
			<band height="87">
				<rectangle>
					<reportElement mode="Transparent" x="0" y="27" width="802" height="40" backcolor="#FCFCC8"/>
				</rectangle>
				<staticText>
					<reportElement mode="Opaque" x="1" y="67" width="801" height="20" backcolor="#FCFCC8"/>
					<box leftPadding="2">
						<topPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.25"/>
					</box>
					<textElement>
						<font size="8"/>
					</textElement>
					<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="217" y="26" width="100" height="40" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Pershkrimi i Veprimit]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="167" y="26" width="50" height="40" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Data Dok]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="117" y="26" width="50" height="40" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Nr. Dok]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="80" y="26" width="37" height="40" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[LLoji i Dokumentit]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="30" y="26" width="50" height="40" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Data e Regjistrimit]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="0" y="26" width="30" height="40" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Nr Rendor]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="317" y="46" width="77" height="20" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Debi]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="394" y="46" width="77" height="20" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Kredi]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="471" y="46" width="77" height="20" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Progresivi]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="317" y="26" width="231" height="20" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Monedhe Baze]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="0" width="30" height="20"/>
					<box>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<text><![CDATA[Kodi:]]></text>
				</staticText>
				<staticText>
					<reportElement x="317" y="0" width="77" height="20"/>
					<box>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<text><![CDATA[NIPT:]]></text>
				</staticText>
				<staticText>
					<reportElement x="471" y="0" width="154" height="20"/>
					<box>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<text><![CDATA[Monedha e Llogarise:]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement x="394" y="0" width="77" height="20"/>
					<box>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{company_nipt}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="625" y="0" width="77" height="20"/>
					<box>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{account_currency}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="30" y="0" width="87" height="20"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{supplier_code}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="117" y="0" width="100" height="20"/>
					<box>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<text><![CDATA[Emri i Furnitorit:  ]]></text>
				</staticText>
				<subreport>
					<reportElement x="0" y="66" width="803" height="20"/>
					<subreportParameter name="accounting_db">
						<subreportParameterExpression><![CDATA[$P{accounting_db}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter name="supplier_code">
						<subreportParameterExpression><![CDATA[$F{supplier_code}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter name="date_begin">
						<subreportParameterExpression><![CDATA[$P{dateBegin}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter name="show_canceled">
						<subreportParameterExpression><![CDATA[$P{show_canceled}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<returnValue subreportVariable="debitHomeCur" toVariable="debitHomeCurSubreport"/>
					<returnValue subreportVariable="creditHomeCur" toVariable="creditHomeCurSubreport"/>
					<returnValue subreportVariable="debitAccCur" toVariable="debitAccCurSubreport"/>
					<returnValue subreportVariable="creditAccCur" toVariable="creditAccCurSubreport"/>
					<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "supplier_folder_subrep_status.jasper"]]></subreportExpression>
				</subreport>
				<textField isBlankWhenNull="true">
					<reportElement x="217" y="0" width="100" height="20"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{EMRI}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement mode="Transparent" x="702" y="46" width="78" height="20" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Progresivi]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="625" y="46" width="77" height="20" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Kredi]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="548" y="26" width="232" height="20" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Monedhe Llogarie]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="548" y="46" width="77" height="20" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Debi]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Transparent" x="780" y="26" width="23" height="40" backcolor="#FCFCC8"/>
					<box leftPadding="2" bottomPadding="3">
						<topPen lineWidth="0.25"/>
						<leftPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
						<rightPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="6" isBold="true"/>
					</textElement>
					<text><![CDATA[Monedha e transaksionit]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="40">
				<rectangle>
					<reportElement mode="Transparent" x="0" y="0" width="802" height="40" backcolor="#FCFCC8"/>
				</rectangle>
				<staticText>
					<reportElement x="471" y="0" width="77" height="20"/>
					<box leftPadding="2">
						<topPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
					</box>
					<textElement>
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement x="702" y="0" width="100" height="20"/>
					<box leftPadding="2">
						<topPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement x="217" y="0" width="100" height="20"/>
					<box leftPadding="2">
						<topPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.25"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Totali]]></text>
				</staticText>
				<staticText>
					<reportElement x="217" y="20" width="100" height="20"/>
					<box leftPadding="2">
						<topPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Debitor/Kreditor]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="0" width="217" height="20"/>
					<box leftPadding="2">
						<topPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.25"/>
					</box>
					<textElement>
						<font size="8"/>
					</textElement>
					<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="20" width="217" height="20"/>
					<box leftPadding="2">
						<topPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
					</box>
					<textElement>
						<font size="8"/>
					</textElement>
					<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement x="471" y="20" width="77" height="20"/>
					<box leftPadding="2">
						<topPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
					</box>
					<textElement>
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[]]></text>
				</staticText>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="548" y="0" width="77" height="20"/>
					<box rightPadding="2">
						<topPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{totalDebitAccCur}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="625" y="0" width="77" height="20"/>
					<box rightPadding="2">
						<topPen lineWidth="0.25"/>
						<bottomPen lineWidth="0.25"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{totalCreditAccCur}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="317" y="0" width="77" height="20"/>
					<box rightPadding="2">
						<bottomPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{totalDebitHomeCur}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="394" y="0" width="77" height="20"/>
					<box rightPadding="2">
						<bottomPen lineWidth="0.5"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{totalCreditHomeCur}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="317" y="20" width="77" height="20">
						<printWhenExpression><![CDATA[$V{totalDebitHomeCur}.compareTo( $V{totalCreditHomeCur} ) > 0]]></printWhenExpression>
					</reportElement>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TotalDebitHome}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="394" y="20" width="77" height="20">
						<printWhenExpression><![CDATA[$V{totalCreditHomeCur}.compareTo( $V{totalDebitHomeCur} ) > 0]]></printWhenExpression>
					</reportElement>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TotalCreditHome}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="548" y="20" width="77" height="20">
						<printWhenExpression><![CDATA[$V{totalDebitAccCur}.compareTo( $V{totalCreditAccCur} ) > 0]]></printWhenExpression>
					</reportElement>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TotalDebitAcc}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="625" y="20" width="77" height="20">
						<printWhenExpression><![CDATA[$V{totalCreditAccCur}.compareTo( $V{totalDebitAccCur} ) > 0]]></printWhenExpression>
					</reportElement>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TotalCreditAcc}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="702" y="20" width="102" height="20"/>
					<box leftPadding="2">
						<topPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<group name="order">
		<groupExpression><![CDATA[$F{Order}]]></groupExpression>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="104" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="625" y="79" width="77" height="20"/>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Printoi:]]></text>
			</staticText>
			<textField>
				<reportElement x="702" y="79" width="100" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{name_surname}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="780" y="0" width="23" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="730" y="0" width="50" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[nga]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="46" width="803" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="13" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{report_title}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="625" y="0" width="77" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Faqe]]></text>
			</staticText>
			<textField>
				<reportElement x="0" y="0" width="625" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{company_name}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Page">
				<reportElement x="702" y="0" width="28" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="dd/MM/yyyy">
				<reportElement x="217" y="20" width="100" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateEnd})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="167" y="20" width="50" height="20"/>
				<box bottomPadding="7"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[-]]></text>
			</staticText>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy">
				<reportElement x="117" y="20" width="50" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateBegin})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="20" width="117" height="20"/>
				<box leftPadding="2"/>
				<textElement/>
				<text><![CDATA[Periudha]]></text>
			</staticText>
		</band>
	</pageHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<rectangle>
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="801" height="19" forecolor="#FFD7DA" backcolor="#FFD7DA">
					<printWhenExpression><![CDATA[$F{transaction_status}.compareTo( 0 ) > 0]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<rectangle>
				<reportElement mode="Transparent" x="0" y="0" width="802" height="20" backcolor="#FCFCC8"/>
			</rectangle>
			<textField>
				<reportElement x="30" y="0" width="50" height="20">
					<printWhenExpression><![CDATA[$F{Order}==1]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2">
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{transaction_system_date}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="80" y="0" width="37" height="20">
					<printWhenExpression><![CDATA[$F{Order}==1]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2">
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{documenttype_name}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="117" y="0" width="50" height="20">
					<printWhenExpression><![CDATA[$F{Order}==1]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2">
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{document_number}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="167" y="0" width="50" height="20">
					<printWhenExpression><![CDATA[$F{Order}==1]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2">
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{transaction_date}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="217" y="0" width="100" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Pershkrimi}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="317" y="0" width="77" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{debi}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="394" y="0" width="77" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{kredi}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="548" y="0" width="77" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{debi_llog}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="625" y="0" width="77" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{kredi_llog}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="471" y="0" width="77" height="20"/>
				<box rightPadding="2">
					<topPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{progressivReal}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="702" y="0" width="78" height="20"/>
				<box rightPadding="2">
					<topPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{progressivReal_llog}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="780" y="0" width="23" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="0.25"/>
					<leftPen lineWidth="0.25"/>
					<bottomPen lineWidth="0.25"/>
					<rightPen lineWidth="0.25"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="36" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
			<textField>
				<reportElement x="0" y="16" width="802" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bottom_message}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="1" y="0" width="801" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
