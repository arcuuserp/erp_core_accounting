<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="purchasesJournalBillingReport" language="groovy" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="accounting_db" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="dateBegin" class="java.lang.String"/>
	<parameter name="dateEnd" class="java.lang.String"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["C://reports/MyReports/accounting/"]]></defaultValueExpression>
	</parameter>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="report_title" class="java.lang.String"/>
	<parameter name="name_surname" class="java.lang.String"/>
	<parameter name="bottom_message" class="java.lang.String"/>
	<parameter name="month_human" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateBegin}).toString()+" - "+new SimpleDateFormat("yyyy-MM-dd").parse($P{dateEnd}).toString()]]></defaultValueExpression>
	</parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_docs_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals('0')?"header.id_docstatus<>5 AND ":""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT header.purchaseinvoiceheader_id, header.`document_number`, header.`serial_number`, header.`document_date`, header.`description`, header.`supplier_code`, header.`currency`, s.`company_nipt`,pb.`item_code`,pb.`item_description`,pb.`quantity`,pb.unit_name, pb.`price`,pb.`home_cur_subtotal_no_vat`,pb.`home_cur_subtotal_vat`,  pb.`home_cur_subtotal_with_vat`,pb.`subtotal_no_vat`,pb.`subtotal_vat`,pb.`subtotal_with_vat`, CONCAT(w.`warehouse_code`, "-", w.`warehouse_name`) AS warehouse
FROM $P!{accounting_db}.purchaseinvoiceheader header
INNER JOIN $P!{accounting_db}.purchaseinvoicebody pb
ON header.`purchaseinvoiceheader_id` = pb.`id_purchaseinvoiceheader`
INNER JOIN $P!{accounting_db}.supplier s
On header.`id_contact` = s.supplier_id
LEFT JOIN $P!{accounting_db}.warehouse w
ON w.`warehouse_id` = header.`idwarehouse_entry` AND w.deleted = 0
WHERE $P!{exclude_canceled_docs_cond} header.`document_date` >= $P{dateBegin}  AND header.`document_date` <= $P{dateEnd}
ORDER BY header.`document_date`, header.`document_number`]]>
	</queryString>
	<field name="purchaseinvoiceheader_id" class="java.lang.Integer"/>
	<field name="document_number" class="java.lang.String"/>
	<field name="serial_number" class="java.lang.String"/>
	<field name="document_date" class="java.sql.Timestamp"/>
	<field name="description" class="java.lang.String"/>
	<field name="supplier_code" class="java.lang.String"/>
	<field name="currency" class="java.lang.String"/>
	<field name="company_nipt" class="java.lang.String"/>
	<field name="item_code" class="java.lang.String"/>
	<field name="item_description" class="java.lang.String"/>
	<field name="quantity" class="java.math.BigDecimal"/>
	<field name="unit_name" class="java.lang.String"/>
	<field name="price" class="java.math.BigDecimal"/>
	<field name="home_cur_subtotal_no_vat" class="java.math.BigDecimal"/>
	<field name="home_cur_subtotal_vat" class="java.math.BigDecimal"/>
	<field name="home_cur_subtotal_with_vat" class="java.math.BigDecimal"/>
	<field name="subtotal_no_vat" class="java.math.BigDecimal"/>
	<field name="subtotal_vat" class="java.math.BigDecimal"/>
	<field name="subtotal_with_vat" class="java.math.BigDecimal"/>
	<field name="warehouse" class="java.lang.String"/>
	<variable name="currency_1" class="java.lang.Integer" resetType="Group" resetGroup="purchaseinvoiceheader" calculation="Count">
		<variableExpression><![CDATA[$F{currency}]]></variableExpression>
	</variable>
	<variable name="total_rows" class="java.lang.Integer" resetType="Group" resetGroup="purchaseinvoiceheader">
		<variableExpression><![CDATA[$V{purchaseinvoiceheader_COUNT}]]></variableExpression>
	</variable>
	<variable name="subtotal_no_vat_1" class="java.math.BigDecimal" resetType="Group" resetGroup="purchaseinvoiceheader" calculation="Sum">
		<variableExpression><![CDATA[$F{subtotal_no_vat}]]></variableExpression>
	</variable>
	<variable name="subtotal_vat_1" class="java.math.BigDecimal" resetType="Group" resetGroup="purchaseinvoiceheader" calculation="Sum">
		<variableExpression><![CDATA[$F{subtotal_vat}]]></variableExpression>
	</variable>
	<variable name="subtotal_with_vat_1" class="java.math.BigDecimal" resetType="Group" resetGroup="purchaseinvoiceheader" calculation="Sum">
		<variableExpression><![CDATA[$F{subtotal_with_vat}]]></variableExpression>
	</variable>
	<group name="purchaseinvoiceheader">
		<groupExpression><![CDATA[$F{purchaseinvoiceheader_id}]]></groupExpression>
		<groupHeader>
			<band height="12">
				<rectangle>
					<reportElement x="326" y="0" width="228" height="12"/>
					<graphicElement>
						<pen lineStyle="Dashed"/>
					</graphicElement>
				</rectangle>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="40" height="12"/>
					<box leftPadding="2"/>
					<textElement>
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{document_number}]]></textFieldExpression>
				</textField>
				<textField pattern="dd.MM.yyyy" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="40" y="0" width="42" height="12"/>
					<textElement>
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{document_date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="82" y="0" width="88" height="12"/>
					<box leftPadding="2"/>
					<textElement>
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{description}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="170" y="0" width="80" height="12"/>
					<box leftPadding="2"/>
					<textElement>
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{supplier_code}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="274" y="0" width="52" height="12"/>
					<box leftPadding="2"/>
					<textElement>
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{company_nipt}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="326" y="0" width="76" height="12"/>
					<box leftPadding="2">
						<topPen lineWidth="0.0" lineStyle="Dashed"/>
						<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{serial_number}]]></textFieldExpression>
				</textField>
				<textField evaluationTime="Group" evaluationGroup="purchaseinvoiceheader" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="250" y="0" width="24" height="12"/>
					<box leftPadding="2"/>
					<textElement>
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{currency_1}]]></textFieldExpression>
				</textField>
				<textField pattern="dd.MM.yyyy" isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="402" y="0" width="44" height="12"/>
					<box leftPadding="2">
						<topPen lineWidth="0.0" lineStyle="Dashed"/>
						<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
					</box>
					<textElement>
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{document_date}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement stretchType="RelativeToTallestObject" x="446" y="0" width="108" height="12"/>
					<box leftPadding="2">
						<topPen lineWidth="0.0" lineStyle="Dashed"/>
						<bottomPen lineWidth="0.0" lineStyle="Dashed"/>
					</box>
					<textElement>
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{warehouse}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="16">
				<staticText>
					<reportElement x="0" y="0" width="40" height="16"/>
					<box>
						<topPen lineWidth="1.0" lineStyle="Dashed"/>
					</box>
					<textElement verticalAlignment="Top">
						<font size="8"/>
					</textElement>
					<text><![CDATA[Gjithsej]]></text>
				</staticText>
				<textField evaluationTime="Group" evaluationGroup="purchaseinvoiceheader" isBlankWhenNull="true">
					<reportElement x="40" y="0" width="42" height="16"/>
					<box>
						<topPen lineWidth="1.0" lineStyle="Dashed"/>
					</box>
					<textElement verticalAlignment="Top">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{total_rows}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="326" y="0" width="76" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{subtotal_no_vat_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="402" y="0" width="76" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{subtotal_vat_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="478" y="0" width="76" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{subtotal_with_vat_1}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="106" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<textField>
				<reportElement x="0" y="80" width="554" height="20"/>
				<box rightPadding="20"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{name_surname}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" isBlankWhenNull="true">
				<reportElement x="514" y="0" width="40" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="0" width="402" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{company_name}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="402" y="0" width="44" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Faqe       ]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="60" width="170" height="20"/>
				<box leftPadding="0" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Periudha:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="446" y="0" width="32" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="40" width="554" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{report_title}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="478" y="0" width="36" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[nga]]></text>
			</staticText>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy">
				<reportElement x="170" y="60" width="76" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateBegin})]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="dd/MM/yyyy">
				<reportElement x="274" y="60" width="128" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateEnd})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="246" y="60" width="28" height="20"/>
				<box bottomPadding="7"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[-]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="42" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="0" y="20" width="40" height="20"/>
				<box leftPadding="2">
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Artikull]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="0" width="40" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Nr.]]></text>
			</staticText>
			<staticText>
				<reportElement x="40" y="0" width="42" height="20"/>
				<box>
					<topPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Data]]></text>
			</staticText>
			<staticText>
				<reportElement x="40" y="20" width="130" height="20"/>
				<box leftPadding="2">
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Pershkrim]]></text>
			</staticText>
			<staticText>
				<reportElement x="170" y="0" width="80" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Furnitori]]></text>
			</staticText>
			<staticText>
				<reportElement x="170" y="20" width="104" height="20"/>
				<box>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Sasi/Njesi]]></text>
			</staticText>
			<staticText>
				<reportElement x="250" y="0" width="24" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Mon]]></text>
			</staticText>
			<staticText>
				<reportElement x="274" y="20" width="52" height="20"/>
				<box>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Cmimi]]></text>
			</staticText>
			<staticText>
				<reportElement x="274" y="0" width="52" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Nipt]]></text>
			</staticText>
			<staticText>
				<reportElement x="326" y="20" width="76" height="20"/>
				<box>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Vl. pa t.v.sh.]]></text>
			</staticText>
			<staticText>
				<reportElement x="402" y="20" width="76" height="20"/>
				<box>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[t.v.sh.]]></text>
			</staticText>
			<staticText>
				<reportElement x="478" y="20" width="76" height="20"/>
				<box>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Vl. me t.v.sh.]]></text>
			</staticText>
			<staticText>
				<reportElement x="326" y="0" width="76" height="20"/>
				<box>
					<topPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[nr, dt. fature]]></text>
			</staticText>
			<staticText>
				<reportElement x="402" y="0" width="152" height="20"/>
				<box>
					<topPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Magazina]]></text>
			</staticText>
			<staticText>
				<reportElement x="82" y="0" width="88" height="20"/>
				<box>
					<topPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="16" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="82" height="16"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{item_code}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="82" y="0" width="88" height="16"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{item_description}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="170" y="0" width="80" height="16"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{quantity}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="250" y="0" width="24" height="16"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{unit_name}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="274" y="0" width="52" height="16"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{price}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="326" y="0" width="76" height="16"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_no_vat}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="402" y="0" width="76" height="16"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_vat}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="478" y="0" width="76" height="16"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subtotal_with_vat}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="60" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
			<subreport>
				<reportElement x="-20" y="0" width="596" height="30"/>
				<subreportParameter name="bottom_message">
					<subreportParameterExpression><![CDATA[$P{bottom_message}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "footer.jasper"]]></subreportExpression>
			</subreport>
		</band>
	</pageFooter>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="0" width="554" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
