<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="bankBalance" language="groovy" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" isFloatColumnFooter="true">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="fDateEnd" class="java.lang.String"/>
	<parameter name="dateEnd" class="java.lang.String"/>
	<parameter name="companyName" class="java.lang.String"/>
	<parameter name="dateBegin" class="java.lang.String"/>
	<parameter name="fDateBegin" class="java.lang.String"/>
	<parameter name="acc_shared_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="accounting_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="sysdb_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="date" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[$P{fDateBegin}+" - "+$P{fDateEnd}]]></defaultValueExpression>
	</parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_docs_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals('0')?"header.id_docstatus<>5 AND ":""]]></defaultValueExpression>
	</parameter>
	<parameter name="advancedFilters" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT IFNULL(alldata.gjendja, 0) AS gjendja, IFNULL(alldata.gjendja_home, 0) AS gjendja_home,
		IFNULL(alldata.arketim, 0) AS arketim, IFNULL(alldata.arketim_home, 0) AS arketim_home,
		IFNULL(alldata.pagese, 0) AS pagese, IFNULL(alldata.pagese_home, 0) AS pagese_home,
		alldata.id_currency, alldata.currency, alldata.cur_desc,
		alldata.id_cash, alldata.name, alldata.code, alldata.home_cur, alldata.id_account
FROM (

SELECT * FROM (
SELECT
	ROUND(SUM(value_tab.value_cash), 2) AS gjendja, ROUND(SUM(value_tab.value_cash_home), 2) AS gjendja_home
	, value_tab.id_currency , value_tab.id AS id_cash, value_tab.name, value_tab.code
	, value_tab.currency , value_tab.cur_desc, value_tab.id_account
	FROM
		(
				SELECT cash.opening_balance_home_currency AS 'value_cash_home', cash.opening_balance AS 'value_cash'
					, cash.id, cash.id_currency, cash.name, cash.code, cur.name AS currency
					, cur.description AS cur_desc, cash.id_account
					FROM $P!{accounting_db}.cash
						INNER JOIN $P!{db_sysdb}.currencies cur
							ON cash.id_currency = cur.id
					WHERE cash.unit_type = 1

			UNION ALL

				SELECT header.home_cur_total_cashed AS 'value_cash_home', header.total_cashed AS 'value_cash', cash.id
						, cash.id_currency, cash.name, cash.code, cur.name AS currency, cur.description AS cur_desc
						, cash.id_account
					FROM $P!{accounting_db}.cashreceipt header
						INNER JOIN $P!{accounting_db}.cash ON header.id_cash = cash.id AND cash.unit_type = 1
						INNER JOIN $P!{db_sysdb}.currencies cur ON header.id_currency = cur.id
					WHERE $P!{exclude_canceled_docs_cond} header.document_date < $P{dateBegin}

			UNION ALL


				SELECT header.home_cur_total_amount_payed * -1 AS 'value_cash_home', header.total_amount_payed * -1 AS 'value_cash'
						, cash.id, cash.id_currency, cash.name, cash.code, cur.name AS currency
						, cur.description AS cur_desc, cash.id_account
					FROM $P!{accounting_db}.cashpayment header
						INNER JOIN $P!{accounting_db}.cash ON header.id_cash = cash.id AND cash.unit_type = 1
						INNER JOIN $P!{db_sysdb}.currencies cur ON header.id_currency = cur.id
					WHERE $P!{exclude_canceled_docs_cond} header.document_date < $P{dateBegin}
		) value_tab
	GROUP BY value_tab.id_currency, value_tab.id ORDER BY value_tab.id_currency, value_tab.id ) b

LEFT JOIN

	(SELECT SUM(header.total_cashed) AS 'arketim', SUM(header.home_cur_total_cashed) AS 'arketim_home', cash.id, cash.id_currency AS cur_id
		FROM $P!{accounting_db}.cashreceipt header
			INNER JOIN $P!{accounting_db}.cash ON header.id_cash = cash.id AND cash.unit_type = 1
			INNER JOIN $P!{db_sysdb}.currencies cur ON header.id_currency = cur.id
		WHERE $P!{exclude_canceled_docs_cond} header.document_date >= $P{dateBegin} AND header.document_date <= $P{dateEnd}
		GROUP BY cash.id ORDER BY cash.id ASC) a
		ON b.id_cash = a.id

LEFT JOIN

	(SELECT SUM(header.total_amount_payed) AS 'pagese', SUM(header.home_cur_total_amount_payed) AS 'pagese_home', cash.id AS c_id, cash.id_currency AS id_cur
		FROM $P!{accounting_db}.cashpayment header
			INNER JOIN $P!{accounting_db}.cash ON header.id_cash = cash.id AND cash.unit_type = 1
			INNER JOIN $P!{db_sysdb}.currencies cur ON header.id_currency = cur.id
		WHERE $P!{exclude_canceled_docs_cond} header.document_date >= $P{dateBegin} AND header.document_date <= $P{dateEnd}
		GROUP BY cash.id ORDER BY cash.id ASC) c
		ON c.c_id = a.id

LEFT JOIN
	(SELECT currency AS home_cur FROM $P!{db_sysdb}.company LIMIT 1) d ON 1=1

) alldata WHERE $P!{advancedFilters}]]>
	</queryString>
	<field name="gjendja" class="java.math.BigDecimal"/>
	<field name="gjendja_home" class="java.math.BigDecimal"/>
	<field name="arketim" class="java.math.BigDecimal"/>
	<field name="arketim_home" class="java.math.BigDecimal"/>
	<field name="pagese" class="java.math.BigDecimal"/>
	<field name="pagese_home" class="java.math.BigDecimal"/>
	<field name="id_currency" class="java.lang.String"/>
	<field name="currency" class="java.lang.String"/>
	<field name="cur_desc" class="java.lang.String"/>
	<field name="id_cash" class="java.lang.Long"/>
	<field name="cash_name" class="java.lang.String"/>
	<field name="cash_code" class="java.lang.String"/>
	<field name="home_cur" class="java.lang.String"/>
	<variable name="dateTime" class="java.util.Date">
		<variableExpression><![CDATA[new Date()]]></variableExpression>
	</variable>
	<variable name="endPeriodTot" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{gjendja}.add( $F{arketim} ).subtract( $F{pagese} )]]></variableExpression>
	</variable>
	<variable name="groupBeginTot" class="java.math.BigDecimal" resetType="Group" resetGroup="currencyGroup" calculation="Sum">
		<variableExpression><![CDATA[$F{gjendja}]]></variableExpression>
	</variable>
	<variable name="groupCashTot" class="java.math.BigDecimal" resetType="Group" resetGroup="currencyGroup" calculation="Sum">
		<variableExpression><![CDATA[$F{arketim}]]></variableExpression>
	</variable>
	<variable name="groupPayTot" class="java.math.BigDecimal" resetType="Group" resetGroup="currencyGroup" calculation="Sum">
		<variableExpression><![CDATA[$F{pagese}]]></variableExpression>
	</variable>
	<variable name="groupEndTot" class="java.math.BigDecimal" resetType="Group" resetGroup="currencyGroup" calculation="Sum">
		<variableExpression><![CDATA[$V{endPeriodTot}]]></variableExpression>
	</variable>
	<variable name="reportBeginTot" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{gjendja_home}]]></variableExpression>
	</variable>
	<variable name="reportCashTot" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{arketim_home}]]></variableExpression>
	</variable>
	<variable name="reportPayTot" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{pagese_home}]]></variableExpression>
	</variable>
	<variable name="reportEndTot" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{gjendja}.add( $F{arketim} ).subtract( $F{pagese} )]]></variableExpression>
	</variable>
	<variable name="endPeriodTotHome" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$F{gjendja_home}.add( $F{arketim_home} ).subtract( $F{pagese_home} )]]></variableExpression>
	</variable>
	<variable name="groupBeginTotHome" class="java.math.BigDecimal" resetType="Group" resetGroup="currencyGroup" calculation="Sum">
		<variableExpression><![CDATA[$F{gjendja_home}]]></variableExpression>
	</variable>
	<variable name="groupCashTotHome" class="java.math.BigDecimal" resetType="Group" resetGroup="currencyGroup" calculation="Sum">
		<variableExpression><![CDATA[$F{arketim_home}]]></variableExpression>
	</variable>
	<variable name="groupPayTotHome" class="java.math.BigDecimal" resetType="Group" resetGroup="currencyGroup" calculation="Sum">
		<variableExpression><![CDATA[$F{pagese_home}]]></variableExpression>
	</variable>
	<variable name="groupEndTotHome" class="java.math.BigDecimal" resetType="Group" resetGroup="currencyGroup" calculation="Sum">
		<variableExpression><![CDATA[$V{endPeriodTotHome}]]></variableExpression>
	</variable>
	<group name="currencyGroup">
		<groupExpression><![CDATA[$F{id_currency}]]></groupExpression>
		<groupHeader>
			<band height="47">
				<textField>
					<reportElement x="0" y="27" width="42" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="42" y="27" width="236" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{cur_desc}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="40">
				<textField>
					<reportElement x="0" y="0" width="194" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="194" y="0" width="84" height="20"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{groupBeginTot}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="278" y="0" width="84" height="20"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{groupCashTot}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="362" y="0" width="84" height="20"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{groupPayTot}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="446" y="0" width="84" height="20"/>
					<box rightPadding="2">
						<topPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{groupEndTot}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="194" y="20" width="84" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$V{groupBeginTotHome}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="278" y="20" width="84" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$V{groupCashTotHome}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="362" y="20" width="84" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$V{groupPayTotHome}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="446" y="20" width="84" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$V{groupEndTotHome}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="530" y="20" width="25" height="20"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{home_cur}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="77" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<textField evaluationTime="Report">
				<reportElement stretchType="RelativeToTallestObject" x="506" y="57" width="49" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" x="482" y="57" width="24" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[nga]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" x="362" y="57" width="84" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box rightPadding="4"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Faqe]]></text>
			</staticText>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="446" y="57" width="36" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy HH.mm.ss">
				<reportElement stretchType="RelativeToTallestObject" x="42" y="57" width="152" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{dateTime}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" x="0" y="57" width="42" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[dt.]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" x="194" y="57" width="84" height="20"/>
				<box leftPadding="0" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Periudha: ]]></text>
			</staticText>
			<textField>
				<reportElement x="0" y="0" width="555" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{companyName}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="20" width="555" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<text><![CDATA[Banka - Verifikues]]></text>
			</staticText>
			<textField isStretchWithOverflow="true">
				<reportElement stretchType="RelativeToTallestObject" x="278" y="57" width="84" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{date}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="71" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="446" y="20" width="109" height="20"/>
				<box leftPadding="2" rightPadding="25">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Periudhe]]></text>
			</staticText>
			<staticText>
				<reportElement x="362" y="20" width="84" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Pagesa]]></text>
			</staticText>
			<staticText>
				<reportElement x="278" y="20" width="84" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Arkëtime]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="1" width="42" height="39"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Banka]]></text>
			</staticText>
			<staticText>
				<reportElement x="42" y="1" width="152" height="39"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Përshkrimi]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="0" width="194" height="20"/>
				<box>
					<topPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement x="446" y="0" width="109" height="20"/>
				<box rightPadding="25">
					<topPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Fund]]></text>
			</staticText>
			<staticText>
				<reportElement x="194" y="0" width="84" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Fillim]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="278" y="0" width="168" height="20" backcolor="#F1EFE2"/>
				<box>
					<topPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[LËVIZJE]]></text>
			</staticText>
			<staticText>
				<reportElement x="194" y="20" width="84" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Periudhe]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="40" splitType="Stretch">
			<textField>
				<reportElement x="0" y="0" width="42" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{cash_code}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="42" y="0" width="152" height="20"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{cash_name}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="194" y="0" width="84" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{gjendja}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="278" y="0" width="84" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{arketim}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="362" y="0" width="84" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{pagese}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="446" y="0" width="84" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{endPeriodTot}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="530" y="0" width="25" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="194" y="20" width="84" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{gjendja_home}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="278" y="20" width="84" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{arketim_home}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="362" y="20" width="84" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{pagese_home}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="446" y="20" width="84" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{endPeriodTotHome}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="530" y="20" width="25" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{home_cur}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band height="21" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
			<staticText>
				<reportElement x="0" y="1" width="194" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="278" y="1" width="84" height="20"/>
				<box>
					<topPen lineWidth="2.5" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{reportCashTot}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="362" y="1" width="84" height="20"/>
				<box>
					<topPen lineWidth="2.5" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{reportPayTot}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="446" y="1" width="84" height="20"/>
				<box>
					<topPen lineWidth="2.5" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{reportEndTot}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="194" y="1" width="84" height="20"/>
				<box>
					<topPen lineWidth="2.5" lineStyle="Double"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{reportBeginTot}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="530" y="1" width="25" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{home_cur}]]></textFieldExpression>
			</textField>
		</band>
	</columnFooter>
	<pageFooter>
		<band height="20" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
		</band>
	</pageFooter>
	<summary>
		<band height="42" splitType="Stretch"/>
	</summary>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="16" width="556" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
