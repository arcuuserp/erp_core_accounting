<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="journal_item_state_excel" language="groovy" pageWidth="555" pageHeight="802" whenNoDataType="BlankPage" columnWidth="555" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="dateBegin" class="java.lang.String"/>
	<parameter name="item_code" class="java.lang.String"/>
	<parameter name="warehouse_code" class="java.lang.String"/>
	<parameter name="accounting_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="db_acc_shared" class="java.lang.String"/>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_docs_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals('0')?"header.status<>5 AND ":""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT SUM(w.quantity/w.coefficient) AS quantity_state, SUM(w.quantity*w.cost/w.coefficient) AS value_state
FROM 	(SELECT header.`id`,header.`warehouse_code`,header.`warehouse_name`,wh.`description`, i.`name`, i.`code`, i.`sale_description`, header.`documenttype_name`, header.`document_number`, header.`document_date`,header.`serial_number`,d.`description` AS document_description, IFNULL(enb.`cost`,0) AS cost, IFNULL(uc.`coefficient`,0) AS coefficient, IFNULL(enb.`quantity`,0) AS quantity
	FROM $P!{accounting_db}.`entrywarehouse` header
	INNER JOIN $P!{accounting_db}.`entrywarehousebody` enb
	ON enb.`id_entrywarehouse` = header.`id`
	INNER JOIN $P!{accounting_db}.`warehouse` wh
	ON wh.`id` = header.`id_warehouse`
	LEFT JOIN $P!{accounting_db}.items i
	ON i.`id`=enb.`id_item`
	LEFT JOIN $P!{db_acc_shared}.`unitconversions` uc
	ON uc.`idunitentry` = i.`id_unit` AND uc.`idunitexit`=enb.`id_unit`
	LEFT JOIN $P!{accounting_db}.`documenttypes` d
	ON d.`id` = header.`id_documenttype`
	WHERE header.`deleted` = 0 AND $P!{exclude_canceled_docs_cond} header.`document_date`<$P{dateBegin} AND header.`warehouse_code` = $P{warehouse_code} AND i.`code` = $P{item_code}
	UNION ALL
	SELECT header.id, header.`warehouse_code`,header.`warehouse_name`, wh.`description`, i.`name`, i.`code`, i.`sale_description`, header.`documenttype_name`, header.`document_number`, header.`document_date`,header.`serial_number`, d.`description` AS document_description, IFNULL(exb.cost,0) AS cost, IFNULL(uc.`coefficient`,0) AS coefficient, IFNULL(-1*exb.`quantity`,0) AS quantity
	FROM $P!{accounting_db}.`exitwarehouse` header
	INNER JOIN $P!{accounting_db}.`exitwarehousebody` exb
	ON exb.`id_exitwarehouse` = header.id
	INNER JOIN $P!{accounting_db}.`warehouse` wh
	ON wh.`id` = header.`id_warehouse`
	LEFT JOIN $P!{accounting_db}.items i
	ON i.`id`=exb.`id_item`
	LEFT JOIN $P!{db_acc_shared}.`unitconversions` uc
	ON uc.`idunitentry` = i.`id_unit` AND uc.`idunitexit`=exb.`id_unit`
	LEFT JOIN $P!{accounting_db}.`documenttypes` d
	ON d.`id` = header.`id_documenttype`
	WHERE header.deleted = 0 AND $P!{exclude_canceled_docs_cond} header.`document_date`<$P{dateBegin} AND header.`warehouse_code` = $P{warehouse_code} AND i.`code` = $P{item_code} )w
ORDER BY w.warehouse_code, w.code]]>
	</queryString>
	<field name="quantity_state" class="java.math.BigDecimal"/>
	<field name="value_state" class="java.math.BigDecimal"/>
	<variable name="value/unit" class="java.math.BigDecimal">
		<variableExpression><![CDATA[($F{quantity_state}==0)? $F{value_state}:$F{value_state}.divide($F{quantity_state},2,java.math.RoundingMode.HALF_UP)]]></variableExpression>
	</variable>
	<variable name="quantity_state" class="java.math.BigDecimal">
		<variableExpression><![CDATA[($F{quantity_state}==null)? 0:$F{quantity_state}]]></variableExpression>
	</variable>
	<variable name="value_state" class="java.math.BigDecimal">
		<variableExpression><![CDATA[($F{value_state}==null)? 0:$F{value_state}]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<detail>
		<band height="20" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="80" height="20" backcolor="#FACCCC"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[gjendja fillestare]]></text>
			</staticText>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement x="116" y="0" width="46" height="20" backcolor="#FACCCC"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{value/unit}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="80" y="0" width="36" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{quantity_state}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="162" y="0" width="44" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{value_state}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="206" y="0" width="48" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{quantity_state}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="254" y="0" width="52" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{value_state}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
