<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="customerAnalyticActionsReport" language="groovy" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="bottom_message" class="java.lang.String"/>
	<parameter name="name_surname" class="java.lang.String"/>
	<parameter name="report_title" class="java.lang.String"/>
	<parameter name="dateEnd" class="java.lang.String"/>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="dateBegin" class="java.lang.String"/>
	<parameter name="accounting_db" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["C://reports/MyReports/accounting/"]]></defaultValueExpression>
	</parameter>
	<parameter name="month_human" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[$P{dateBegin}+" - "+$P{dateEnd}]]></defaultValueExpression>
	</parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_docs_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals('0')?"header.id_docstatus<>5 AND ":""]]></defaultValueExpression>
	</parameter>
	<parameter name="customerCondition" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT header.id_docstatus, c.`id`, c.`contact_code`, c.`contact_firstname`,c.`id_currency`, c.`currency`AS customer_currency, c.`company_address`, header.`id` AS id, header.`serial_number`, header.`document_number`, header.`document_date`, header.description,IFNULL(header.`home_cur_total_with_vat`,0) AS home_value,
	IFNULL(header.`home_cur_total_with_vat`/header.`customer_cur_exchange_rate`,0) AS curr_value, IFNULL(t.payed,0) AS payed, IFNULL(t.payed_home_curr,0) AS payed_home_curr
	FROM $P!{accounting_db}.contacts c
	LEFT JOIN $P!{accounting_db}.`salesinvoice` header
	ON c.id = header.id_contact AND $P!{exclude_canceled_docs_cond} LEFT(header.`document_date`,10) >= $P{dateBegin}   AND LEFT(header.`document_date`,10) <= $P{dateEnd}
	LEFT JOIN
	(SELECT rp.`subject_id` AS id ,SUM(IFNULL(rp.home_cur_amount_payed/rp.receipt_currency_exchange_rate,0)) AS payed, SUM(IFNULL(rp.home_cur_amount_payed,0)) AS payed_home_curr
	FROM $P!{accounting_db}.`r_payment` rp
	INNER JOIN $P!{accounting_db}.`cashreceipt` ch
	ON ch.id = rp.id_cashreceipt
	WHERE rp.deleted = 0 AND rp.`subject` = "saleinvoiceheader" AND LEFT(rp.`payment_date`,10) >= $P{dateBegin}  AND LEFT(rp.`payment_date`,10) <= $P{dateEnd}
GROUP BY rp.subject_id)t
	ON  t.id = header.id
	WHERE c.deleted = 0 $P!{customerCondition}
ORDER BY c.contact_code,header.document_number, header.document_date]]>
	</queryString>
	<field name="customer_id" class="java.lang.Integer"/>
	<field name="id_docstatus" class="java.lang.Integer"/>
	<field name="customer_code" class="java.lang.String"/>
	<field name="contact_display_name" class="java.lang.String"/>
	<field name="id_currency" class="java.lang.Integer"/>
	<field name="customer_currency" class="java.lang.String"/>
	<field name="company_address" class="java.lang.String"/>
	<field name="id" class="java.lang.Integer"/>
	<field name="serial_number" class="java.lang.String"/>
	<field name="document_number" class="java.lang.String"/>
	<field name="document_date" class="java.sql.Timestamp"/>
	<field name="description" class="java.lang.String"/>
	<field name="home_value" class="java.math.BigDecimal"/>
	<field name="curr_value" class="java.math.BigDecimal"/>
	<field name="payed" class="java.math.BigDecimal"/>
	<field name="payed_home_curr" class="java.math.BigDecimal"/>
	<variable name="notpayed" class="java.math.BigDecimal">
		<variableExpression><![CDATA[($F{id_docstatus}.compareTo( 5 ) == 0) ? 0 : $F{home_value} - $F{payed_home_curr}]]></variableExpression>
	</variable>
	<variable name="home_value_1" class="java.math.BigDecimal" resetType="Group" resetGroup="customer" calculation="Sum">
		<variableExpression><![CDATA[($F{id_docstatus}.compareTo( 5 ) == 0) ? 0 : $F{home_value}]]></variableExpression>
	</variable>
	<variable name="payed_home_curr_1" class="java.math.BigDecimal" resetType="Group" resetGroup="customer" calculation="Sum">
		<variableExpression><![CDATA[($F{id_docstatus}.compareTo( 5 ) == 0) ? 0 : $F{payed_home_curr}]]></variableExpression>
	</variable>
	<variable name="notpayed_1" class="java.math.BigDecimal" resetType="Group" resetGroup="customer" calculation="Sum">
		<variableExpression><![CDATA[$V{notpayed}]]></variableExpression>
	</variable>
	<group name="customer">
		<groupExpression><![CDATA[$F{customer_id}]]></groupExpression>
		<groupFooter>
			<band height="18">
				<textField isBlankWhenNull="true">
					<reportElement x="262" y="0" width="54" height="16"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{customer_code}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="316" y="0" width="80" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="0.75" lineStyle="Dashed"/>
					</box>
					<textElement textAlignment="Right">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{home_value_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="396" y="0" width="80" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="0.75" lineStyle="Dashed"/>
					</box>
					<textElement textAlignment="Right">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{payed_home_curr_1}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement x="476" y="0" width="79" height="16"/>
					<box rightPadding="2">
						<topPen lineWidth="0.75" lineStyle="Dashed"/>
					</box>
					<textElement textAlignment="Right">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{notpayed_1}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="130" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<textField isBlankWhenNull="true">
				<reportElement x="476" y="0" width="30" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" isBlankWhenNull="true">
				<reportElement x="524" y="0" width="31" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="506" y="0" width="18" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[nga]]></text>
			</staticText>
			<staticText>
				<reportElement x="396" y="0" width="80" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Faqe       ]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="0" width="396" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{company_name}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="40" width="555" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{report_title}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="80" width="555" height="20"/>
				<box rightPadding="20"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{name_surname}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="60" width="262" height="20"/>
				<box leftPadding="0" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Periudha:]]></text>
			</staticText>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy">
				<reportElement x="262" y="60" width="54" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateBegin})]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="dd/MM/yyyy">
				<reportElement x="336" y="60" width="140" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[new SimpleDateFormat("yyyy-MM-dd").parse($P{dateEnd})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="316" y="60" width="20" height="20"/>
				<box bottomPadding="7"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[-]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="20" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="0" y="0" width="50" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Klienti]]></text>
			</staticText>
			<staticText>
				<reportElement x="50" y="0" width="160" height="20"/>
				<box leftPadding="2">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Pershkrim]]></text>
			</staticText>
			<staticText>
				<reportElement x="210" y="0" width="106" height="20"/>
				<box>
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Fatura]]></text>
			</staticText>
			<staticText>
				<reportElement x="316" y="0" width="80" height="20"/>
				<box rightPadding="2">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Detyrim]]></text>
			</staticText>
			<staticText>
				<reportElement x="396" y="0" width="80" height="20"/>
				<box rightPadding="2">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Detyrim Paguar]]></text>
			</staticText>
			<staticText>
				<reportElement x="476" y="0" width="79" height="20"/>
				<box rightPadding="2">
					<topPen lineWidth="0.75"/>
					<bottomPen lineWidth="0.75"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<text><![CDATA[Detyrim Mbetur]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="16" splitType="Stretch">
			<rectangle>
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="555" height="15" forecolor="#FFD7DA" backcolor="#FFD7DA">
					<printWhenExpression><![CDATA[$F{id_docstatus}.compareTo( 5 ) == 0]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="130" y="0" width="80" height="16">
					<printWhenExpression><![CDATA[$V{customer_COUNT} == 1]]></printWhenExpression>
				</reportElement>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{company_address}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="210" y="0" width="52" height="16"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serial_number}]]></textFieldExpression>
			</textField>
			<textField pattern="dd.MM.yyyy" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="262" y="0" width="54" height="16"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{document_date}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="316" y="0" width="80" height="16"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{home_value}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="476" y="0" width="79" height="16"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{notpayed}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToTallestObject" x="396" y="0" width="80" height="16"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{payed_home_curr}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="0" width="50" height="16"/>
				<box leftPadding="2"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customer_code}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="50" y="0" width="80" height="16"/>
				<box leftPadding="2"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{contact_display_name}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="65" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
			<subreport>
				<reportElement x="-20" y="0" width="595" height="35"/>
				<subreportParameter name="bottom_message">
					<subreportParameterExpression><![CDATA[$P{bottom_message}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "footer.jasper"]]></subreportExpression>
			</subreport>
		</band>
	</pageFooter>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="18" width="556" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
