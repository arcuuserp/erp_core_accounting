<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="customerTotalSales" language="groovy" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="user_name" class="java.lang.String"/>
	<parameter name="fDateEnd" class="java.lang.String"/>
	<parameter name="fDateBegin" class="java.lang.String"/>
	<parameter name="dateEnd" class="java.lang.String"/>
	<parameter name="dateBegin" class="java.lang.String"/>
	<parameter name="company_name" class="java.lang.String"/>
	<parameter name="acc_shared_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="accounting_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_accounting"]]></defaultValueExpression></parameter>
	<parameter name="sysdb_db" class="java.lang.String"><defaultValueExpression><![CDATA["erp_core"]]></defaultValueExpression></parameter>
	<parameter name="excel_export" class="java.lang.String">
		<defaultValueExpression><![CDATA["false"]]></defaultValueExpression>
	</parameter>
	<parameter name="date" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[$P{fDateBegin}+" - "+$P{fDateEnd}]]></defaultValueExpression>
	</parameter>
	<parameter name="show_canceled" class="java.lang.String">
		<defaultValueExpression><![CDATA[0]]></defaultValueExpression>
	</parameter>
	<parameter name="exclude_canceled_docs_cond" class="java.lang.String">
		<defaultValueExpression><![CDATA[$P{show_canceled}.equals('0')?"header.id_docstatus<>5 AND ":""]]></defaultValueExpression>
	</parameter>
	<parameter name="customerCondition" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT header.id_contact, cus.contact_display_name, cus.contact_code, header.id_currency, c.name AS currency, c.description AS cur_desc, SUM(header.total_with_vat) AS total, SUM(header.home_cur_total_with_vat) AS total_home
	FROM $P!{accounting_db}.salesinvoice header
		INNER JOIN $P!{accounting_db}.contacts cus ON header.id_contact = cus.id
		INNER JOIN $P!{db_sysdb}.currencies c ON header.id_currency = c.id
	WHERE $P!{exclude_canceled_docs_cond} header.document_date >= $P{dateBegin} AND header.document_date <= $P{dateEnd} $P!{customerCondition}
		GROUP BY id_currency, id_contact
		ORDER BY id_currency, id_contact]]>
	</queryString>
	<field name="id_contact" class="java.lang.Integer"/>
	<field name="contact_display_name" class="java.lang.String"/>
	<field name="customer_code" class="java.lang.String"/>
	<field name="id_currency" class="java.lang.Integer"/>
	<field name="currency" class="java.lang.String"/>
	<field name="cur_desc" class="java.lang.String"/>
	<field name="total" class="java.math.BigDecimal"/>
	<field name="total_home" class="java.math.BigDecimal"/>
	<variable name="dateTime" class="java.util.Date">
		<variableExpression><![CDATA[new Date()]]></variableExpression>
	</variable>
	<group name="currencyGroup">
		<groupExpression><![CDATA[$F{id_currency}]]></groupExpression>
		<groupHeader>
			<band height="30">
				<textField>
					<reportElement x="0" y="10" width="100" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{currency}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="100" y="10" width="258" height="20"/>
					<box leftPadding="2">
						<bottomPen lineWidth="1.0"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{cur_desc}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="96" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" x="491" y="60" width="30" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[nga]]></text>
			</staticText>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="458" y="60" width="33" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" x="358" y="60" width="100" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box rightPadding="4"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Faqe]]></text>
			</staticText>
			<textField evaluationTime="Report">
				<reportElement stretchType="RelativeToTallestObject" x="521" y="60" width="34" height="20">
					<printWhenExpression><![CDATA[!$P{excel_export}.equals("true")]]></printWhenExpression>
				</reportElement>
				<box leftPadding="2" rightPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="20" width="555" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<text><![CDATA[Klientë sipas xhiros]]></text>
			</staticText>
			<textField evaluationTime="Report" pattern="dd/MM/yyyy HH.mm.ss">
				<reportElement stretchType="RelativeToTallestObject" x="18" y="60" width="82" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$V{dateTime}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" x="100" y="60" width="118" height="20"/>
				<box leftPadding="0" rightPadding="2">
					<topPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Periudha: ]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" x="0" y="60" width="18" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[dt.]]></text>
			</staticText>
			<textField>
				<reportElement x="0" y="0" width="555" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{company_name}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="358" y="40" width="197" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{user_name}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement stretchType="RelativeToTallestObject" x="218" y="60" width="140" height="20"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{date}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="22" splitType="Stretch">
			<printWhenExpression><![CDATA[!$P{excel_export}.equals("true") || $V{PAGE_NUMBER}==1]]></printWhenExpression>
			<staticText>
				<reportElement x="458" y="1" width="97" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Monedhë vendi]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="1" width="100" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Klient]]></text>
			</staticText>
			<staticText>
				<reportElement x="100" y="1" width="258" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Përshkrim]]></text>
			</staticText>
			<staticText>
				<reportElement x="358" y="1" width="100" height="20"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Blerje]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<textField pattern="#,##0.00">
				<reportElement x="358" y="0" width="100" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{total}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="458" y="0" width="97" height="20"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{total_home}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="0" width="100" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{customer_code}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="100" y="0" width="258" height="20"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{contact_display_name}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="42" splitType="Stretch"/>
	</summary>
	<noData>
		<band height="50">
			<staticText>
				<reportElement x="0" y="15" width="556" height="20"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Filtrimi nuk solli asnje rezultat.]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>
