<!DOCTYPE html>
<html lang="en" ng-app="erpApp" ng-strict-di xmlns:ng="http://angularjs.org" >
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Arpad ERP App</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- Bootstrap theme -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="css/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet">


	<link rel="stylesheet" href="libs/kendoui/styles/kendo.common.min.css" />
	<!-- Allow user modified themes by switching this line -->
	<link rel="stylesheet" href="libs/kendoui/styles/kendo.common-bootstrap.core.min.css" />
	<link rel="stylesheet" href="libs/kendoui/styles/kendo.common-bootstrap.min.css" />
	<link rel="stylesheet" href="libs/kendoui/styles/kendo.bootstrap.min.css" />
	<!--link rel="stylesheet" href="libs/kendoui/styles/kendo.dataviz.min.css" />
	<link rel="stylesheet" href="libs/kendoui/styles/kendo.dataviz.bootstrap.min.css" /-->
	<link rel="stylesheet" href="css/kendoui-override-bootstrap.css" />

	<link href="css/oc-modal.css" rel="stylesheet">
	<link href="libs/ocmodal/css/ocModal.animations.css" rel="stylesheet">

	<link href="css/metro-tiles.css" rel="stylesheet">

	<link href="css/animations.css" rel="stylesheet">
	<!-- MAIN page styles -->
	<link href="css/main-styles.css" rel="stylesheet">





	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<!--script src="libs/kendoui/js/jquery.min.js"></script-->
	<script src="libs/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="libs/bootstrap.js"></script>

	<!--script src="libs/underscore.js"></script-->

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<script src="libs/upload/es5-shim.min.js"></script>
	<script src="libs/upload/es5-sham.min.js"></script>
	<![endif]-->

	<!-- http://behigh.github.io/bootstrap_dropdowns_enhancement -->
	<link href="libs/bootstrap-dropdown-enhancement/css/dropdowns-enhancement.css" rel="stylesheet">
	<script src="libs/bootstrap-dropdown-enhancement/js/dropdowns-enhancement.js"></script>


	<script src="libs/angular-1.4.3/angular.js"></script>
	<script src="libs/angular-1.4.3/angular-cookies.min.js"></script>


	<script src="libs/components/directives/directives.js"></script>
	<script src="libs/components/directives/kendo-custom-directives.js"></script>
	<!--script src="libs/components/directives/lazytabsDirectives.js"></script-->
	<script src="libs/components/filters/filters.js"></script>
	<script src="libs/components/services/services.js"></script>

	<script src="libs/oclazyload/ocLazyLoad.js"></script>
	<script src="libs/ocmodal/ocModal.js"></script>

	<script src="libs/infinite-scroll/ng-infinite-scroll.min.js"></script>

	<script src="libs/translate/angular-translate.js"></script>
	<script src="libs/translate/angular-translate-handler-log.js"></script>
	<script src="libs/translate/angular-translate-loader-static-files.js"></script>
	<script src="libs/translate/angular-translate-storage-cookie.min.js"></script>
	<script src="libs/translate/angular-translate-storage-local.min.js"></script>
	<script src="libs/translate/angular-translate-interpolation-messageformat.min.js"></script>

	<!-- ui-ROUTES
	AVENIR COKAJ:
		REMOVED =>  throw new Error("State '"+state_name+"'' is already defined") ON ui-router
					IN ORDER TO ALLOW STATE OVERRIDING
		ADDED   =>  if($state.$current.popup || ($state['previous']!=null && $state.previous.state['popup'] && $state.$current.url != "/")) return;
					THIS ALLOWS THE POPUP TO OPEN AND CLOSE WITHOUT REFRESHING THE VIEW UNDERNEATH IT EXCEPT WHEN IT IS ON HOME PAGE

		IMPORTANT => route OVERRIDES files must be placed before the regular routes
	-->
	<script src="libs/angular-ui/angular-ui-router.js"></script>
	<script src="libs/angular-ui/ct-ui-router-extras.js"></script>
	<script src="libs/components/services/stateConfigProvider.js"></script>

	<!-- Main app (config) -->
	<script src="modules/app.js"></script>

	<!-- place overrides above regular states doing so
	the duplicates will be skipped therefore overriding existing states -->
	<?php if(config('app.override_version') != "none"): ?>
		<script src="overrides/<?php echo config('app.override_version'); ?>/states.js"></script>
	<?php endif; ?>

	<script src="modules/core/states.js"></script>

	<?php

	//to-do:
	$languageCode = 'sq-AL';
	//$languageCode = 'en-US';

	$installed_modules = config('app.installed_modules');
	foreach($installed_modules as $moduleName=>$installed):
		if($installed): ?>
			<script src="modules/<?php echo $moduleName; ?>/states.js"></script>
		<?php endif; endforeach; ?>

	<script src="libs/kendoui/js/kendo.core.min.js"></script>
	<script src="libs/kendoui/js/kendo.treeview.min.js"></script>

	<!-- KENDO UI CUSTOM COMMON PACKAGE
	grunt custom:core,angular,data,draganddrop,fx,sortable,view,list,popup,tooltip,binder,validator,userevents,
	mobile.scroller,mobile.view,mobile.loader,mobile.pane,mobile.popover,mobile.shim,mobile.actionsheet,
	columnsorter,filtermenu,menu,columnmenu,groupable,filtercell,selectable,reorderable,resizable,excel,
	tabstrip,splitter,numerictextbox,maskedtextbox,dropdownlist,combobox,autocomplete,multiselect,calendar,datepicker,panelbar,notification,grid -->
	<script src="libs/kendoui/js/ace.kendo.custom.common.min.js"></script>

	<!-- PDF GENERATION and CHARTS => grunt custom:pdf,color,drawing -->
	<script src="libs/kendoui/js/ace.kendo.custom.pdf.charts.min.js"></script>

	<!-- EVEN THOUGH IT WAS INCLUDED IN THE COMMON PACKAGE IT IS STILL REQUIRED FOR THE GRID TO WORK -->
	<script src="libs/kendoui/js/kendo.dropdownlist.min.js"></script>
	<script src="libs/kendoui/js/kendo.list.min.js"></script>
	<script src="libs/kendoui/js/kendo.numerictextbox.min.js"></script>
	<script src="libs/kendoui/js/kendo.autocomplete.min.js"></script>
	<script src="libs/kendoui/js/kendo.filtercell.min.js"></script>
	<script src="libs/kendoui/js/kendo.datepicker.min.js"></script>


	<script src="libs/kendoui/js/kendo.treeview.min.js"></script>

	<!-- REQUIRED ONLY WHEN NEEDED
	<script src="libs/kendoui/js/kendo.listview.min.js"></script>
	<script src="libs/kendoui/js/kendo.slider.min.js"></script>
	<script src="libs/kendoui/js/kendo.upload.min.js"></script>
	RICH TEXT EDITOR - TO BE INCLUDED ONLY WHEN REQUIRED
	<script src="libs/kendoui/js/kendo.editor.min.js"></script-->

	<!-- HARDLY EVER USED - COULD BE INCLUDED ONLY WHEN REQUIRED
	<script src="libs/kendoui/js/kendo.timepicker.min.js"></script>
	<script src="libs/kendoui/js/kendo.datetimepicker.min.js"></script-->

	<!--<script src="libs/kendoui/js/kendo.all.min.js"></script>
	<script src="libs/kendoui/js/Kendo UI Professional Q3 2014/src/src/kendo.all.js"></script>-->

    <script src="libs/kendoui/js/cultures/kendo.culture.<?php echo $languageCode; ?>.js"></script>
    <script src="libs/kendoui/js/messages/kendo.messages.<?php echo $languageCode; ?>.js"></script>


	<!-- global helper functions -->
	<script src="libs/components/helper.js"></script>
	<script src="libs/components/jquery.urldecoder.js"></script>

	<style>

	</style>

	<!-- could easily use a custom property of the state here instead of 'name' -->
	<title ng-bind="$state.current"></title>

</head>
<body ng-controller="BodyCtrl">

<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand btn btn-lg" href="#/home">DEKA - ERP </a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-left first-nav-menu-list-group">
				<!--li><a href="#"><i class="fa fa-chevron-left"></i> Back</a></li-->
				<!--li><a href="#"><i class="fa fa-star-o"></i> </a></li-->
				<li><a href="#"><i class="fa fa-tachometer"></i></a></li>
				<li><a href="#"><i class="fa fa-line-chart"></i> </a></li>
			</ul>
			<form class="navbar-form navbar-left">
				<!--<input type="text" class="form-control" placeholder="Search...">-->
				<span class="k-textbox k-space-left k-form-control">
                    <input type="text" placeholder="Search..."/>
                    <a href="#" class="k-icon k-i-search">&nbsp;</a>
                </span>
			</form>
			<ul class="nav navbar-nav navbar-left">
				<li class="dropdown">
		          <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
			          <i class="fa fa-history"></i></span>
		          </a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="#">Action</a></li>
		            <li><a href="#">Another action</a></li>
		            <li><a href="#">Something else here Something else here Something else here Something else here</a></li>
		            <li class="divider"></li>
		            <li><a href="#">Separated link</a></li>
		          </ul>
		        </li>
				<li class="dropdown dropdown-large">
					<a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<i class="fa fa-plus"></i></span>
					</a>
					<ul class="dropdown-menu dropdown-menu-large pull-center row">
						<li class="col-sm-3">
							<ul>
								<li class="dropdown-header" translate="ACC_COMMON.CUSTOMERS"></li>
								<li><a href="#" translate="ACC_DOCTYPE.SALES_INVOICE">  </a></li>
								<li><a href="#" translate="ACC_DOCTYPE.CASH_RECEIPT"> </a></li>
								<li class="divider"></li>
								<li><a href="#" translate="ACC_DOCTYPE.SALES_ORDER"> </a></li>
								<li><a href="#" translate="ACC_DOCTYPE.PROPOSED_OFFER"> </a></li>
								<li><a href="#" translate="ACC_DOCTYPE.SALES_DISCOUNT"> </a></li>
								<li><a href="#" translate="ACC_DOCTYPE.RETURN_SALES"> </a></li>
								<li class="divider"></li>
								<li><a href="/#/accounting/customers/create" translate="CONTACTS.CREATE_NEWCUSTOMER"> </a></li>
							</ul>
						</li>
						<li class="col-sm-3">
							<ul>
								<li class="dropdown-header" translate="ACC_COMMON.CASH_BANK"></li>
								<li><a href="#" translate="ACC_DOCTYPE.CASH_PAYMENT"> </a></li>
								<li><a href="#" translate="ACC_DOCTYPE.CASH_RECEIPT"> </a></li>
								<li class="divider"></li>
								<li><a href="#" translate="ACC_DOCTYPE.SALES_INVOICE"> </a></li>
								<li class="divider"></li>
								<li><a href="/#/accounting/cash/create" translate="CASH.CREATE_NEWCASHBANK"></a></li>
							</ul>
						</li>
						<li class="col-sm-3">
							<ul>
								<li class="dropdown-header" translate="WAREHOUSE.WAREHOUSE"></li>
								<li><a href="#" translate="ACC_DOCTYPE.WAREHOUSE_ENTRY"></a></li>
								<li><a href="#" translate="ACC_DOCTYPE.WAREHOUSE_EXIT"></a></li>
								<li class="divider"></li>
								<li><a href="#" translate="ACC_DOCTYPE.SALES_INVOICE"></a></li>
								<li><a href="#" translate="ACC_DOCTYPE.PURCHASE_INVOICE"></a></li>
								<li class="divider"></li>
								<li><a ui-sref="accounting/warehousesCreate" translate="WAREHOUSE.CREATE_NEWAREHOUSE"></a></li>
							</ul>
						</li>
						<li class="col-sm-3">
							<ul>
								<li class="dropdown-header">Navbar</li>
								<li><a href="#">Default navbar</a></li>
								<li><a href="#">Buttons</a></li>
								<li><a href="#">Text</a></li>
								<li><a href="#">Non-nav links</a></li>
								<li><a href="#">Component alignment</a></li>
								<li><a href="#">Fixed to top</a></li>
								<li><a href="#">Fixed to bottom</a></li>
								<li><a href="#">Static top</a></li>
								<li><a href="#">Inverted navbar</a></li>
							</ul>
						</li>
					</ul>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#"><i class="fa fa-cog"></i> </a></li>
				<li><a href="#"><i class="fa fa-user"></i> <span class="hidden-sm">User Name</span></a></li>
				<li><a href="#"><i class="fa fa-question-circle"></i></a></li>
			</ul>
		</div>
	</div>
</nav>

<div class="container-fluid main-container">
	<div id="main-sidebar">
		<div class="box-col main-sidebar-select-container">
			<select kendo-drop-down-list style="width: 100%; display: block;" class="custom-dropdown" k-options="menuLeftDropDownOptions"></select>
		</div>

		<div class="main-sidebar-menu k-content" data-ng-include data-src="menu_left_selected.view"></div>

	    <a id="main-sidebar-toggle-btn" type="button" class="btn" ng-click="toggleMenu()">
		    <i class="fa fa-angle-double-left"></i>
		</a>
	</div>

	<div id="main-content-view">
		<div ui-view></div>
	</div>
</div>

<script>

	angular.module("erpApp").constant("CSRF_TOKEN", '<?php echo csrf_token(); ?>');
	angular.module("erpApp").constant("OVERRIDE_VERSION", '<?php echo config('app.override_version'); ?>');

	angular.module("erpApp").constant("PREFERRED_LANGUAGE", '<?php echo $languageCode; ?>');
	<?php
		$i18n_files_conf = config('app.i18n_files');
		$i18nFiles = [];
		$installed_modules = config('app.installed_modules');
		foreach($installed_modules as $moduleName=>$installed){
			if($installed)
				$i18nFiles[$moduleName] = $i18n_files_conf[$moduleName];
		}
	?>
	angular.module("erpApp").constant("i18n_FILES", angular.fromJson( <?php echo json_encode($i18nFiles); ?> ));
</script>

</body>
</html>