<?php

/*
|--------------------------------------------------------------------------
| Accounting Routes
|--------------------------------------------------------------------------
*/

//Route::resource('access','CustomersController');


Route::group(['prefix' => 'deka', 'namespace' => 'App\Overrides\Deka\Core\Controllers'], function() {

	Route::group(array('prefix' => 'core'), function() {

		//Route::resource('prove','ProveController');

	});

	Route::group(array('prefix' => 'accounting'), function() {

		//Route::resource('ace-test','AceTestController');

	});
});


