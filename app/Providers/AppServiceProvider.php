<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Cache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
	{
		Cache::extend('file_tags', function($app)
		{
			$store = new \App\Libs\Extensions\FileStoreWithTags($app['files'], storage_path().'/framework/cache');
			return Cache::repository($store);
		});
		
	}

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
		
		$this->app->bind('ace-kendoui-datasource', function($app)
		{
			return new \App\Libs\KendoUI\DataSource();
		});
		
		$this->app->bind('ace-core-utils', function($app)
		{
			return new \App\Libs\Utils\CoreUtils();
		});
		$this->app->bind('ace-acc-utils', function($app)
		{
			return new \App\Libs\Utils\AccUtils();
		});
	}
}
