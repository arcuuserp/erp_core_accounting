<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|


Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/new/', function()
{
	$configValues = array(
		'modules_installed' => array('accounting'),
	);

	return View::make('mainnew', $configValues);
});


Route::get('/', function()
{
	return View::make('main');
});

Route::get('/ace-test', function()
{
	/*$result = Cache::rememberForever('test/test_1', function()
	{
		return DB::connection('acc')->select('select * from tests');
	});*/
$result = DB::connection('acc')->select('select * from tests');
	
	return response()->json(['one'=>'two', 'result'=>$result]);
});


/*
Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::post('login', array('as' => 'login', 'uses' => "AuthController@loginUser"));
Route::get('login-check', array('as' => 'login-check', 'uses' => "AuthController@checkLogin"));
Route::get('logout', array('as' => 'logout', 'uses' => "AuthController@index"));
*/
//Route::group(['domain' => '{account}.myapp.com'], function()
/*
Route::group(array('prefix' => 'core'), function() {

	Route::get('popuptest/show/{id?}', function($id){
		$result = "test id = ".$id;
		return Response::json($result);
	});
	Route::get('popuptest', function(){
		$result = "test result list";
		return Response::json($result);
	});
	Route::resource('prove','ProveController');

});
*/

Route::get('forget', function(){
	Cache::flush();
	echo 'Cache cleared';
});

Route::get('test-cache', function(){

	$permissions = Cache::rememberForever('test/type_1', function()
	{
		return array('test'=>1);
	});

	echo 'created';
});

Route::get('test-rowcount', function(){

	for ($i = 0; $i < 10000000; $i++) {
	    $b = $i % 1000;
		DB::insert("insert into count_test (b, c, d) values ($b, ROUND(RAND()*10), MD5($i))");

	    //mysql_query("INSERT INTO count_test SET b=, c=ROUND(RAND()*10), d=MD5($i)");
	}

	echo 'inserted';
});


// CATCH ALL ROUTE =============================  
// all routes that are not home or api will be redirected to the frontend 
// this allows angular to route them 
//App::missing(function($exception) { 
//    return Redirect::to('/');
//});


//fall-back url if missed typed the address
//this must be placed last
Route::any('{all}', function($uri)
{
	info('WRONG ROUTE: '.$uri);
    return Redirect::to('/');
})->where('all', '.*');

