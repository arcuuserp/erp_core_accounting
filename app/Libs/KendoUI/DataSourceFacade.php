<?php namespace App\Libs\KendoUI;

use Illuminate\Support\Facades\Facade as IlluminateFacade;

class DataSourceFacade extends IlluminateFacade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'ace-kendoui-datasource'; }

}