<?php namespace App\Libs\KendoUI;

use Illuminate\Support\Facades\DB;

class DataSource {

    private $stringOperators = array(
        'eq' => 'LIKE',
        'neq' => 'NOT LIKE',
        'doesnotcontain' => 'NOT LIKE',
        'contains' => 'LIKE',
        'startswith' => 'LIKE',
        'endswith' => 'LIKE'
    );

    private $operators = array(
        'eq' => '=',
        'gt' => '>',
        'gte' => '>=',
        'lt' => '<',
        'lte' => '<=',
        'neq' => '!='
    );

    private $aggregateFunctions = array(
        'average' => 'AVG',
        'min' => 'MIN',
        'max' => 'MAX',
        'count' => 'COUNT',
        'sum' => 'SUM'
    );

	private $request;
	private $dbConnName;

    function __construct($request, $dbConnName='core') {

	    $this->request = $request;
	    $this->dbConnName = $dbConnName;
    }
	
	private function page() {
        return ' LIMIT :skip,:take';
    }
	
/*
    private function total($tableName, $properties, $request) {
        if (isset($request->filter)) {
            $where = $this->filter($properties, $request->filter);
            $statement = $this->db->prepare("SELECT COUNT(*) FROM $tableName $where");
            $this->bindFilterValues($statement, $request->filter);
        } else {
            $statement = $this->db->prepare("SELECT COUNT(*) FROM $tableName");
        }

        $statement->execute();

        $total = $statement->fetch(PDO::FETCH_NUM);
        return (int)($total[0]);
    }

    private function group($data, $groups, $table, $request, $propertyNames) {
        if (count($data) > 0) {
            return $this->groupBy($data, $groups, $table, $request, $propertyNames);
        }
        return array();
    }

    private function groupBy($data, $groups, $table, $request, $propertyNames) {
        if (count($groups) > 0) {
            $field = $groups[0]->field;
            $count = count($data);
            $result = array();
            $value = $data[0][$field];
            $aggregates = isset($groups[0]->aggregates) ? $groups[0]->aggregates : array();

            $hasSubgroups = count($groups) > 1;
            $groupItem = $this->createGroup($field, $value, $hasSubgroups, $aggregates, $table, $request, $propertyNames);

            for ($index = 0; $index < $count; $index++) {
                $item = $data[$index];
                if ($item[$field] != $value) {
                    if (count($groups) > 1) {
                        $groupItem["items"] = $this->groupBy($groupItem["items"], array_slice($groups, 1), $table, $request, $propertyNames);
                    }

                    $result[] = $groupItem;

                    $groupItem = $this->createGroup($field, $data[$index][$field], $hasSubgroups, $aggregates, $table, $request, $propertyNames);
                    $value = $item[$field];
                }
                $groupItem["items"][] = $item;
            }

            if (count($groups) > 1) {
                $groupItem["items"] = $this->groupBy($groupItem["items"], array_slice($groups, 1), $table, $request, $propertyNames);
            }

            $result[] = $groupItem;

            return $result;
        }
        return array();
    }

    private function addFilterToRequest($field, $value, $request) {
        $filter = (object)array(
            'logic' => 'and',
            'filters' => array(
                (object)array(
                    'field' => $field,
                    'operator' => 'eq',
                    'value' => $value
                ))
            );

        if (isset($request->filter)) {
            $filter->filters[] = $request->filter;
        }

        return (object) array('filter' => $filter);
    }

    private function addFieldToProperties($field, $propertyNames) {
        if (!in_array($field, $propertyNames)) {
            $propertyNames[] = $field;
        }
        return $propertyNames;
    }

    private function createGroup($field, $value, $hasSubgroups, $aggregates, $table, $request, $propertyNames) {
        if (count($aggregates) > 0) {
            $request = $this->addFilterToRequest($field, $value, $request);
            $propertyNames = $this->addFieldToProperties($field, $propertyNames);
        }

        $groupItem = array(
            'field' => $field,
            'aggregates' => $this->calculateAggregates($table, $aggregates, $request, $propertyNames),
            'hasSubgroups' => $hasSubgroups,
            'value' => $value,
            'items' => array()
        );

        return $groupItem;
    }
*/

	/****
    private function calculateAggregates($table, $aggregates, $request, $propertyNames) {
        $count = count($aggregates);

        if (count($aggregates) > 0) {
            $functions = array();

            for ($index = 0; $index < $count; $index++) {
                $aggregate = $aggregates[$index];
                $name = $this->aggregateFunctions[$aggregate->aggregate];
                $functions[] = $name.'('.$aggregate->field.') as '.$aggregate->field.'_'.$aggregate->aggregate;
            }

            $sql = sprintf('SELECT %s FROM %s', implode(', ', $functions), $table);

            if (isset($request->filter)) {
                $sql .= $this->filter($propertyNames, $request->filter, $propertyNames);
            }

            $statement = $this->db->prepare($sql);

            if (isset($request->filter)) {
                $this->bindFilterValues($statement, $request->filter);
            }

            $statement->execute();

            $result = $statement->fetchAll(PDO::FETCH_ASSOC);

            return $this->convertAggregateResult($result[0]);
        }
        return (object)array();
	}*/


    private function convertAggregateResult($propertyNames) {
        $result = array();

        foreach($propertyNames as $property => $value) {
            $item = array();
            $split = explode('_', $property);
            $field = $split[0];
            $function = $split[1];
            if (array_key_exists($field, $result)) {
                $result[$field][$function] = $value;
            } else {
                $result[$field] = array($function => $value);
            }
        }

        return $result;
    }

    private function mergeSortDescriptors()
    {
        $sort = isset($this->request['sort']) && count($this->request['sort']) ? $this->request['sort'] : array();
        $groups =isset($this->request['group']) && count($this->request['group']) ? $this->request['group'] : array();

        return array_merge($sort, $groups);
    }

    private function sort($propertyNames, $sort, $map=null)
    {
        $count = count($sort);
        if ($count > 0)
        {
            //###$sql = ' ORDER BY ';
            $order = array();
            for ($index = 0; $index < $count; $index ++) {
                $field = $sort[$index]['field'];

                if (in_array($field, $propertyNames))
                {
					if(isset($map[$field]))
						$field = $map[$field];
					
                    $dir = 'ASC';
                    if ($sort[$index]['dir'] == 'desc')
                        $dir = 'DESC';

                    $order[] = "$field $dir";
                }
            }

            return implode(',', $order);
        }

        return '';
    }

    private function where($properties, $filter, $all, $propertyNames, $map)
    {
		if (isset($filter['filters'])) {
            $logic = ' AND ';

            if ($filter['logic'] == 'or') {
                $logic = ' OR ';
            }

            $filters = $filter['filters'];

            $where = array();

            for ($index = 0; $index < count($filters); $index++) {
                $where[] = $this->where($properties, $filters[$index], $all, $propertyNames, $map);
            }

            $where = implode($logic, $where);

            return "($where)";
        }

        if(isset($filter['field']))
        {
	        $field = $filter['field'];
	        if (in_array($field, $propertyNames) || isset($filter['extra_filter'])) {
	            $type = "string";

	            $index = array_search($filter, $all);

	            $value = ":filter$index";

	            if(isset($filter['extra_filter'])) {
		            $type = $filter['type'];
	            } else if (isset($properties[$field]['type'])) {
	                $type = $properties[$field]['type'];
	            } else if ($this->isDate($filter['value'])) {
	                $type = "date";
	            } else if (array_key_exists($filter['operator'], $this->operators) && !$this->isString($filter['value'])) {
	                $type = "number";
	            }

				if(isset($map[$field]))
					$field = $map[$field];

	            if ($type == "date") {
	                $field = "date($field)";
	                $value = "date($value)";
	            }

	            if ($type == "string") {
	                $operator = $this->stringOperators[$filter['operator']];
	            } else {
	                if(isset($this->operators[$filter['operator']]))
		                $operator = $this->operators[$filter['operator']];
	                else
		                $operator = $this->stringOperators[$filter['operator']];
	            }

	            return "$field $operator $value";
	        }
        }
    }

    private function flatten(&$all, $filter) {
        if (isset($filter['filters'])) {
            $filters = $filter['filters'];

            for ($index = 0; $index < count($filters); $index++) {
                $this->flatten($all, $filters[$index]);
            }
        } else {
            $all[] = $filter;
        }
    }

    private function filter($properties, $filter, $propertyNames, $fieldMapping) {
        $all = array();

        $this->flatten($all, $filter);

        $where = $this->where($properties, $filter, $all, $propertyNames, $fieldMapping);

        return $where;
    }

    private function isDate($value) {
	    return (\DateTime::createFromFormat('D M d Y H:i:s T', $value) !== FALSE);
	}

    private function isString($value) {
        return !is_bool($value) && !is_numeric($value) && !$this->isDate($value);
    }

	/* fieldname => array('type' => 'date', 'show' => true) */
    protected function propertyNames($properties) 
	{
        $names = array();
		$field = "";
		
        foreach ($properties as $key => $value) 
		{
            if (is_string($value)) {
                $field = $value;
            } else {
                $field = $key;
            }
			
			$names[] = $field;
        }
        return $names;
    }

    private function bindFilterValues($statement, $filter, $debug=false)
	{
		if($this->request['filter'] == "") return;

        $filters = array();
        $this->flatten($filters, $filter);

		$filtersLength = count($filters);
		$debugStr = "";

		info($filtersLength);

        for ($index = 0; $index < $filtersLength; $index++)
        {
	        if(isset($filters[$index]['value']))
	        {
				$value = $filters[$index]['value'];
				$operator = $filters[$index]['operator'];

		        if($this->isDate($value))
		        {
			        $value = \DateTime::createFromFormat('D M d Y H:i:s T', $value)->format('Y-m-d');
		        }

				if ($operator == 'contains' || $operator == 'doesnotcontain') {
					$value = "%$value%";
				} else if ($operator == 'startswith') {
				    $value = "$value%";
				} else if ($operator == 'endswith') {
				    $value = "%$value";
				}

				if($debug)
					$debugStr .= ":filter$index => $value, ";

				$statement->bindValue(":filter$index", $value);
	        }
        }


	    if($debug)
	        info('DB-PREP-PARAMS', [$debugStr]);
    }

	public function extraFilter($field, $value, $type='number', $operator='eq', $logic='and') {
        $filter = array(
            'logic' => $logic,
            'filters' => array(
                array(
                    'field' => $field,
                    'operator' => $operator,
                    'value' => $value,
                    'type' => $type,
                    'extra_filter' => true,
                ))
            );

        if (isset($this->request['filter'])) {
            $filter['filters'][] = $this->request['filter'];
        }

        return array('filter' => $filter);
    }

	/* fieldname => array('type' => 'date', 'show' => true) */
    public function prepareColumns($properties, $fieldMapping=null, $includeIdField=true)
    {
        $field = "";
		$fields = "";

	    if($includeIdField && !isset($properties['id']))
		    $properties['id'] = 'id';

		$len = count($properties);
		$i = 0;

        foreach ($properties as $key => $value)
		{
			$i++;
            if (is_string($value)) {
                $field = $value;
            }
            else
            {
                if (isset($value['show']) && $value['show']===false)
	                continue;
                else
                    $field = $key;
            }

			if(isset($fieldMapping[$field]))
				$field = $fieldMapping[$field].' AS '.$field;

			$fields .= $field.($i==$len?"":", ");
        }

        return $fields;
    }

    public function prepareFilters($properties, $includeWHERE_OR_AND=true, $fieldMapping=null)
    {
	    $where = "";
		if(isset($this->request['filter']) && $this->request['filter'] != "")
		{
            $propertyNames = $this->propertyNames($properties);
			$where = $this->filter($properties, $this->request['filter'], $propertyNames, $fieldMapping);
		}


	    if(trim($where) != "")
	    {
		    //IF IT IS BOOLEAN AND NOT FALSE => 'WHERE' ELSE IT MUST BE STRING THEREFORE APPLY IT VALUE
		    $includeWHERE_OR_AND = (is_bool($includeWHERE_OR_AND)&&$includeWHERE_OR_AND!=false?'WHERE':$includeWHERE_OR_AND);

		    $where = ' '.$includeWHERE_OR_AND.' '.$where;
	    }

		return $where;
    }

    public function prepareSort($properties, $includeORDER_BY=true, $fieldMapping=null)
    {
	    $sort = "";
        $mergedSort = $this->mergeSortDescriptors();
	    if(count($mergedSort)>0)
	    {
	        $propertyNames = $this->propertyNames($properties);
	        $sort = $this->sort($propertyNames, $mergedSort, $fieldMapping);
	        if($includeORDER_BY && trim($sort) != "")
			    $sort = " ORDER BY ".$sort;
	    }

		return $sort;
    }

	/*
	 * PREPARE ALL: COLUMN-NAMES, FILTERS, SORT
	 */
    public function prepare($properties, $fieldMapping=null, $mapSortFields=false)
    {
        $result = array();

		$result['columns'] = $this->prepareColumns($properties, $fieldMapping);

        $propertyNames = $this->propertyNames($properties);
	    if(isset($this->request['filter']) && $this->request['filter'] != "")
			$result['filters'] = $this->filter($properties, $this->request['filter'], $propertyNames, $fieldMapping);
		else $result['filters'] = "";

        $sort = $this->mergeSortDescriptors();
        $result['sort'] = $this->sort($propertyNames, $sort, ($mapSortFields==true?$fieldMapping:null) );

		return $result;
	}
	
    public function executeResult($sql, $selectFieldsStr='*', $debug=false)
    {
		$sql = 'SELECT '.$selectFieldsStr.' '.$sql;
		
        if (isset($this->request['skip']) && isset($this->request['take'])) {
			$sql .= $this->page();//' LIMIT :skip,:take'
        }

	    if($debug)
	        info('DB-PREP-QUERY', [$sql]);

        $statement = DB::connection($this->dbConnName)->getPdo()->prepare($sql);
		
        if (isset($this->request['filter'])) {
            $this->bindFilterValues($statement, $this->request['filter'], $debug);
        }
		
        if (isset($this->request['skip']) && isset($this->request['take'])) {
            $statement->bindValue(':skip', (int)$this->request['skip']);
            $statement->bindValue(':take', (int)$this->request['take']);
        }

        $statement->execute();
		
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
	}
	
    public function executeCount($sql, $runAlways=false, $selectFieldsStr="count(*) AS count")
	{
        if ($runAlways || (isset($this->request['page']) && $this->request['page'] == 1))
        {
			$sql = 'SELECT '.$selectFieldsStr.' '.$sql;
			
			//$db = DB::connection($dbConnName)->getPdo();		
			$statement = DB::connection($this->dbConnName)->getPdo()->prepare($sql);

			if (isset($this->request['filter'])) {
				$this->bindFilterValues($statement, $this->request['filter']);
			}

			$statement->execute();

			$data = $statement->fetchAll(\PDO::FETCH_ASSOC);
			return $data[0]['count'];
		}
		
        return null;
	}

    public function rawQuery($sql)
	{
        $statement = DB::connection($this->dbConnName)->getPdo()->query($sql);
		return $statement->fetchAll(\PDO::FETCH_ASSOC);

        return null;
	}

	/*
    public function read($sql, $properties, $request = null) {
        $result = array();

        $propertyNames = $this->propertyNames($properties);

        //### $result['total'] = $this->total($table, $properties, $request);

        $sql = sprintf('SELECT %s FROM %s', implode(', ', $propertyNames), $table);

        if (isset($request->filter)) {
            $sql .= $this->filter($properties, $request->filter);
        }

        $sort = $this->mergeSortDescriptors($request);

        if (count($sort) > 0) {
            $sql .= $this->sort($propertyNames, $sort);
        }

        if (isset($request->skip) && isset($request->take)) {
            $sql .= $this->page();
        }

        $statement = $this->db->prepare($sql);

        if (isset($request->filter)) {
            $this->bindFilterValues($statement, $request->filter);
        }

        if (isset($request->skip) && isset($request->take)) {
            $statement->bindValue(':skip', (int)$request->skip, PDO::PARAM_INT);
            $statement->bindValue(':take', (int)$request->take, PDO::PARAM_INT);
        }

        $statement->execute();

        $data = $statement->fetchAll(PDO::FETCH_ASSOC);

        if (isset($request->group) && count($request->group) > 0) {
            $data = $this->group($data, $request->group, $table, $request, $propertyNames);
            $result['groups'] = $data;
        } else {
            $result['data'] = $data;
        }

        if (isset($request->aggregate)) {
            $result["aggregates"] = $this->calculateAggregates($table, $request->aggregate, $request, $propertyNames);
        }

        return $result;
    }

    public function readJoin($table, $joinTable, $properties, $key, $column, $request = null) {
        $result = $this->read($table, $properties, $request);

        for ($index = 0, $count = count($result['data']); $index < $count; $index++) {
            $sql = sprintf('SELECT %s FROM %s WHERE %s = %s', $column, $joinTable, $key, $result['data'][$index][$key]);

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $data = $statement->fetchAll(PDO::FETCH_NUM);
            $result['data'][$index]['Attendees'] = $data;
        }

        return $result;
    }
	*/
}

?>
