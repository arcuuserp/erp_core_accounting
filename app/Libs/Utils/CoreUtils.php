<?php namespace App\Libs\Utils;

use Illuminate\Support\Facades\DB;

class CoreUtils {

	/**
	 * START A TRANSACTION PROCESS ON MANY DATABASES
	 * WHICH ARE STORED IN "transactionConnections"
	 * AND ARE REFERENCED LATER ONE TO BE COMMITED OR
	 * ROLLED BACK ACCORDINGLY ALL AT ONCE
	 */
	private $transactionConnections;
	public function beginManyTransactions($connections)
	{
		$this->transactionConnections = $connections;
		foreach($connections as $conn)
		{
			\DB::connection($conn)->beginTransaction();
		}
		return $this;
	}
	public function commitMany()	{
		foreach($this->transactionConnections as $conn)
			\DB::connection($conn)->commit();
	}
	public function rollbackMany()	{
		foreach($this->transactionConnections as $conn)
			\DB::connection($conn)->rollback();
	}//END MANY TRANSACTIONS

	static public function companyDetails()
	{
		$result = \Cache::rememberForever('company/details', function()
		{
			return \Core\Models\Companydetail::first();
		});
		
		return $result;
	}

	static function getSettings($name=null)
	{
		$settings = \Cache::rememberForever('settings', function()
		{
			$indexedSettings = array();
			$allSettings = \Core\Models\Setting::all();

			foreach($allSettings as $setting)
			{
				$indexedSettings[$setting->name] = $setting->value;
			}
			return $indexedSettings;
		});

		if($name==null)
			return $settings;

		return $settings[$name];
	}

	/*
	* field = [id or name]
	*/
	static public function homeCurrency($field=null)
	{
		$result = \Cache::rememberForever('company/currency', function()
		{
			$companydetail = \Core\Models\Companydetail::first();
			return ['id'=>$companydetail->id_currency, 'name'=>$companydetail->currency];
		});

		if($field==null)
			return $result;

		return $result[$field];
	}
	
	static function currencyName($id)
	{
		$currencies = \Cache::rememberForever('currencies/indexed', function()
		{
			$indexedCurrencies = array();
			
			$sql = "SELECT  id, name
                      FROM  currencies
                     WHERE deleted=0
                       AND active=1";
			$result = \DB::connection('core')->getPdo()->query($sql)->fetchAll(\PDO::FETCH_ASSOC);
			
			foreach($result as $currency)
			{
				$indexedCurrencies[$currency['id']] = $currency['name'];
			}
			return $indexedCurrencies;
		});

		return $currencies[$id];
	}
	
	/***************  UNITS ***************/
	static public function subjectUnits($subject_id=-1, $subject="item")
	{
		if($subject_id == -1)
			return [];

		$result = \Cache::rememberForever('units/'.$subject.'_'.$subject_id, function() use ($subject_id, $subject)
		{
			$sql = "SELECT  uc.id
						  , uc.subject
						  , uc.subject_id
						  , uc.idunitentry
						  , uc.idunitexit
						  , uc.coefficient
						  , uc.description
						  , uc.purchase_default
						  , uc.sales_default
						  , u1.fullunit    AS unitentry_fullunit
						  , u2.fullunit    AS unitexit_fullunit
						  , u1.symbol      AS unitentry_symbol
						  , u1.name        AS unitentry_name
						  , u1.description AS unitentry_description
						  , u2.symbol      AS unitexit_name
					 FROM unitconversions uc
					INNER JOIN units u1
					   ON (uc.idunitentry = u1.id)
					INNER JOIN units u2
					   ON (uc.idunitexit = u2.id)
					WHERE uc.deleted = 0
					  AND u1.deleted = 0
					  AND u2.deleted = 0
					  AND subject = :subject
					  AND subject_id = :subjectID";

			$statement = \DB::connection('core')->getPdo()->prepare($sql);
			$statement->bindValue(':subject', $subject);
			$statement->bindValue(':subjectID', $subject_id);
			$statement->execute();
			return $statement->fetchAll(\PDO::FETCH_ASSOC);
		});

		return $result;
	}

	public function conversionCoefficient($subject, $subject_id, $idunitentry, $idunitexit)
	{
		if( $idunitentry == $idunitexit)
			return 1;

		$seg_start = \DB::table('unitconversions')->where('deleted', 0)
									->where('idunitentry', $idunitentry)
									->where('subject', $subject)
									->where('subject_id', $subject_id)
									->get();

		$cnt_seg_start = count($seg_start);
		if($cnt_seg_start>0)
		{
			$seg_finish = \DB::table('unitconversions')->where('deleted', 0)
										->where('idunitexit', $idunitexit)
										->where('subject', $subject)
										->where('subject_id', $subject_id)
										->get();

			$cnt_seg_finish = count($seg_finish);
			if($cnt_seg_finish>0)
			{
				for($i=0;$i<$cnt_seg_start;$i++)
				{
					for($j=0;$j<$cnt_seg_finish;$j++)
					{
						//rasti i ndalimit
						if($seg_start[$i]->idunitexit ==  $idunitexit)
						{
							return $seg_start[$i]->coefficient;
						}
						else if ($seg_finish[$j]->idunitentry == $idunitentry)
						{
							return $seg_finish[$i]->coefficient;
						}
					}
				}
				//if we arrived here it means that there is no direct connection between the two units
				for($i=0;$i<$cnt_seg_start;$i++)
				{
					for($j=0;$j<$cnt_seg_finish;$j++)
					{
						$coefficient = $seg_start[$i]->coefficient * $seg_finish[$i]->coefficient * $this->conversionCoefficient($subject, $subject_id, $seg_start[$i]->idunitexit, $seg_finish[$i]->idunitentry);
						//mund te shkruhet si: return $coefficient | 0;
						if($coefficient > 0)
						{
							//echo "koef ".$coefficient;
							return $coefficient;
						}
						else
							return 0;
					}
				}
			}
		}
	}



	public function jasperLogin()
	{
		$statusCode = 500;
		$response = 'SERVER.JASPER_LOGIN_FAILED';

		$url = "http://localhost:8080/jasperserver/rest/login";
		$credentials = array('j_username'=>'jasperadmin',
							 'j_password'=>'jasperadmin');

		try {
	        $curl = curl_init($url);

            curl_setopt($curl, CURLOPT_FORBID_REUSE,        false);
            curl_setopt($curl, CURLOPT_FRESH_CONNECT,       false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,      true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,      0);
            curl_setopt($curl, CURLOPT_DNS_CACHE_TIMEOUT,   1024);
            curl_setopt($curl, CURLOPT_COOKIESESSION,       false);
            curl_setopt($curl, CURLOPT_ENCODING,            'identity');
			curl_setopt($curl, CURLOPT_COOKIEFILE,          '/dev/null');
            curl_setopt($curl, CURLOPT_HEADER,              true);
	        //post request
            curl_setopt($curl, CURLOPT_POST,                true);
            curl_setopt($curl, CURLOPT_POSTFIELDS,          http_build_query($credentials));

            $body = curl_exec($curl);
			//var_dump($body);
            $head = curl_getinfo($curl);
			//var_dump($head);
			$statusCode = $head['http_code'];

			if($statusCode == 200)
	        {
		        /*preg_match('/^Set-Cookie: (.*?);/m', $body, $m);
		        //var_dump($m);//$m[1] holds the jsession variable
			    $jSession = $m[1];
				$response = ['jasper_cookie'=>'Cookie: $Version=0; '.$jSession.'; $Path=/jasperserver'];
				*/

		        // Cookie: $Version=0; JSESSIONID=52E79BCEE51381DF32637EC69AD698AE; $Path=/jasperserver
				// Cookie: JSESSIONID=<sessionID>; $Path=<pathToJRS>
				// Extract the Cookie and save the string in my session for further requests.
				preg_match('/^Set-Cookie: (.*?)$/sm', $body, $cookie);
				$_SESSION["JSCookie"] = '$Version=0; ' . str_replace('Path', '$Path', $cookie[1]);

				//preg_match('/^Set-Cookie: (.*?);/m', $body, $m);
		        //$response = ['jasper_cookie'=>'Cookie: $Version=0; '.$m[1].'; $Path=/jasperserver'];

				// Grab the JS Session ID and set the cookie in the right path so
				// when I present an iFrame I can use that session to be authenticated
				// For this to work JS and the App have to run in the same domain

				preg_match_all('/=(.*?);/' , $cookie[1], $cookievalue);
	            $JRSSessionID = $cookievalue[1][0];
	            $JRSPath = $cookievalue[1][1];
		        $_SESSION["JRSSessionID"] = $JRSSessionID;
                $_SESSION["JRSPath"] = $JRSPath;
		        $response = [];
		        $response['JRSSessionID'] = $JRSSessionID;
		        $response['JRSPath'] = $JRSPath;
		        setcookie('JSESSIONID', $JRSSessionID, time() + (60*60*24), $JRSPath, null, false, false);
	        }
			else
			{
				throw new \Exception("Error code: {$statusCode} on POST request ({$url}), BODY: {$body}", $statusCode);
			}

            curl_close($curl);
        }
		catch (\Exception $e)
		{
			$statusCode = 500;
			info($e->getLine(), [$e->getCode(), $e->getMessage()]);
		}

		return ['response'=>$response, 'code'=>$statusCode];
	}


	public function reportsBottomMessage($lang=1, $moduleName="",$html=false)
	{
		global $reg;
		$variable = $reg->init("variable", null);
		//
		$from = $variable->getVariable("Conjunctions", "from", $lang);
		$on = $variable->getVariable("Conjunctions", "on", $lang);
		$date = $variable->getVariable("Conjunctions", "date", $lang);
		$report = $variable->getVariable("Reports", "report", $lang);
		$erpName = $variable->getVariable("Reports", "erpName", $lang);
		$vendorName = $variable->getVariable("Reports", "vendorName", $lang);
		$vendorWebsite = $variable->getVariable("Reports", "vendorWebsite", $lang);
		$vendorName = ($vendorWebsite != "")?$vendorName.", ":$vendorName ;
		$vendorWebsite = $html?"<a href='http://{$vendorWebsite}'>{$vendorWebsite}</a>":$vendorWebsite;
		$vendorTel = $variable->getVariable("Reports", "vendorTel", $lang);
		$vendorTel = ($vendorTel != "")?" ".$vendorTel:"";
		$vendorEmail = $variable->getVariable("Reports", "vendorEmail", $lang);
		$vendorEmail = ($vendorEmail != "")?" ".$vendorEmail:"";
		$moduleName = ($moduleName != "")?" ".$moduleName:"";
		return $report." ".$from." ".$erpName.$moduleName." ".$on." ".$date." ".date("d.m.Y H:i:s")." ".$vendorName.$vendorWebsite.$vendorTel.$vendorEmail;
	}

	public function reportDateAndUser($lang=1)
	{
		global $reg;
		$variable = $reg->init("variable", null);
		$sessionSite = $reg->init("sessionSite", null);
		//
		$from = $variable->getVariable("Conjunctions", "from", $lang);
		$on = $variable->getVariable("Conjunctions", "on", $lang);
		$date = $variable->getVariable("Conjunctions", "date", $lang);
		$printed = $variable->getVariable("Reports", "printed", $lang);
		$name = $sessionSite->user->name;
		$surname = $sessionSite->user->surname;
		return $printed." ".$on." ".$date." ".date("d.m.Y H:i:s")." ".$from." ".$name." ".$surname;
	}

	public function reportUser($lang=1)
	{
		global $reg;
		$variable = $reg->init("variable", null);
		$sessionSite = $reg->init("sessionSite", null);
		//
		$user = $variable->getVariable("Forms", "user", $lang);
		$name = $sessionSite->user->name;
		$surname = $sessionSite->user->surname;
		return $user." : ".$name." ".$surname;
	}

/**************************************************************************************************
********************					HELPER FUNCTIONS 					***********************	
/**************************************************************************************************
	
	
	/**
	 * explodeX($delimiters,$string)
	 * The function returns an array of strings exploded by given string for given delimiters
	 * $delimiters is an array with delimiters
	 * $string is the string to be exploded
	 * */    
	static public function explodeX($delimiters, $string)
	{
		$return_array = Array($string); // The array to return
		$d_count = 0;
		while (isset($delimiters[$d_count])) // Loop to loop through all delimiters
		{
			$new_return_array = Array();
			foreach($return_array as $el_to_split) // Explode all returned elements by the next delimiter
			{
				$put_in_new_return_array = explode($delimiters[$d_count],$el_to_split);
				foreach($put_in_new_return_array as $substr) // Put all the exploded elements in array to return
				{
					if($substr!="")
						$new_return_array[] = $substr;
				}
			}
			$return_array = $new_return_array; // Replace the previous return array by the next version
			$d_count++;
		}
		
		return $return_array; // Return the exploded elements
	}
	
	//group elements in $in_arr by the field names in $fields array
	static public function array_group($in_arr, $fields, $aggregate_fn)
	{
		$out_arr = array();
		foreach($in_arr as $el)
		{
			$field = $el[$fields[0]];
			if(!isset($out_arr[ $field ]))
			{
				$out_arr[$field] = [];
				$out_arr[$field]['elements'] = [];
				for($j=0;$j<count($aggregate_fn);$j++)
				{
					$out_arr[ $field ][ $aggregate_fn[$j]['field'].'_'.$aggregate_fn[$j]['fn'] ] = 0;
				}
			}

			$out_arr[$field]['elements'][] = $el;
			for($j=0;$j<count($aggregate_fn);$j++)
			{
				//vetem sum njeh per momentin, me vone nese duhet bejme nje switch case per cdo operator qe do suportojme
				$out_arr[ $field ][ $aggregate_fn[$j]['field'].'_'.$aggregate_fn[$j]['fn'] ] += $el[ $aggregate_fn[$j]['field'] ];
			}
		}

		//ti grupojme sipas fushes se radhes tani
		array_shift($fields);

		if(count($fields)==0)
			return $out_arr;
		else
		{
			foreach($out_arr as $key => $value)
			{
				$out_arr[$key]['elements'] = \CoreUtils::array_group($value['elements'], $fields, $aggregate_fn);
			}
			return $out_arr;
		}

	}
}