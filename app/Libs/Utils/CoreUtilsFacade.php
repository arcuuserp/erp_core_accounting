<?php namespace App\Libs\Utils;

use Illuminate\Support\Facades\Facade as IlluminateFacade;

class CoreUtilsFacade extends IlluminateFacade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'ace-core-utils'; }

}