<?php namespace App\Libs\Utils;

use \Accounting\Models\Transaction;
use \Accounting\Models\DocumentTransaction;

class AccUtils {

	static public function db($conn='acc', $pdo=null)
	{
		if($conn=='r')
		{
			$pdo = ($pdo==null);//if null then true
			$conn = 'acc_read';
		}
		if($pdo===true)
			return \DB::connection($conn)->getPdo();
		else
			return \DB::connection($conn);
	}

	static public function saveDocumentTransaction($model)
	{
		$model_name = $model->getTable();

		$doctrans = new DocumentTransaction();
		//fusha te pergjithshme
		$doctrans->subject = $model_name;
		$doctrans->subject_id = $model->id;

		$doctrans->id_docstatus = $model->id_docstatus;
		$doctrans->serial_number = $model->serial_number;
		$doctrans->document_date = $model->document_date;
		$doctrans->document_number = $model->document_number;
		$doctrans->description = $model->description;
		$doctrans->currency = $model->currency;
		$doctrans->exchange_rate = $model->exchange_rate;
		$doctrans->created_at = $model->created_at;

		//
		$doctrans->id_documentgroup = $model->id_documentgroup;
		$doctrans->id_documenttype = $model->id_documenttype;
		$doctrans->body_type = $model->body_type;
		$doctrans->doc_type_system_name = $model->doc_type_system_name;
		$doctrans->id_cash = $model->id_cash;
		$doctrans->cash_name = $model->cash_name;
		$doctrans->cash_code = $model->cash_code;
		$doctrans->id_warehouse = $model->id_warehouse;
		$doctrans->warehouse_name = $model->warehouse_name;
		$doctrans->warehouse_code = $model->warehouse_code;
		$doctrans->id_exchangeratecategories = $model->id_exchangeratecategories;
		$doctrans->id_commercialterms = $model->id_commercialterms;
		$doctrans->id_currency = $model->id_currency;
		$doctrans->contact_currency_exchange_rate = $model->contact_currency_exchange_rate;
		$doctrans->total_no_vat = $model->total_no_vat;
		$doctrans->total_vat = $model->total_vat;
		$doctrans->total_with_vat = $model->total_with_vat;
		$doctrans->home_cur_total_no_vat = $model->home_cur_total_no_vat;
		$doctrans->home_cur_total_vat = $model->home_cur_total_vat;
		$doctrans->home_cur_total_with_vat = $model->home_cur_total_with_vat;
		$doctrans->total_no_vat_for_interest_calc = $model->total_no_vat_for_interest_calc;
		$doctrans->total_with_vat_for_interest_calc = $model->total_with_vat_for_interest_calc;
		$doctrans->discount_percent = $model->discount_percent;
		$doctrans->paid_status = $model->paid_status;
		$doctrans->id_user = $model->id_user;



		//swich case me keto
		//`ledgerentry`, `salesinvoice`, `purchaseinvoice`, `salesorder`, `purchaseorder`, `cashreceipt`, `cashpayment`

		switch ($model_name) {
			case 'ledgerentry':
				$doctrans->id_currency = $model->id_currency;
				$doctrans->id_docstatus = $model->id_docstatus;
				$doctrans->total_debit_or_paid = $model->total_debit;
				$doctrans->total_credit_or_cashed = $model->total_credit;
				$doctrans->home_cur_debit_or_paid = $model->home_cur_total_debit;
				$doctrans->home_cur_credit_or_cashed = $model->home_cur_total_credit;
				break;
			case 'cashpayment':
				$doctrans->id_currency = $model->id_currency;
				$doctrans->id_docstatus = $model->id_docstatus;
				$doctrans->total_debit_or_paid = $model->total_amount_payed;
				$doctrans->total_credit_or_cashed = $model->total_cashed;
				$doctrans->home_cur_debit_or_paid = $model->home_cur_total_amount_payed;
				$doctrans->home_cur_credit_or_cashed = $model->home_cur_total_cashed;
				break;
			case 'purchaseorder':
				$doctrans->id_docstatus = $model->processed;
				$doctrans->id_contact = $model->id_contact;
				$doctrans->contact_name = $model->contact_display_name;
				break;
			case 'cashreceipt':
				$doctrans->id_docstatus = $model->id_docstatus;
				$doctrans->id_currency = $model->id_currency;
				$doctrans->id_contact = $model->id_contact;
				$doctrans->id_contact = $model->id_supplier;
				break;
			case 'purchaseinvoice':
				$doctrans->id_contact = $model->id_contact;
				$doctrans->contact_name = $model->contact_display_name;
				break;
			case 'salesorder':
				$doctrans->id_contact = $model->id_contact;
				$doctrans->contact_name = $model->contact_display_name;
				break;
			case 'salesinvoice':
				$doctrans->id_contact = $model->id_contact;
				$doctrans->contact_name = $model->contact_display_name;
				break;
		}

		$doctrans->save();
	}

	static public function cancelDocumentTransaction($modelID)
	{

	}


	static function currencyName($id)
	{
		$currencies = \Cache::rememberForever('currencies/indexed', function()
		{
			$indexedCurrencies = array();

			$sql = "SELECT  id, name
                      FROM  currencies
                     WHERE deleted=0
                       AND active=1";
			$result = \DB::connection('core')->getPdo()->query($sql)->fetchAll(\PDO::FETCH_ASSOC);

			foreach($result as $currency)
			{
				$indexedCurrencies[$currency['id']] = $currency['name'];
			}
			return $indexedCurrencies;
		});

		return $currencies[$id];
	}

	static public function accountBalance($id_account, $subject=null, $subject_id=null)
	{
		$subject = $subject?:"account";
		$subject_id = $subject_id?:$id_account;
		$now = date("Y-m-d");
		$b_res = \DB::connection('acc')->select("CALL account_totals($id_account, '$now','{$subject}',{$subject_id});");
		$balance = new \stdclass();
		$balance->credit_home_cur = 0;
		$balance->debit_home_cur = 0;
		$balance->credit = 0;
		$balance->debit = 0;
		if(count($b_res)>0)
		{
			$balance->credit_home_cur = $b_res[0]->credit_home_cur_total;
			$balance->debit_home_cur = $b_res[0]->debit_home_cur_total;
			$balance->credit = $b_res[0]->credit_account_cur_total;
			$balance->debit = $b_res[0]->debit_account_cur_total;
		}
		return $balance;
	}

	static public function loadAccount($account_id, $getbalance=false)
	{
		$acc = \Accounting\Models\Account::findOrFail($account_id);
		if($acc)
		{
			$acc->code_name = $acc->code .' - '. $acc->name;
			if($getbalance)
			{
				$balance = $acc->balance();
				$acc->debit = $balance->debit;
				$acc->credit = $balance->credit;
				$acc->balance = abs($balance->debit - $balance->credit);
				$acc->r_balance = ($acc->account_type == 1?($balance->credit - $balance->debit): ($balance->debit-$balance->credit));
				if(($acc->account_type != 3) && ($acc->debit !=null) && ($acc->credit !=null))
				{
					if((($acc->account_type == 1) && ($acc->debit > $acc->credit)) || (($acc->account_type == 0) && ($acc->debit < $acc->credit)))
					{
						$notification = new \stdclass();
						$notification->section = 'Accounts';
						$notification->varname = 'abnormalBalance';
						$acc->notification = $notification;
					}
				}
			}
		}
		return $acc;
	}

	static public function loadContact($contact_id, $includeBalance=true)
	{
		$sql = "SELECT id,
					   contact_display_name AS display,
					   id_currency,
					   id_account,
					   id_commercialterms,
					   debit_limit_warning,
					   debit_limit_block
				  FROM contacts
				 WHERE id = :contactID
				 LIMIT 1";

		$statement = \AccUtils::db('r')->prepare($sql);
		$statement->bindValue(':contactID', $contact_id);
		$statement->execute();
		$contact = $statement->fetch(\PDO::FETCH_OBJ);

		if($includeBalance)
		{	$balance = \AccUtils::accountBalance($contact->id_account, "customer", $contact->id);
			$contact->account_balance = abs($balance->debit - $balance->debit);
		}

		return $contact;
	}

	static public function loadItem($item_id, $id_contact, $tran_type, $internally=true)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		try {
			$c_item = \Warehouse\Models\Item::findOrFail($item_id);

			$item = new \stdClass();
			$item->id = $c_item->id;
			$item->code = $c_item->code;
			$item->name = $c_item->name;
			$item->include_in_interest_calc = $c_item->include_in_interest_calc;
			//$r_item->tran_type = $tran_type;

			//nese nuk eshte objekt inventari nuk ka llogari blerje
			if($c_item->idpurchase_account==-1 || $c_item->id_itemkind==2 || $c_item->id_itemkind==5 || $c_item->id_itemkind==6)
			{
				$item->idcost_account = $c_item->idcost_account;
				$item->idpurchase_account = -1;
			}
			else
			{
				$item->idcost_account = $c_item->idcost_account;
				$item->idpurchase_account = $c_item->idpurchase_account;
			}
			$item->idsale_account = $c_item->idsale_account;
			$item->idsale_discount_account_with_minus = $c_item->idsale_discount_account_with_minus;
			$item->idpurchase_discount_account = $c_item->idpurchase_discount_account;
			$item->idsale_discount_account = $c_item->idsale_discount_account;
			//$item->{$tran_type.'_description'} = $c_item->{$tran_type.'_description'};

			$item->price = 0;
			if($tran_type == 'sale')
			{
				$item->description = $c_item->sale_description;
				$item_price = \AccUtils::loadItemPrice($item->id, $id_contact);
				if($item_price['found'] && $item_price['formula'] == false)
				{
					$item->price = $item_price['price'];
				}
				else
				{
					$sql = "SELECT il.value
							  FROM itempricelevels il
							 INNER JOIN items i
							    ON (il.id_item = i.id)
							 WHERE il.id_item = :itemID
							   AND il.deleted = 0
							   AND il.level = 1
							 LIMIT 1";

					$statement = \DB::connection('acc')->getPdo()->prepare($sql);
					$statement->bindValue(':itemID', $item->id);
					$statement->execute();
					$itempricelevel = $statement->fetch(\PDO::FETCH_OBJ);

					if($itempricelevel)
						$item->price = $itempricelevel->value;
				}
			}
			else
			{
				if($tran_type == 'purchase')
					$item->description = $c_item->purchase_description;

				$item->price = $c_item->price;
			}


			// Per kontrollin e sasise maximale - 27.01.2014
			$coreDBName = \DB::getDatabaseName();
			$sql = "SELECT IFNULL(i.max_quantity, 0) AS max_quantity, IFNULL(SUM(progressive_quantity), 0) AS totquantity
					  FROM warehousedistinctitems wdi
					 INNER JOIN warehouse w ON (wdi.id_warehouse = w.id)
					 INNER JOIN items i ON (wdi.id_item = i.id)
					 INNER JOIN itemgroups g ON (i.id_itemgroup = g.id)
					 INNER JOIN ".$coreDBName.".units un ON wdi.id_unit = un.id
					 WHERE w.deleted = 0 AND i.deleted = 0 AND i.id = ".$item->id."
					 LIMIT 1";
			$result_max = \DB::connection('acc')->getPdo()->query($sql)->fetch(\PDO::FETCH_OBJ);

			if($result_max)
			{
				$item->max_quantity = $result_max->max_quantity;
				$item->totquantity = $result_max->totquantity;
			}/**/

			$item_units = \CoreUtils::subjectUnits($item->id);

			//duhen gjithashtu koeficentet e konvertimit nga njesia baze ne cdo njesi tjeter
			for($i=0; $i<count($item_units); $i++)
			{
				//$item->unit_price[$unit->idunitentry] = \CoreUtils::conversionCoefficient("item", $item->id, $unit->idunitentry, $c_item->id_unit);
				$item_units[$i]->price_ratio = \CoreUtils::conversionCoefficient("item", $item->id, $item_units[$i]->idunitentry, $c_item->id_unit);
			}
			$default_unit = new \stdclass();
			$du = \Core\Models\Unit::find($c_item->id_unit, ['symbol', 'fullunit']);
			$default_unit->idunitentry = $c_item->id_unit;
			$default_unit->unitentry_symbol = $du->symbol;
			$default_unit->unitentry_fullunit = $du->fullunit;
			$default_unit->price_ratio = 1;
			array_unshift($item_units, $default_unit);

			$item->id_unit = $c_item->id_unit;
			/*  TODO:
				TO BE REVISED
				$item->unit_name = $default_unit->unitentry_symbol;
				$item->unitentry_fullunit = $default_unit->unitentry_fullunit;
				$item->sales_id_unit = $c_item->sales_id_unit;
				$item->sales_unit = $c_item->sales_unit;
				$item->purchase_id_unit = $c_item->purchase_id_unit;
				$item->purchase_unit = $c_item->purchase_unit;
			*/
			$item->id_currency =  $c_item->id_currency;
			$item->exchange_rate =  1;
			$item->id_itemkind = $c_item->id_itemkind;
			$item->id_tax = $c_item->id_tax;
			$item->cost = 0;//kosto duhet te zhvillohet me marrjen e kostos sipas llogaritjes

			//Marrim Lot-et per kete Artikull - 14.02.2014
			/*$lots = \DB::connection('acc')->table('lot')->where('deleted', 0)->where('active', 1)
								->where('id_item', $item->id)->get();
			$item->lots = $lots;*/

			$response = ['item'=>$item, 'units'=>$item_units];
			$statusCode = 200;
		}
		catch (\Exception $e)
		{
			info($e->getLine(), [$e->getCode(), $e->getMessage(), $e->getFile()]);
			info($e->getTraceAsString());

			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		if($internally)
			return ['response'=>$response, 'code'=>$statusCode];

		return response()->json($response, $statusCode);
	}


	static public function loadItemPrice($id_item, $id_contact, $internally=true)
	{
		$response = ['found'=>false];

		$sql = "SELECT il.value
				  FROM itempricelevels il
				 INNER JOIN customer_itempricelevel cil
					ON (il.id = cil.id_itempricelevel
						AND cil.id_contact = :contactID AND cil.deleted = 0)
				 WHERE il.id_item = :itemID
				   AND il.deleted = 0
				 LIMIT 1";

		$statement = \DB::connection('acc')->getPdo()->prepare($sql);
		$statement->bindValue(':contactID', $id_contact);
		$statement->bindValue(':itemID', $id_item);
		$statement->execute();
		$itempricelevel = $statement->fetch(\PDO::FETCH_OBJ);

		if($itempricelevel)
		{
			if(!is_numeric($itempricelevel->value))
			{
				$response['formula'] = true;
				$response['id_formula'] = $itempricelevel->id_formula;
			}
			else
			{
				$response['formula'] = false;
				$response['price'] = $itempricelevel->value;
			}

			$response['found'] = true;
		}

		if($internally)
			return $response;

		return response()->json($response);
	}

/*
	static public function loadAccountFromCode($code)
	{
		$acc = $this->reg->init('account', $this->accounting);
		$acc = $acc->load("code = '$code'");
		if(count($acc)>0)
		{
			$acc->code_name = $acc->code .' - '. $acc->name;
			return $acc;
		}
	}
*/
	static public function transaction($params)
	{
		$defaultParams = ["id_account"=>1,
						"transaction_type_name"=>'credit',
						"documenttype_name"=>'invoice',
						"id_documenttype"=>1,
						"document_id"=>1,
						"document_number"=>null,
						"id_currency"=>null,
						"exchange_rate"=>1,
						"transaction_date"=>'2015-01-15',
						"total"=>0,
						"description"=>'transaction',
						"subject"=>null,
						"subject_id"=>null,
						"account_cur_exchange_rate"=>null,
						"account_cur_total"=>-1,
						];

		$params = array_merge($defaultParams, $params);

		$transaction = new \Accounting\Models\Transaction();
		$transaction->id_account = $params['id_account'];
		$transaction->transaction_type_name = $params['transaction_type_name'];
		$transaction->transaction_status = 0;
		//dokumenti qe justifikon kete veprim
		$transaction->documenttype_name = $params['documenttype_name'];
		$transaction->id_documenttype = $params['id_documenttype'];
		$transaction->document_id = $params['document_id'];

		if($params['document_number'] == null)
			$params['document_number'] = $params['document_id'];
		$transaction->document_number = $params['document_number'];

		if($params['subject']==null)
			$params['subject'] = 'account';
		if($params['subject_id']==null)
			$params['subject_id'] = $transaction->id_account;

		$transaction->subject = $params['subject'];
		$transaction->subject_id = $params['subject_id'];

		if($params['id_currency']==null)
		{
			$transaction->id_currency = \CoreUtils::homeCurrency('id');
			$transaction->currency = \CoreUtils::homeCurrency('name');
			$transaction->exchange_rate = 1;
		}
		else
		{
			$transaction->id_currency = $params['id_currency'];
			$transaction->exchange_rate = $params['exchange_rate'];
			$transaction->currency = \CoreUtils::currencyName($transaction->id_currency);
		}
		$transaction->total = $params['total'];
		$transaction->transaction_date = $params['transaction_date'];
		$transaction->description = $params['description'];

		$acc = \AccUtils::loadAccount($transaction->id_account);
		//echo "id_account = {$transaction->id_account} || account=> \r\n";

		$transaction->parent_code = $acc->parent_code;
		$transaction->code = $acc->code;
		$transaction->id_accountgroup = $acc->id_accountgroup;
		$transaction->account_id_currency = $acc->id_currency;

		$transaction->name = $acc->name;
		$transaction->active_pasive = $acc->active_pasive;
		$transaction->revenue_expenditure = $acc->revenue_expenditure;

		//Credit/Debit	1/0
		$transaction->transaction_type_id = $transaction->transaction_type_name=="credit"?1:0;
		if($transaction->account_id_currency != $transaction->id_currency)
		{
			$transaction->account_currency = \CoreUtils::currencyName($transaction->account_id_currency);
		}else
			$transaction->account_currency = $transaction->currency;

		if($params['account_cur_exchange_rate'] == null || ($acc->id_currency == $transaction->id_currency))
			$params['account_cur_exchange_rate'] = 1;
		$transaction->account_cur_exchange_rate = $params['account_cur_exchange_rate'];

		if($params['account_cur_total'] > -1)
			$transaction->account_cur_total = $params['account_cur_total'];
		else
			$transaction->account_cur_total = $transaction->total / $transaction->account_cur_exchange_rate;

		$transaction->home_cur_total = $transaction->total * $transaction->exchange_rate;
		$transaction->transaction_system_date = date('Y-m-d H:i:s');
		$transaction->id_user = 1;//$this->sessionSite->user->user_id;
		$transaction->save();
	}

/*
	function transactionDifference(
		$params = array("id_account"=>1,
						"transaction_type_name"=>'credit',
						"documenttype_name"=>'invoice',
						"id_documenttype"=>1,
						"document_id"=>1,
						"document_number"=>'0005',
						"id_currency"=>2,
						"exchange_rate"=>140,
						"transaction_date"=>'2012-03-15',
						"total"=>30,
						"description"=>'test transaction',
						"subject"=>null,
						"subject_id"=>null,
						"account_cur_exchange_rate"=>1
						))
	{
		$resp = new stdclass();
		$resp->error = 1;
		$resp->section = "Forms";
		$resp->varname = "transactionFailed";
		//
		$id_account = $params['id_account'];
		$transaction_type_name = $params['transaction_type_name'];
		$transaction_type_id = (($transaction_type_name=='debit')?0:1);
		$documenttype_name = $params['documenttype_name'];
		$id_documenttype = $params['id_documenttype'];
		$document_id = $params['document_id'];
		$document_number = $params['document_number'];
		$id_currency = $params['id_currency'];
		$exchange_rate = $params['exchange_rate'];
		$total = $params['total'];
		$description = $params['description'];
		$subject = $params['subject'];
		$subject_id = $params['subject_id'];
		$account_cur_exchange_rate = $params['account_cur_exchange_rate'];
		$transaction_date = $params['transaction_date'];
		$transaction_identifier = "{$document_id}|{$documenttype_name}";
		try
		{
			$currencyCRUD = $this->reg->init('currencyCRUD');
			$transaction = $this->reg->init('transaction', $this->accounting);
			//GJEJME SHUMEN E VLERAVE TE VEPRIMEVE NE KETE LLOGARI QE LIDHEN ME KETE DOKUMENT
			$sql = "SELECT id_account, document_id, id_documenttype, transaction_identifier
						, SUM(total) AS total
					FROM transaction
					WHERE id_account={$id_account} AND id_documenttype={$id_documenttype}
						AND transaction_type_id = {$transaction_type_id} AND transaction_status=0
						AND transaction_identifier='{$transaction_identifier}'
						AND transaction_date = '{$transaction_date}';";
			$rows = $this->accounting->db->executeQuery($sql);
			//echo $sql."\r\n";
			$count_rows = count($rows);
			if($count_rows>0)
			{
				$t = $rows[0];
				$dif = $total - $t["total"];
				$dif = round($dif,4);
				//NESE VLERA E RE ESHET E NDRYSHME NGA TOTALI I TE GJITHA VLERAVE QE JANE REGJITRUAR
				//ME KOHEN PER KETE ARSYE PER KETE DOKUMENT NE KETE LLOGARI ATHERE
				//RUAJ VETEM DIFERENCEN NE MENYRE QE TOTALI I RI TE JETE I BARABARTE ME VLEREN E RE
				if($dif != 0)
				{
					$params['total'] = $dif;
					$resp = $this->transaction($params);
					if($resp->error != 0)
						throw new Exception();
				}
			}
			//
			$resp->error=0;
			$resp->section = "Forms";
			$resp->varname = "successfullySaved";
			return $resp;
		}
		catch(Exception $e)
		{
			//print_r($e);
			return $resp;
		}
	}
*/

	static public function cancelLedgerEntry($transaction_identifier)
	{
		$transactions = Transaction::where('transaction_identifier', $transaction_identifier)
									->where('transaction_status', 0)
									->get();

		if(count($transactions) == 0)
			throw new \Exception($response = 'ACC_SERVER.CANCELING_LEDGER_NOT_FOUND', $statusCode = 500);

		$dt = date('Y-m-d H:i:s');
		foreach($transactions as $transaction)
		{
			$transaction->transaction_status = 1;
			$transaction->save();

			$trn = new Transaction();
			$trn->transaction_type_name = ($transaction->transaction_type_name=="credit"?"debit":"credit");
			$trn->transaction_type_id = $transaction->transaction_type_name=="credit"?1:0;
			$trn->description = "Anullim i veprimit [".$transaction->description.']';
			$trn->transaction_system_date = $dt;
			$trn->transaction_status = 2;
			$trn->save();
		}
	}

}