<?php

/*
|--------------------------------------------------------------------------
| Accounting Routes
|--------------------------------------------------------------------------
*/

Route::get('accounts/browse',           'AccountsController@browse');
Route::get('accounts/load/{type?}',     'AccountsController@load')->where('type', '[0-2]');
Route::get('accounts/grid',            	'AccountsController@grid');
Route::resource('accounts',			    'AccountsController');
Route::get('accountgroups/load',        'AccountgroupsController@load');
Route::resource('accountgroups',	    'AccountgroupsController');

Route::get('cash/load/{type_id}',       'CashController@load');
Route::get('cash/browse',               'CashController@browse');
Route::resource('cash',				    'CashController');
Route::resource('cashreceipts',			'CashReceiptsController');

Route::get('commercialterms/load',      'CommercialtermsController@load');
Route::resource('commercialterms',	    'CommercialtermsController');
Route::get('contacts/load/{type_id}',  	'ContactsController@load');
Route::get('contacts/browse',          	'ContactsController@browse');
Route::get('contacts/grid',             'ContactsController@grid');
Route::resource('contacts',             'ContactsController');

Route::get('documentbatches/create/{documentgroup}/{type}', 'DocumentbatchesController@create');
Route::resource('documentbatches',   	    'DocumentbatchesController');
Route::any('docstransactions/load/{doc?}',  'DocstransactionsController@load');
Route::get('docstransactions/grid',         'DocstransactionsController@grid');
Route::get('documentgroups/load',           'DocumentgroupsController@load');
Route::get('documentgroups/grid',           'DocumentgroupsController@grid');
Route::resource('documentgroups',           'DocumentgroupsController');

Route::get('financialstatementpositions/load',  'FinancialStatementPositionsController@load');
Route::resource('financialstatementpositions',  'FinancialStatementPositionsController');

Route::resource('ledgerentries',		'LedgerEntriesController');

Route::resource('purchaseinvoices',		'PurchaseinvoicesController');
Route::resource('purchaseorder',		'PurchaseorderController');

/* REPORTS */
Route::controller('reports/account', 	'ReportsAccountController');
/* 
Route::controller('reports/cash-bank', 	'ReportsCashBankController');
Route::controller('reports/sales', 		'ReportsSalesController');
Route::controller('reports/purchase',	'ReportsPurchaseController');
Route::controller('reports/docs',		'ReportsDocumentController');
Route::controller('reports/maturiry',	'ReportsMaturityController');
Route::controller('reports/warehouse',	'ReportsWarehouseController');
*/

Route::resource('receivedoffers',		'ReceivedoffersController');
Route::resource('requestoffers',		'RequestoffersController');

Route::resource('salesinvoices',		'SalesInvoicesController');
Route::resource('salesorder',			'SalesorderController');

Route::get('taxes/load',	            'TaxesController@load');
Route::resource('taxes',	            'TaxesController');
Route::get('transactions/grid',         'TransactionsController@grid');
Route::resource('transactions',         'TransactionsController');

Route::resource('entrywarehouse',       'EntrywarehouseController');

Route::resource('exitwarehouse',       'ExitwarehouseController');





