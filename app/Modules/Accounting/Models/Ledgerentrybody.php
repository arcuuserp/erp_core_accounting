<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Ledgerentrybody extends Model {

	protected $table = 'ledgerentrybody';
	protected $connection = 'acc';
	public $timestamps = false;

}
