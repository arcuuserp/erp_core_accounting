<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Commercialterms extends Model {

	protected $table = 'commercialterms';
	protected $connection = 'acc';
	public $timestamps = false;

}
