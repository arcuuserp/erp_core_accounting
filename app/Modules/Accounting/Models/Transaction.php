<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

	protected $table = 'transaction';
	protected $connection = 'acc';
	public $timestamps = false;

}
