<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Financialstatementposition extends Model {

	protected $table = 'financialstatementpositions';
	protected $connection = 'acc';
	public $timestamps = false;

}
