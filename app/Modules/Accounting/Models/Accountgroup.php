<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Accountgroup extends Model {

	protected $table = 'accountgroups';
	protected $connection = 'acc';
	public $timestamps = false;

}
