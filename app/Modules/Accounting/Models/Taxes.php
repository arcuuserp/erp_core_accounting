<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Taxes extends Model {

	protected $table = 'taxes';
	protected $connection = 'acc';
	public $timestamps = false;

}
