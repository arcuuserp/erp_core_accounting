<?php namespace Accounting\Models;

use Accounting\Models\Documentbatch;
use Illuminate\Database\Eloquent\Model;

class Documentgroup extends Model {

	protected $table = 'documentgroups';
	protected $connection = 'acc';
	public $timestamps = false;

	
	/*************************************************************************************************	
	*	on manual numbers suggest next increased number
	*	manual numbers can be mixed with letters and special chars therefore
	*	first find the numbers section, then increase its value as a suggestion
	*	position translation:  0 => no incrementation suggestion
	*			   		positive => number group/section found starting from the left side 
	*			   		negative => number group/section found starting from the right side 
	**************************************************************************************************/ 
	//public $next_doc_number_suggest_pos = 0;//0=no increment, negative=right-index, possitive=left-index
	//public $next_serial_suggest_pos = 0;//0=no increment, negative=right-index, possitive=left-index
	
	/****************** regex explanation ***********************
	 *	split number from letters (?=\d) (?=[a-z])
	 *	or
	 *	split on special chars "-_/,\" = [\-_\/,\\\]
	 */		
	//preg_split('/(?<=\d)(?=[a-z])|(?<=[a-z])(?=\d)|(?<=[\-_\/,\\\])(?=\d)|(?<=\d)(?=[\-_\/,\\\])|(?<=[\-_\/,\\\])(?=[a-z])|(?<=[a-z])(?=[\-_\/,\\\])/i', "MA-0982-0/9_90\ioo,66")
	
	public function nextNumber($docNumValue, $serialValue)
	{
		$resp = new \stdclass();
		$resp->error = 0;
		//$resp->varname = '';
		//$resp->section = 'Documents';

		$resp->next_doc_number = '';
		$resp->next_serial = '';

		if($this->next_doc_number_suggest_pos != 0 && $docNumValue != null && $docNumValue != '')
		{
			$splitNumbersArray = preg_split('/(?<=\d)(?=[a-z])|(?<=[a-z])(?=\d)|(?<=[\-_\/,\\\])(?=\d)|(?<=\d)(?=[\-_\/,\\\])|(?<=[\-_\/,\\\])(?=[a-z])|(?<=[a-z])(?=[\-_\/,\\\])/i', $docNumValue);

			$arrayLength = count($splitNumbersArray);
			if($arrayLength == 1)
			{
				$resp->next_doc_number = $splitNumbersArray[0] + 1;
			}
			else if($arrayLength > 1)
			{
				$numberIndex = 0;
				$lastNumberFound = -1;
				$numberFound = false;
				if($this->next_doc_number_suggest_pos < 0)//reverse array
				{
					$pos = $this->next_doc_number_suggest_pos * -1;
					for($i=$arrayLength-1; $i>=0; $i--)
					{
						if(is_numeric($splitNumbersArray[$i]))
						{
							if($lastNumberFound == -1)
								$lastNumberFound = $i;

							$numberIndex++;
							if($pos == $numberIndex)
							{
								$splitNumbersArray[$i] = $splitNumbersArray[$i]+1;
								$resp->next_doc_number = implode($splitNumbersArray);
								$numberFound = true;
								break;
							}
						}
					}
				}
				else
				{
					for($i=0;$i<$arrayLength;$i++)
					{
						if(is_numeric($splitNumbersArray[$i]))
						{
							$lastNumberFound = $i;
							$numberIndex++;
							if($this->next_doc_number_suggest_pos == $numberIndex)
							{
								$splitNumbersArray[$i] = $splitNumbersArray[$i]+1;
								$resp->next_doc_number = implode($splitNumbersArray);
								$numberFound = true;
								break;
							}
						}
					}
				}

				if(!$numberFound && $lastNumberFound > -1)
				{
					$splitNumbersArray[$lastNumberFound] = $splitNumbersArray[$lastNumberFound]+1;
					$resp->next_doc_number = implode($splitNumbersArray);
				}
			}
		}

		if($this->next_serial_suggest_pos != 0 && $serialValue != null && $serialValue != '')
		{
			$splitNumbersArray = preg_split('/(?<=\d)(?=[a-z])|(?<=[a-z])(?=\d)|(?<=[\-_\/,\\\])(?=\d)|(?<=\d)(?=[\-_\/,\\\])|(?<=[\-_\/,\\\])(?=[a-z])|(?<=[a-z])(?=[\-_\/,\\\])/i', $serialValue);

			$arrayLength = count($splitNumbersArray);
			if($arrayLength == 1)
			{
				$resp->next_serial = $splitNumbersArray[0] + 1;
			}
			else if($arrayLength > 1)
			{
				$numberIndex = 0;
				$lastNumberFound = -1;
				$numberFound = false;
				if($this->next_serial_suggest_pos < 0)//reverse array
				{
					$pos = $this->next_serial_suggest_pos * -1;
					for($i=$arrayLength-1; $i>=0; $i--)
					{
						if(is_numeric($splitNumbersArray[$i]))
						{
							if($lastNumberFound == -1)
								$lastNumberFound = $i;

							$numberIndex++;
							if($pos == $numberIndex)
							{
								$splitNumbersArray[$i] = $splitNumbersArray[$i]+1;
								$resp->next_serial = implode($splitNumbersArray);
								$numberFound = true;
								break;
							}
						}
					}
				}
				else
				{
					for($i=0;$i<$arrayLength;$i++)
					{
						if(is_numeric($splitNumbersArray[$i]))
						{
							$lastNumberFound = $i;
							$numberIndex++;
							if($this->next_serial_suggest_pos == $numberIndex)
							{
								$splitNumbersArray[$i] = $splitNumbersArray[$i]+1;
								$resp->next_serial = implode($splitNumbersArray);
								$numberFound = true;
								break;
							}
						}
					}
				}

				if(!$numberFound && $lastNumberFound > -1)
				{
					$splitNumbersArray[$lastNumberFound] = $splitNumbersArray[$lastNumberFound]+1;
					$resp->next_serial = implode($splitNumbersArray);
				}
			}
		}

		return $resp;
	}

	public function generate($model=null, $internalSystemGeneration=false, $allowDiffDateDuplication=false, $updateGeneratedNumbers=false)
	{
		$resp = new \stdclass();
		$resp->error = 0;
		$resp->doc_tbl = $this->document_table;
		$response = '';
		$arr['from_date'] = 'desc';
		$doc_count_total = -1;

		if($model!=null)
		{
			$model_name = $model->getTable();
			$pk_value = $model->id;
		}

		if($internalSystemGeneration == true)
		{
			$doc_count_total = \AccUtils::db()->table($this->document_table)->count();
			$now = time();
			$a_z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$rand_letter = $a_z[rand(0,25)].$a_z[rand(0,25)];
			$resp->serial_number = $this->symbol."-".$now.$rand_letter."-".($doc_count_total+1);
		}
		else if($this->manual_serial==0)//0=auto, 1=manual
		{
			$query = 'SELECT *
						FROM documentbatches
					   WHERE deleted = 0
					     AND type = 1
					     AND id_documentgroup = '.$this->id.'
					     AND (NOW() BETWEEN from_date AND till_date)
					   ORDER BY from_date DESC
					   LIMIT 1';
			$documentbatch = \AccUtils::db('r')->query($query)->fetch(\PDO::FETCH_OBJ);

			if(!$documentbatch)
			{
				$resp->error = 1;
				throw new \Exception($response = 'DOCUMENT_GROUP.NO_SERIALS_BATCH_AVAILABLE', $statusCode = 412);
			}
			else
			{
				//more than one batch has been defined, find only docs using serials from this batch
				$doc_count_total = \AccUtils::db()->table($this->document_table)->count();
				if(!$updateGeneratedNumbers)
					$doc_count_total++;
				//echo $documentbatch->to_number." - ".$documentbatch->from_number." - ".$documentbatch->current." \r\n";
				if($documentbatch->to_number >= $documentbatch->current)
				{
					$pad = $documentbatch->digits;

					$delimiters = array(' ',',','-','_','/','\\');
					$arr_symbols = array();
					$arr_symbols['SYM'] = $this->symbol;
					$arr_symbols['INC'] = $doc_count_total;
					$arr_symbols['SERIAL'] = str_pad(strval(($documentbatch->current==0?$documentbatch->from_number:$documentbatch->current) + 1), $pad, "0", STR_PAD_LEFT);
					$arr_symbols['NUMBER'] = str_pad(strval(($documentbatch->current==0?$documentbatch->from_number:$documentbatch->current) + 1), $pad, "0", STR_PAD_LEFT);

					$used_symbols = \CoreUtils::explodeX($delimiters, $this->serial_format);
					$symbol_values = array();

					foreach($used_symbols as $used_symbol)
					{
						if(isset($arr_symbols[strtoupper($used_symbol)]))
						{
							$symbol_values[$used_symbol] = $arr_symbols[strtoupper($used_symbol)];
							//replace symbol with value now
							$pattern="/\b$used_symbol\b/";
							$this->serial_format = preg_replace($pattern , $symbol_values[$used_symbol], $this->serial_format);
						}
					}
					$resp->serial_number = $this->serial_format;

					Documentbatch::where('id', $documentbatch->id)
						->update(['current' => ($documentbatch->current==0?$documentbatch->from_number:$documentbatch->current) + 1]);
				}
			}
		}
		else if($model != null && $this->allow_serial_duplication_notify != 1)//0=dont allow duplication, 1=allow, 2=allow on confirmation
		{
			$sql = "SELECT 1 FROM $model_name
					 WHERE serial_number = :manual_serial ";

			if($updateGeneratedNumbers == true)
				$sql .= " AND id <> $pk_value ";
			if($allowDiffDateDuplication == true)
				$sql .= " AND document_date = :document_date ";

			$sql .= " LIMIT 1";
			$statement = \AccUtils::db('r')->prepare($sql);
			$statement->bindValue(':manual_serial', $model->serial_number);

			if($allowDiffDateDuplication == true)
				$statement->bindValue(':document_date', $model->document_date);

			$statement->execute();
			$result = $statement->fetch(\PDO::FETCH_ASSOC);

			if($result)
			{
				$resp->error = 3;

				if($allowDiffDateDuplication == true)
				{
					throw new \Exception($response = 'DOCUMENT_GROUP.DUPLICATE_SERIAL_SAME_DATE', $statusCode = 412);
				}

				throw new \Exception($response = 'DOCUMENT_GROUP.DUPLICATE_MANUAL_DOC_SERIAL', $statusCode = 412);
			}
			$resp->serial_number = $model->serial_number;
		}
		else
		{
			$resp->serial_number = $model->serial_number;
		}

		if($internalSystemGeneration)
		{
			//return the save system generate number as serial_number
			$resp->document_number = $resp->serial_number;
		}
		else if($this->manual_number==0)//0=auto, 1=manual
		{
			$query = "SELECT *
						FROM documentbatches
					   WHERE deleted = 0
					     AND type = 0
					     AND id_documentgroup = ".$this->id."
					     AND (NOW() BETWEEN from_date AND till_date)
					   ORDER BY from_date DESC
					   LIMIT 1";
			$documentbatch = \AccUtils::db('r')->query($query)->fetch(\PDO::FETCH_OBJ);

			if(!$documentbatch)
			{
				$resp->error = 2;
				throw new \Exception($response = 'DOCUMENT_GROUP.NO_DOC_NUMBER_BATCH_AVAILABLE', $statusCode = 412);
			}
			else
			{
				if($doc_count_total==-1)
				{
					$doc_count_total = \AccUtils::db()->table($this->document_table)->count();
					if(!$updateGeneratedNumbers)
						$doc_count_total++;
				}
				if($documentbatch->to_number >= $documentbatch->current)
				{
					$pad = $documentbatch->digits;

					$delimiters = array(' ',',','-','_','/','\\');
					$arr_symbols = array();
					$arr_symbols['SYM'] = $this->symbol;
					$arr_symbols['INC'] = $doc_count_total;
					$arr_symbols['SERIAL'] = str_pad(strval(($documentbatch->current==0?$documentbatch->from_number:$documentbatch->current) + 1), $pad, "0", STR_PAD_LEFT);
					$arr_symbols['NUMBER'] = str_pad(strval(($documentbatch->current==0?$documentbatch->from_number:$documentbatch->current) + 1), $pad, "0", STR_PAD_LEFT);

					$used_symbols = \CoreUtils::explodeX($delimiters, $this->number_format);
					$symbol_values = array();
					foreach($used_symbols as $used_symbol)
					{
						if(isset($arr_symbols[strtoupper($used_symbol)]))
						{
							$symbol_values[$used_symbol] = $arr_symbols[strtoupper($used_symbol)];
							//replace symbol with value now
							$pattern="/\b$used_symbol\b/";
							$this->number_format = preg_replace($pattern , $symbol_values[$used_symbol], $this->number_format);
						}
					}
					$resp->document_number = $this->number_format;

					Documentbatch::where('id', $documentbatch->id)
						->update(['current' => ($documentbatch->current==0?$documentbatch->from_number:$documentbatch->current) + 1]);
				}
			}
		}
		else if($model != null && $this->allow_number_duplication_notify != 1)//0=dont allow duplication, 1=allow, 2=allow on confirmation
		{
			$sql = "SELECT 1 FROM $model_name
					 WHERE document_number = :manual_doc_number ";

			if($updateGeneratedNumbers == true)
				$sql .= " AND id <> $pk_value ";
			if($allowDiffDateDuplication == true)
				$sql .= " AND document_date = :document_date ";

			$sql .= " LIMIT 1";
			$statement = \AccUtils::db('r')->prepare($sql);
			$statement->bindValue(':manual_doc_number', $model->document_number);

			if($allowDiffDateDuplication == true)
				$statement->bindValue(':document_date', $model->document_date);

			$statement->execute();
			$result = $statement->fetch(\PDO::FETCH_ASSOC);

			if($result)
			{
				$resp->error = 4;

				if($allowDiffDateDuplication == true)
					throw new \Exception($response = 'DOCUMENT_GROUP.DUPLICATE_DOC_NUMBER_SAME_DATE', $statusCode = 412);

				throw new \Exception($response = 'DOCUMENT_GROUP.DUPLICATE_MANUAL_DOC_NUMBER', $statusCode = 412);
			}
			$resp->document_number = $model->document_number;
		}
		else
		{
			$resp->document_number = $model->document_number;
		}

		if($updateGeneratedNumbers && $model != null && $resp->error == 0 && ($this->manual_serial==0 || $this->manual_number==0))//update only if auto generated
		{
			\AccUtils::db()->table($model_name)->where('id', $pk_value)
						->update(['serial_number' => $resp->serial_number, 'document_number' => $resp->document_number]);
		}

		if($model!=null)
		{
			$model->document_number = $resp->document_number;
			$model->serial_number = $resp->serial_number;
		}
		return $resp;
	}
	
}
