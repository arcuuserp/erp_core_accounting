<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

	protected $table = 'contacts';
	protected $connection = 'acc';
	public $timestamps = false;

}
