<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Salesorder extends Model {

	protected $table = 'salesorder';
	protected $connection = 'acc';
	public $timestamps = false;


    /*
	Debi llogaria 411	klienti	vlera eshte totali I vleres se artikullit + TVSH - zbritje nese ka	njesoj si furnitori ruan info per vleren ne monedhe baze dhe monedhe transaksioni + kursin e ketij transaksioni
	Debi		 4457	TVSH	llogaritet si norme e tvsh shumezim vleren e artikullit
	Kredi 		  705	Te ardhura	cmimi I shitjes	(idsale_account)
	*/
    public function itemsToGL($entries, $customer_id_account)
	{
		$aggregate_fn = ['field'=>'subtotal_no_vat', 'fn'=>'SUM'];

		$entries = \CoreUtils::array_group($entries, ['idsale_account'], [$aggregate_fn]);

		foreach($entries as $idsale_account=>$entry)
		{
			if($entry['subtotal_no_vat_SUM'] > 0)
			{
				\AccUtils::transaction(["id_account"=>$idsale_account,
										"transaction_type_name"=>'credit',
										"documenttype_name"=>'sales_invoice_art',
										"id_documenttype"=>3,
										"document_id"=>$this->id,
										"document_number"=>$this->document_number,
										"id_currency"=>$this->id_currency,
										"exchange_rate"=>$this->exchange_rate,
										"transaction_date"=>$this->document_date,
										"total"=>$entry['subtotal_no_vat_SUM'],
										"description"=>"Fature Shitje",
										"account_cur_exchange_rate" => (isset($entry['elements'][0]['exchange_rate'])?$entry['elements'][0]['exchange_rate']: $entry['elements'][0]['ci_exchange_rate'])
										]);
			}
		}
		//kreditimi tvsh-se
		//SAVE IN THE ACCOUNT FOUND IN PREFERENCES FOR THE VAT
		if($this->total_vat > 0)
		{
			$id_account_vat_on_sale = \CoreUtils::getSettings('acc_vat_on_sale');

			\AccUtils::transaction(["id_account"=>$id_account_vat_on_sale,
									"transaction_type_name"=>'credit',
									"documenttype_name"=>'sales_invoice_art',
									"id_documenttype"=>3,
									"document_id"=>$this->id,
									"document_number"=>$this->document_number,
									"id_currency"=>$this->id_currency,
									"exchange_rate"=>$this->exchange_rate,
									"transaction_date"=>$this->document_date,
									"total"=>$this->total_vat,
									"description"=>"Fature Shitje"
									]);
		}
		//debitojme klientin me totalin
		if($this->total_with_vat > 0)
		{
			\AccUtils::transaction(["id_account"=>$customer_id_account,
									"transaction_type_name"=>'debit',
									"documenttype_name"=>'sales_invoice_art',
									"id_documenttype"=>3,
									"document_id"=>$this->id,
									"document_number"=>$this->document_number,
									"subject"=>"customer",
									"subject_id"=>$this->id_customer,
									"id_currency"=>$this->id_currency,
									"exchange_rate"=>$this->exchange_rate,
									"transaction_date"=>$this->document_date,
									"total"=>$this->total_with_vat,
									"description"=>"Fature Shitje",
									"account_cur_exchange_rate" => $this->customer_cur_exchange_rate
									]);
		}
	}

}
