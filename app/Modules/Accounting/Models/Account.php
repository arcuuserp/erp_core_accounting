<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model {

	protected $table = 'accounts';
	protected $connection = 'acc';
	public $timestamps = false;

	public function balance()
	{
		return \AccUtils::accountBalance($this->id);
	}
}
