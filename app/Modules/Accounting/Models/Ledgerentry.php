<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Ledgerentry extends Model {

	protected $table = 'ledgerentry';
	protected $connection = 'acc';
	public $timestamps = false;
}
