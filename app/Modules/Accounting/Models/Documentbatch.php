<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Documentbatch extends Model {

	protected $table = 'documentbatches';
	protected $connection = 'acc';
	public $timestamps = false;

}
