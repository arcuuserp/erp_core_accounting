<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Docstransaction extends Model {

	protected $table = 'document_transactions';
	protected $connection = 'acc';
	public $timestamps = false;

}
