<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Documenttype extends Model {

	protected $table = 'documenttypes';
	protected $connection = 'acc';
	public $timestamps = false;

	/*
		1-purchase_invoice_art
		2-purchase_invoice_acc
		3-sales_invoice_art
		4-sales_invoice_acc
		48-payment_rate

		5-inventory_entry_purchase
		6-inventory_entry_warehouse
		7-inventory_entry_prod
		8-inventory_entry_acc
		46-inventory_entry_item

		9-inventory_exit_sales
		10-inventory_exit_warehouse
		11-inventory_exit_prod
		12-inventory_exit_acc
		47-inventory_exit_item

		13-opening_cash
		14-opening_bank
		15-opening_supplier
		16-opening_customer
		17-opening_inventory
		18-opening_account

		19-cash_receipt_invoices
		42-cash_receipt_accounts
		20-bank_receipt_invoices
		43-bank_receipt_accounts

		21-cash_payment_invoices
		44-cash_payment_accounts
		22-bank_payment_invoices
		45-bank_payment_accounts

		23-ledger_entry
		24-export_decl
		25-import_decl
		26-sales_order
		27-purchase_order
		28-proposed_offer
		29-received_offer
		30-return_purchase_invoice_art
		31-return_purchase_invoice_acc
		32-return_sales_invoice_art
		33-return_sales_invoice_acc
		34-sales_credit_note_art
		35-sales_credit_note_acc
		36-purchase_credit_note_acc
		37-purchase_credit_note_art
		38-inventory_adjustment
		39-exchange_rate_adjustment
		40-auto_exchange_rate_adjustment
		41-salary_accounting_entry
	*/

	static public function getDocumentType($system_name, $called_internally=false)
	{
		$resp = new stdclass();
		$resp->error = 1;
		$resp->varname = 'transactionFailed';
		$resp->section = 'Forms';

		try
		{
			$resp->error=0;
			$resp->data = Documenttype::where("system_name", $system_name)->firstOrFail();
		}
		catch(\Exception $e)
		{
			$resp->error = 1;
			$resp->varname = 'documentTypeNotFound';
			$resp->section = 'Accounting';
		}

		if(!$called_internally)
			echo json_encode($resp);
		else
			return $resp;
	}

}
