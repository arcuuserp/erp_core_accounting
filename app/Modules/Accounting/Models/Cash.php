<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class Cash extends Model {

	protected $table = 'cash';
	protected $connection = 'acc';
	public $timestamps = false;

}
