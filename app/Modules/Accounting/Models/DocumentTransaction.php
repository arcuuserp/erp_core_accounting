<?php namespace Accounting\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentTransaction extends Model {

	protected $table = 'document_transactions';
	protected $connection = 'acc';
	public $timestamps = false;

}
