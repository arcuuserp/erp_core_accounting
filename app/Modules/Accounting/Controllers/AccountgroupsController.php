<?php namespace Accounting\Controllers;

use Accounting\Models\Accountgroup;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AccountgroupsController extends Controller {


	public function load()//0=basics 1=with currency 2=not_linkedaccounts
	{
		$result = \Cache::rememberForever('accountgroups/load', function()
		{
			$sql = "SELECT  id,
							accountgroup_name
                      FROM  accountgroups
                      WHERE deleted=0";

			$statement = \AccUtils::db('r')->query($sql);
			return $statement->fetchAll(\PDO::FETCH_ASSOC);
		});

		return response()->json(['data'=>$result]);
	}

	public function browse(Request $request)
	{
		$ds = new \App\Libs\KendoUI\DataSource($request->all(), 'acc_read');//READ ONLY erp_acc_passive
		$properties = ['accountgroup_name'];

		$select = $ds->prepareColumns($properties);
		$cond = $ds->prepareFilters($properties, "AND");
		$sort = $ds->prepareSort($properties, true);

		$query = " FROM accountgroups WHERE deleted=0".$cond.$sort;//." LIMIT 20 ";
		$response['data'] = $ds->executeResult($query, $select, true);
		return response()->json($response);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
		$response = "";
		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();

		return response()->json($result);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();

		$result['model'] = Accountgroup::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$accountgroup = Accountgroup::findOrFail($request->get('id'));

				$nameExists = Accountgroup::where('accountgroup_name', $request->get('accountgroup_name'))
					->where('id', '<>', $accountgroup->id)
					->exists();

			}
			else
			{
				$accountgroup = new Accountgroup();
				$accountgroup->created_at = date("Y-m-d H:i:s");

				$nameExists = Accountgroup::where('accountgroup_name', $request->get('accountgroup_name'))
					->where('deleted', 0)
					->exists();

			}

			if($nameExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			//if($codeExists)
				//throw new \Exception($response = 'VALIDATOR.DUPLICATE_CODE', $statusCode = 412);

			$accountgroup->accountgroup_name = $request->get('accountgroup_name');
			$accountgroup->description = $request->get('description');
			$accountgroup->id_accountgroup = $request->get('id_accountgroup');
			$accountgroup->save();

			\Cache::flushTagDir('accountgroups');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			info($e->getLine());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
