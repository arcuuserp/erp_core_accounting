<?php namespace Accounting\Controllers;

use Accounting\Models\Documentbatch;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DocumentbatchesController extends Controller {

 
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
		$response = "";
		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($documentgroup, $type)
	{
		$result = array();
		$result['model'] = ['active'=>1, 'id_documentgroup'=>$documentgroup, 'type'=>$type];
		return response()->json($result);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();
		$result['model'] = Documentbatch::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$documentbatch = Documentbatch::findOrFail($request->get('id'));
			}
			else
			{
				$documentbatch = new Documentbatch();
			}

			$documentbatch->id_documentgroup = $request->get('id_documentgroup');
			$documentbatch->from_number = $request->get('from_number');
			$documentbatch->to_number = $request->get('to_number');
			$documentbatch->current = $request->get('current');
			$documentbatch->digits = $request->get('digits');
			$documentbatch->from_date = $request->get('from_date');
			$documentbatch->till_date = $request->get('till_date');
			$documentbatch->type = $request->get('type');
			$documentbatch->save();

			\Cache::flushTagDir('documentbatch');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			info($e->getLine());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
