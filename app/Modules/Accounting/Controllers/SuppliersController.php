<?php namespace Accounting\Controllers;

use Accounting\Models\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class SuppliersController extends Controller {


	public function load(Request $request)
	{
		$ds = new \App\Libs\KendoUI\DataSource($request->all(), 'acc_read');//READ ONLY erp_acc_passive

		//$ds->extraFilter('deleted', 0);
		//$ds->extraFilter('is_supplier', 1);//field, value, $type='number', operator='eq', logic='and'

		$properties = [
		        'contact_display_name',
		        'contact_code',
		    ];

		$select = $ds->prepareColumns($properties);
		$where = $ds->prepareFilters($properties, "AND");
		$sort = $ds->prepareSort($properties, true);

		$query = " FROM contacts WHERE deleted=0 AND is_supplier=1 ".$where.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);//true=debug
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}

	public function grid(Request $request)
	{
		$inputs = $request->all();
		$ds = new \App\Libs\KendoUI\DataSource($inputs, 'acc_read');//READ ONLY erp_acc_passive

		info('inputs', [$inputs]);

		$properties = [
		        'id_documentgroup',
		        'document_date' => array('type' => 'date'),
		        'description',
		        'document_number',
		        'currency',
		        'total_with_vat' => array('type' => 'number'),
		        'id_contact' => array('type' => 'number'),
		    ];

		$fieldMapping = null;/*array(
			'name'=>'currencies.name',
			'active'=>'currencies.active'
		);*/

		$select = $ds->prepareColumns($properties, $fieldMapping);
		$where = $ds->prepareFilters($properties, true, $fieldMapping);
		info($where );
		$sort = $ds->prepareSort($properties, true, $fieldMapping);

		$select .= ',subject';
		$query = " FROM document_transactions ".$where.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);//true=debug
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$result = array();
		return response()->json($result);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();
		$result['currencies'] = app()->make('\Core\Controllers\CurrenciesController')->activeCurrencies(true);

		return response()->json($result);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();
		$result['currencies'] = app()->make('\Core\Controllers\CurrenciesController')->activeCurrencies(true);

		$result['model'] = Contact::find($id);
		return response()->json($result);
	}
	

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$contact = Contact::findOrFail($request->get('id'));

				$nameExists = Contact::where('company_name', $request->get('company_name'))
					->where('id', '<>', $contact->id)
					->exists();

				$codeExists = Contact::where('contact_firstname', $request->get('contact_firstname'))
					->where('id', '<>', $contact->id)
					->exists();
			}
			else
			{
				$contact = new Contact();
				$contact->created_at = date("Y-m-d H:i:s");

				$nameExists = Contact::where('company_name', $request->get('company_name'))
					->where('deleted', 0)
					->exists();

				$codeExists = Contact::where('contact_firstname', $request->get('contact_firstname'))
					->where('deleted', 0)
					->exists();
			}

			if($nameExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			if($codeExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);

			$contact->contact_firstname = $request->get('contact_firstname');
			$contact->contact_lastname = $request->get('contact_lastname');
			$contact->contact_code = $request->get('contact_code');
			$contact->company_name = $request->get('company_name');
			$contact->contact_display_name = $request->get('contact_display_name');
			$contact->phonenumber = $request->get('phonenumber');
			$contact->email = $request->get('email');
			$contact->address = $request->get('address');
			$contact->city = $request->get('city');
			$contact->zip_code = $request->get('zip_code');
			$contact->district = $request->get('district');
			$contact->country = $request->get('country');
			$contact->id_currency = $request->get('id_currency');
			$contact->default_itempricelevel = $request->get('default_itempricelevel');
			$contact->id_commercialterms = $request->get('id_commercialterms');
			$contact->opening_balance = $request->get('opening_balance');
			$contact->opening_date = $request->get('opening_date');
			$contact->company_name = $request->get('company_name');
			$contact->company_registration_code = $request->get('company_registration_code');
			$contact->company_admin_namesurname = $request->get('company_admin_namesurname');
			$contact->id_organisationform = $request->get('id_organisationform');
			$contact->company_address = $request->get('company_address');
			$contact->company_contact_person = $request->get('company_contact_person');
			$contact->company_phonenumber = $request->get('company_phonenumber');
			$contact->fax = $request->get('fax');
			$contact->company_email = $request->get('company_email');
			$contact->account = $request->get('account');
			$contact->save();

			\Cache::flushTagDir('contact');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			info($e->getLine());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return response()->json(Contact::find($id));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
