<?php namespace Accounting\Controllers;

use Accounting\Models\Docstransaction;
use App\Http\Controllers\Controller;

use Core\Models\Currency;
use Illuminate\Http\Request;

class DocstransactionsController extends Controller {


	public function load(Request $request, $doc_type=null)
	{
		if($doc_type=='sales')
		{
				$sql = "SELECT united.*,dt.document_name AS `documenttype_name`,dt.system_name
							FROM (SELECT *,
								IF(paid_status=1, 1, 0) AS pay_off,
								(CASE id_currency WHEN 3
									THEN currency_financial_discount
									ELSE home_cur_financial_discount
								END) AS financial_discount,
								(total_with_vat + total_interest_overdue - currency_financial_discount - currency_total_payed) AS currency_total_remaining,
								(home_cur_total_with_vat + (home_currency_total_interest_overdue - home_currency_financial_discount - home_currency_total_payed)) AS home_currency_total_remaining,
								(CASE id_currency WHEN 3
									THEN (total_with_vat + total_interest_overdue - currency_financial_discount - currency_total_payed)
									ELSE (home_cur_total_with_vat * 140 + (home_cur_total_interest_overdue - home_cur_financial_discount - home_cur_total_payed))
								END) AS total_remaining
							     FROM
								(SELECT
									r_payment_id,
									0 AS amount_payed,
									getInvoiceDueDate(sinv.saleinvoiceheader_id) AS due_date,
									sinv.saleinvoiceheader_id AS subject_id,
									sinv.saleinvoiceheader_id AS doc_id,
									sinv.document_date,
									sinv.home_cur_total_with_vat,
									sinv.total_with_vat,
									sinv.id_currency,
									sinv.currency,
									sinv.exchange_rate,
									sinv.document_number,
									sinv.serial_number,
									sinv.paid_status,
									sinv.description,
									sinv.id_docstatus,
									sinv.customer_name AS thirdparty_name, sinv.customer_code AS thirdparty_code,
									IFNULL(SUM(rp.amount_payed), 0) AS total_payed,
									(CASE
										WHEN rp.id_currency = sinv.id_currency
											THEN IFNULL(SUM(rp.amount_payed), 0)
										WHEN sinv.id_currency = 1
											THEN IFNULL(SUM(rp.home_cur_amount_payed), 0)
										ELSE
											IFNULL(SUM(rp.amount_payed)/sinv.exchange_rate, 0)
									END) AS currency_total_payed,
									(CASE
										WHEN rp.id_currency = sinv.id_currency
											THEN IFNULL(SUM(sidc.total), 0)
										WHEN sinv.id_currency = 1
											THEN IFNULL(SUM(rp.home_cur_amount_payed), 0)
										ELSE
											IFNULL(SUM(sidc.total)/sinv.exchange_rate, 0)
									END) AS total_interest_overdue,
									IF(DATEDIFF( NOW(), DATE_ADD(sinv.document_date, INTERVAL ct.days_discount_applied DAY)) < 0, sinv.total_with_vat * ct.discount_percentage * 1/100.0, 0)
										AS currency_financial_discount,
									IFNULL(
										sidc.home_cur_total * 140
									,0) AS home_cur_total_interest_overdue,
									IFNULL(
										sidc.home_cur_total
									,0) AS home_currency_total_interest_overdue,
									(CASE ct.standard WHEN 1 THEN
										IF (DATEDIFF(NOW(),DATE_ADD(sinv.document_date,INTERVAL ct.days_net_amount DAY)) < 0,'false','true')
										ELSE 'N/A'
									END) AS overdue_from_std,
									IF(DATEDIFF( NOW(), DATE_ADD(sinv.document_date, INTERVAL ct.days_discount_applied DAY)) < 0, sinv.home_cur_total_with_vat * ct.discount_percentage *140/100.0, 0)
										AS home_cur_financial_discount,
									IF(DATEDIFF( NOW(), DATE_ADD(sinv.document_date, INTERVAL ct.days_discount_applied DAY)) < 0, sinv.home_cur_total_with_vat * ct.discount_percentage * 1/100.0, 0)
										AS home_currency_financial_discount,
									IFNULL((
										 SUM(rp.home_cur_amount_payed)*140
									),0) AS home_cur_total_payed,
									IFNULL((
										 SUM(rp.home_cur_amount_payed)
									),0) AS home_currency_total_payed,
									(CASE sinv.body_type WHEN 'items' THEN 'sales_invoice_art'
										WHEN 'accounts' THEN 'sales_invoice_acc'
									END) AS documenttype_name_sys,
									(CASE sinv.body_type WHEN 'items' THEN 3
										WHEN 'accounts' THEN 4
									END) AS id_documenttype
								FROM
									saleinvoiceheader sinv
									INNER JOIN commercialterms ct
										ON sinv.id_commercialterms = ct.commercialterms_id
									LEFT JOIN r_payment rp
										ON rp.subject='saleinvoiceheader' AND rp.subject_id = sinv.saleinvoiceheader_id
									LEFT JOIN saleinvoice_term_calc sidc
										ON  sidc.id_saleinvoiceheader = sinv.saleinvoiceheader_id

									GROUP BY sinv.saleinvoiceheader_id
								) AS dmy
							) AS united
							INNER JOIN documenttype dt
								ON dt.system_name = CONVERT(documenttype_name_sys USING utf8)

							ORDER BY subject_id";

			$statement = \AccUtils::db('r')->prepare($sql);
			//$statement->bindValue(':parent_code', $parent_code);
			$statement->execute();
			$result = $statement->fetchAll(\PDO::FETCH_ASSOC);

		}
		else if($doc_type=='bank')
		{
			$sql = "SELECT id
							 , code
							 , concat(code, ' - ', name) as code_name
							 , id_account
							 , id_currency
	                      FROM cash
	                     WHERE deleted=0
	                       AND active=1
	                       AND unit_type=1";

			$statement = \AccUtils::db('r')->prepare($sql);
			//$statement->bindValue(':parent_code', $parent_code);
			$statement->execute();
			$result = $statement->fetchAll(\PDO::FETCH_ASSOC);
		}
		else
		{
			$result = [];
			return response()->json($result);
		}

		if($result['cache_version'] == $request->get('cache_version'))
			$result = ['use_cache' => true];

		return response()->json($result);
	}


	public function grid(Request $request)
	{
		$inputs = $request->all();
		$ds = new \App\Libs\KendoUI\DataSource($inputs, 'acc_read');//READ ONLY erp_acc_passive

		info('inputs', [$inputs]);

		$properties = [
		        'id_documentgroup',
		        'document_date' => array('type' => 'date'),
		        'description',
		        'document_number',
		        'currency',
		        'total_with_vat' => array('type' => 'number'),
		        'id_contact' => array('type' => 'number'),
		        'id_cash' => array('type' => 'number'),
		        'id_warehouse' => array('type' => 'number'),
		    ];

		$fieldMapping = null;/*array(
			'name'=>'currencies.name',
			'active'=>'currencies.active'
		);*/

		$select = $ds->prepareColumns($properties, $fieldMapping);
		$where = $ds->prepareFilters($properties, true, $fieldMapping);
		$sort = $ds->prepareSort($properties, true, $fieldMapping);

		$select .= ',subject';
		$query = " FROM document_transactions ".$where.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);//true=debug
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	     //
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
