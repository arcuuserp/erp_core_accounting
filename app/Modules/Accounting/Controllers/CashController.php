<?php namespace Accounting\Controllers;

use Accounting\Models\Cash;
use App\Http\Controllers\Controller;

use Core\Models\Currency;
use Illuminate\Http\Request;

class CashController extends Controller {

	public function load(Request $request, $type_or_id=null)
	{
		if($type_or_id=='cash')
		{
			$result = \Cache::cacheDataForever('cash/cash', function()
			{
				$sql = "SELECT id
							 , code
							 , concat(code, ' - ', name) as code_name
							 , id_account
							 , id_currency
	                      FROM cash
	                     WHERE deleted=0
	                       AND active=1
	                       AND unit_type=0";

				$statement = \AccUtils::db('r')->query($sql);

				return $statement->fetchAll(\PDO::FETCH_ASSOC);
			});
		}
		else if($type_or_id=='bank')
		{
			$result = \Cache::cacheDataForever('cash/bank', function()
			{
				$sql = "SELECT id
							 , code
							 , concat(code, ' - ', name) as code_name
							 , id_account
							 , id_currency
	                      FROM cash
	                     WHERE deleted=0
	                       AND active=1
	                       AND unit_type=1";

				$statement = \AccUtils::db('r')->query($sql);

				return $statement->fetchAll(\PDO::FETCH_ASSOC);
			});
		}
		else if($type_or_id=='all')
		{
			$result = \Cache::cacheDataForever('cash/all', function()
			{
				$sql = "SELECT id
							 , code
							 , concat(code, ' - ', name) as code_name
							 , id_account
							 , id_currency
							 , unit_type
	                      FROM cash
	                     WHERE deleted=0
	                       AND active=1";

				$statement = \AccUtils::db('r')->query($sql);

				return $statement->fetchAll(\PDO::FETCH_ASSOC);
			});
		}
		else
		{
			$result = Cash::find($type_or_id);
			//get balance
			return response()->json($result);
		}

		if($result['cache_version'] == $request->get('cache_version'))
			$result = ['use_cache' => true];

		return response()->json($result);
	}

	public function browse(Request $request)
	{
		$ds = new \App\Libs\KendoUI\DataSource($request->all(), 'acc_read');//READ ONLY erp_acc_passive

		$ds->extraFilter('deleted', 0);

		$properties = [
		        'name',
		        'code',
		    ];

		$select = $ds->prepareColumns($properties);
		$where = $ds->prepareFilters($properties, true);
		$sort = $ds->prepareSort($properties, true);

		$query = " FROM cash ".$where.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);//true=debug
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}

	public function grid(Request $request)
	{
		$inputs = $request->all();
		$ds = new \App\Libs\KendoUI\DataSource($inputs, 'acc_read');//READ ONLY erp_acc_passive

		info('inputs', [$inputs]);

		$properties = [
		        'id_documentgroup',
		        'document_date' => array('type' => 'date'),
		        'description',
		        'document_number',
		        'currency',
		        'total_with_vat' => array('type' => 'number'),
		        'id_contact' => array('type' => 'number'),
		    ];

		$fieldMapping = null;/*array(
			'name'=>'currencies.name',
			'active'=>'currencies.active'
		);*/

		$select = $ds->prepareColumns($properties, $fieldMapping);
		$where = $ds->prepareFilters($properties, true, $fieldMapping);
		$sort = $ds->prepareSort($properties, true, $fieldMapping);

		$select .= ',subject';
		$query = " FROM document_transactions ".$where.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);//true=debug
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$result = array();
		return response()->json($result);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data = [
			'currencies_version' => \Cache::getCacheVersion('currencies/active'),
			'home_id_currency' => \CoreUtils::homeCurrency('id'),
		];

		return response()->json($data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = [
			'currencies_version' => \Cache::getCacheVersion('currencies/active'),
			'home_id_currency' => \CoreUtils::homeCurrency('id'),
		];
		$data['model'] = Cash::find($id);

		return response()->json($data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';


		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$cash = Cash::findOrFail($request->get('id'));

				$nameExists = Cash::where('name', $request->get('name'))
					->where('id', '<>', $cash->id)
					->exists();

				$codeExists = Cash::where('code', $request->get('code'))
					->where('id', '<>', $cash->id)
					->exists();
			}
			else
			{
				$cash = new Cash();
				$cash->created_at = date("Y-m-d H:i:s");

				$nameExists = Cash::where('name', $request->get('name'))
					->where('deleted', 0)
					->exists();

				$codeExists = Cash::where('code', $request->get('code'))
					->where('deleted', 0)
					->exists();
			}

			if($nameExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			if($codeExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);

			$cash->unit_type = $request->get('unit_type');
			$cash->code = $request->get('code');
			$cash->name = $request->get('name');
			$cash->description = $request->get('description');
			$cash->id_currency = $request->get('id_currency');
			$cash->currency = Currency::find($cash->id_currency)->name;
			$cash->id_account = $request->get('id_account');
			$cash->active = $request->get('active');
			$cash->opening_balance = $request->get('opening_balance');
			$cash->opening_exchange_rate = $request->get('opening_exchange_rate');
			$cash->opening_balance_home_currency = $request->get('opening_balance_home_currency');
			$cash->opening_date = $request->get('opening_date');
			$cash->save();

			\Cache::flushTagDir('cash');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
