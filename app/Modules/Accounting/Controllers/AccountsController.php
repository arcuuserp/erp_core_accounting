<?php namespace Accounting\Controllers;

use Accounting\Models\Account;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use Core\Models\Currency;
use Illuminate\Http\Request;

class AccountsController extends Controller {

	public function load(Request $request, $type=0)//0=all 1=not_linkedaccounts
	{
		if($type==1)
		{
			$result = \Cache::cacheDataForever('accounts/not_linkedaccounts', function()
			{
				//concat(code, ' - ', name) as code_name
				$sql = "SELECT  id,
								code,
								name
	                      FROM  accounts
	                      WHERE deleted=0
	                        AND id NOT IN (SELECT id_account FROM linkedaccounts)
						  ORDER BY code";

				$statement = \AccUtils::db('r')->query($sql);

				return $statement->fetchAll(\PDO::FETCH_ASSOC);
			});
		}
		elseif($type==2)//simple without code_name
		{
			$result = \Cache::cacheDataForever('accounts/load_no_code_name', function()
			{
				$sql = "SELECT  id,
								code,
								name,
								id_currency
	                      FROM  accounts
	                      WHERE deleted=0
	                      ORDER BY code";

				$statement = \AccUtils::db('r')->query($sql);

				return $statement->fetchAll(\PDO::FETCH_ASSOC);
			});
		}
		else
		{
			$result = \Cache::cacheDataForever('accounts/load', function()
			{
				$sql = "SELECT  id,
								code,
								name,
								concat(code, ' - ', name) as code_name,
								id_currency
	                      FROM  accounts
	                      WHERE deleted=0
	                      ORDER BY code";

				$statement = \AccUtils::db('r')->query($sql);

				return $statement->fetchAll(\PDO::FETCH_ASSOC);
			});
		}

		if($result['cache_version'] == $request->get('cache_version'))
			$result = ['use_cache' => true];

		return response()->json($result);
	}

	public function browse(Request $request)
	{
		info("browse", [$request->all()]);
		/*if(!$request->exists("filter"))
		{
			return response()->json(['data'=>[["name"=>"-"]]]);
		}*/

		$ds = new \App\Libs\KendoUI\DataSource($request->all(), 'acc_read');//READ ONLY erp_acc_passive

		$properties = [
		        'name',
		        'code',
		    ];

		$select = $ds->prepareColumns($properties);
		$cond = $ds->prepareFilters($properties, "AND");
		$sort = $ds->prepareSort($properties, true);

		$query = " FROM accounts WHERE deleted=0".$cond.$sort;//." LIMIT 20 ";

		$response['data'] = $ds->executeResult($query, "id, code, concat(code, ' - ', name) as label", true);//true=debug
		//$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$result = array();
		$result['tree_data'] = $this->getAccountsHierarchy();
		return response()->json($result);
	}

	private function getAccountsHierarchy()
	{
		$result = \Cache::rememberForever('accounts/tree', function()
		{
			$sql = "SELECT id, code, name, parent_code, description, active_pasive,
						   revenue_expenditure, account_class_code, account_total_code,
						   account_type, standard, id_currency, created_at
					  FROM accounts
					 WHERE deleted=0
	                   AND parent_code = :parent_code";

			$statement = \AccUtils::db('r')->prepare($sql);
			//PASS THE STATEMENT AS REFERENCE SO THAT IT DOESN'T NEED TO BE EXECUTED EACH TIME
			//WHEN ONLY THE BINDVALUE PARAM IS CHANGED AN NOT THE REST OF THE QUERY
			return $this->buildAccountsHierarchy(0, $statement);
		});

		return $result;
	}

	private function buildAccountsHierarchy($parent_code, $statement)
	{
		$statement->bindValue(':parent_code', $parent_code);
		$statement->execute();
		$result = $statement->fetchAll(\PDO::FETCH_ASSOC);
		$n = count($result);

		for($i=0; $i<$n; $i++)
		{
			$children = $this->buildAccountsHierarchy($result[$i]['code'], $statement);
			if($children!=NULL && count($children)>0)
				$result[$i]['items'] = $children;
		}

		return $result;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data = [
			'currencies_version' => \Cache::getCacheVersion('currencies/active'),
			'home_id_currency' => \CoreUtils::homeCurrency('id'),
		];
		
		return response()->json($data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = [
			'currencies_version' => \Cache::getCacheVersion('currencies/active'),
			'home_id_currency' => \CoreUtils::homeCurrency('id'),
		];
		$data['model'] = Account::find($id);
		
		return response()->json($data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		try {

			if($request->exists('id'))
			{
				$account = Account::findOrFail($request->get('id'));

				$nameExists = Account::where('name', $request->get('name'))
					->where('id', '<>', $account->id)
					->exists();

				$codeExists = Account::where('code', $request->get('code'))
					->where('id', '<>', $account->id)
					->exists();
			}
			else
			{
				$account = new Account();
				$account->created_at = date("Y-m-d H:i:s");

				$nameExists = Account::where('name', $request->get('name'))
					->where('deleted', 0)
					->exists();

				$codeExists = Account::where('code', $request->get('code'))
					->where('deleted', 0)
					->exists();
			}

			if($nameExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			if($codeExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);

			$account->name = $request->get('name');
			$account->code = $request->get('code');
			$account->parent_code = $request->get('parent_code');
			$account->id_accountgroup = $request->get('id_accountgroup');
			$account->account_type = $request->get('account_type');
			$account->id_currency = $request->get('id_currency');
			$account->currency = Currency::find($account->id_currency)->name;
			$account->id_financialstatementposition = $request->get('id_financialstatementposition');
			$account->description = $request->get('description');
			$account->active = $request->get('active');
			$account->opening_balance = $request->get('opening_balance');
			$account->opening_exchange_rate = $request->get('opening_exchange_rate');
			$account->opening_date = $request->get('opening_date');
			$account->save();

			\Cache::flushTagDir('accounts');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getLine(), [$e->getCode(), $e->getMessage(), $e->getTraceAsString()]);
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
