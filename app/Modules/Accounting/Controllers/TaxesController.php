<?php namespace Accounting\Controllers;

use Accounting\Models\Taxes;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class TaxesController extends Controller {

	public function load(Request $request)
	{
		$result = \Cache::cacheDataForever('taxes', function()
		{
			$sql = "SELECT id, code, name, tax_rate, percentage
                      FROM taxes
                     WHERE deleted=0
                       AND active=1";

			$statement = \DB::connection('acc')->getPdo()->query($sql);

			return $statement->fetchAll(\PDO::FETCH_ASSOC);
		});

		if($result['cache_version'] == $request->get('cache_version'))
			$result = ['use_cache' => true];

		return response()->json($result);
	}

	public function grid(Request $request)
	{
		$inputs = $request->all();
		$ds = new \App\Libs\KendoUI\DataSource($inputs, 'acc_read');//READ ONLY erp_acc_passive

		info('inputs', [$inputs]);

		$properties = [
		        'id_documentgroup',
		        'document_date' => array('type' => 'date'),
		        'description',
		        'document_number',
		        'currency',
		        'total_with_vat' => array('type' => 'number'),
		        'id_contact' => array('type' => 'number'),
		        'id_cash' => array('type' => 'number'),
		        'id_warehouse' => array('type' => 'number'),
		    ];

		$fieldMapping = null;/*array(
			'name'=>'currencies.name',
			'active'=>'currencies.active'
		);*/

		$select = $ds->prepareColumns($properties, $fieldMapping);
		$where = $ds->prepareFilters($properties, true, $fieldMapping);
		$sort = $ds->prepareSort($properties, true, $fieldMapping);

		$select .= ',subject';
		$query = " FROM document_transactions ".$where.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);//true=debug
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}

	public function view()
	{
		return response()->json([]);
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
		$response = "";
		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}
	

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return response()->json(Contact::find($id));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
