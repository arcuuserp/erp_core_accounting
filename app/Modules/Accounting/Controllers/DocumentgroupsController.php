<?php namespace Accounting\Controllers;

use Accounting\Models\Documentgroup;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DocumentgroupsController extends Controller {

	public function load($internalCall=false)
	{
		$result = \Cache::rememberForever('documentgroups/load', function()
		{
			$sql = "SELECT * FROM documentgroups";

			$statement = \AccUtils::db('r')->query($sql);
			return $statement->fetchAll(\PDO::FETCH_ASSOC);
		});

		if($internalCall)
			return $result;

		return response()->json(['data'=>$result]);
	}

	public function grid(Request $request)
	{
		$ds = new \App\Libs\KendoUI\DataSource($request->all(), 'acc_read');//READ ONLY erp_acc_passive

		$properties = [
		        'id_documentgroup',
		        'from_number',
		        'to_number',
		        'current',
		        'from_date',
		        'till_date',
		        'type',
		    ];

		$select = $ds->prepareColumns($properties);
		$cond = $ds->prepareFilters($properties, "AND");
		$sort = $ds->prepareSort($properties, true);

		$query = " FROM documentbatches WHERE deleted=0 ".$cond.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);//true=debug
		$response['total'] = $ds->executeCount($query);


		return response()->json($response);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$result = array();
		$result['list_data'] = $this->load(true);
		return response()->json($result);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();
		$result['currencies'] = app()->make('\Core\Controllers\CurrenciesController')->activeCurrencies(true);

		return response()->json($result);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();
		$result['currencies'] = app()->make('\Core\Controllers\CurrenciesController')->activeCurrencies(true);

		$result['model'] = Documentgroup::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$items = $request->get('items');
		try {

			\DB::beginTransaction();

			foreach($items as $doc_item)
			{
				$doc = Documentgroup::find($doc_item['id']);

				$doc->serial_format = $doc_item['serial_format'];
				$doc->number_format = $doc_item['number_format'];
				$doc->manual_number = $doc_item['manual_number'];
				$doc->allow_number_duplication_notify = $doc_item['allow_number_duplication_notify'];
				$doc->next_doc_number_suggest_pos = $doc_item['next_doc_number_suggest_pos'];
				$doc->manual_serial = $doc_item['manual_serial'];
				$doc->allow_serial_duplication_notify = $doc_item['allow_serial_duplication_notify'];
				$doc->next_serial_suggest_pos = $doc_item['next_serial_suggest_pos'];
				$doc->save();
			}

			\DB::commit();
			\Cache::flushTagDir('documentgroups');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			\DB::rollback();
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
