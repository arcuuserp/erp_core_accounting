<?php namespace Accounting\Controllers;

use Accounting\Models\Documentgroup;
use Accounting\Models\Requestoffer;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class RequestoffersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$result = array();
		return response()->json($result);
	}

   	public function echoNextSuggestedNumber()
	{
		$dg = Documentgroup::find(2);
		$resp = $this->getNextSuggestedNumber($dg);
		$resp->error = 0;
		echo json_encode($resp);
	}

	private function getNextSuggestedNumber($docGroup)
	{
		$resp = ['document_number'=>"(auto)", 'serial_number'=>"(auto)"];

		if($docGroup->manual_number==1 || $docGroup->manual_serial==1)
		{
			$query = "SELECT document_number, serial_number
						FROM salesinvoice
					   WHERE id = (SELECT MAX(id) FROM salesinvoice)
					   LIMIT 1";
			$result = \AccUtils::db('r')->query($query)->fetch(\PDO::FETCH_ASSOC);

			$suggestion = $docGroup->nextNumber($result['document_number'], $result['serial_number']);

			if($docGroup->manual_number==1)
				$resp['document_number'] =  $suggestion->next_doc_number;
			if($docGroup->manual_serial==1)
				$resp['serial_number'] =  $suggestion->next_serial;
		}

		return $resp;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data = [
			'home_currency' => \CoreUtils::homeCurrency(),
			'currencies_version' => \Cache::getCacheVersion('currencies/active'),
			'taxes_version' => \Cache::getCacheVersion('taxes'),
			'commercialterms_version' => \Cache::getCacheVersion('commercialterms'),
			'customers_version' => \Cache::getCacheVersion('contacts/customers'),
			'accounts_version' => \Cache::getCacheVersion('accounts/load'),
			'items_version' => \Cache::getCacheVersion('items/load'),
			//'warehouses_version' => \Cache::getCacheVersion('warehouses/all'),
		];

		$dg = Documentgroup::find(2);
		$data['model'] = $this->getNextSuggestedNumber($dg);
		$data['model']['id_currency'] = $data['home_currency']['id'];

		return response()->json($data);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();

		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		//return;

	    \AccUtils::db()->beginTransaction();
		//$transactions = \CoreUtils::beginManyTransactions(['core', 'acc']);
		try {

			//GENERATE SERIALS
			$dg = Documentgroup::find(2);

			if($request->exists('id'))
			{
				$header = Salesorder::findOrFail($request->get('id'));
				//CANCEL THE PREVIOUS ACCOUNTING TRANSACTIONS
				$this->cancelDocRevertTransaction($header->id, false);
				//on edit, skip auto-generation because the doc numbers can't change automatically
				//only manually inserted values are allowed to be change
				if($dg->manual_serial==0)
					$dg->manual_serial = -1;//skip
				if($dg->manual_number==0)
					$dg->manual_number = -1;//skip
			}
			else
			{
				$header = new Salesorder();
				$header->created_at = date("Y-m-d H:i:s");
			}

			$header->serial_number = $request->get("serial_number");
			$header->document_number = $request->get("document_number");
			$header->document_date = $request->get("document_date");

			$header->id_currency = $request->get("id_currency");
			$header->currency = \CoreUtils::currencyName($header->id_currency);
			$header->exchange_rate = $request->get("exchange_rate");

			$header->description = $request->get("description");
			$header->id_commercialterms = $request->get("id_commercialterms");

			$contact = $request->get("contact");
			if($contact)
			{
				$header->id_contact = $contact['id'];
				$header->contact_display_name = $contact['display'];
			}
			else
			{
				$response = ['ACC_COMMON.CUSTOMER','VALIDATION.REQUIRED_FIELD'];
				throw new \Exception("Missing Customer", $statusCode = 412);
			}

			if($request->exists("id_warehouse"))
				$header->idwarehouse_exit = $request->get("id_warehouse");

			$header->agent = $request->get("agent");

			//$header->customer_cur_exchange_rate = $request->get("customer_cur_exchange_rate");
			$header->total_no_vat = $request->get("total_no_tax");
			$header->total_vat = $request->get("total_taxable");
			$header->total_with_vat = $request->get("total_with_tax");
	
			$header->home_cur_total_no_vat = $header->total_no_vat * $header->exchange_rate;
			$header->home_cur_total_vat = $header->total_vat * $header->exchange_rate;
			$header->home_cur_total_with_vat = $header->total_with_vat * $header->exchange_rate;
			
			$header->discount_percent = $request->get("discount_percent");

			//$header->body_type = $request->get("body_type");//items/accounts

			//GENERATE SERIAL/DOC NUMBERS
			$dg->generate($header);

			$header->save();
			$header_lastID = $header->id;

			//$entries = $this->input->post("entries");
			//cc_distribution
			//$costcenterschemaCRUD = $this->reg->init('costcenterschemaCRUD');

			$accEntries = $request->get("body_acc");
			$itemsEntries = $request->get("body_items");
			if(count($itemsEntries) == 0 && count($accEntries) == 0)
			{
				$response = ['ACC_VALID.EMPTY_ITEMS_BODY', 'ACC_VALID.EMPTY_ACCOUNTS_BODY'];
				throw new \Exception('EMPTY_ITEMS_BODY && EMPTY_ACCOUNTS_BODY', $statusCode = 412);
			}

			if(count($itemsEntries) > 0)
			{
				$warehouse_entries = array();
				$total_no_vat_for_interest_calc = 0;
				$total_with_vat_for_interest_calc = 0;

				//$inventorymethodCRUD = $this->reg->init("inventorymethodCRUD();
				$bodyItemsPrep = [];
				foreach($itemsEntries as $entry)
				{
					if(isset($entry['include_in_interest_calc']) && $entry['include_in_interest_calc']==1)
					{
						$total_no_vat_for_interest_calc += $entry['subtotal_no_vat'];
						$total_with_vat_for_interest_calc += $entry['subtotal_with_vatvat'];
					}

					$itemBody = [];

					/* TODO:
					if(isset($entry['item_price['id_formula))
						$itemBody['id_formula'] = $entry['item_price['id_formula;
					*/

					$itemBody['id_saleinvoice'] = $header_lastID;

					$itemBody['id_item'] = $entry['subject']['id'];
					$itemBody['item_code'] = $entry['subject']['code'];
					$itemBody['item_name'] = $entry['subject']['name'];
					$itemBody['id_currency'] = $entry['subject']['id_currency'];

					$itemBody['idsale_account'] = $entry['subject']['idsale_account'];
					$itemBody['idpurchase_account'] = $entry['subject']['idpurchase_account'];

					$itemBody['item_description'] = $entry['description'];

					$itemBody['id_unit'] = $entry['unit']['idunitentry'];
					$itemBody['unit_name'] = $entry['unit']['unitentry_symbol'];

					$itemBody['quantity'] = $entry['quantity'];
					$itemBody['price'] = $entry['price'];
					$itemBody['exchange_rate'] = $entry['subject']['exchange_rate'];

					$itemBody['subtotal_no_vat'] = $entry['amount'] - $entry['taxable_amount'];
					$itemBody['subtotal_vat'] = $entry['taxable_amount'];
					$itemBody['subtotal_with_vat'] = $entry['amount'];

					$itemBody['home_cur_subtotal_no_vat'] = $itemBody['subtotal_no_vat'] * $header->exchange_rate;
					$itemBody['home_cur_subtotal_vat'] = $itemBody['subtotal_vat'] * $header->exchange_rate;
					$itemBody['home_cur_subtotal_with_vat'] = $itemBody['subtotal_with_vat'] * $header->exchange_rate;

					/* TODO:
					 * - subtotal_no_vat_no_discount
					 * - id_lot
					 */

					$bodyItemsPrep[] = $itemBody;

					/* TODO:
					 * idwarehouse_exit
					 */

				}


				$header->total_no_vat_for_interest_calc = $total_no_vat_for_interest_calc;
				$header->total_with_vat_for_interest_calc = $total_with_vat_for_interest_calc;
				$header->save();

				\AccUtils::db()->table('requestofferbody')->insert($bodyItemsPrep);

                $header->itemsToGL($bodyItemsPrep, $contact['id_account']);
			}
			/*
			$accEntries = $request->get("body_acc");
			if(count($accEntries) == 0)
				throw new \Exception($response = 'ACC_VALID.EMPTY_ACCOUNTS_BODY', $statusCode = 412);

			foreach($accEntries as $entry)
			{
				$accBody = new Salesinvoicebodyaccounts();

				$accBody->id_ledgerentry = $header_lastID;
				$accBody->id_account = $entry['subject']['id'];
				$accBody->account_code = $entry['subject']['code'];
				$accBody->account_name = $entry['subject']['name'];
				$accBody->description = $entry['description'];
				$accBody->debit = $entry['debit'];
				$accBody->credit = $entry['credit'];

				$subject_exchange_rate = isset($entry['subject']['ci_exchange_rate'])?$entry['subject']['ci_exchange_rate']:1;

				$accBody->account_id_currency = $entry['subject']['id_currency'];
				if($accBody->account_id_currency != $header->id_currency)
				{
					$accBody->account_currency = $entry['subject']['currency'];
					$accBody->account_cur_exchange_rate = $subject_exchange_rate;
					$accBody->account_cur_debit = $entry['debit'] * $subject_exchange_rate;
					$accBody->account_cur_credit = $entry['credit'] * $subject_exchange_rate;
				}
				else
				{
					$accBody->account_currency = $header->currency;
					$accBody->account_cur_exchange_rate = 1;
					$accBody->account_cur_debit = $entry['debit'];
					$accBody->account_cur_credit = $entry['credit'];
				}
				$accBody->home_cur_debit = $entry['debit'] * $header->exchange_rate;
				$accBody->home_cur_credit = $entry['credit'] * $header->exchange_rate;
				//SAVE BODY ENTRY
				$accBody->save();

				if($accBody->debit > 0)
				{
					\AccUtils::transaction(["id_account"=>$accBody->id_account,
							"transaction_type_name"=>'debit',
							"documenttype_name"=>'ledger_entry',
							"id_documenttype"=>23,
							"document_id"=>$header_lastID,
							//"document_number"=>$header->document_number,
							"id_currency"=>$header->id_currency,
							"exchange_rate"=>$header->exchange_rate,
							"transaction_date"=>$header->document_date,
							"total"=>$accBody->debit,
							"description"=>"Flete Kontabel",
							"account_cur_exchange_rate" => $subject_exchange_rate
							]);
				}
				if($accBody->credit > 0)
				{
					\AccUtils::transaction(["id_account"=>$accBody->id_account,
							"transaction_type_name"=>'credit',
							"documenttype_name"=>'ledger_entry',
							"id_documenttype"=>23,
							"document_id"=>$header_lastID,
							//"document_number"=>$header->document_number,
							"id_currency"=>$header->id_currency,
							"exchange_rate"=>$header->exchange_rate,
							"transaction_date"=>$header->document_date,
							"total"=>$accBody->credit,
							"description"=>"Flete Kontabel",
							"account_cur_exchange_rate" => $subject_exchange_rate
							]);
				}
			}
			*/

			if($request->get("response_action") == "new")
			{
				$response = $this->getNextSuggestedNumber($dg);
				$header_lastID = "";
			}
			else
			{
				$response = ['serial_number' => $header->serial_number,
							 'document_number' => $header->document_number];
			}

			\AccUtils::db()->commit();//$transactions->commitMany();
			//\AccUtils::saveDocumentTransaction($header);

			$statusCode = 200;
			$response['header_id'] = $header_lastID;
		}
		catch (\Exception $e)
		{
	        \AccUtils::db()->rollback();//$transactions->rollbackMany();

			info($e->getLine(), [$e->getCode(), $e->getMessage(), $e->getFile()]);
			info($e->getTraceAsString());

			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	public function destroy($id)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

	    \DB::beginTransaction();
		try {
			$this->cancelDocRevertTransaction($id, true);
		}
		catch (\Exception $e)
		{
	        \DB::rollback();
		}
		\DB::commit();

		return response()->json($response, $statusCode);
	}

   	private function cancelDocRevertTransaction($id, $cancel)
	{
		if($cancel)
		{
			$delete_status = 5;//set the deleted value to 5 in order to distinguesh it and show up in canceled docs
			Salesinvoice::where('id', $id)->update(['id_docstatus' => 5]);
		}
		else
			$delete_status = 1;

		$transIdentifier = $id."|ledger_entry";
		$cancel_transaction_resp = \AccUtils::cancelLedgerEntry($transIdentifier, true);
		if($cancel_transaction_resp === false)
			throw new \Exception("revert transaction error");

		Ledgerentrybody::where('id_ledgerentry', $id)->update(['deleted' => $delete_status]);
		\AccUtils::cancelDocumentTransaction($id);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

}
