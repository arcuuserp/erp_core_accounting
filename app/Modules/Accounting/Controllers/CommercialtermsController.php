<?php namespace Accounting\Controllers;

use Accounting\Models\Commercialterms;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class CommercialtermsController extends Controller {

	public function load(Request $request)
	{
		$result = \Cache::cacheDataForever('commercialterms', function()
		{
			$sql = "SELECT  id, name, days_net_amount
                      FROM  commercialterms
                     WHERE deleted=0";

			$statement = \AccUtils::db('r')->query($sql);

			return $statement->fetchAll(\PDO::FETCH_ASSOC);
		});

		if($result['cache_version'] == $request->get('cache_version'))
			$result = ['use_cache' => true];

		return response()->json($result);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
		$response = "";
		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();
		return response()->json($result);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();
		$result['model'] = Commercialterms::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$commercialterms = Commercialterms::findOrFail($request->get('id'));

				$nameExists = Commercialterms::where('name', $request->get('name'))
					->where('id', '<>', $commercialterms->id)
					->exists();

				$codeExists = Commercialterms::where('standard', $request->get('standard'))
					->where('id', '<>', $commercialterms->id)
					->exists();
			}
			else
			{
				$commercialterms = new Commercialterms();
				$commercialterms->created_at = date("Y-m-d H:i:s");

				$nameExists = Commercialterms::where('name', $request->get('name'))
					->where('deleted', 0)
					->exists();

				$codeExists = Commercialterms::where('standard', $request->get('standard'))
					->where('deleted', 0)
					->exists();
			}

			if($nameExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			if($codeExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);

			$commercialterms->name = $request->get('name');
			$commercialterms->standard = $request->get('standard');
			$commercialterms->days_net_amount = $request->get('days_net_amount');
			$commercialterms->discount_percentage = $request->get('discount_percentage');
			$commercialterms->days_discount_applied = $request->get('days_discount_applied');
			$commercialterms->date_dependent = $request->get('date_dependent');
			$commercialterms->commercialTermsInterestAppliesTo_value = $request->get('commercialTermsInterestAppliesTo_value');
			$commercialterms->date_net_amount = $request->get('date_net_amount');
		    $commercialterms->commercialTermsMonth_value = $request->get('commercialTermsMonth_value');
            $commercialterms->commercialTermsInterestBase_value = $request->get('commercialTermsInterestBase_value');
			$commercialterms->commercialTermsMonth_value = $request->get('commercialTermsMonth_value');
			$commercialterms->interest = $request->get('interest');
			$commercialterms->commercialTermsInterestType_value = $request->get('commercialTermsInterestType_value');
			$commercialterms->commercialTermsMaxInterestCalcType_value = $request->get('commercialTermsMaxInterestCalcType_value');
			$commercialterms->maxInterestCalc = $request->get('maxInterestCalc');
			$commercialterms->save();

			\Cache::flushTagDir('commercialterms');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
