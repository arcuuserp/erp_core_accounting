<?php namespace Accounting\Controllers;

use Accounting\Models\Documentgroup;
use Accounting\Models\Ledgerentry;
use Accounting\Models\Ledgerentrybody;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class LedgerEntriesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$result = array();
		return response()->json($result);
	}

   	public function echoNextSuggestedNumber()
	{
		$dg = Documentgroup::find(21);
		$resp = $this->getNextSuggestedNumber($dg);
		$resp->error = 0;
		echo json_encode($resp);
	}

	private function getNextSuggestedNumber($docGroup)
	{
		$resp = ['document_number'=>"(auto)", 'serial_number'=>"(auto)"];

		if($docGroup->manual_number==1 || $docGroup->manual_serial==1)
		{
			$query = "SELECT document_number, serial_number
						FROM ledgerentry
					   WHERE id = (SELECT MAX(id) FROM ledgerentry)
					   LIMIT 1";
			$result = \AccUtils::db('r')->query($query)->fetch(\PDO::FETCH_ASSOC);

			$suggestion = $docGroup->nextNumber($result['document_number'], $result['serial_number']);

			if($docGroup->manual_number==1)
				$resp['document_number'] =  $suggestion->next_doc_number;
			if($docGroup->manual_serial==1)
				$resp['serial_number'] =  $suggestion->next_serial;
		}

		return $resp;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data = [
			'home_id_currency' => \CoreUtils::homeCurrency('id'),
			'currencies_version' => \Cache::getCacheVersion('currencies/active'),
			'accounts_version' => \Cache::getCacheVersion('accounts/load'),
		];

		$dg = Documentgroup::find(21);
		$data['model'] = $this->getNextSuggestedNumber($dg);
		$data['model']['id_currency'] = $data['home_id_currency'];

		return response()->json($data);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();

		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

	    \AccUtils::db()->beginTransaction();
		//$transactions = \CoreUtils::beginManyTransactions(['core', 'acc']);
		try {
			//GENERATE SERIALS
			$dg = Documentgroup::find(21);

			if($request->exists('id'))
			{
				$header = Ledgerentry::findOrFail($request->get('id'));
				//CANCEL THE PREVIOUS ACCOUNTING TRANSACTIONS
				$this->cancelDocRevertTransaction($header->id, false);
				//on edit, skip auto-generation because the doc numbers can't change automatically
				//only manually inserted values are allowed to be change
				if($dg->manual_serial==0)
					$dg->manual_serial = -1;//skip
				if($dg->manual_number==0)
					$dg->manual_number = -1;//skip
			}
			else
			{
				$header = new Ledgerentry();
				$header->created_at = date("Y-m-d H:i:s");
			}

			$header->serial_number = $request->get("serial_number");
			$header->document_number = $request->get("document_number");
			$header->document_date = $request->get("document_date");

			$header->id_currency = $request->get("id_currency");
			$header->currency = \CoreUtils::currencyName($header->id_currency);
			$header->exchange_rate = $request->get("exchange_rate");

			$header->description = $request->get("description");
			$header->total_debit = $request->get("total_debit");
			$header->total_credit = $request->get("total_credit");

			$header->home_cur_total_debit = $header->total_debit * $header->exchange_rate;
			$header->home_cur_total_credit = $header->total_credit * $header->exchange_rate;

			//GENERATE SERIAL/DOC NUMBERS
			$dg->generate($header);

			$header->save();
			$header_lastID = $header->id;

			$accEntries = $request->get("body_acc");
			if(count($accEntries) == 0)
				throw new \Exception($response = 'ACC_VALID.EMPTY_ACCOUNTS_BODY', $statusCode = 412);

			foreach($accEntries as $entry)
			{
				$accBody = new Ledgerentrybody();

				$accBody->id_ledgerentry = $header_lastID;
				$accBody->id_account = $entry['subject']['id'];
				$accBody->account_code = $entry['subject']['code'];
				$accBody->account_name = $entry['subject']['name'];
				$accBody->description = isset($entry['description'])?$entry['description']:"";
				$accBody->debit = $entry['debit'];
				$accBody->credit = $entry['credit'];

				$subject_exchange_rate = isset($entry['subject']['ci_exchange_rate'])?$entry['subject']['ci_exchange_rate']:1;

				$accBody->account_id_currency = $entry['subject']['id_currency'];
				if($accBody->account_id_currency != $header->id_currency)
				{
					$accBody->account_currency = $entry['subject']['currency'];
					$accBody->account_cur_exchange_rate = $subject_exchange_rate;
					$accBody->account_cur_debit = $entry['debit'] * $subject_exchange_rate;
					$accBody->account_cur_credit = $entry['credit'] * $subject_exchange_rate;
				}
				else
				{
					$accBody->account_currency = $header->currency;
					$accBody->account_cur_exchange_rate = 1;
					$accBody->account_cur_debit = $entry['debit'];
					$accBody->account_cur_credit = $entry['credit'];
				}
				$accBody->home_cur_debit = $entry['debit'] * $header->exchange_rate;
				$accBody->home_cur_credit = $entry['credit'] * $header->exchange_rate;
				//SAVE BODY ENTRY
				$accBody->save();

				if($accBody->debit > 0)
				{
					\AccUtils::transaction(["id_account"=>$accBody->id_account,
							"transaction_type_name"=>'debit',
							"documenttype_name"=>'ledger_entry',
							"id_documenttype"=>23,
							"document_id"=>$header_lastID,
							//"document_number"=>$header->document_number,
							"id_currency"=>$header->id_currency,
							"exchange_rate"=>$header->exchange_rate,
							"transaction_date"=>$header->document_date,
							"total"=>$accBody->debit,
							"description"=>"Flete Kontabel",
							"account_cur_exchange_rate" => $subject_exchange_rate
							]);
				}
				if($accBody->credit > 0)
				{
					\AccUtils::transaction(["id_account"=>$accBody->id_account,
							"transaction_type_name"=>'credit',
							"documenttype_name"=>'ledger_entry',
							"id_documenttype"=>23,
							"document_id"=>$header_lastID,
							//"document_number"=>$header->document_number,
							"id_currency"=>$header->id_currency,
							"exchange_rate"=>$header->exchange_rate,
							"transaction_date"=>$header->document_date,
							"total"=>$accBody->credit,
							"description"=>"Flete Kontabel",
							"account_cur_exchange_rate" => $subject_exchange_rate
							]);
				}
			}

			if($request->get("response_action") == "new")
			{
				$response = $this->getNextSuggestedNumber($dg);
				$header_lastID = "";
			}
			else
			{
				$response = ['serial_number' => $header->serial_number,
							 'document_number' => $header->document_number];
			}

			//\AccUtils::saveDocumentTransaction($header);
			\AccUtils::db()->commit();//$transactions->commitMany();

			$statusCode = 200;
			$response['header_id'] = $header_lastID;
		}
		catch (\Exception $e)
		{
	        \AccUtils::db()->rollback();//$transactions->rollbackMany();

			info($e->getLine(), [$e->getCode(), $e->getMessage(), $e->getFile()]);
			info($e->getTraceAsString());

			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	public function destroy($id)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

	    \DB::beginTransaction();
		try {
			$this->cancelDocRevertTransaction($id, true);
		}
		catch (\Exception $e)
		{
	        \DB::rollback();
		}
		\DB::commit();

		return response()->json($response, $statusCode);
	}

   	private function cancelDocRevertTransaction($id, $cancel)
	{
		if($cancel)
		{
			$delete_status = 5;//set the deleted value to 5 in order to distinguesh it and show up in canceled docs
			Ledgerentry::where('id', $id)->update(['id_docstatus' => 5]);
		}
		else
			$delete_status = 1;

		$transIdentifier = $id."|ledger_entry";
		$cancel_transaction_resp = \AccUtils::cancelLedgerEntry($transIdentifier, true);
		if($cancel_transaction_resp === false)
			throw new \Exception("revert transaction error");

		Ledgerentrybody::where('id_ledgerentry', $id)->update(['deleted' => $delete_status]);
		\AccUtils::cancelDocumentTransaction($id);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

}
