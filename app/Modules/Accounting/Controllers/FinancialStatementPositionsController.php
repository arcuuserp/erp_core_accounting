<?php namespace Accounting\Controllers;

use Accounting\Models\Financialstatementposition;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class FinancialStatementPositionsController extends Controller {

    public function load()//0=basics 1=with currency 2=not_linkedaccounts
	{
		$result = \Cache::rememberForever('financialstatementpositions/load', function()
		{
			$sql = "SELECT  id,
							name
                      FROM  financialstatementpositions
                      WHERE deleted=0";

			$statement = \AccUtils::db('r')->query($sql);
			return $statement->fetchAll(\PDO::FETCH_ASSOC);
		});

		return response()->json(['data'=>$result]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
		$response = "";
		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();
		$result['financialstatementpositions'] = Financialstatementposition::where('deleted', 0)
										->select('id', 'name')
										->get();

		return response()->json($result);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();

		$result['model'] = Financialstatementposition::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$financialstatementposition = Financialstatementposition::findOrFail($request->get('id'));

				$nameExists = Financialstatementposition::where('name', $request->get('name'))
					->where('id', '<>', $financialstatementposition->id)
					->exists();
			}
			else
			{
				$financialstatementposition = new Financialstatementposition();
				$financialstatementposition->created_at = date("Y-m-d H:i:s");

				$nameExists = Financialstatementposition::where('fsp_name', $request->get('fsp_name'))
					->where('deleted', 0)
					->exists();

			}

			if($nameExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			
			$financialstatementposition->name = $request->get('name');
			$financialstatementposition->id_financialstatementposition = $request->get('id_financialstatementposition');
			$financialstatementposition->statement = $request->get('statement');
			$financialstatementposition->description = $request->get('description');
			$financialstatementposition->save();

			\Cache::flushTagDir('financialstatementposition');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			info($e->getLine());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
