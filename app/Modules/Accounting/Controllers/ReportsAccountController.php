<?php namespace Accounting\Controllers;

use App\Http\Controllers\ReportController;
use App\Http\Requests;

use Illuminate\Http\Request;

class ReportsAccountController extends ReportController {

	/*
	*	method 	= GET
	*	route  	= ledger (or /accounting/reports/account/ledger)
	*/
	public function getLedger()
	{
		$jasperLogin = \CoreUtils::jasperLogin();
		$companyDetails = \CoreUtils::companyDetails();

		$data = [
			'report_path' => "/accounting/account/ledger",
			'report_title' => 'REPORT_TITLES.TEST_ONE',
			'report_parameters' => [
				'core_name' => \DB::getDatabaseName(),
				'acc_name' => \AccUtils::db()->getDatabaseName(),
				'company_name' => $companyDetails->name,
				'currency' => $companyDetails->currency,
				'id_currency' => $companyDetails->id_currency,
				'name_surname' => '***TestUser',
				'bottom_message' => $this->reportsBottomMessage(),
			]
		];

		return response()->json($data);
	}

}
