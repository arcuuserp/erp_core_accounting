<?php namespace Warehouse\Models;

use Illuminate\Database\Eloquent\Model;

class Itemaccountsschema extends Model {

	protected $table = 'item_accounts_schemas';
	protected $connection = 'acc';
	public $timestamps = false;

}
