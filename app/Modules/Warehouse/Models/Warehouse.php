<?php namespace Warehouse\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model {

	protected $table = 'warehouse';
	protected $connection = 'acc';
	public $timestamps = false;

}
