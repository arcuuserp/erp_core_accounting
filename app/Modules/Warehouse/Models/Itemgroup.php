<?php namespace Warehouse\Models;

use Illuminate\Database\Eloquent\Model;

class Itemgroup extends Model {

	protected $table = 'itemgroups';
	protected $connection = 'acc';
	public $timestamps = false;

}
