<?php namespace Warehouse\Models;

use Illuminate\Database\Eloquent\Model;

class Itemkind extends Model {

	protected $table = 'itemkinds';
	protected $connection = 'acc';
	public $timestamps = false;

}
