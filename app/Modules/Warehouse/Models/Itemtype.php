<?php namespace Warehouse\Models;

use Illuminate\Database\Eloquent\Model;

class Itemtype extends Model {

	protected $table = 'itemtypes';
	protected $connection = 'acc';
	public $timestamps = false;

}
