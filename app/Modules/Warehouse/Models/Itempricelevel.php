<?php namespace Warehouse\Models;

use Illuminate\Database\Eloquent\Model;

class Itempricelevel extends Model {

	protected $table = 'itempricelevels';
	protected $connection = 'acc';
	public $timestamps = false;

}
