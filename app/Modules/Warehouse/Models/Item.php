<?php namespace Warehouse\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model {

	protected $table = 'items';
	protected $connection = 'acc';
	public $timestamps = false;

}
