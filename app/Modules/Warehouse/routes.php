<?php

/*
|--------------------------------------------------------------------------
| Accounting Routes
|--------------------------------------------------------------------------
*/

Route::match(['get', 'post'], 'items/load/{type_id?}', 'ItemsController@load');
Route::get('items/grid',	            'ItemsController@grid');
Route::get('items/grid2',	            'ItemsController@grid2');
Route::resource('items',	            'ItemsController');

Route::resource('itempricelevels',	    'ItempricelevelsController');
Route::resource('industrycategories',   'IndustrycategoriesController');
Route::get('itemkinds/load',            'ItemkindsController@load');
Route::resource('itemkinds',	        'ItemkindsController');
Route::get('itemtypes/load',            'ItemtypesController@load');
Route::resource('itemtypes',	        'ItemtypesController');
Route::get('itemgroups/load',           'ItemgroupsController@load');
Route::resource('itemgroups',	        'ItemgroupsController');
Route::any('itemaccountsschemas/load',  'ItemaccountsschemasController@load');
Route::get('itemaccountsschemas/grid',  'ItemaccountsschemasController@grid');
Route::resource('itemaccountsschemas',	'ItemaccountsschemasController');

Route::get('warehouses/load/{id?}',     'WarehousesController@load');
Route::get('warehouses/browse',         'WarehousesController@browse');
Route::resource('warehouses',		    'WarehousesController');
