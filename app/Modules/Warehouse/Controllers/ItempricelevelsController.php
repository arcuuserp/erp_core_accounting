<?php namespace Warehouse\Controllers;

use Warehouse\Models\Itempricelevel;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ItempricelevelsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
		$response = "";
		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();

		return response()->json($result);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();

		$result['model'] = Itempricelevel::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$itempricelevel = $request->all();
		info('inputs', [$itempricelevel]);

		try {

			$price_levels_count = 10;
			foreach($itempricelevel['models'] as $item)
			{
				Itempricelevel::where('id_item', $item['id_item'])->delete();
				//loop through each level based on a dynamic length
				foreach($item as $column=>$value)
				{
					$columnSplit = explode("_",$column);
					if($columnSplit[0]=='level' && $value!='') 
					{
						$itempricelevel = new Itempricelevel();
						$itempricelevel->id_item = $item['id_item'];
						$itempricelevel->level = $columnSplit[1];
						$itempricelevel->value = $value;
						$itempricelevel->created_at = date("Y-m-d H:i:s");
						$itempricelevel->save();
					}					
				}
			}

			\AccUtils::db()->select('CALL itempricelevels()');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			info($e->getLine());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
