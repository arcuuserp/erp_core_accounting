<?php namespace Warehouse\Controllers;

use Warehouse\Models\Item;
use Warehouse\Models\Itemkind;
use Warehouse\Models\Itemtype;
use Warehouse\Models\Itemgroup;
use Warehouse\Models\Itempricelevel;
use Core\Models\Unit;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ItemsController extends Controller {


	public function itemPrice($id_item, $id_contact)
	{
		$response = \AccUtils::loadItemPrice($id_item, $id_contact, false);
		return response()->json($response);
	}

	public function load(Request $request, $type_or_id=null)
	{
		if(is_numeric($type_or_id))//load one
		{
			$result = \AccUtils::loadItem($type_or_id, $request->get('id_contact'), $request->get('transaction_type'));
			return response()->json($result['response'], $result['code']);
		}
		else
		{
			if($type_or_id == 'all')
			{
				$result = \Cache::cacheDataForever('items/all', function()
				{
					$sql = "SELECT id, code, name
		                      FROM items
		                     WHERE deleted=0
		                       AND active=1
	                         ORDER BY code, name";

					$statement = \DB::connection('acc')->getPdo()->query($sql);

					return $statement->fetchAll(\PDO::FETCH_ASSOC);
				});
			}
			else if($type_or_id == 'warehouse')
			{
				$result = \Cache::cacheDataForever('items/warehouse', function()
				{
					$sql = "SELECT id, code, name
		                      FROM items
		                     WHERE deleted=0
		                       AND active
	                         ORDER BY code, name";

					$statement = \DB::connection('acc')->getPdo()->query($sql);

					return $statement->fetchAll(\PDO::FETCH_ASSOC);
				});
			}
			else//ONLY products and services
			{
				$result = \Cache::cacheDataForever('items/load', function()
				{
					$sql = "SELECT id, code, name
		                      FROM items
		                     WHERE deleted=0
		                       AND active=1
		                       AND id_itemkind <> 4
	                         ORDER BY code, name";

					$statement = \DB::connection('acc')->getPdo()->query($sql);

					return $statement->fetchAll(\PDO::FETCH_ASSOC);
				});
			}

			if($result['cache_version'] == $request->get('cache_version'))
				$result = ['use_cache' => true];

			return response()->json($result);
		}
	}

	/* not used
	public function load(Request $request)
	{
		$ds = new \App\Libs\KendoUI\DataSource($request->all(), 'acc_read');//READ ONLY erp_acc_passive

		$ds->extraFilter('deleted', 0);
		//$ds->extraFilter('is_cutomer', 1);//field, value, $type='number', operator='eq', logic='and'

		$properties = [
		        'name',
		        'code',
		    ];

		$select = $ds->prepareColumns($properties);
		$where = $ds->prepareFilters($properties, true);
		$sort = $ds->prepareSort($properties, true);

		$query = " FROM items ".$where.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);//true=debug
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}*/


	public function grid(Request $request)
	{
		$inputs = $request->all();
		$ds = new \App\Libs\KendoUI\DataSource($inputs, 'acc_read');//READ ONLY erp_acc_passive

		info('inputs', [$inputs]);

		$properties = [
		        'code',
		        'name',
		        'actual_quantity',
		        'unit',
		        'cost',
		        'purchase_account',
		        'sale_account',
		        'cost_account',
		       	'account',
		       	'barcode',
		       	'opening_date',
		       	'id_itemkind',
		       	'id_itemtype',
		       	'id_itemgroup',
		    ];

		$select = $ds->prepareColumns($properties);
		$cond = $ds->prepareFilters($properties, "AND");
		$sort = $ds->prepareSort($properties, true);

		$query = " FROM items WHERE deleted=0 ".$cond.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);//true=debug
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}

	public function grid2(Request $request)
	{
		$inputs = $request->all();
		$ds = new \App\Libs\KendoUI\DataSource($inputs, 'acc_read');//READ ONLY erp_acc_passive

		info('inputs', [$inputs]);

		$properties = [
		        'id_item',
		        'code',
		        'name',
		        'unit',
	        	'id_itemkind',
		       	'id_itemtype',
		       	'id_itemgroup',
		    ];

	   	$price_levels_count = (int) \CoreUtils::getSettings('price_levels_count');

	 	for($i=1; $i<$price_levels_count; $i++) {
	 		$properties[] = 'level_'.$i;
	 	}

		$select = $ds->prepareColumns($properties);
		$cond = $ds->prepareFilters($properties);
		$sort = $ds->prepareSort($properties, true);

		$query = " FROM tbl_itempricelevels ".$cond.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);//true=debug
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}



	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$result = array();
		$result['tree_data'] = $this->getItemKindsHierarchy();
		$result['price_levels_count'] = (int) \CoreUtils::getSettings('price_levels_count');
		return response()->json($result);
	}

	private function getItemKindsHierarchy($id=null, $exclude_id=null)
	{
		$result = Itemkind::where('deleted', 0)
							->where('id', '<>', 4)
							->get();

		foreach($result as $itemkind)
		{
			$itemkind->type = 'itemkind';
			$children = $this->getItemTypesHierarchy($itemkind->id);
			if($children!=NULL && count($children)>0)
				$itemkind->items = $children;
		}

		return $result;
	}

	private function getItemTypesHierarchy($itemkind_id)
	{
		$result = Itemtype::where('deleted', 0)
							->where('id_itemkind', $itemkind_id)
							->get();

		foreach($result as $itemtype)
		{
			$itemtype->type = 'itemtype';
			$children = $this->getItemGroupsHierarchy($itemtype->id, $itemkind_id);
			if($children!=NULL && count($children)>0)
				$itemtype->items = $children;
		}

		return $result;
	}

	private function getItemGroupsHierarchy($itemtype_id, $id_itemkind, $id_itemgroup=0)
	{
		$result = Itemgroup::where('deleted', 0)
							->where('id_itemtype', $itemtype_id)
							->where('id_itemgroup', $id_itemgroup)
							->get();

		foreach($result as $itemgroup)
		{
			$itemgroup->id_itemkind = $id_itemkind;
			$itemgroup->type = 'itemgroup';
			$children = $this->getItemGroupsHierarchy($itemtype_id, $id_itemkind, $itemgroup->id);
			if($children!=NULL && count($children)>0)
				$itemgroup->items = $children;
		}

		return $result;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data = [
			'currencies_version' => \Cache::getCacheVersion('currencies/active'),
			'home_id_currency' => \CoreUtils::homeCurrency('id'),
			'itemkinds_version' => \Cache::getCacheVersion('itemkinds/load'),
			'itemtypes_version' => \Cache::getCacheVersion('itemtypes/load'),
			'itemgroups_version' => \Cache::getCacheVersion('itemgroups/load'),
		];

		return response()->json($data);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		/*$result = array();
		$result['currencies'] = app()->make('\Core\Controllers\CurrenciesController')->activeCurrencies(true);

		$result['model'] = Item::find($id);
		return response()->json($result);
		*/
		$data = [
			'currencies_version' => \Cache::getCacheVersion('currencies/active'),
			'home_id_currency' => \CoreUtils::homeCurrency('id'),
			'taxes_version' => \Cache::getCacheVersion('taxes')
		];
		
		$data['model'] = Item::find($id);
		
		return response()->json($data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$item = Item::findOrFail($request->get('id'));

				$nameExists = Item::where('name', $request->get('name'))
					->where('id', '<>', $item->id)
					->exists();

				$codeExists = Item::where('code', $request->get('code'))
					->where('id', '<>', $item->id)
					->exists();
			}
			else
			{
				$item = new Item();
				$item->created_at = date("Y-m-d H:i:s");


				$nameExists = Item::where('name', $request->get('name'))
					->where('deleted', 0)
					->exists();

				$codeExists = Item::where('code', $request->get('code'))
					->where('deleted', 0)
					->exists();
			}

			if($nameExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			if($codeExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);

			$item->id_itemkind = $request->get('id_itemkind');
			$item->id_itemtype = $request->get('id_itemtype');
			$item->id_itemgroup = $request->get('id_itemgroup');
			$item->code = $request->get('code');
			$item->name = $request->get('name');
			$item->id_unit = $request->get('id_unit');
			$item->barcode = $request->get('barcode');
			$item->active = $request->get('active');
			$item->include_in_interest_calc = $request->get('include_in_interest_calc');
			$item->idpurchase_account = $request->get('idpurchase_account');
			$item->idcost_account = $request->get('idcost_account');
			$item->idpurchase_discount_account = $request->get('idpurchase_discount_account');
			$item->cost = $request->get('cost');
			$item->purchase_description = $request->get('purchase_description');
			$item->idsale_account = $request->get('idsale_account');
			$item->idsale_discount_account = $request->get('idsale_discount_account');
			$item->idsale_discount_account_with_minus = $request->get('idsale_discount_account_with_minus');
			$item->price = $request->get('price');
			$item->id_tax = $request->get('id_tax');
			$item->sale_description = $request->get('sale_description');
			$item->id_account = $request->get('id_account');
			$item->id_currency = $request->get('id_currency');
			$item->opening_quantity = $request->get('opening_quantity');
			$item->opening_amount = $request->get('opening_amount');
			$item->opening_exchange_rate = $request->get('opening_exchange_rate');
			$item->opening_date = $request->get('opening_date');
			$item->id_warehouse = $request->get('id_warehouse');
			$item->reorder_point = $request->get('reorder_point');
			$item->max_quantity = $request->get('max_quantity');
			$item->save();

			\Cache::flushTagDir('Item');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
