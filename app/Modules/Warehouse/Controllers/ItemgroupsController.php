<?php namespace Warehouse\Controllers;

use Warehouse\Models\Item;
use Warehouse\Models\Itemgroup;
use Warehouse\Models\Itemtype;
use Warehouse\Models\Itemkind;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ItemgroupsController extends Controller {

    public function load(Request $request)
	{
		$result = \Cache::cacheDataForever('itemgroups/load', function()
		{
			$sql = "SELECT id, name, description, accountinginventorymethod_name, id_item_accounts_schema, id_itemtype
                      FROM  itemgroups
                      WHERE deleted=0";

			$statement = \AccUtils::db('r')->query($sql);
			return $statement->fetchAll(\PDO::FETCH_ASSOC);
		});

		if($result['cache_version'] == $request->get('cache_version'))
			$result = ['use_cache' => true];

		return response()->json($result);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$result = array();
		$result['tree_data'] = $this->getItemKindsHierarchy();
		$result['price_levels_count'] = (int) \CoreUtils::getSettings('price_levels_count');
		return response()->json($result);
	}

	private function getItemKindsHierarchy($id=null, $exclude_id=null)
	{
		$result = Itemkind::where('deleted', 0)
							->where('id', '<>', 4)
							->get();

		foreach($result as $itemkind)
		{
			$itemkind->type = 'itemkind';
			$children = $this->getItemTypesHierarchy($itemkind->id);
			if($children!=NULL && count($children)>0)
				$itemkind->items = $children;
		}

		return $result;
	}

	private function getItemTypesHierarchy($itemkind_id)
	{
		$result = Itemtype::where('deleted', 0)
							->where('id_itemkind', $itemkind_id)
							->get();

		foreach($result as $itemtype)
		{
			$itemtype->type = 'itemtype';
			$children = $this->getItemGroupsHierarchy($itemtype->id, $itemkind_id);
			if($children!=NULL && count($children)>0)
				$itemtype->items = $children;
		}

		return $result;
	}

	private function getItemGroupsHierarchy($itemtype_id, $id_itemkind, $id_itemgroup=0)
	{
		$result = Itemgroup::where('deleted', 0)
							->where('id_itemtype', $itemtype_id)
							->where('id_itemgroup', $id_itemgroup)
							->get();

		foreach($result as $itemgroup)
		{
			$itemgroup->id_itemkind = $id_itemkind;
			$itemgroup->type = 'itemgroup';
			$children = $this->getItemGroupsHierarchy($itemtype_id, $id_itemkind, $itemgroup->id);
			if($children!=NULL && count($children)>0)
				$itemgroup->items = $children;
		}

		return $result;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();

		return response()->json($result);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();

		$result['model'] = Itemgroup::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$itemgroup = Itemgroup::findOrFail($request->get('id'));

				$nameExists = Itemgroup::where('name', $request->get('name'))
					->where('id', '<>', $itemgroup->id)
					->exists();
			}
			else
			{
				$itemgroup = new Itemgroup();
				//$itemgroup->created_at = date("Y-m-d H:i:s");

				$nameExists = Itemgroup::where('name', $request->get('name'))
					->where('deleted', 0)
					->exists();

			}

			//if($nameExists)
			//	throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			
			$itemgroup->name = $request->get('name');
			$itemgroup->description = $request->get('description');
			$itemgroup->id_itemgroup = 0;//
			$itemgroup->id_itemtype = $request->get('id_itemtype');
			$itemgroup->accountinginventorymethod_name = $request->get('accountinginventorymethod_name');
			$itemgroup->id_item_accounts_schema = $request->get('id_item_accounts_schema');
			$itemgroup->save();

			\Cache::flushTagDir('Itemgroup');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			info($e->getLine());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
