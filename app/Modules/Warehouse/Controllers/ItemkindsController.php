<?php namespace Warehouse\Controllers;

use Warehouse\Models\Itemkind;
use Warehouse\Models\Itemtype;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ItemkindsController extends Controller {

	public function load(Request $request)
	{
		$result = \Cache::cacheDataForever('itemkinds/load', function()
		{
			$sql = "SELECT  id,
							name
                      FROM  itemkinds
                      WHERE deleted=0";

			$statement = \AccUtils::db('r')->query($sql);
			return $statement->fetchAll(\PDO::FETCH_ASSOC);
		});

		if($result['cache_version'] == $request->get('cache_version'))
			$result = ['use_cache' => true];

		return response()->json($result);
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
		$response = "";
		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();
		$result['itemtypes'] = Itemtype::where('deleted', 0)
										->select('id', 'name')
										->get();
		return response()->json($result);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();

		$result['model'] = Itemkind::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$itemkind = Itemkind::findOrFail($request->get('id'));

				$nameExists = Itemkind::where('name', $request->get('name'))
					->where('id', '<>', $itemkind->id)
					->exists();
			}
			else
			{
				$itemkind = new Itemkind();
				$itemkind->created_at = date("Y-m-d H:i:s");

				$nameExists = Itemkind::where('name', $request->get('name'))
					->where('deleted', 0)
					->exists();

			}

			if($nameExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			
			$itemkind->name = $request->get('name');
			$itemkind->system_id_itemtype = $request->get('system_id_itemtype');
			$itemkind->description = $request->get('description');
			$itemkind->save();

			\Cache::flushTagDir('Itemkind');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			info($e->getLine());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
