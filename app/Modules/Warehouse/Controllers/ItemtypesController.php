<?php namespace Warehouse\Controllers;

use Warehouse\Models\Itemtype;
use Warehouse\Models\Itemkind;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ItemtypesController extends Controller {

	public function load(Request $request)
	{
		$result = \Cache::cacheDataForever('itemtypes/load', function()
		{
			$sql = "SELECT  id,
							name,
							id_itemkind
                      FROM  itemtypes
                      WHERE deleted=0";

			$statement = \AccUtils::db('r')->query($sql);
			return $statement->fetchAll(\PDO::FETCH_ASSOC);
		});

		if($result['cache_version'] == $request->get('cache_version'))
			$result = ['use_cache' => true];

		return response()->json($result);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
		$response = "";
		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();
		$result['itemtypes'] = Itemtype::where('deleted', 0)
										->select('id', 'name')
										->get();
		$result['itemkinds'] = Itemkind::where('deleted', 0)
										->select('id', 'name')
										->get();

		return response()->json($result);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();

		$result['model'] = Itemtype::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$itemtype = Itemtype::findOrFail($request->get('id'));

				$nameExists = Itemtype::where('name', $request->get('name'))
					->where('id', '<>', $itemtype->id)
					->exists();
			}
			else
			{
				$itemtype = new Itemtype();
				$itemtype->created_at = date("Y-m-d H:i:s");

				$nameExists = Itemtype::where('name', $request->get('name'))
					->where('deleted', 0)
					->exists();

			}

			if($nameExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			
			$itemtype->name = $request->get('name');
			$itemtype->id_itemkind = $request->get('id_itemkind');
			$itemtype->system_id_itemtype = $request->get('system_id_itemtype');
			$itemtype->description = $request->get('description');
			$itemtype->save();

			\Cache::flushTagDir('Itemtype');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			info($e->getLine());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
