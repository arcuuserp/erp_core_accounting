<?php namespace Warehouse\Controllers;

use Warehouse\Models\Itemaccountsschema;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ItemaccountsschemasController extends Controller {

	public function load(Request $request)
	{
		$itemkindID = $request->get('id_itemkind');
		info($itemkindID);

		$sql = "SELECT id, name, id_itemkind, code
                  FROM  item_account_schemas
                  WHERE id_itemkind = :itemkindID";

		$statement = \AccUtils::db('r')->prepare($sql);
		$statement->bindValue(':itemkindID', $itemkindID);
		$statement->execute();
		$schemas = $statement->fetchAll(\PDO::FETCH_ASSOC);

		$result = [];
		foreach ($schemas as $schema)
		{
			$schema['accounts'] = \AccUtils::db()->table('item_account_schemas_accounts')
												->where('id_schema', $schema['id'])
												->select()
												->get();
			$result[] =  $schema;
		}

		return response()->json($result);
	}


	public function grid(Request $request)
	{
		$ds = new \App\Libs\KendoUI\DataSource($request->all(), 'acc_read');//READ ONLY erp_acc_passive

		$properties = [
		        'id_documentgroup',
		        'from_number',
		        'to_number',
		        'current',
		        'from_date',
		        'till_date',
		        'type',
		    ];

		$select = $ds->prepareColumns($properties);
		$cond = $ds->prepareFilters($properties, "AND");
		$sort = $ds->prepareSort($properties, true);

		$query = " FROM item_account_schemas WHERE deleted=0 ".$cond.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);//true=debug
		$response['total'] = $ds->executeCount($query);


		return response()->json($response);
	}

	

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$data = [
			'itemkinds_version' => \Cache::getCacheVersion('itemkinds/load')
		];

		$data['schemas_structure'] = \AccUtils::db()->table('item_account_schemas_structure')
													->where('active', 1)
													->select()
													->get();
												

		return response()->json($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();
		$result['currencies'] = app()->make('\Core\Controllers\CurrenciesController')->activeCurrencies(true);

		return response()->json($result);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();
		$result['currencies'] = app()->make('\Core\Controllers\CurrenciesController')->activeCurrencies(true);

		$result['model'] = Documentgroup::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$items = $request->get('items');
		try {

			\DB::beginTransaction();

			foreach($items as $doc_item)
			{
				$doc = Documentgroup::find($doc_item['id']);

				
				
				$doc->serial_format = $doc_item['serial_format'];
				$doc->number_format = $doc_item['number_format'];
				$doc->manual_number = $doc_item['manual_number'];
				$doc->allow_number_duplication_notify = $doc_item['allow_number_duplication_notify'];
				$doc->next_doc_number_suggest_pos = $doc_item['next_doc_number_suggest_pos'];
				$doc->manual_serial = $doc_item['manual_serial'];
				$doc->allow_serial_duplication_notify = $doc_item['allow_serial_duplication_notify'];
				$doc->next_serial_suggest_pos = $doc_item['next_serial_suggest_pos'];
				$doc->save();
			}

			\DB::commit();
			\Cache::flushTagDir('Documentgroup');

			$statusCode = 200;
			$response = '';

		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			\DB::rollback();
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
