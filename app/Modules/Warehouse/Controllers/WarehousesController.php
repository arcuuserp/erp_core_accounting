<?php namespace Warehouse\Controllers;

use Warehouse\Models\Warehouse;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class WarehousesController extends Controller {

	public function load(Request $request, $id=null)
	{
		if($id)
		{
			$result = Warehouse::find($id);
		}
		else
		{
			$result = \Cache::cacheDataForever('warehouses/all', function()
			{
				$sql = "SELECT id, code, concat(code, ' - ', name) as code_name
	                      FROM warehouse
	                     WHERE deleted=0
	                       AND active=1";

				$statement = \DB::connection('acc')->getPdo()->query($sql);

				return $statement->fetchAll(\PDO::FETCH_ASSOC);
			});

			if($result['cache_version'] == $request->get('cache_version'))
				$result = ['use_cache' => true];
		}

		return response()->json($result);
	}

	public function browse(Request $request)
	{
		$ds = new \App\Libs\KendoUI\DataSource($request->all(), 'acc_read');//READ ONLY erp_acc_passive

		$ds->extraFilter('deleted', 0);

		$properties = [
		        'name',
		        'code',
		    ];

		$select = $ds->prepareColumns($properties);
		$where = $ds->prepareFilters($properties, true);
		$sort = $ds->prepareSort($properties, true);

		$query = " FROM warehouse ".$where.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);//true=debug
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}

	public function grid(Request $request)
	{
		$inputs = $request->all();
		$ds = new \App\Libs\KendoUI\DataSource($inputs, 'acc_read');//READ ONLY erp_acc_passive

		info('inputs', [$inputs]);

		$properties = [
		        'id_documentgroup',
		        'document_date' => array('type' => 'date'),
		        'description',
		        'document_number',
		        'currency',
		        'total_with_vat' => array('type' => 'number'),
		        'id_contact' => array('type' => 'number'),
		    ];

		$fieldMapping = null;/*array(
			'name'=>'currencies.name',
			'active'=>'currencies.active'
		);*/

		$select = $ds->prepareColumns($properties, $fieldMapping);
		$where = $ds->prepareFilters($properties, true, $fieldMapping);
		$sort = $ds->prepareSort($properties, true, $fieldMapping);

		$select .= ',subject';
		$query = " FROM document_transactions ".$where.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);//true=debug
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$result = array();
		return response()->json($result);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();
		$result['units'] = \DB::table('units')
								->where('deleted', 0)
								->select('id', 'name')
								->get();

		return response()->json($result);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();

		$result['model'] = Warehouse::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$warehouse = Warehouse::findOrFail($request->get('id'));

				$nameExists = Warehouse::where('name', $request->get('name'))
					->where('id', '<>', $warehouse->id)
					->exists();

				$codeExists = Warehouse::where('code', $request->get('code'))
					->where('id', '<>', $warehouse->id)
					->exists();
			}
			else
			{
				$warehouse = new Warehouse();
				$warehouse->created_at = date("Y-m-d H:i:s");


				$nameExists = Warehouse::where('name', $request->get('name'))
					->where('deleted', 0)
					->exists();

				$codeExists = Warehouse::where('code', $request->get('code'))
					->where('deleted', 0)
					->exists();
			}

			if($nameExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			if($codeExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);

			$warehouse->code = $request->get('code');
			$warehouse->name = $request->get('name');
			$warehouse->id_unit = $request->get('id_unit');
			$warehouse->address = $request->get('address');
			$warehouse->responsible_name = $request->get('responsible_name');
			$warehouse->description = $request->get('description');
			$warehouse->active = $request->get('active');
			$warehouse->logical = $request->get('logical');
			$warehouse->min_quantity = $request->get('min_quantity');
			$warehouse->max_quantity = $request->get('max_quantity');
			$warehouse->id_unit = $request->get('id_unit');
			$warehouse->allow_sales_if_nobalance = $request->get('allow_sales_if_nobalance');
			$warehouse->notify_if_nobalance = $request->get('notify_if_nobalance');
			$warehouse->save();

			\Cache::flushTagDir('warehouse');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
