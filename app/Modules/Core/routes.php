<?php

/*
|--------------------------------------------------------------------------
| Core Routes
|--------------------------------------------------------------------------
*/
Route::resource('companydetails',   	'CompanydetailsController');
Route::get('currencies/load',       	'CurrenciesController@load');
Route::get('currencies/grid',       	'CurrenciesController@grid');
Route::resource('currencies',       	'CurrenciesController');

Route::get('industrycategories/grid',   'IndustrycategoriesController@grid');
Route::resource('industrycategories',   'IndustrycategoriesController');

Route::resource('reports',       	    'ReportsController');

Route::get('roles/grid',       	        'RolesController@grid');
Route::resource('roles',       	        'RolesController');

Route::resource('settings',       		'SettingsController');

Route::get('taxes/grid',   				'TaxesController@grid');
Route::resource('taxes',   				'TaxesController');

Route::resource('unit',	                'UnitController');
Route::get('users/grid',       	        'UsersController@grid');
Route::resource('users',       	        'UsersController');




//DevContorllers
//Route::get('dev/kendo', 'DevController@buildKendoComponents');
Route::get('dev/report', 'DevController@report');