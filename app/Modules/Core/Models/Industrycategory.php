<?php namespace Core\Models;

use Illuminate\Database\Eloquent\Model;

class Industrycategory extends Model {

	protected $table = 'industrycategories';
	protected $connection = 'core';
	public $timestamps = false;

}
