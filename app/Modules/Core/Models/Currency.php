<?php namespace Core\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model {

	protected $table = 'currencies';
	protected $connection = 'core';
	public $timestamps = false;

}
