<?php namespace Core\Models;

use Illuminate\Database\Eloquent\Model;

class Prove extends Model {

	protected $table = 'proves';
	protected $connection = 'core';
	protected $guarded = array('id');
	public $timestamps = false;

}
