<?php namespace Core\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

	protected $connection = 'core';
	public $timestamps = false;

}
