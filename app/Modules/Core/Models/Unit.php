<?php namespace Core\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model {

	protected $table = 'units';
	protected $connection = 'core';
	public $timestamps = false;

}
