<?php namespace Core\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

	protected $table = 'settings';
	protected $connection = 'core';
	public $timestamps = false;

}
