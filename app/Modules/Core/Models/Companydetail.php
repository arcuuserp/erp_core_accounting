<?php namespace Core\Models;

use Illuminate\Database\Eloquent\Model;

class Companydetail extends Model {

	protected $table = 'company';
	protected $connection = 'core';
	public $timestamps = false;

}
