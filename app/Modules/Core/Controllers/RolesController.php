<?php namespace Core\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Core\Models\Role;
use Illuminate\Http\Request;


class RolesController extends Controller {

	public function grid(Request $request)
	{
		
		$ds = new \App\Libs\KendoUI\DataSource($request->all(), 'core');//READ ONLY erp_acc_generics
		//HERE COULD BE ADDED ADDITIONAL FILTER
		
		$properties = [
		        'role_name',
		        'description',
		       ];

		$select = $ds->prepareColumns($properties);
		$cond = $ds->prepareFilters($properties, "AND");
		$sort = $ds->prepareSort($properties, true);

		
		$query = " FROM roles ".$cond.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}

	//http://stackoverflow.com/questions/15205239/call-a-controller-in-laravel-4

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
			$response = '';

		return response()->json($response, $statusCode);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$statusCode = 200;
		$response = "";
		return response()->json($response, 200);
		//return response()->make($response, $statusCode);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$roles = Role::findOrFail($request->get('id'));
			}
			else
			{
				$roles = new Role();
				$roles->created_at =date('Y-m-d H:i:s');
			}

			$roles->role_name = $request->get('role_name');
			$roles->description = $request->get('description');
			$roles->save();

			\Cache::flushTagDir('roles');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = Role::find($id);
		return response()->json($result);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
