<?php namespace Core\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Core\Models\Setting;
use Illuminate\Http\Request;


class SettingsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
			$response = '';

		return response()->json($response, $statusCode);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$statusCode = 200;
		$response = "";
		return response()->json($response, 200);
		//return response()->make($response, $statusCode);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$inputs = $request->all();
		info('inputs', [$inputs]);

		try {

			\DB::beginTransaction();
			$sql = "UPDATE settings set value=:value WHERE name=:name";
			$statement = \DB::connection('core')->getPdo()->prepare($sql);

			foreach($inputs as $name => $value)
			{
				$statement->bindParam(':name', $name);
				$statement->bindParam(':value', $value);
				$statement->execute(); 
			}

			\DB::commit();
			\Cache::flushTagDir('settings');

			$statusCode = 200;
			$response = '';

		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			\DB::rollback();
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($code)
	{
		$settingsgroup = \DB::table('settingsgroup')->where('code', $code)
								->where('deleted', 0)
								->select('id')
								->first();

		$settings = Setting::where('id_settingsgroup', $settingsgroup->id)->get();
		//info('settings', [$settings]);

		$result = [];
		foreach($settings as $setting)
		{
			$result[$setting->name] = $setting->value;
		}

		return response()->json($result);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		
		$result = Currency::find($id);
		return response()->json($result);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
