<?php namespace Core\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Core\Models\Currency;
use Illuminate\Http\Request;


class CurrenciesController extends Controller {

	public function grid(Request $request)
	{
		$inputs = $request->all();
		$ds = new \App\Libs\KendoUI\DataSource($inputs, 'core');//READ ONLY erp_acc_generics
		//HERE COULD BE ADDED ADDITIONAL FILTER
		$ds->extraFilter('deleted', 1);//field, value, operator='eq', logic='and'

		$properties = [
		        'name',
		        'description',
		        'created_at' => array('type' => 'date'),
		        'active' => array('type' => 'number'),
		    ];

		$fieldMapping = array(
			'name'=>'currencies.name',
			'active'=>'currencies.active'
		);

		$select = $ds->prepareColumns($properties, $fieldMapping);
		$where = $ds->prepareFilters($properties, true, $fieldMapping);
		$sort = $ds->prepareSort($properties, true, $fieldMapping);

		$select .= ',created_at';
		$query = " FROM currencies ".$where.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}

	//http://stackoverflow.com/questions/15205239/call-a-controller-in-laravel-4
	public function activeCurrencies($internal=false)
	{
		$result = \Cache::rememberForever('currencies/active', function()
		{
			$sql = "SELECT  id, name
                      FROM  currencies
                     WHERE deleted=0
                       AND active=1";

			$statement = \DB::connection('core')->getPdo()->query($sql);

			return $statement->fetchAll(\PDO::FETCH_ASSOC);
		});

		if($internal)
			return $result;

		return response()->json($result);
	}

	public function load(Request $request)
	{
		$result = \Cache::cacheDataForever('currencies/active', function()
		{
			$sql = "SELECT id, name
                      FROM currencies
                     WHERE deleted=0
                       AND active=1";

			$statement = \DB::connection('core')->getPdo()->query($sql);

			return $statement->fetchAll(\PDO::FETCH_ASSOC);
		});

		if($result['cache_version'] == $request->get('cache_version'))
			$result = ['use_cache' => true];

		return response()->json($result);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
			$response = '';

		return response()->json($response, $statusCode);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$statusCode = 200;
		$response = "";
		return response()->json($response, 200);
		//return response()->make($response, $statusCode);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$currency = Currency::findOrFail($request->get('id'));
			}
			else
			{
				$currency = new Currency();
				$currency->created_at =date('Y-m-d H:i:s');
			}

			$currency->name = $request->get('name');
			$currency->description = $request->get('description');
			$currency->active = $request->get('active');
			$currency->created_at = $request->get('created_at');
			$currency->save();

			\Cache::flushTagDir('currencies');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = Currency::find($id);
		return response()->json($result);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
