<?php namespace Core\Controllers;

use Core\Models\Industrycategory;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class IndustrycategoriesController extends Controller {

 	public function grid(Request $request)
	{
		$inputs = $request->all();
		$ds = new \App\Libs\KendoUI\DataSource($inputs, 'core');
		

		$properties = [
		        'name',
		        'description',
		        'parent_name',
		    ];

		$fieldMapping = array(
			'name'=>'child.name',
			'description'=>'child.description',
			'id'=>'child.id',
			'parent_name'=>'parent.name',
		);

		$select = $ds->prepareColumns($properties, $fieldMapping);
		$where = $ds->prepareFilters($properties, true, $fieldMapping);
		$sort = $ds->prepareSort($properties, true, $fieldMapping);

		$query = " FROM industrycategories AS child 
						INNER JOIN industrycategories AS parent
						ON (child.id_industrycategory=parent.id) ".$where.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
		$response = "";
		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();
		$result['industrycategoriesParent'] = Industrycategory::where('deleted', 0)
										->select('id', 'name')
										->get();
		return response()->json($result);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();
		$result['model'] = Industrycategory::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$industrycategorie = Industrycategory::findOrFail($request->get('id'));

				$nameExists = Industrycategory::where('name', $request->get('name'))
					->where('id', '<>', $industrycategorie->id)
					->exists();
			}
			else
			{
				$industrycategorie = new Industrycategory();
				$industrycategorie->created_at = date("Y-m-d H:i:s");

				$nameExists = Industrycategory::where('name', $request->get('name'))
					->where('deleted', 0)
					->exists();

			}

			if($nameExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			
			$industrycategorie->name = $request->get('name');
			$industrycategorie->id_industrycategory = $request->get('id_industrycategory');
			$industrycategorie->description = $request->get('description');
			$industrycategorie->save();

			\Cache::flushTagDir('industrycategory');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			info($e->getLine());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
