<?php namespace Core\Controllers;

use Core\Models\Taxes;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class TaxesController extends Controller {

 	public function grid(Request $request)
	{
		$inputs = $request->all();
		$ds = new \App\Libs\KendoUI\DataSource($inputs, 'acc');//READ ONLY erp_acc_generics
		//HERE COULD BE ADDED ADDITIONAL FILTER
		$ds->extraFilter('deleted', 1);//field, value, operator='eq', logic='and'

		$properties = [
		        'name',
		        'percentage',
		        'tax_rate' => array('type' => 'number'),
		        'active' => array('type' => 'number'),
		    ];


		$select = $ds->prepareColumns($properties);
		$where = $ds->prepareFilters($properties,"AND");
		$sort = $ds->prepareSort($properties, true);

		$query = " FROM taxes WHERE deleted=0 ".$where.$sort;

		$response['data'] = $ds->executeResult($query, $select, true);
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
		$response = "";
		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();

		return response()->json($result);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();
		$result['model'] = Taxes::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		try {

			if($request->exists('id'))
			{
				$taxes = Taxes::findOrFail($request->get('id'));

				$nameExists = Taxes::where('name', $request->get('name'))
					->where('id', '<>', $taxes->id)
					->exists();
			}
			else
			{
				$taxes = new Taxes();
				$taxes->created_at = date("Y-m-d H:i:s");

				$nameExists = Taxes::where('name', $request->get('name'))
					->where('deleted', 0)
					->exists();

			}

			if($nameExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			
			$taxes->name = $request->get('name');
			$taxes->percentage = $request->get('percentage');
			$taxes->tax_rate = $request->get('tax_rate');
			$taxes->active = $request->get('active');
			$taxes->save();

			\Cache::flushTagDir('taxes');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			info($e->getLine());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		try {

			$taxes = Taxes::findOrFail($id);
			$taxes->deleted = 1;
			$taxes->save();

			\Cache::flushTagDir('taxes');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

}
