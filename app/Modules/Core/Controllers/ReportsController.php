<?php namespace Core\Controllers;

use Jaspersoft\Client\Client as JasperClient;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ReportsController extends Controller {


	public function pdf()
	{
		$statusCode = 500;
		$response = 'SERVER.JASPER_LOGIN_FAILED';

		$url = "http://localhost:8080/jasperserver/rest/login";
		$credentials = array('j_username'=>'jasperadmin',
							 'j_password'=>'jasperadmin');

		try {
	        $curl = curl_init($url);

            curl_setopt($curl, CURLOPT_FORBID_REUSE,        false);
            curl_setopt($curl, CURLOPT_FRESH_CONNECT,       false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,      true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,      0);
            curl_setopt($curl, CURLOPT_DNS_CACHE_TIMEOUT,   1024);
            curl_setopt($curl, CURLOPT_COOKIESESSION,       false);
            curl_setopt($curl, CURLOPT_ENCODING,            'identity');
			curl_setopt($curl, CURLOPT_COOKIEFILE,          '/dev/null');
            curl_setopt($curl, CURLOPT_HEADER,              true);
	        //post request
            curl_setopt($curl, CURLOPT_POST,                true);
            curl_setopt($curl, CURLOPT_POSTFIELDS,          http_build_query($credentials));

            $body = curl_exec($curl);
			//var_dump($body);
            $head = curl_getinfo($curl);
			//var_dump($head);
			$statusCode = $head['http_code'];

			if($statusCode == 200)
	        {
		        /*preg_match('/^Set-Cookie: (.*?);/m', $body, $m);
		        //var_dump($m);//$m[1] holds the jsession variable
			    $jSession = $m[1];
				$response = ['jasper_cookie'=>'Cookie: $Version=0; '.$jSession.'; $Path=/jasperserver'];
				*/

		        // Cookie: $Version=0; JSESSIONID=52E79BCEE51381DF32637EC69AD698AE; $Path=/jasperserver
				// Cookie: JSESSIONID=<sessionID>; $Path=<pathToJRS>
				// Extract the Cookie and save the string in my session for further requests.
				preg_match('/^Set-Cookie: (.*?)$/sm', $body, $cookie);
				$_SESSION["JSCookie"] = '$Version=0; ' . str_replace('Path', '$Path', $cookie[1]);

				// Grab the JS Session ID and set the cookie in the right path so
				// when I present an iFrame I can use that session to be authenticated
				// For this to work JS and the App have to run in the same domain

				preg_match_all('/=(.*?);/' , $cookie[1], $cookievalue);
	            $JRSSessionID = $cookievalue[1][0];
	            $JRSPath = $cookievalue[1][1];
	            $_SESSION["JRSSessionID"] = $JRSSessionID;
	            $_SESSION["JRSPath"] = $JRSPath;
				setcookie('JSESSIONID', $JRSSessionID , time() + (60*60*24), $JRSPath);
	        }
			else
			{
				throw new \Exception("Error code: {$statusCode} on POST request ({$url}), BODY: {$body}", $statusCode);
			}

            curl_close($curl);
        }
		catch (\Exception $e)
		{
			$statusCode = 500;
			info($e->getLine(), [$e->getCode(), $e->getMessage()]);
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
		$data = [];
		/*
		$c = new JasperClient(
				"http://localhost:8080/jasperserver",
				"jasperadmin",
				"jasperadmin"
			);
		
		$data['report'] = $c->reportService()->runReport('/reports/test_one', 'html');//pdf
		*/
		$data['report'] = 'user';
		
		return response($data, 200);
	}

	public function report()
	{
		$c = new JasperClient(
				"http://localhost:8080/jasperserver",
				"jasperadmin",
				"jasperadmin"
			);

		//Blank_A4
		//$report = $c->reportService()->runReport('/reports/interactive/CustomersReport', 'html');//pdf
		$report = $c->reportService()->runReport('/reports/test_one', 'html');//pdf

		/* PDF Download
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename=report.pdf');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . strlen($report));
		header('Content-Type: application/pdf');
		*/

		echo $report;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();

		return response()->json($result);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();
		$result['model'] = Taxes::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		try {

			if($request->exists('id'))
			{
				$taxes = Taxes::findOrFail($request->get('id'));

				$nameExists = Taxes::where('name', $request->get('name'))
					->where('id', '<>', $taxes->id)
					->exists();
			}
			else
			{
				$taxes = new Taxes();
				$taxes->created_at = date("Y-m-d H:i:s");

				$nameExists = Taxes::where('name', $request->get('name'))
					->where('deleted', 0)
					->exists();

			}

			if($nameExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			
			$taxes->name = $request->get('name');
			$taxes->percentage = $request->get('percentage');
			$taxes->tax_rate = $request->get('tax_rate');
			$taxes->active = $request->get('active');
			$taxes->save();

			\Cache::flushTagDir('taxes');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			info($e->getLine());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
	}

}
