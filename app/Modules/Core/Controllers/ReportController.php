<?php namespace Core\Controllers;

use Jaspersoft\Client\Client as JasperClient;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;


class DevController extends Controller {


    public function post($url, $arg = null) {
        try {
	        $curl = curl_init($url);

            curl_setopt($curl, CURLOPT_FORBID_REUSE,        false);
            curl_setopt($curl, CURLOPT_FRESH_CONNECT,       false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,      true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,      0);
            curl_setopt($curl, CURLOPT_DNS_CACHE_TIMEOUT,   1024);
            curl_setopt($curl, CURLOPT_COOKIESESSION,       false);
            curl_setopt($curl, CURLOPT_ENCODING,            'identity');
			curl_setopt($curl, CURLOPT_COOKIEFILE,          '/dev/null');
            curl_setopt($curl, CURLOPT_HEADER,              true);
	        //post request
            curl_setopt($curl, CURLOPT_POST,                true);
            curl_setopt($curl, CURLOPT_POSTFIELDS,          http_build_query($arg));

            $body = curl_exec($curl);
            $head = curl_getinfo($curl);

	        if($head['http_code'] == 200)
	        {
		        preg_match('/^Set-Cookie: (.*?);/m', $body, $m);
		        var_dump($m);
			    //$m[1] holds the jsession variable
			    $jSession = $m[1];
				$cookie = 'Cookie: $Version=0; '.$jSession.'; $Path=/jasperserver';
	        }
            curl_close($curl);

        } catch(\Exception $e) {
            throw $e;
        }
        if ($head['http_code'] >= 400) {
	        echo 'error, head: '.$head['http_code'].', body:'.$body.', url:'.$url;
	        //throw new \Exception("{$head['http_code']} on POST request ({$url})", $head['http_code'], $body);
        }

	    //session id
	    //https://github.com/marianol/JasperServer-for-PHP/blob/master/web_root/login.php
	    /*
	     * // Cookie: JSESSIONID=<sessionID>; $Path=<pathToJRS>
			// Extract the Cookie and save the string in my session for further requests.
			preg_match('/^Set-Cookie: (.*?)$/sm', $body, $cookie);
			$_SESSION["JSCookie"] = '$Version=0; ' . str_replace('Path', '$Path', $cookie[1]);

			// Grab the JS Session ID and set the cookie in the right path so
			// when I present an iFrame I can use that session to be authenticated
			// For this to work JS and the App have to run in the same domain

			preg_match_all('/=(.*?);/' , $cookie[1], $cookievalue);
            $JRSSessionID = $cookievalue[1][0];
            $JRSPath = $cookievalue[1][1];
            $_SESSION["JRSSessionID"] = $JRSSessionID;
            $_SESSION["JRSPath"] = $JRSPath;
			setcookie('JSESSIONID', $JRSSessionID , time() + (3600 * 3), $JRSPath);
	     * */
        return array('header' => $head, '-------------------------------body' => $body);
    }

	public function report1()
	{
		$statusCode = 500;
		$response = 'SERVER.JASPER_LOGIN_FAILED';

		$url = "http://localhost:8080/jasperserver/rest/login";
		$credentials = array('j_username'=>'jasperadmin',
							 'j_password'=>'jasperadmin');

		try {
	        $curl = curl_init($url);

            curl_setopt($curl, CURLOPT_FORBID_REUSE,        false);
            curl_setopt($curl, CURLOPT_FRESH_CONNECT,       false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,      true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,      0);
            curl_setopt($curl, CURLOPT_DNS_CACHE_TIMEOUT,   1024);
            curl_setopt($curl, CURLOPT_COOKIESESSION,       false);
            curl_setopt($curl, CURLOPT_ENCODING,            'identity');
			curl_setopt($curl, CURLOPT_COOKIEFILE,          '/dev/null');
            curl_setopt($curl, CURLOPT_HEADER,              true);
	        //post request
            curl_setopt($curl, CURLOPT_POST,                true);
            curl_setopt($curl, CURLOPT_POSTFIELDS,          http_build_query($credentials));

            $body = curl_exec($curl);
			//var_dump($body);
            $head = curl_getinfo($curl);
			//var_dump($head);
			$statusCode = $head['http_code'];

			if($statusCode == 200)
	        {
		        /*preg_match('/^Set-Cookie: (.*?);/m', $body, $m);
		        //var_dump($m);//$m[1] holds the jsession variable
			    $jSession = $m[1];
				$response = ['jasper_cookie'=>'Cookie: $Version=0; '.$jSession.'; $Path=/jasperserver'];
				*/

		        // Cookie: $Version=0; JSESSIONID=52E79BCEE51381DF32637EC69AD698AE; $Path=/jasperserver
				// Cookie: JSESSIONID=<sessionID>; $Path=<pathToJRS>
				// Extract the Cookie and save the string in my session for further requests.
				preg_match('/^Set-Cookie: (.*?)$/sm', $body, $cookie);
				$_SESSION["JSCookie"] = '$Version=0; ' . str_replace('Path', '$Path', $cookie[1]);
				$response = $cookie[1];
				// Grab the JS Session ID and set the cookie in the right path so
				// when I present an iFrame I can use that session to be authenticated
				// For this to work JS and the App have to run in the same domain

				preg_match_all('/=(.*?);/' , $cookie[1], $cookievalue);
	            $JRSSessionID = $cookievalue[1][0];
	            $JRSPath = $cookievalue[1][1];
	            $_SESSION["JRSSessionID"] = $JRSSessionID;
	            $_SESSION["JRSPath"] = $JRSPath;
				setcookie('JSESSIONID', $JRSSessionID , time() + (60*60*24), $JRSPath);
	        }
			else
			{
				throw new \Exception("Error code: {$statusCode} on POST request ({$url}), BODY: {$body}", $statusCode);
			}

            curl_close($curl);
        }
		catch (\Exception $e)
		{
			$statusCode = 500;
			info($e->getLine(), [$e->getCode(), $e->getMessage()]);
		}

		return response()->json($response, $statusCode);
	}

	public function report()
	{
		$jasperLogin = \CoreUtils::jasperLogin();
		return response('');
	}

	public function report4()
	{
		$jasperLogin = \CoreUtils::jasperLogin();
		$jasperLogin = $jasperLogin['response'];
		setcookie('JSESSIONID', $jasperLogin['JRSSessionID'] , time() + (60*60*24), $jasperLogin['JRSPath'], null, false, false);

		$client = new JasperClient(
								"http://localhost:8080/jasperserver",
								"jasperadmin",
								"jasperadmin"
							);

		$report = $client->reportService()->runReport('/reports/test_one', 'html');//pdf
		//$report[] = $jasperLogin['jasper_cookie'];

		return response($report);
	}

	public function report3()
	{
		$client = new JasperClient(
								"http://localhost:8080/jasperserver",
								"jasperadmin",
								"jasperadmin"
							);

		$service = $client->getService();
        $restUrl2 = $client->getURL();
		//try {
			$url = $restUrl2 . '/login?j_username=jasperadmin&j_password=jasperadmin';
			print_r($url);
			$data = $service->prepAndSend($url, array(200), 'POST', ['j_username'=>'jasperadmin',
									'j_password'=>'jasperadmin'], true, 'application/json', 'application/json');

			print_r($data);
		/*}
		catch (\Jaspersoft\Exception\RESTRequestException $e)
		{
			info($e->getLine(), [$e->getCode(), $e->getMessage(), $e->getFile()]);
			info($e->getTraceAsString());
		}
		catch (\Exception $e)
		{
			info($e->getLine(), [$e->getCode(), $e->getMessage(), $e->getFile()]);
			info($e->getTraceAsString());
		}*/

		//Blank_A4
		//$report = $c->reportService()->runReport('/reports/interactive/CustomersReport', 'html');//pdf
		//$report = $client->reportService()->runReport('/reports/test_one', 'html');//pdf

		/* PDF Download
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename=report.pdf');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . strlen($report));
		header('Content-Type: application/pdf');
		*/

		//echo $report;
	}









	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function buildKendoComponents()
	{
		$statusCode = 200;
		$response = '';

		$allContent = "";
		$componentsList = [ 'core','angular','data','draganddrop','fx','sortable','view','list','popup','tooltip','binder','validator','userevents',
							'mobile.scroller','mobile.view','mobile.loader','mobile.pane','mobile.popover','mobile.shim','mobile.actionsheet',
							'columnsorter','filtermenu','menu','columnmenu','groupable','filtercell','selectable','reorderable','resizable','excel',
							'tabstrip','splitter','numerictextbox','maskedtextbox','dropdownlist','combobox','autocomplete','multiselect','calendar',
							'datepicker','panelbar','notification','grid'];
		//for each
		foreach ($componentsList as $component ){
		
			$allContent .= file_get_contents(public_path().'/libs/kendoui/js/kendo.'.$component.'.min.js');

		}

		//write $allContent to file
		file_put_contents(public_path().'/libs/kendoui/js/custom/kendo.'.microtime().'.min.js', $allContent);

		return response()->json($response, $statusCode);
	}

	

}
