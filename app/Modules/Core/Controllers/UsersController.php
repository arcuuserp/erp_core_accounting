<?php namespace Core\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Core\Models\User;
use Illuminate\Http\Request;


class UsersController extends Controller {

	public function grid(Request $request)
	{
		
		$ds = new \App\Libs\KendoUI\DataSource($request->all(), 'core');//READ ONLY erp_acc_generics
		//HERE COULD BE ADDED ADDITIONAL FILTER
		
		$properties = [
		        'username',
		        'email',
		        'firstname',
		        'lastname',
		        'id_role',
		        'status',
		        'tel',
		        'role_name',
		       ];

		$fieldMapping = array(
			'id'=>'users.id'
		);
		$select = $ds->prepareColumns($properties,$fieldMapping);
		$cond = $ds->prepareFilters($properties, "AND",$fieldMapping);
		$sort = $ds->prepareSort($properties, true,$fieldMapping);

		$query = " FROM users 
					INNER JOIN roles 
						ON users.id_role=roles.id ".$cond.$sort;
		$response['data'] = $ds->executeResult($query, $select, true);
		$response['total'] = $ds->executeCount($query);

		return response()->json($response);
	}

	//http://stackoverflow.com/questions/15205239/call-a-controller-in-laravel-4

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
			$response = '';

		return response()->json($response, $statusCode);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		
		$result = array();
		$result['roles_list'] = \DB::table('roles')->where('deleted', 0)
												->select('id', 'role_name')
												->get();
		return response()->json($result);
		//return response()->make($response, $statusCode);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$users = User::findOrFail($request->get('id'));
			}
			else
			{
				$users = new User();
				$users->created_at =date('Y-m-d H:i:s');
			}

			$users->username = $request->get('username');
			$users->password = $request->get('password');
			$users->email = $request->get('email');
			$users->firstname = $request->get('firstname');
			$users->lastname = $request->get('lastname');
			$users->id_role = $request->get('id_role');
			$users->description = $request->get('description');
			$users->tel = $request->get('tel');
			$users->status = $request->get('status');
			$users->save();

			\Cache::flushTagDir('users');

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();
		$result['roles_list'] = \DB::table('roles')->where('deleted', 0)
												->select('id', 'role_name')
												->get();
		$result['model'] = User::find($id);
		return response()->json($result);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
