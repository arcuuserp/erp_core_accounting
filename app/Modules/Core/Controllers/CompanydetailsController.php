<?php namespace Core\Controllers;

use Core\Models\Companydetail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class CompanydetailsController extends Controller {

	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$statusCode = 200;
		$response = "";
		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$result = array();
		$result['currencies'] = app()->make('\Core\Controllers\CurrenciesController')->activeCurrencies(true);
		$result['taxpayerstatus'] = \DB::table('taxpayerstatus')->where('deleted', 0)
												->select('id', 'name')
												->get();
		$result['organisationforms'] = \DB::table('organisationforms')->where('deleted', 0)
													->select('id', 'name')
													->get();
		$result['industrycategory'] = \DB::table('industrycategories')->where('deleted', 0)
													->select('id', 'name')
													->get();
		$result['model'] = Companydetail::find(1);
		
		return response()->json($result);
	}

    /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result = array();

		$result['model'] = Companydetail::find($id);
		return response()->json($result);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$statusCode = 500;
		$response = 'SERVER.FAILED';

		$input = $request->all();
		info('inputs', [$input]);

		try {

			if($request->exists('id'))
			{
				$companydetail = Companydetail::findOrFail($request->get('id'));

				$nameExists = Companydetail::where('name', $request->get('name'))
					->where('id', '<>', $companydetail->id)
					->exists();
			}
			else
			{
				$companydetail = new Companydetail();
				$companydetail->created_at = date("Y-m-d H:i:s");

				$nameExists = Companydetail::where('name', $request->get('name'))
					->where('deleted', 0)
					->exists();

			}

			if($nameExists)
				throw new \Exception($response = 'VALIDATOR.DUPLICATE_NAME', $statusCode = 412);
			
			$companydetail->name = $request->get('name');
			$companydetail->company_nipt = $request->get('company_nipt');
			$companydetail->id_industrycategory = $request->get('id_industrycategory');
			$companydetail->id_organisationform = $request->get('id_organisationform');
			$companydetail->id_taxpayerstatus = $request->get('id_taxpayerstatus');
			$companydetail->id_currency = $request->get('id_currency');
			$companydetail->id_administrativeregion = $request->get('id_administrativeregion');
			$companydetail->company_address = $request->get('company_address');
			$companydetail->phonenumber = $request->get('phonenumber');
			$companydetail->fax = $request->get('fax');
			$companydetail->email = $request->get('email');
			$companydetail->activity_detail = $request->get('activity_detail');
			$companydetail->save();

			\Cache::forever('companydetails/all', $companydetail);
			\Cache::forever('companydetails/currency', $companydetail->id_currency);

			$statusCode = 200;
			$response = '';
		}
		catch (\Exception $e)
		{
			info($e->getMessage());
			info($e->getLine());
			if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
			{
				$statusCode = 412;
				$response = 'SERVER.RECORD_NOT_FOUND';
			}
		}

		return response()->json($response, $statusCode);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
