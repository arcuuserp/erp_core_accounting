<?php namespace Core\Controllers;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Core\Models\User;
use Illuminate\Http\Request;

class AuthController extends Controller {

	public function index()
	
		Auth::logout();
		/**
		return Response::json([
				'flash' => 'you have been disconnected'],
			200
		);*/
		
		return response()->json($response);
	}

	public function checkLogin()
	{
		$response = "";
		if(Auth::check())
		{
			return Response::json(Auth::user()->toArray(), 202);
		}

		return response()->json($response, 401);
		//return Response::json(['flash' => null ], 401);
	}

	public function getLoggedInUser()
	{
		$response = "";
		if(Auth::check())
		{
			return Response::json(Auth::user()->toArray(), 202);
		}

		return response()->json($response, 401);
		//return Response::json(['flash' => null ], 401);
	}

	public function loginUser()
	{
		$validator = Validator::make(Input::all(),
						array(
							//'email' => 'required|email',
							'username' => 'required',
							'password' => 'required'
						)
					);

		if($validator->passes())
		{
			$remember = (Input::has('remember') && Input::get('remember')===true)? true : false;

			$credentials = array(
				//'email' => Input::get('email'),
				'username' => Input::get('username'),
				'password' => Input::get('password'),
				'status' => 1);//active

			if (Auth::attempt($credentials, $remember)) {
				return Response::json(Auth::user()->toArray(), 202);
			}
			else
			{
				$attemptingUser = User::where('username', Input::get('username'))->first();
				if($attemptingUser)
				{
					if($attemptingUser->status == 2)//blocked
					{
						sleep(10);
						return Response::make('You have been blocked from the system due to continues incorrect credentials. <br> Ask the administrator for assistance to restore your account.', 412);
					}
					else
						$this->registerUserThrottling($attemptingUser->id);
				}
				else
					$this->registerUserThrottling(0);
			}
		}

		return Response::json(['flash' => 'Authentication Failed'], 401);
	}


	/**
	 * Implements login throttling
	 * Reduces the efectiveness of brute force attacks
	 *
	 * @param int $user_id
	 */
	public function registerUserThrottling($user_id)
	{
		$failedLogin = new UserFailedLogin();
		$failedLogin->id_users = $user_id;
		$failedLogin->ip_address = Request::getClientIp();
		$failedLogin->attempted = time();
		$failedLogin->save();

		$attempts = UserFailedLogin::where('ip_address', Request::getClientIp())
								   ->where('attempted', '>=', (time() - 3600*2))//last 2 hours
								   ->count();

		switch ($attempts) {
			case 1:
				sleep(2);
				break;
			case 2:
				sleep(4);
				break;
			case 3:
				sleep(6);
				break;
			default:
				sleep(10);
				break;
		}

		if ($attempts > 3)
		{
			if($user_id > 0)
			{
				$blockUser = User::find($user_id);
				$blockUser->status = 2;
				$blockUser->save();
			}
		}
	}


	public function emailCredentialsForgot()
	{
		$statusCode = 500;
		$statusMsg = "";

		if(Input::exists('email'))
		{
			$result = User::where('email', Input::get('email'))->get();

			if(count($result) > 0) {
				foreach ($result as $user) {
					$this->sendForgotEmail($user);
				}

				if (count($result) > 1)
					$statusMsg = "Many accounts seem to have the same email address, therefore you will recieve multiple emails for each user. Check the appropriate username on the email to reset its password.";
				else
					$statusMsg = "Your password reset link was sent to your email. <br>Please follow the link on the email to recover your account.";

				$statusCode = 412;
			}
			else
			{
				$statusMsg = "The provided email could not be found in our system. Please enter a correct email or username.";
				$statusCode = 406;
			}
		}
		else if(Input::exists('username'))
		{
			$user = User::where('username', Input::get('username'))->first();
			if ($user != null)
			{
				$this->sendForgotEmail($user);
				$statusMsg = "Your password reset link was sent to {$user->email}. <br>Please follow the link on the email to recover your account.";
				$statusCode = 412;
			}
			else
			{
				$statusMsg = "The provided username could not be found in our system. Please enter a correct username or email.";
				$statusCode = 406;
			}
		}

		return Response::make($statusMsg, $statusCode);
	}

	private function sendForgotEmail($user)
	{
		$user->email_code = str_random(20).time();
		$user->save();

		$backUrl = Request::root().'/#/reset-password/'.$user->username.'/'.$user->email_code;
		$dynamic_vars = array("FIRST_NAME"=>$user->firstname,
			"USERNAME"=>$user->username,
			"REDIRECT_LINK_TO_PASSWORD_RECOVERY"=>$backUrl,
		);

		$this->sendEmail($user->email, $user->firstname, 'Login Credentials ', 'email_credentials_forgot', $dynamic_vars);
	}

	public function resetPassword()
	{
		$statusCode = 500;
		$statusMsg = "";

		$user = User::where('username', Input::get('username'))
					->where('email_code', Input::get('email_code'))
					->first();

		if($user != null)
		{
			$user->password = Input::get('password');
			$user->email_code = '';
			$user->save();
			$statusCode = 200;
		}
		else
		{
			$statusMsg = "The provided information was incorrect, it may have been expired, please try requesting another password reset.";
			$statusCode = 406;
		}

		return Response::make($statusMsg, $statusCode);
	}
}
