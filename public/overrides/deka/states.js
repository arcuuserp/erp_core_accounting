
erpApp.config(['$stateProvider','$ocLazyLoadProvider',
	function (  $stateProvider , $ocLazyLoadProvider ) {
	
	$stateProvider
        .state("deka-home", {
			url: "/deka-home",
			// Example of an inline template string. By default, templates
			// will populate the ui-view within the parent state's template.
			// For top level states, like this one, the parent template is
			// the index.html file. So this template will be inserted into the
			// ui-view within index.html.
			template: '<p class="lead">TEST = Welcome to the UI-Router Demo</p>' +
			'<p>Use the menu above to navigate. ' +
			'<a href="#/user/42">Bob</a> to see a url redirect in action.</p>'
		});
		
		
}]);