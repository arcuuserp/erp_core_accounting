//Module for users management
angular.module('accounting.documentbatches', [])

.controller('documentbatchesCTRL', ['$scope', '$http', '$init', '$state', function($scope, $http, $init, $state) {

}])

.controller('documentbatchesFormCTRL', ['$scope', '$http', '$init','$state', '$stateParams', function($scope, $http, $init, $stateParams,$state){

	 
	$scope.model = $init.data.model;
	if(!$init.data.model)
	{
		$scope.model = {active: 1, id_documentgroup: $stateParams.documentgroup, type: $stateParams.type};
    }

	$scope.save = function(type)
	{
		if ($scope.validator.validate())
		{
			var postData = $scope.model;
			postData.returnModel = (type=="keep");

			$http.post($init.data.resource_url, postData).success(function()
			{
				if(type == 'close')
					$scope.closeModal();
			});
		}
	};

}])

///////////////
;//end module//
///////////////

