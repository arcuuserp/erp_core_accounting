//Module for users management
angular.module('accounting.documentgroups', [])

.controller('documentgroupsCTRL', ['$scope', '$http', '$init', '$state', '$translate', function($scope, $http, $init, $state, $translate) {
  	
	$scope.mainListData = $init.data.list_data;
	$scope.selectedItem = {};

	$scope.getSelectedItem = function(item)
	{
		if(item)
			$scope.selectedItem = item;

		$scope.selectedItem.numberSuggestDirectionRadio = ($scope.selectedItem.next_doc_number_suggest_pos < 0)?0:1;
		$scope.selectedItem.serialSuggestDirectionRadio = ($scope.selectedItem.next_serial_number_suggest_pos < 0)?0:1;

		//reload grid for the receipt books
		$scope.grid.dataSource.filter({
		    logic: "and", filters: [{field:"id_documentgroup", operator:"eq", value:$scope.selectedItem.id},
		    {field:"type", operator:"eq", value: $scope.tabnavigator.select().index()}]
	    });
	};

	$scope.insertCode =	function(value, format_type){

		var cursor = $('#input_'+format_type).prop('selectionStart');
		var inputValue = $scope.selectedItem[format_type];
		
		$scope.selectedItem[format_type]= inputValue.substring(0, cursor) + value + inputValue.substring(cursor);
	};
	
	$scope.$on("kendoWidgetCreated", function(event, widget)
	{
		if (widget === $scope.grid)
		{
			$scope.grid.wrapper.height(getGridHeight( $(window).width() ));
			$scope.grid.resize(true);
			$("#grid tr.k-alt").removeClass("k-alt");
		}
		if(widget === $scope.tabnavigator)
		{
			$scope.tabnavigator.bind("show", function()
				{
					$scope.getSelectedItem()
				});
		}
	});
    


	/********************************************************
	* GRID OPTIONS
	*******************************************************/
	$(window).on("resize", function() {
		kendo.resize($(".k-grid-content"));
	});

	function getGridHeight(innerWidth)
	{
		var mainContentViewHeight = $('#main-content-view').height() - 300;
		if(innerWidth <= 767)
		{
			mainContentViewHeight += 55;
		}

		return mainContentViewHeight;
	}

	$(window).on("resize", function(e)
	{
		$scope.grid.wrapper.height(getGridHeight(e.currentTarget.innerWidth));
		$scope.grid.resize(true);
	});

	$scope.filterInit = function (event) {
		var filterType = event.sender.dataSource.options.schema.model.fields[event.field].type;
		if (filterType == "date" || filterType == "number")
		{
			var container = event.container;
			var beginOperator = container.find("[data-role=dropdownlist]:eq(0)").data("kendoDropDownList");
			var endOperator = container.find("[data-role=dropdownlist]:eq(2)").data("kendoDropDownList");

			container.data("kendoPopup").bind("open", function (e)
			{
				if(filterType == "date")
				{
					beginOperator.value("lte");
					beginOperator.trigger("change");
			        endOperator.value("gte");
					endOperator.trigger("change");
				}
				else if(filterType == "number")
				{
					beginOperator.value("gte");
					beginOperator.trigger("change");
					endOperator.value("lte");
					endOperator.trigger("change");
				}
			});
		}
	};

	$scope.gridResultTotal = 0;
	$scope.gridOptions = {
		selectable: "row",
		change: onChange,
		scrollable: { virtual: true },
		mobile: true,
		resizable: true,
		sortable: {
            mode: "multiple",
            allowUnsort: true
        },
		reorderable: true,
		filterable: { mode: "menu, row" },
		autoBind: false,
		dataSource: {
			serverPaging: true,
			serverFiltering: true,
			serverSorting: true,
			pageSize: 20,
			transport: {
				read: {
					url: "/accounting/documentgroups/grid"
				}
			},
			schema: {
				data: function (result) {
					return result.data;
				},
				total: function (result) {
					if(result.total)
						$scope.gridResultTotal = result.total;

					return $scope.gridResultTotal;
				},
				model: {
			        fields: {
				        from_number: {
					        type: "number",
					        parse: function (data) { return data; }
				        },
				        to_number: {
					        type: "number",
					        parse: function (data) { return data; }
				        },
				        current: {
					        type: "number",
					        parse: function (data) { return data; }
				        },
				        from_date: { type: "date" },
				        till_date: { type: "date" }
			        }
				}
			}
		},
		columns: [
			{field: 'from_number', title: 'Fillon nga', width: '20%', attributes: { style:"text-align:center;" },
					filterable: { cell: { showOperators: false, operator: "gte"} }
			},
			{field: 'to_number', title: 'Deri ne', width: '20%', attributes: { style:"text-align:center;" },
					filterable: { cell: { showOperators: false, operator: "gte"} }
			},
			{field: 'current', title: 'Aktuale', width: '20%', attributes: { style:"text-align:center;" },
					filterable: { cell: { showOperators: false, operator: "eq"} }
			},
			{field: 'from_date', title: 'Nga Data', width: '20%', format: "{0:dd/MM/yyyy}",
					filterable: {
						ui: dateFilterFunc,
						cell: {
							showOperators: false, operator: "lte", /*, enabled: false */
							template: function(args){
								dateFilterFunc(args.element);
							}
						}
					}
			},			
			{field: 'till_date', title: 'Deri me', width: '20%', format: "{0:dd/MM/yyyy}",
					filterable: {
						ui: dateFilterFunc,
						cell: {
							showOperators: false, operator: "lte", /*, enabled: false */
							template: function(args){
								dateFilterFunc(args.element);
							}
						}
					}
			}
		]
	};

	function dateFilterFunc(element) {
		 element.kendoDatePicker({
			 parseFormats: Utils.dateParseFormats()
		 });
    }

	$scope.createBatch = function() {
		$state.go('accounting/documentbatchesCreate', {documentgroup: $scope.selectedItem.id, type: $scope.tabnavigator.select().index() });
	}

	function onChange(arg) {
		var data = this.dataItem(this.select());
		$state.go('accounting/documentbatchesEdit', {id: data.id});
	}



	$scope.save = function()
	{
		var modifiedItems = [];
		var n = $scope.mainListData.length;
		for(var i=0; i<n; i++)
		{
			//check if it was selected at least once
			if($scope.mainListData[i]['numberSuggestDirectionRadio'])
				modifiedItems.push($scope.mainListData[i]);
		}

		var postData = {items: modifiedItems};

		$http.post($init.data.resource_url, postData).success(function()
		{
			$state.reload();
		});
	};

	$scope.refresh = function()
	{
		$state.reload();
	};


}])

.controller('documentgroupsFormCTRL', ['$scope', '$http', '$init','$state', function($scope, $http, $init,$state){

	
	$scope.currencies = $init.data.currencies;
	
	$scope.model = $init.data.model;
	if(!$init.data.model)
	{
		$scope.model = {active: 1, id_currency: 2};
    }

	

	$scope.save = function()
	{
		if ($scope.validator.validate())
		{
			$http.post($init.data.resource_url, $scope.model).success(function()
			{
				$state.reload();
			});
		}
	};

	$scope.refresh = function()
	{
		$state.reload();
	};

}])	


///////////////
;//end module//
///////////////
