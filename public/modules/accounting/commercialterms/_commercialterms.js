//Module for users management
angular.module('accounting.commercialterms', [])

.controller('commercialtermsCTRL', ['$scope', '$http', '$init', '$state', function($scope, $http, $init, $state) {

}])

.controller('commercialtermsFormCTRL', ['$scope', '$http', '$init', function($scope, $http, $init){

	
	$scope.commercialTermsMonth = [{id:'actual', name:"Aktual"},{id:'next', name:"Pasardhes"}];
	$scope.commercialTermsInterestBase =  [{id:'day', name:"Dite"},{id:'mounth', name:"Muaj"},{id:'year', name:"Vit"}];
	$scope.commercialTermsInterestType = [{id:'percent', name:"Perqindje (%)"},{id:'absolut', name:"Vlere Absolute"}];
	$scope.commercialTermsMaxInterestCalcType = [{id:'percent', name:"Perqindje (%)"},{id:'nolimit', name:"Pa limit"},{id:'absolut', name:"Vlere Absolute"}];

	$scope.model = $init.data.model;
	if(!$init.data.model)
	{
		$scope.model = {active: 1};
    }


	$scope.save = function(type)
	{
		if ($scope.validator.validate())
		{
			var postData = $scope.model;
			postData.returnModel = (type=="keep");

			$http.post($init.data.resource_url, postData).success(function()
			{
				if(type == 'close')
					$scope.closeModal();
			});
		}
	}
}])


///////////////
;//end module//
///////////////

