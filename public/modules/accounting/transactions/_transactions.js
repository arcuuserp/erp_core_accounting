//Module for users management
angular.module('accounting.transactions', [])

.controller('transactionsCTRL', ['$scope', '$http', '$init', '$state', '$translate', function($scope, $http, $init, $state, $translate) {
	
	/********************************************************
	* GRID OPTIONS
	*******************************************************/
	function getGridHeight(innerWidth)
	{
		var mainContentViewHeight = $('#main-content-view').height() - 100;
		if(innerWidth <= 767)
			mainContentViewHeight -= 40;
		return mainContentViewHeight;
	}

	$(window).on("resize", function(e)
	{
		$scope.grid.wrapper.height(getGridHeight(e.currentTarget.innerWidth));
		$scope.grid.resize(true);
	});

	$scope.filterInit = function (event) {
		var filterType = event.sender.dataSource.options.schema.model.fields[event.field].type;
		if (filterType == "date" || filterType == "number")
		{
			var container = event.container;
			var beginOperator = container.find("[data-role=dropdownlist]:eq(0)").data("kendoDropDownList");
			var endOperator = container.find("[data-role=dropdownlist]:eq(2)").data("kendoDropDownList");

			container.data("kendoPopup").bind("open", function (e)
			{
				if(filterType == "date")
				{
					beginOperator.value("lte");
					beginOperator.trigger("change");
			        endOperator.value("gte");
					endOperator.trigger("change");
				}
				else if(filterType == "number")
				{
					beginOperator.value("gte");
					beginOperator.trigger("change");
					endOperator.value("lte");
					endOperator.trigger("change");
				}
			});
		}
	};

	$scope.gridResultTotal = 0;
	$scope.gridOptions = {
		selectable: "row",
		change: onChange,
		scrollable: { virtual: true },
		mobile: true,
		resizable: true,
		sortable: {
            mode: "multiple",
            allowUnsort: true
        },
		reorderable: true,
		filterable: { mode: "menu, row" },
		autoBind: true,
		dataSource: {
			serverPaging: true,
			serverFiltering: true,
			serverSorting: true,
			pageSize: 40,
			transport: {
				read: {
					url: "/accounting/transactions/grid"
				}
			},
			schema: {
				data: function (result) {
					return result.data;
				},
				total: function (result) {
					if(result.total)
						$scope.gridResultTotal = result.total;

					return $scope.gridResultTotal;
				},
				model: {
			        fields: {
				        document_date: { type: "date" },
				        total_with_vat: {
					        type: "number", parse: function (data) { return data; }
				        }
			        }
				}
			}
		},
		columns: [
			{field: 'id_documentgroup', title: 'Tipi i Dokumentit', width: '15%',
					template: function(item){ return getDocumentTypeLang(item.subject); },
					filterable: {
						ui: docTypeFilterFunc,
		                cell: {
			                showOperators: false,
			                template: function(args) {
				                docTypeFilterFunc(args.element);
			                }
		                }
		            }
			},
			{field: 'document_date', title: 'Data Krijimit', width: '15%', format: "{0:dd/MM/yyyy HH:mm}",
					filterable: {
						ui: dateFilterFunc,
						cell: {
							showOperators: false, operator: "lte", /*, enabled: false */
							template: function(args){
								dateFilterFunc(args.element);
							}
						}
					}
			},
			{field: 'description', title: 'Pershkrimi', width: '25%',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			},
			{field: 'document_number', title: 'Numer Dokumenti', width: '15%',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3/*dataSource: {}*/} }
			},
			{field: 'currency', title: 'Monedha', width: '10%',
					filterable: {
						cell: {
							showOperators: false, operator: "eq", dataSource: {}
							/*,template: function(input){
                                //input.width(100);
                            }*/
						} }
			},
			{field: 'total_with_vat', title: 'Shuma', width: '20%', attributes: { style:"text-align:right;" },
					template: function(item) { return kendo.toString(parseFloat(item.total_with_vat), "c"); },
					filterable: { cell: { showOperators: false, operator: "gte"} }
			}
		]
	};

	function getDocumentTypeLang(docType){
		var documentTypeLabels = {  saleinvoiceheader: 'Fature Shitje',
							        purchaseinvoiceheader: 'Fatura Blerje'};

		return documentTypeLabels[docType];
	}

	function dateFilterFunc(element) {
		 element.kendoDatePicker({
			 parseFormats: Utils.dateParseFormats()
		 });
    }

	function docTypeFilterFunc(element) {
        element.kendoDropDownList({
            dataSource: new kendo.data.DataSource({ data: [
                { label: "Fature Shitje", data: 2 },
				{ label: "Mandat Arketimi", data: 3 },
				{ label: "Flete Dalje Magazine", data: 4 },
				{ label: "Urdher Shitje", data: 1 },
				{ label: "Oferta te Dhena", data: 1 },
				{ label: "Zbritje nga Shitjet", data: 1 },
				{ label: "Kthim Fature Blerje", data: 1 }
            ] }),
            optionLabel: "- Te Gjitha",
            dataTextField: "label",
            dataValueField: "data",
	        valuePrimitive: true,
	        height: 400
        });

		//set list width
		element.data("kendoDropDownList").list.width(200);
    }

	function onChange(arg) {
		var data = this.dataItem(this.select());
		//$state.go($init.data.resource_url+'Edit', {id: data.id});
		$state.go('accounting/salesinvoicesCreate', {id: data.id});
	}

	$scope.$on("kendoWidgetCreated", function(event, widget)
	{
		if (widget === $scope.grid)
		{
			$scope.grid.wrapper.height(getGridHeight( $(window).width() ));
			$scope.grid.resize(true);
			$("#grid tr.k-alt").removeClass("k-alt");
			$scope.grid.element.on('dblclick', function (e) {console.log(e) });
		}
	});



}])

///////////////
;//end module//
///////////////


