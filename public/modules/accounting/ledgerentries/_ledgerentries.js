//Module for users management
angular.module('accounting.ledgerentries', [])

.controller('ledgerentriesCTRL', ['$scope', '$http', '$init', '$state', function($scope, $http, $init, $state) {

}])

.controller('ledgerentriesFormCTRL', ['$scope', '$http', '$init', '$translate', '$ocModal', '$aceStorage', function($scope, $http, $init, $translate, $ocModal, $aceStorage){

	$scope.lang = {};
	$translate(['ACC_COMMON.SUBJECT_ACCOUNT','INPUTS.DESCRIPTION', 'ACC_COMMON.CREDIT', 'ACC_COMMON.DEBIT', 'FORM.TOTAL',
		'VALIDATION.VALIDATE_FIELDS', 'VALIDATION.CHECK_REQUIRED_FIELDS', 'VALIDATION.INVALID_CELL_AT_ROW',
		'ACC_VALID.DEBIT_OR_CREDIT_REQUIRED', 'ACC_VALID.DEBIT_NOT_EQ_TO_CREDIT', 'ACC_VALID.EMPTY_ACCOUNTS_BODY'
		]).then(function (translations) {
		$scope.lang = translations;

		$scope.accountsGridOptionsLoad();
	});

	$scope.model = $init.data.model;
	$scope.currencies = $aceStorage.kendoDataSource('/core/currencies/load', $init.data['currencies_version']);
	$scope.home_id_currency = $init.data['home_id_currency'];

	if(!$init.data.model['id'])
	{
		$init.data.model['exchange_rate'] = 1;
    }

	$scope.currencyChange = function(e)
	{
		if($scope.model.id_currency == $scope.home_id_currency)
			$scope.model.exchange_rate = 1;
		else
			$scope.model.exchange_rate = $aceStorage.get('last_exchange_rate_'+$scope.model.id_currency, 1);
	};

	$scope.updateExchangeRate = function(value)
	{
		$aceStorage.set('last_exchange_rate_'+$scope.model.id_currency, (value)?value:$scope.model.exchange_rate);
	};

	/**
	 *  GRID
	 */
	$scope.gridDropdownPopupClosed = true;

	$scope.$on("kendoWidgetCreated", function(event, widget)
	{
		if (widget === $scope.grid)
		{
			//ON CLICK OUTSIDE
			$(document).mouseup(function (e)
			{
			    var container = $($scope.grid.element);
			    if (!container.is(e.target) && container.has(e.target).length === 0)
			    {
				    if($scope.gridDropdownPopupClosed)
				        $scope.grid.saveRow();
			    }
			});

			//ON ROW CLICK
			$scope.grid.element.delegate("tbody > tr > td", "click dblclick", function () {
				var thisRow = $(this).parent();
				$scope.saveRowAndSetFocus($(thisRow).index(), $(this).index(), $(thisRow).hasClass('k-grid-edit-row'));
			});

			//DELETE BUTTON
			$scope.grid.element.delegate("tbody > tr.k-grid-edit-row > td:last-child > a", "focus mousedown", function (e) {
				if(e.type == "mousedown")
				{
					$scope.grid.cancelChanges();
					$scope.grid.refresh();
				}
				else //focus event
				{
					var nextRow = $(this).parent().parent().next();
					$scope.saveRowAndSetFocus($(nextRow).index(), 1);
				}
			});
		}
	});

	$scope.saveRowAndSetFocus = function(rowIndex, cellIndex, editMode)
	{
		if($scope.grid.dataSource.total() == rowIndex+1)
			$scope.grid.dataSource.pushCreate($scope.empty_row());

		if(!editMode)
		{
			if($scope.grid.dataSource.hasChanges())
				$scope.grid.saveRow();

			var row = $($scope.grid.element).find("tbody > tr:eq(" + ( rowIndex ) + ")");
			//find again this-row because it lost reference
			$scope.grid.editRow(row);
			$(row).children().eq(cellIndex).find("input").select();
		}
	};

	$scope.empty_row = function(row){
		var d = new Date();
		var uid = d.getTime()+'_'+Math.random();
		return {row: row, id: uid};
	};

	$scope.addExtraRows = function(){
		$scope.grid.dataSource.pushCreate($scope.empty_row());
		$scope.grid.dataSource.pushCreate($scope.empty_row());
		$scope.grid.dataSource.pushCreate($scope.empty_row());
		$scope.grid.dataSource.pushCreate($scope.empty_row());
		$scope.grid.dataSource.pushCreate($scope.empty_row());
	};

	$scope.resetRowData = function(init){
		var grid_data = [$scope.empty_row(1),
						 $scope.empty_row(2),
						 $scope.empty_row(3),
						 $scope.empty_row(4),
						 $scope.empty_row(5)];
		if(init)
			return grid_data;

		$scope.grid.dataSource.data(grid_data);
	};

	$scope.grid_data = $scope.resetRowData(true);

	//$aceStorage.clear();
	$scope.itemsComboBoxDataSource = $aceStorage.kendoDataSource('/accounting/accounts/load', $init.data['accounts_version']);

    $scope.subjectComboBoxEditor = function (container, options) {
        $('<input data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoMultiFilterComboBox({
		        dataSource: $scope.itemsComboBoxDataSource,
		        headerTemplate: '<a style="display: block; width: 100%; background-color: #ddd;" href="#">Add New</a>',
		        multiFilterFields: [
				        { field: "code", operator: "startswith"},
				        { field: "name", operator: "contains"}
			        ],
		        filter: 'startswith',
		        suggest: true,
                dataValueField: "id",
                dataTextField: "code_name",
		        open: function() { $scope.gridDropdownPopupClosed = false; },
		        close: function() { $scope.gridDropdownPopupClosed = true; },
                change: function () {
	                if (this.value() && this.select() === -1) {
		                this.value("");
			            options.model.set('subject', null);
		            }
	                else
	                {
		                if(!Utils.isSet(options.model.get('description')) && Utils.isSet($scope.model.description))
		                    options.model.set('description', $scope.model.description);
	                }
                }
            });
    };

	$scope.debitItemEditor = function(container, options) {
		$('<input data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoNumericTextBox({
		        spinners : false,
		        format: 'c',
		        decimals: 3,
				change: function() {
					if(options.model.get('credit') > 0 && this.value() > 0)
					{
						options.model.set('credit', null);
					}
			    }
	        });
	};

	$scope.creditItemEditor = function(container, options) {
		$('<input data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoNumericTextBox({
		        spinners : false,
		        format: 'c',
		        decimals: 3,
				change: function() {
					if(options.model.get('debit') > 0 && this.value() > 0)
					{
						options.model.set('debit', null);
					}
			    }
	        });
	};


	$scope.accountsGridOptionsLoad = function()
	{
		$scope.gridOptions = {
			columns: [
				{field: 'row', title: '#', width: '30px', attributes: { "class": "row-col" }},
				{field: 'subject', title: $scope.lang['ACC_COMMON.SUBJECT_ACCOUNT'], width: '280px',
						template: function(item) {
							if(item.subject==null) return "";
							return item.subject['code_name'];
						},
						editor: function (container, options) {
					        $scope.subjectComboBoxEditor(container, options);
					    }
				},
				{field: 'description', title: $scope.lang['INPUTS.DESCRIPTION'],
						footerTemplate: "<b>"+$scope.lang['FORM.TOTAL']+"</b>"
				},
				{field: 'debit', title: $scope.lang['ACC_COMMON.DEBIT'], width: '140px', attributes: { "class": "numeric-cell" },
						editor: function (container, options) {
					        $scope.debitItemEditor(container, options);
					    },
						template: function(item) {
							var value = parseFloat(item.debit);
							if(isNaN(value)) return "";
							return kendo.toString(value, "c");
						},
						footerTemplate: function(item) {
							var value = parseFloat(item.debit['sum']);
							if(isNaN(value)) return "";
							return kendo.toString(value, "c");
						}
				},
				{field: 'credit', title: $scope.lang['ACC_COMMON.CREDIT'], width: '140px', attributes: { "class": "numeric-cell"},
						editor: function (container, options) {
					        $scope.creditItemEditor(container, options);
					    },
						template: function(item) {
							var value = parseFloat(item.credit);
							if(isNaN(value)) return "";
							return kendo.toString(value, "c");
						},
						footerTemplate: function(item) {
							var value = parseFloat(item.credit['sum']);
							if(isNaN(value)) return "";
							return kendo.toString(value, "c");
						}
				},
				{command: [{ name: "destroy", text: "", className: "destroy-btn-cls", iconClass: "fa fa-trash-o", width: 30}], width: 30 }
			],
			dataSource: {
				//autoSync: true,
				data: $scope.grid_data,
				schema: {
					model: {
						id: "id",
				        fields: {
	                        row: {editable: false},
					        subject: {},
					        description: {type: "string"},
					        debit: {type: "number"},
					        credit: {type: "number"}
				        }
					}
				},
				aggregate: [
		            { field: "debit", aggregate: "sum" },
		            { field: "credit", aggregate: "sum" }
		        ]
			},
			dataBound: function () {
	            var rows = this.items();
				angular.forEach(rows, function(item, i) {
				    var rowScope = angular.element(item).scope();
	                rowScope.dataItem['row'] = i+1;
				});
	        },
			remove: function(e) {
				if(e.sender.dataSource.total() < 3)
				{
					$scope.grid.dataSource.pushCreate($scope.empty_row());
				}
			},
			editable: {
				mode: "inline",
	            confirmation: false
			},
			mobile: true,
			resizable: true,
			scrollable: false
		};
	};

	$scope.save = function(type)
	{
		var body_acc = [];
		var validationMsg = [];
		if ($scope.validator.validate())
		{
			var aggregates = $scope.grid.dataSource.aggregates();
			if(aggregates.debit['sum'] != aggregates.credit['sum'])
				validationMsg.push($scope.lang['ACC_VALID.DEBIT_NOT_EQ_TO_CREDIT']);

			var acc_rows = $scope.grid.dataSource.data();
			var length = acc_rows.length;
			for(var i=0; i<length; i++)
			{
				if(acc_rows[i]['subject'])
				{
					if(!acc_rows[i]['debit'] && !acc_rows[i]['credit'])
						validationMsg.push($scope.lang['ACC_VALID.DEBIT_OR_CREDIT_REQUIRED']+acc_rows[i]['row']);

					body_acc.push(acc_rows[i]);
				}
				else if(acc_rows[i]['debit'] || acc_rows[i]['credit']) {
					validationMsg.push($scope.lang['ACC_COMMON.SUBJECT_ACCOUNT']+$scope.lang['VALIDATION.INVALID_CELL_AT_ROW']+acc_rows[i]['row']);
				}
			}

			if(body_acc.length == 0)
				validationMsg.push($scope.lang['ACC_VALID.EMPTY_ACCOUNTS_BODY']);
		}
		else
		{
			validationMsg.push($scope.lang['VALIDATION.CHECK_REQUIRED_FIELDS']);
		}

		if(validationMsg.length > 0)
		{
			$ocModal.alert(validationMsg, $scope.lang['VALIDATION.VALIDATE_FIELDS']);
		}
		else
		{
			var postData = $scope.model;
			//console.log('model: ', $scope.model);
			postData.response_action = type;

			postData.body_acc = body_acc;//postData.body_items = body_items;
			postData.total_debit = aggregates.debit['sum'];
			postData.total_credit = aggregates.credit['sum'];

			$http.post($init.data.resource_url, postData).success(function()
			{
				if(type == 'close')
					$scope.closeModal();
			});
		}
	};

}])



///////////////
;//end module//
///////////////

