//Module for users management
angular.module('accounting.cashreceipts', [])

.controller('cashreceiptsCTRL', ['$scope', '$http', '$init', '$state', function($scope, $http, $init, $state) {

}])

.controller('cashreceiptsFormCTRL', ['$scope', '$http', '$init', '$translate', '$ocModal', '$aceStorage', function($scope, $http, $init, $translate, $ocModal, $aceStorage){

	$scope.lang = {};
	$translate(['ACC_COMMON.SUBJECT_ARTICLE', 'ACC_COMMON.SUBJECT_ACCOUNT', 'INPUTS.VALUE', 'INPUTS.DESCRIPTION',
		'INPUTS.QUANTITY', 'INPUTS.UNIT', 'INPUTS.PRICE', 'INPUTS.TAX', 'INPUTS.AMOUNT',
		'VALIDATION.VALIDATE_FIELDS', 'VALIDATION.REQUIRED_FIELD', 'VALIDATION.CHECK_REQUIRED_FIELDS',
		'VALIDATION.INVALID_CELL_AT_ROW', 'ACC_VALID.DEBIT_OR_CREDIT_REQUIRED', 'ACC_VALID.DEBIT_NOT_EQ_TO_CREDIT',
		'ACC_VALID.EMPTY_ITEMS_BODY', 'ACC_VALID.NO_QUANTITY_SET_AT_ROW', 'ACC_VALID.NO_PRICE_SET_AT_ROW',
		'ACC_VALID.EMPTY_ACCOUNTS_BODY', 'ACC_VALID.NO_VALUE_SET_AT_ROW', 'ACC_COMMON.CUSTOMER']).then(function (translations) {
		$scope.lang = translations;

		$scope.invoicesGridOptionsLoad();
	});

	$scope.model = $init.data.model;
	$scope.customers = $aceStorage.kendoDataSource('/accounting/contacts/load/customers', $init.data['customers_version']);
	$scope.currencies = $aceStorage.kendoDataSource('/core/currencies/load', $init.data['currencies_version']);
	//$scope.taxes = $aceStorage.kendoDataSource('/accounting/taxes/load', $init.data['taxes_version']);
	$scope.commercialterms = $aceStorage.kendoDataSource('/accounting/commercialterms/load', $init.data['commercialterms_version']);
	$scope.cash_data = $aceStorage.kendoDataSource('/accounting/cash/load/cash', $init.data['cash_data_version']);
	$scope.home_currency = $init.data['home_currency'];
	$scope.discountPercent = 0;

	if(!$init.data.model['id'])
	{
		$scope.showCurrency = false;
		$init.data.model['exchange_rate'] = 1;

	    $scope.itemsSubtotalNoTax = 0;
	    $scope.itemsTaxableSubtotal = 0;
	    $scope.itemsSubtotalWithTax = 0;
    }
	else
	{
		/*TODO:
			1. selected contact
			2. load grid rows
		 */

		$scope.showCurrency = true;
	}

	$scope.selectedContact = {};
	$scope.contactChange = function(e)
	{
		if (e.sender.select() == -1) {
            e.sender.value("");
			$scope.selectedContact = {};
			return;
        }

		$scope.selectedContact = {id: e.sender.dataItem()['id'], display: e.sender.dataItem()['display']};//not by reference

		if($scope.selectedContact['id'])
		{
			$http.get('/accounting/contacts/load/'+$scope.selectedContact['id']).success(function (data) {
				angular.extend($scope.selectedContact, data);

				$scope.model.id_commercialterms = $scope.selectedContact['id_commercialterms'];
			});
		}
	};

	$scope.selectedCash = {};
	$scope.cashChange = function(e)
	{
		if (e.sender.select() == -1) {
            e.sender.value("");
			$scope.selectedCash = {};
			return;
        }

		$scope.selectedCash = {id: e.sender.dataItem()['id'], code_name: e.sender.dataItem()['code_name']};//not by reference

		if($scope.selectedCash['id'])
		{
			$http.get('/accounting/cash/load/'+$scope.selectedCash['id']).success(function (data) {
				angular.extend($scope.selectedCash, data);

				if($scope.selectedCash['id_currency'] != $scope.model.id_currency)
				{
					$scope.model.id_currency = $scope.selectedCash['id_currency'];
					$scope.currencyChange(null);
					$scope.showCurrency = true;
				}
			});
		}
	};

	$scope.currencyChange = function(e)
	{
		if($scope.model.id_currency == $scope.home_currency['id'])
			$scope.model.exchange_rate = 1;
		else
			$scope.model.exchange_rate = $aceStorage.get('last_exchange_rate_'+$scope.model.id_currency, 1);
	};

	$scope.updateExchangeRate = function(value)
	{
		$aceStorage.set('last_exchange_rate_'+$scope.model.id_currency, (value)?value:$scope.model.exchange_rate);
	};



	/**
	 *  GRID
	 */
	$scope.gridDropdownPopupClosed = true;

	$scope.$on("kendoWidgetCreated", function(event, widget)
	{
		if (widget === $scope.invoicesGrid)
		{
			$(document).mouseup(function (e)
			{
			    var container = $($scope.invoicesGrid.element);
			    if (!container.is(e.target) && container.has(e.target).length === 0)
			    {
				    if($scope.gridDropdownPopupClosed)
				        $scope.invoicesGrid.saveRow();
			    }
			});

			$scope.invoicesGrid.element.delegate("tbody > tr > td", "click dblclick", function () {
				var thisRow = $(this).parent();
				$scope.saveRowAndSetFocus($(thisRow).index(), $(this).index(), $(thisRow).hasClass('k-grid-edit-row'));
			});

			$scope.invoicesGrid.element.delegate("tbody > tr.k-grid-edit-row > td:last-child > a", "focus mousedown", function (e) {
				if(e.type == "mousedown")
				{
					$scope.invoicesGrid.cancelChanges();
					$scope.invoicesGrid.refresh();
				}
				else //focus event
				{
					var nextRow = $(this).parent().parent().next();
					$scope.saveRowAndSetFocus($(nextRow).index(), 1);
				}
			});
		}

	});

	$scope.saveRowAndSetFocus = function(rowIndex, cellIndex, editMode)
	{
		if($scope.invoicesGrid.dataSource.total() == rowIndex+1)
			$scope.invoicesGrid.dataSource.pushCreate($scope.empty_row());

		if(!editMode)
		{
			if($scope.invoicesGrid.dataSource.hasChanges())
				$scope.invoicesGrid.saveRow();

			var row = $($scope.invoicesGrid.element).find("tbody > tr:eq(" + ( rowIndex ) + ")");
			//find again this-row because it lost reference
			$scope.invoicesGrid.editRow(row);
			$(row).children().eq(cellIndex).find("input").select();
		}
	};

	$scope.invoicesGridOptionsLoad = function()
	{
		$scope.invoicesGridOptions = {
			columns: [
				{field: 'description', title: 'Pershkrimi', width: '280px',
						template: function(item) {
							if(item.subject==null) return "";
							return item.subject['code_name'];
						}
				},
				{field: 'due_date', title: 'Afati', width: '90px',
						template: function(item) {
							if(item.subject==null) return "";
							return item.subject['code_name'];
						}
				},
				{field: 'original_amount', title: 'Vlera Origjinale', width: '90px', attributes: { "class": "numeric-cell" },
						template: function(item) {
							if(item.subject==null) return "";
							return item.subject['code_name'];
						}
				},
				{field: 'balance', title: 'Shuma e Mbetur', width: '90px', attributes: { "class": "numeric-cell" },
						template: function(item) {
							if(item.subject==null) return "";
							return item.subject['code_name'];
						}
				},
				{field: 'pay', title: 'Likujdo', width: '70px'
				},
				{field: 'payment', title: $scope.lang['INPUTS.PRICE'], width: '120px', attributes: { "class": "numeric-cell" },
						editor: function (container, options) {
					        $scope.priceNumericEditor(container, options);
					    },
						template: function(item) {
							var value = parseFloat(item.price);
							if(isNaN(value)) return "";
							return kendo.toString(value, "c");
						}
				}
			],
			dataSource: {
				schema: {
					data: function (result) {
						return result.data;
					},
					total: function (result) {
						if(result.total)
							$scope.gridResultTotal = result.total;

						return $scope.gridResultTotal;
					},
					model: {
				        id: "id",
						fields: {
	                        description: {editable: false},
	                        due_date: {editable: false},
	                        original_amount: {editable: false},
	                        balance: {editable: false},
	                        pay: {
		                        type: "boolean",
		                        editable: true,
		                        validation: { required: false }
		                    },
	                        payment: { editable: true, type: "number" }
	                    }
					}
				},
				autoBind: false,
				transport: {
					read: {
						url: '/accounting/docstransactions/load/sales'
					}
				},
				batch: true
			},
			editable: true,
			mobile: true,
			resizable: true,
			scrollable: false
		};
	};

    $scope.calcSubtotal = function (model)
    {
		var quantityValue = parseFloat(model.get('quantity'));
		var priceValue = parseFloat(model.get('price'));
	    var subTotalValue = (quantityValue * priceValue);
	    var taxValue = 0;
	    var tax = model.get('tax');
	    if(tax)
	    {
		    if(tax['percentage'] === 0)
		        taxValue = parseFloat(tax['tax_rate']);
		    else
		        taxValue = parseFloat(tax['tax_rate']) * subTotalValue;
	    }

	    model['taxable_amount'] = taxValue;
	    subTotalValue = subTotalValue + taxValue;
	    model['amount'] = subTotalValue;
	    //model.set('amount', subTotalValue);

		//CALCULATE SUB-TOTAL OF ALL ROWS
        $scope.itemsSubtotalNoTax = 0;
        $scope.itemsTaxableSubtotal = 0;
        $scope.itemsSubtotalWithTax = 0;

		var item_rows = $scope.invoicesGrid.dataSource.data();
		var length = item_rows.length;
		for(var i=0; i<length; i++)
		{
			if(item_rows[i]['subject'])
			{
				$scope.itemsSubtotalNoTax += (item_rows[i]['quantity'] * item_rows[i]['price']);
				$scope.itemsTaxableSubtotal += item_rows[i]['taxable_amount'];
				$scope.itemsSubtotalWithTax += item_rows[i]['amount'];
			}
		}
    };

	/********************************************************************************
	 *      SAVE DOCUMENT
	 *******************************************************************************/

	$scope.save = function(type)
	{
		var body_items = [];
		var body_accounts = [];
		var validationMsg = [];
		if ($scope.validator.validate())
		{
			var rows = $scope.invoicesGrid.dataSource.data();
			var length = rows.length;
			var i;
			for(i=0; i<length; i++)
			{
				if(rows[i]['subject'])
				{
					if(!rows[i]['quantity'] || rows[i]['quantity'] < 1)
						validationMsg.push($scope.lang['ACC_VALID.NO_QUANTITY_SET_AT_ROW']+rows[i]['row']);

					if(rows[i]['price'] == null)
						validationMsg.push($scope.lang['ACC_VALID.NO_PRICE_SET_AT_ROW']+rows[i]['row']);

					body_items.push(rows[i]);
				}
				else if(rows[i]['quantity'] || rows[i]['price'] || rows[i]['tax']) {
					validationMsg.push($scope.lang['ACC_COMMON.SUBJECT_ARTICLE']+$scope.lang['VALIDATION.INVALID_CELL_AT_ROW']+rows[i]['row']);
				}
			}

			if(body_items.length == 0 && body_accounts.length == 0)
			{
				validationMsg.push($scope.lang['ACC_VALID.EMPTY_ITEMS_BODY']);
			}
		}
		else
		{
			validationMsg.push($scope.lang['VALIDATION.CHECK_REQUIRED_FIELDS']);
		}

		if(!$scope.selectedContact['id'])
			validationMsg.push($scope.lang['ACC_COMMON.CUSTOMER']+$scope.lang['VALIDATION.REQUIRED_FIELD']);

		if(validationMsg.length > 0)
		{
			$ocModal.alert(validationMsg, $scope.lang['VALIDATION.VALIDATE_FIELDS']);
		}
		else
		{
			var postData = $scope.model;
			//console.log('model: ', $scope.model);
			postData.response_action = type;

			postData.contact = $scope.selectedContact;
			postData.cash = $scope.selectedCash;
			postData.body_items = body_items;

			postData.total_no_tax = $scope.itemsSubtotalNoTax;
			postData.total_taxable = $scope.itemsTaxableSubtotal;
			postData.total_with_tax = $scope.itemsSubtotalWithTax;

			postData.discount_percent = $scope.discountPercent;
			var discount = (postData.total_with_tax * $scope.discountPercent/100);
			postData.final_total = postData.total_with_tax - discount;

			$http.post($init.data.resource_url, postData).success(function()
			{
				if(type == 'close')
					$scope.closeModal();
			});
		}
	};

}])


///////////////
;//end module//
///////////////

