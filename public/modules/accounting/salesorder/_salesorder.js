//Module for users management
angular.module('accounting.salesorder', [])

.controller('salesorderCTRL', ['$scope', '$http', '$init', '$state', function($scope, $http, $init, $state) {

}])

.controller('salesorderFormCTRL', ['$scope', '$http', '$init', '$translate', '$ocModal', '$aceStorage', function($scope, $http, $init, $translate, $ocModal, $aceStorage){

	$scope.lang = {};
	$translate(['VALIDATION.VALIDATE_FIELDS', 'VALIDATION.REQUIRED_FIELD', 'VALIDATION.CHECK_REQUIRED_FIELDS',
		'VALIDATION.INVALID_CELL_AT_ROW', 'ACC_VALID.DEBIT_OR_CREDIT_REQUIRED', 'ACC_VALID.DEBIT_NOT_EQ_TO_CREDIT',
		'ACC_VALID.EMPTY_ITEMS_BODY', 'ACC_VALID.NO_QUANTITY_SET_AT_ROW', 'ACC_VALID.NO_PRICE_SET_AT_ROW',
		'ACC_VALID.EMPTY_ACCOUNTS_BODY', 'ACC_COMMON.CUSTOMER']).then(function (translations) {
		$scope.lang = translations;
	});

	$scope.empty_row = function(row){
		var d = new Date();
		var uid = d.getTime()+'_'+Math.random();
		return {row: row, id: uid};//, description: null
	};

	$scope.addExtraRows = function(){
		$scope.grid.dataSource.pushCreate($scope.empty_row());
		$scope.grid.dataSource.pushCreate($scope.empty_row());
		$scope.grid.dataSource.pushCreate($scope.empty_row());
		$scope.grid.dataSource.pushCreate($scope.empty_row());
		$scope.grid.dataSource.pushCreate($scope.empty_row());
	};

	$scope.resetRowData = function(init){
		var grid_data = [$scope.empty_row(1),
						 $scope.empty_row(2),
						 $scope.empty_row(3)];
		if(init)
			return grid_data;

		$scope.grid.dataSource.data(grid_data);
	};

	$scope.model = $init.data.model;
	$scope.customers = $aceStorage.kendoDataSource('/accounting/contacts/load/customers', $init.data['customers_version']);
	$scope.currencies = $aceStorage.kendoDataSource('/core/currencies/load', $init.data['currencies_version']);
	$scope.taxes = $aceStorage.kendoDataSource('/accounting/taxes/load', $init.data['taxes_version']);
	$scope.commercialterms = $aceStorage.kendoDataSource('/accounting/commercialterms/load', $init.data['commercialterms_version']);
	$scope.home_currency = $init.data['home_currency'];
	$scope.discountPercent = 0;

	if(!$init.data.model['id'])
	{
		$scope.showCurrency = false;
		$init.data.model['exchange_rate'] = 1;

		$scope.grid_data = $scope.resetRowData(true);
    }
	else
	{
		/*TODO:
			1. selected contact
			2. load grid rows
		 */

		$scope.showCurrency = true;
	}


    $scope.itemsGridFilled = false;

	$scope.selectedContact = {};
	$scope.contactChange = function(e)
	{
		if (e.sender.select() == -1) {
            e.sender.value("");
			$scope.selectedContact = {};
			return;
        }

		$scope.selectedContact = {id: e.sender.dataItem()['id'], display: e.sender.dataItem()['display']};//not by reference

		if($scope.selectedContact['id'])
		{
			$http.get('/accounting/contacts/load/'+$scope.selectedContact['id']).success(function (data) {
				angular.extend($scope.selectedContact, data);

				$scope.model.id_commercialterms = $scope.selectedContact['id_commercialterms'];
				if($scope.selectedContact['id_currency'] != $scope.model.id_currency)
				{
					$scope.model.id_currency = $scope.selectedContact['id_currency'];
					$scope.currencyChange(null);
					$scope.showCurrency = true;
				}
			});
		}
	};

	$scope.currencyChange = function(e)
	{
		if($scope.model.id_currency == $scope.home_currency['id'])
			$scope.model.exchange_rate = 1;
		else
			$scope.model.exchange_rate = $aceStorage.get('last_exchange_rate_'+$scope.model.id_currency, 1);
	};

	$scope.updateExchangeRate = function(value)
	{
		$aceStorage.set('last_exchange_rate_'+$scope.model.id_currency, (value)?value:$scope.model.exchange_rate);
	};



	/**
	 *  GRID
	 */
	$scope.gridDropdownPopupClosed = true;

	$scope.$on("kendoWidgetCreated", function(event, widget)
	{
		if (widget === $scope.grid)
		{
			$(document).mouseup(function (e)
			{
			    var container = $($scope.grid.element);
			    if (!container.is(e.target) && container.has(e.target).length === 0)
			    {
				    if($scope.gridDropdownPopupClosed)
				        $scope.grid.saveRow();
			    }
			});

			$scope.grid.element.delegate("tbody > tr > td", "click dblclick", function () {
				var thisRow = $(this).parent();
				$scope.saveRowAndSetFocus($(thisRow).index(), $(this).index(), $(thisRow).hasClass('k-grid-edit-row'));
			});

			$scope.grid.element.delegate("tbody > tr.k-grid-edit-row > td:last-child > a", "focus mousedown", function (e) {
				if(e.type == "mousedown")
				{
					$scope.grid.cancelChanges();
					$scope.grid.refresh();
				}
				else //focus event
				{
					var nextRow = $(this).parent().parent().next();
					$scope.saveRowAndSetFocus($(nextRow).index(), 1);
				}
			});
		}
	});

	$scope.saveRowAndSetFocus = function(rowIndex, cellIndex, editMode)
	{
		if($scope.grid.dataSource.total() == rowIndex+1)
			$scope.grid.dataSource.pushCreate($scope.empty_row());

		if(!editMode)
		{
			if($scope.grid.dataSource.hasChanges())
				$scope.grid.saveRow();

			var row = $($scope.grid.element).find("tbody > tr:eq(" + ( rowIndex ) + ")");
			//find again this-row because it lost reference
			$scope.grid.editRow(row);
			$(row).children().eq(cellIndex).find("input").select();
		}
	};



	//$aceStorage.clear();
	$scope.itemsComboBoxDataSource = $aceStorage.kendoDataSource('/warehouse/items/load', $init.data['items_version']);

    $scope.subjectItemsComboBoxEditor = function (container, options) {

        $('<input data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoMultiFilterComboBox({
		        dataSource: $scope.itemsComboBoxDataSource,
		        headerTemplate: '<a style="display: block; width: 100%; height: 22px; background-color: #ddd;" href="#"><span style="padding: 5px;">Add New</span></a>',
		        template: '<span>#: data.code # - #: data.name #</span>',
		        multiFilterFields: [
				        { field: "code", operator: "startswith"},
				        { field: "name", operator: "contains"}
			        ],
		        filter: 'startswith',
		        suggest: true,
                dataValueField: "id",
                dataTextField: "name",
		        open: function() { $scope.gridDropdownPopupClosed = false; },
		        close: function() { $scope.gridDropdownPopupClosed = true; },
                change: function () {
	                if (this.value() && this.select() === -1) {
		                this.value("");
			            options.model.set('subject', null);
		            }
	                else
		            {
	                    var selectedItem = this.dataItem();
		                if(selectedItem && selectedItem['id'])
		                {
			                var postData = {id_contact: $scope.model.id_contact, transaction_type: 'sale' };
			                $http.post('/warehouse/items/load/'+selectedItem['id'], postData).success(function (data) {
				                options.model.set('units_list', data['units']);
				                options.model.set('unit', data['units'][0]);//selectedItem['id_unit']

				                //ADD THE EXTRA DETAILS TO THE SELECTED ITEM
								angular.extend(selectedItem, data['item']);

				                options.model.set('description', selectedItem['description']);
				                options.model.set('price', selectedItem['price']);
				                options.model.set('tax', $scope.taxes.get(selectedItem['id_tax']));
							});
		                }
		            }
                }
            });
    };

    $scope.unitComboBoxEditor = function (container, options) {

	    var widget = $('<input data-bind="value:' + options.field + '"/>')
            .appendTo(container).kendoDropDownList({
		        dataSource: options.model.get('units_list'),
		        dataValueField: "idunitentry",
                dataTextField: "unitentry_symbol",
		        open: function() { $scope.gridDropdownPopupClosed = false; },
		        close: function() { $scope.gridDropdownPopupClosed = true; }
            }).data("kendoDropDownList");

	    options.model.bind("change", function(e) {
		    if(e.field == 'units_list')
		    {
			    widget.dataSource.data(e.sender['units_list']);
		    }
		});
    };

    $scope.taxesComboBoxEditor = function (container, options) {

	    var widget = $('<input data-bind="value:' + options.field + '"/>')
            .appendTo(container).kendoComboBox({
		        dataSource: $scope.taxes,
		        dataValueField: "id",
                dataTextField: "name",
		        open: function() { $scope.gridDropdownPopupClosed = false; },
		        close: function() { $scope.gridDropdownPopupClosed = true; },
			    change: function () {
				    if (this.value() && this.select() === -1)
				    {
					    if(isNaN(this.value()))
				            options.model.set('tax', $scope.taxes[0]);//no tax 0%
					    else
		                    options.model.set('tax', {id: this.value(), code: 'manual', name: this.value(), tax_rate: parseFloat(this.value()), percentage: 0});
					}
	                else
		            {
	                    var selectedItem = this.dataItem();
				        options.model.set('tax', selectedItem);
		            }

				    //$scope.calcSubtotal(options.model);
			    }
            });
    };

	$scope.quantityNumericEditor = function(container, options) {
		$('<input data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoNumericTextBox({
		        spinners : false,
		        decimals: 5/*,
			    change: function () {
				    $scope.calcSubtotal(options.model);
			    }*/
	        });
	};

	$scope.priceNumericEditor = function(container, options) {
		$('<input data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoNumericTextBox({
		        spinners : false,
		        format: 'c',
		        decimals: 3/*,
			    change: function () {
				    $scope.calcSubtotal(options.model);
			    }*/
	        });
	};

	$scope.gridOptions = {
		columns: [
			{field: 'row', title: '#', width: '30px', attributes: { "class": "row-col" }},
			{field: 'subject', title: 'Artikulli', width: '330px',
					template: function(item) {
						if(item.subject==null) return "";
						return item.subject['code']+' - '+item.subject['name'];
					},
					editor: function (container, options) {
				        $scope.subjectItemsComboBoxEditor(container, options);
				    }
			},
			{field: 'description', title: 'Description'},
			{field: 'quantity', title: 'Sasia', width: '90px', attributes: { "class": "numeric-cell" },
					editor: function (container, options) {
				        $scope.quantityNumericEditor(container, options);
				    },
					template: function(item) {
						var value = parseFloat(item.quantity);
						if(isNaN(value)) return "";
						return Utils.cultureDecimalReplace(value);
					}
			},
			{field: 'unit', title: 'Njesia', width: '100px',
					template: function(item) {
						if(item.unit==null) return "";
						return item.unit['unitentry_symbol'];
					},
					editor: function (container, options) {
				        $scope.unitComboBoxEditor(container, options);
				    }
			},
			{field: 'price', title: 'Cmimi', width: '110px', attributes: { "class": "numeric-cell" },
					editor: function (container, options) {
				        $scope.priceNumericEditor(container, options);
				    },
					template: function(item) {
						var value = parseFloat(item.price);
						if(isNaN(value)) return "";
						return kendo.toString(value, "c");
					}
			},
			{field: 'tax', title: 'Taksa', width: '100px',
					template: function(item) {
						if(item.tax==null) return "";
						return item.tax['name'];
					},
					editor: function (container, options) {
				        $scope.taxesComboBoxEditor(container, options);
				    }
			},
			{field: 'amount', title: 'Shuma', width: '120px', attributes: { style:"text-align:right;" },
					template: function(item) {
						var value = parseFloat(item.amount);
						if(isNaN(value)) return "";
							return kendo.toString(value, "c");
					}
			},
			{command: [{ name: "destroy", text: "", className: "destroy-btn-cls", iconClass: "fa fa-trash-o", width: 30}], width: 30 }
		],
		dataSource: {
			data: $scope.grid_data,
			//push: $scope.setFocusOnRow,
			schema: {
				model: {
					id: "id",
			        fields: {
                        row: {editable: false},
				        subject: { },
				        description: {type: "string"},
                        quantity: { type: "number" },
                        unit: { defaultValue: { id: 1, name: "Unit" } },
						units_list: { },
                        price: { type: "number" },
                        tax: { },
                        amount: { type: "number", editable: false }
                    }
		        }
			}
		},
		dataBound: function () {
            var rows = this.items();
			var hasSubject = false;
			angular.forEach(rows, function(item, i) {
			    var rowScope = angular.element(item).scope();
                rowScope.dataItem['row'] = i+1;

				if(!hasSubject && rowScope.dataItem['subject'])
					hasSubject = true;
			});

			$scope.itemsGridFilled = hasSubject;
			//FORCE UPDATE THE VIEW
			$scope.$apply();
        },
		save: function(options) {
			$scope.calcSubtotal(options.model);
		},
		remove: function(e) {
			if(e.sender.dataSource.total() < 3)
			{
				$scope.grid.dataSource.pushCreate($scope.empty_row());
			}
		},
		editable: {
			mode: "inline",
            confirmation: false
		},
		mobile: true,
		resizable: true,
		scrollable: false
	};

    $scope.itemsSubtotalNoTax = 0;
    $scope.itemsTaxableSubtotal = 0;
    $scope.itemsSubtotalWithTax = 0;
    $scope.calcSubtotal = function (model)
    {
		var quantityValue = parseFloat(model.get('quantity'));
		var priceValue = parseFloat(model.get('price'));
	    var subTotalValue = (quantityValue * priceValue);
	    var taxValue = 0;
	    var tax = model.get('tax');
	    if(tax)
	    {
		    if(tax['percentage'] === 0)
		        taxValue = parseFloat(tax['tax_rate']);
		    else
		        taxValue = parseFloat(tax['tax_rate']) * subTotalValue;
	    }

	    model['taxable_amount'] = taxValue;
	    subTotalValue = subTotalValue + taxValue;
	    model['amount'] = subTotalValue;
	    //model.set('amount', subTotalValue);

		//CALCULATE SUB-TOTAL OF ALL ROWS
        $scope.itemsSubtotalNoTax = 0;
        $scope.itemsTaxableSubtotal = 0;
        $scope.itemsSubtotalWithTax = 0;

		var acc_rows = $scope.grid.dataSource.data();
		var length = acc_rows.length;
		for(var i=0; i<length; i++)
		{
			if(acc_rows[i]['subject'])
			{
				$scope.itemsSubtotalNoTax += (acc_rows[i]['quantity'] * acc_rows[i]['price']);
				$scope.itemsTaxableSubtotal += acc_rows[i]['taxable_amount'];
				$scope.itemsSubtotalWithTax += acc_rows[i]['amount'];
			}
		}
    };


	$scope.save = function(type)
	{
		var body_items = [];
		var validationMsg = [];
		if ($scope.validator.validate())
		{
			var item_rows = $scope.grid.dataSource.data();
			var length = item_rows.length;
			for(var i=0; i<length; i++)
			{
				if(item_rows[i]['subject'])
				{
					if(!item_rows[i]['quantity'] || item_rows[i]['quantity'] < 1)
						validationMsg.push($scope.lang['ACC_VALID.NO_QUANTITY_SET_AT_ROW']+item_rows[i]['row']);

					if(item_rows[i]['price'] == null)
						validationMsg.push($scope.lang['ACC_VALID.NO_PRICE_SET_AT_ROW']+item_rows[i]['row']);

					body_items.push(item_rows[i]);
				}
				else if(item_rows[i]['quantity'] || item_rows[i]['price'] || item_rows[i]['tax']) {
					validationMsg.push('Artikulli'+$scope.lang['VALIDATION.INVALID_CELL_AT_ROW']+item_rows[i]['row']);
				}
			}

			if(body_items.length == 0)
				validationMsg.push($scope.lang['ACC_VALID.EMPTY_ITEMS_BODY']);
		}
		else
		{
			validationMsg.push($scope.lang['VALIDATION.CHECK_REQUIRED_FIELDS']);
		}

		if(!$scope.selectedContact['id'])
			validationMsg.push($scope.lang['ACC_COMMON.CUSTOMER']+$scope.lang['VALIDATION.REQUIRED_FIELD']);

		if(validationMsg.length > 0)
		{
			$ocModal.alert(validationMsg, $scope.lang['VALIDATION.VALIDATE_FIELDS']);
		}
		else
		{
			var postData = $scope.model;
			console.log('model: ', $scope.model);
			postData.response_action = type;

			postData.contact = $scope.selectedContact;
			postData.body_items = body_items;
			/*postData.items_subtotal_no_tax = $scope.itemsSubtotalNoTax;
			postData.items_taxable_subtotal = $scope.itemsTaxableSubtotal;
			postData.items_subtotal_with_tax = $scope.itemsSubtotalWithTax;*/
			postData.total_no_tax = $scope.itemsSubtotalNoTax;
			postData.total_taxable = $scope.itemsTaxableSubtotal;
			postData.total_with_tax = $scope.itemsSubtotalWithTax;

			postData.discount_percent = $scope.discountPercent;
			var discount = ($scope.itemsSubtotalWithTax * $scope.discountPercent/100);
			postData.final_total = $scope.itemsSubtotalWithTax - discount;

			$http.post($init.data.resource_url, postData).success(function()
			{
				if(type == 'close')
					$scope.closeModal();
			});
		}
	};

}])



///////////////
;//end module//
///////////////

