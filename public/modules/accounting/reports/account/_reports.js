angular.module('core.reports', [])

.controller('reportsCTRL', ['$scope', '$http', '$init', '$state','$translate', '$sce', '$jasperReports', function($scope, $http, $init, $state,$translate, $sce, $jasperReports) {

	$scope.report_url = "/reports/test_one";
	$scope.report_title = 'REPORT_TITLES.TEST_ONE';//$init.data['report_title'];//as language var
	$scope.report_parameters = {
		prove1: 'test 111'
	};

	$scope.onReportExecution = function()
	{
		console.log('one');
		$scope.runReport($scope.filter_params);
	};

	/*
	$http.get('/core/dev/report').success(function (data) {
		//$scope.onReportExecution($scope.filter_params);//filter params
	});
	*/
}])

.controller('reportsCTRL2', ['$scope', '$http', '$init', '$state','$translate', '$sce', '$jasperReports', function($scope, $http, $init, $state,$translate, $sce, $jasperReports) {

	$scope.getReportInit = function()
	{
		$jasperReports.init('/reports/test_one', {prove1: 'test 111'}, $scope.outputReport);
	};
/**/
	$http.get('/core/dev/report').success(function (data) {
		$scope.getReportInit();
	});


	$scope.zoom = 100;
	$scope.currentPageInput = 1;
	$scope.currentPageResult = 1;
	$scope.totalPages = 0;
	$('#currentPageInput').keyup(function (e) {
		if(e.which == 13) {
			$scope.getReportPage('input');
		}
	});

	$('#zoomInput').keyup(function (e) {
		if(e.which == 13) {
			$scope.getReportPage('input');
		}
	});


	$scope.getReportPage = function(direction)
	{
		var page = $scope.currentPageResult;
		if(direction == 'next')
		{
			if(page==$scope.totalPages)
				return;
			page++;
		}
		else if(direction == 'prev')
		{
			if(page==1)
				return;
			page--;
		}
		else // input
		{
			page = $scope.currentPageInput;
			if(page < 1 || page > $scope.totalPages)
			{
				$scope.currentPageInput = $scope.currentPageResult;
				return;
			}
		}

		$jasperReports.get('/reports/test_one', page, {prove1: 'test 222'}, $scope.outputReport);
	};

	$scope.zoomPage = function(direction)
	{
		if(direction == 'in')
		{
			$scope.zoom *= 1.4;
		}
		else if(direction == 'out')
		{
			$scope.zoom /= 1.4;
		}

		$scope.zoom = Math.round($scope.zoom / 10) * 10;
		var cssZoomProp = { 'zoom': $scope.zoom/100,
		                '-moz-transform': "scale(" + $scope.zoom/100 + ")"};

		var report = $(".jrPage");
		if(report.width()*($scope.zoom/100) >= $(".report-viewer").width())
			cssZoomProp['-moz-transform-origin'] = "0 0";
		else
			cssZoomProp['-moz-transform-origin'] = "center 0";

		report.css(cssZoomProp);
	};

	$scope.outputReport = function(result)
	{
		if(result.totalPages)
			$scope.totalPages = result.totalPages;

		$scope.currentPageInput = result.page;
		$scope.currentPageResult = result.page;
		$scope.reportOutput = $sce.trustAsHtml(result.html);
		if(!$scope.$$phase)
			$scope.$apply();

		$scope.zoomPage();
	};


	$scope.toggleSplitter = function()
	{
		var toggleBtn = $(".splitter-toggle-btn-right > i");
		if(toggleBtn.hasClass('fa-angle-double-right')) //close tools
		{
			$scope.splitter.toggle(".k-pane:last", false);

			toggleBtn.removeClass('fa-angle-double-right')
					 .addClass('fa-angle-double-left');
		}
		else //show tools
		{
			$scope.splitter.toggle(".k-pane:last", true);

			toggleBtn.removeClass('fa-angle-double-left')
					 .addClass('fa-angle-double-right');
		}
	};

}])

.controller('reportsShowCTRL', ['$scope', '$ocModal', '$rootScope', '$state', function($scope, $ocModal, $rootScope, $state) {

	$scope.close = function()
	{
		//$state.go($state.previous.state.name, $state.previous.params);
		//$rootScope.closePoup();
		$ocModal.closeSelf();
	};

}])

///////////////
;//end module//
///////////////
