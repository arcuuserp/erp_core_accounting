//Module for users management
angular.module('accounting.financialstatementpositions', [])

.controller('financialstatementpositionsCTRL', ['$scope', '$http', '$init', '$state', function($scope, $http, $init, $state) {

}])

.controller('financialstatementpositionsFormCTRL', ['$scope', '$http', '$init', function($scope, $http, $init){

	
	$scope.financialstatementpositions = $init.data.financialstatementpositions; 
	$scope.statementType = [{id:'balance_sheet', name:"Bilanci"},{id:'profit_loss', name:"Pasqyra e te ardhurave dhe shpenzimeve"}];

	$scope.model = $init.data.model;
	if(!$init.data.model)
	{
		$scope.model = {active: 1};
    }


	$scope.save = function(type)
	{
		if ($scope.validator.validate())
		{
			var postData = $scope.model;
			postData.returnModel = (type=="keep");

			$http.post($init.data.resource_url, postData).success(function()
			{
				if(type == 'close')
					$scope.closeModal();
			});
		}
	}
}])

///////////////
;//end module//
///////////////

