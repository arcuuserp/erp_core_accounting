
erpApp.config(['$stateProvider','$locationProvider','$urlRouterProvider','$ocLazyLoadProvider','StateConfigProvider',
	function (  $sp , $locationProvider , $urlRouterProvider , $ocLazyLoadProvider , StateConfigProvider ) {

	StateConfigProvider.preconfig.set({statesReference: $sp, module:'accounting', debug: true});//, override:'deka'});
	var route = StateConfigProvider.stateRoutes;

	$sp.state(route.get('customers', 'main'))

	;//END of stateProvider

		/* config params: 
				param:		single url parameter name OR an array of param names
				size:		popup size = full|lg|md|sm' or null for non-popup
				ctrl: 		fullControllerName
				view: 		full template path
		*/

		route.resource('accounts',          {create: {size:'lg'}, edit: {size:'lg'}});
		route.resource('accountgroups');

		route.resource('cash',              {create: {size:'lg'}, edit: {size:'lg'}});
		route.resource('cashreceipts',      {create: {size:'full'}, edit: {size:'full'}});
		route.resource('commercialterms',   {create: {size:'lg'}, edit: {size:'lg'}});
		route.resource('contacts',          {create: {size:'full'}, edit: {size:'full'}, view:"" });
		//route.resource('customers',         {create: {size:'full'}, edit: {size:'full'}, view:"" });

		route.resource('documentgroups');
		route.resource('documentbatches', 	{create: {params:['documentgroup', 'type']}} );
		
		route.resource('financialstatementpositions');

		route.resource('login',     		{create: {size:'full'}, edit: {size:'full'}});

		route.resource('ledgerentries',     {create: {size:'full'}, edit: {size:'full'}});

		route.resource('purchaseorder',     {create: {size:'full'}, edit: {size:'full'}});
		route.resource('purchaseinvoices',  {create: {size:'full'}, edit: {size:'full'}});

		route.resource('receivedoffers',    {create: {size:'full'}, edit: {size:'full'}});
		route.resource('requestoffers',     {create: {size:'full'}, edit: {size:'full'}});

		route.resource('salesinvoices',     {create: {size:'full'}, edit: {size:'full'}});
		route.resource('salesorder',    	{create: {size:'full'}, edit: {size:'full'}});

		route.resource('transactions',      {create: {size:'full'}, edit: {size:'full'}});

		route.resource('entrywarehouse',    {create: {size:'full'}, edit: {size:'full'}});

		route.resource('exitwarehouse',     {create: {size:'full'}, edit: {size:'full'}});
		//route.resource('suppliers',			{create: {size:'full'}, edit: {size:'full'}});

		route.resource('account', ['ledger'], 'reports');
}]);
