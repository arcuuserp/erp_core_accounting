//Module for users management
angular.module('accounting.accounts', [])

.controller('accountsCTRL', ['$scope', '$http', '$init', '$state','$translate', function($scope, $http, $init, $state,$translate) {

	$scope.treeData = new kendo.data.HierarchicalDataSource({ data: $init.data.tree_data});
	
	$(window).on("resize", function() {
		kendo.resize($(".k-grid-content"));
	});

	$scope.toggleSplitter = function()
	{
		var toggleBtn = $(".splitter-toggle-btn > i");
		if(toggleBtn.hasClass('fa-angle-double-left'))
		{
			$scope.splitter.collapse(".k-pane:first");
			toggleBtn.removeClass('fa-angle-double-left')
					 .addClass('fa-angle-double-right');
		}
		else
		{
			$scope.splitter.expand(".k-pane:first");
			toggleBtn.removeClass('fa-angle-double-right')
					 .addClass('fa-angle-double-left');
		}
	};

	$scope.selectedItem = {};
	$scope.treeOptions = {
		dataSource: $scope.treeData,
		dataTextField:'name',
		change: function(e){ 
			var item = this.dataItem(this.select());

			$scope.selectedItem = angular.copy(item);//not by reference
			$scope.loadItemTabData();
			$scope.$apply();
		}
	};

	$scope.loadedOnce = false;
	$scope.loadItemTabData = function(e)
	{
		if($scope.tabnavigator.select().index()==0)//DETAILED GRID
		{	
			if($scope.selectedItem['code'])
			{
				$scope.grid.dataSource.filter({
				    logic: "and", filters: [{field:"id_contact", operator:"eq", value: $scope.selectedItem.id}]
			    });
			}
		}	
			else //GRIDS TAB
		{
			if($scope.selectedItem['docs_grid_loaded'] !== true)
			{
				var filterField = "id_contact";
				var currFilterObj = $scope.grid.dataSource.filter();
				var currentFilters = currFilterObj ? currFilterObj.filters : [];
				if (currentFilters && currentFilters.length > 0) {
			        for (var i = 0; i < currentFilters.length; i++) {
			            if (currentFilters[i].field == filterField) {
			                currentFilters.splice(i, 1);
				            //currentFilters[i].value = $scope.selectedItem.id;
			                break;
			            }
			        }
			    }

				currentFilters.push({field:filterField, operator:"eq", value:$scope.selectedItem.id});
				$scope.grid.dataSource.filter({
			        logic: "and", filters: currentFilters
			    });
				$scope.selectedItem['docs_grid_loaded'] = true;
			}
		}
		
	};
	
	
	/********************************************************
	* GRID OPTIONS
	*******************************************************/
	function getGridHeight(innerWidth)
	{
		var mainContentViewHeight = $('#main-content-view').height() - 85;
		if(innerWidth <= 767)
			mainContentViewHeight -= 40;
		return mainContentViewHeight;
	}


	$(window).on("resize", function(e)
	{
		$scope.grid.wrapper.height(getGridHeight(e.currentTarget.innerWidth));
		$scope.grid.resize(true);

	});

	$scope.filterInit = function (event) {
		var filterType = event.sender.dataSource.options.schema.model.fields[event.field].type;
		if (filterType == "date" || filterType == "number")
		{
			var container = event.container;
			var beginOperator = container.find("[data-role=dropdownlist]:eq(0)").data("kendoDropDownList");
			var endOperator = container.find("[data-role=dropdownlist]:eq(2)").data("kendoDropDownList");

			container.data("kendoPopup").bind("open", function (e)
			{
				if(filterType == "date")
				{
					beginOperator.value("lte");
					beginOperator.trigger("change");
			        endOperator.value("gte");
					endOperator.trigger("change");
				}
				else if(filterType == "number")
				{
					beginOperator.value("gte");
					beginOperator.trigger("change");
					endOperator.value("lte");
					endOperator.trigger("change");
				}
			});
		}
	};

	$scope.gridResultTotal = 0;
	$scope.gridOptions = {
		autoBind: false,
		dataSource: {
			schema: {
				data: function (result) {
					return result.data;
				},
				total: function (result) {
					if(result.total)
						$scope.gridResultTotal = result.total;

					return $scope.gridResultTotal;
				},
				model: {
			        fields: {
			        	document_date: { type: "date" },
				        total_with_vat: {
					        type: "number", parse: function (data) { return data; }
				        }
			        }
				}
			},
			pageSize: 30,
			serverPaging: true,
			serverFiltering: true,
			serverSorting: true,
			transport: {
				read: {
					url: "/accounting/docstransactions/grid"
				}
			}
		},
		columns: [
			{field: 'id_documentgroup', title: 'Tipi i Dokumentit', width: '15%',
					template: function(item){ return getDocumentTypeLang(item.subject); },
					filterable: {
						ui: docTypeFilterFunc,
		                cell: {
			                showOperators: false,
			                template: function(args) {
				                docTypeFilterFunc(args.element);
			                }
		                }
		            }
			},
			{field: 'document_date', title: 'Data Krijimit', width: '15%', format: "{0:d}", parseFormat: "yyyy-MM-dd",
					filterable: {
						ui: dateFilterFunc,
						cell: {
							showOperators: false, operator: "lte", /*, enabled: false */
							template: function(args){
								dateFilterFunc(args.element);
							}
						}
					}

			},
			{field: 'description', title: 'Pershkrimi', width: '21%',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			},
			{field: 'document_number', title: 'Numer Dokumenti', width: '15%',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			},
			{field: 'currency', title: 'Monedha', width: '10%',
					filterable: {
						cell: {
							showOperators: false, operator: "eq", dataSource: {}
						} }
			},
			{field: 'total_with_vat', title: 'Shuma', width: '20%', attributes: { style:"text-align:right;" },
					template: function(item) { return kendo.toString(parseFloat(item.total_with_vat), "n"); },
					filterable: { cell: { showOperators: false, operator: "gte"} }
			}
		],
		scrollable: {
			virtual: true
		},
		mobile: true,
		resizable: true,
		sortable: {
            mode: "multiple",
            allowUnsort: true
        },
		reorderable: true,
		filterable: {
            mode: "menu, row"
        },
		selectable: "row",
		change: onChange
	};

	function getDocumentTypeLang(docType){
		var documentTypeLabels = {  saleinvoiceheader: 'Fature Shitje',
							        purchaseinvoiceheader: 'Fatura Blerje'};

		return documentTypeLabels[docType];
	}

	function dateFilterFunc(element) {
		 element.kendoDatePicker({
			 parseFormats: Utils.dateParseFormats()
		 });
    }

    function docTypeFilterFunc(element) {
        element.kendoDropDownList({
            dataSource: new kendo.data.DataSource({ data: [
                { label: "Fature Shitje", data: 2 },
				{ label: "Mandat Arketimi", data: 3 },
				{ label: "Flete Dalje Magazine", data: 4 },
				{ label: "Urdher Shitje", data: 1 },
				{ label: "Oferta te Dhena", data: 1 },
				{ label: "Zbritje nga Shitjet", data: 1 },
				{ label: "Kthim Fature Blerje", data: 1 }
            ] }),
            optionLabel: "- Te Gjitha",
            dataTextField: "label",
            dataValueField: "data",
	        valuePrimitive: true,
	        height: 400
        });

		//set list width
		element.data("kendoDropDownList").list.width(200);
    }

	function onChange(arg) {
		var data = this.dataItem(this.select());
		$state.go($init.data.resource_url+'Edit', {id: data.id});
	}


	
	//grid
	$scope.$on("kendoWidgetCreated", function(event, widget){

		if (widget === $scope.grid)
		{
			$scope.grid.wrapper.height(getGridHeight( $(window).width() ));
			$scope.grid.resize(true);
			$("#grid tr.k-alt").removeClass("k-alt");
		}
		if(widget === $scope.tabnavigator)
		{
			$scope.tabnavigator.bind("show", $scope.loadItemTabData);
		}
	});

	
	$scope.save = function(type)
	{
		$scope.grid.saveChanges();
	}
	
}])

.controller('accountsFormCTRL', ['$scope', '$http', '$init','$aceStorage', function($scope, $http, $init, $aceStorage){

	$scope.accountstates = [{id:0, name:"Debitore"},{id:1, name:"Kreditore"},{id:2, name:"Pacaktuar"}];
	$scope.currencies = $aceStorage.kendoDataSource('/core/currencies/load', $init.data['currencies_version']);
	$scope.home_id_currency = $init.data['home_id_currency'];
	
	$scope.model = $init.data.model;
	if(!$init.data.model)
	{
		$scope.model = {account_type: 0, active: 1, id_currency: $scope.home_id_currency, opening_exchange_rate: 1};
    }
    else
    {
    	if ($scope.model['opening_balance'] && $scope.model.opening_balance .trim() != "") 
    	{
	    $scope.set_opening_balance_chbox = true;
	    }
	    else
	    {
    	$scope.set_opening_balance_chbox = false;
	    }
    };

	$scope.currencyChange = function(e)
	{
		if($scope.model.id_currency == $init.data['home_id_currency'])
			$scope.model.opening_exchange_rate = 1;
		else
			$scope.model.opening_exchange_rate = $aceStorage.get('last_exchange_rate_'+$scope.model.id_currency, 1);
		
		$scope.bildOpeningBalanceCurrency();
	};

	$scope.updateExchangeRate = function(value)
	{
		$aceStorage.set('last_exchange_rate_'+$scope.model.id_currency, (value)?value:$scope.model.opening_exchange_rate);
		$scope.bildOpeningBalanceCurrency();
	};

    $scope.bildOpeningBalanceCurrency = function () 
    {
    	var opening_balance=0;
    	var opening_exchange_rate=1;
		
		if ($scope.model['opening_balance'])
			opening_balance = parseFloat($scope.model['opening_balance']);
		if ($scope.model['opening_exchange_rate'] &&  parseFloat($scope.model.opening_exchange_rate) > 0)
			opening_exchange_rate = parseFloat($scope.model['opening_exchange_rate']);
		
		$scope.model.opening_balance_home_currency = opening_balance*opening_exchange_rate;
		
    };

	$scope.parent_code_combo.bind("change", function(e)
	{
		if($scope.model['code'] == null || ($scope.model['code'] && $scope.model.code.trim() == ""))
		{
			var selectedItem = this.dataItem();
			$scope.$apply(function(){
				$scope.model.code = selectedItem.code;
				angular.element('#code_input').focus();
			});
		}
	});

	$scope.save = function(type)
	{
		if ($scope.validator.validate())
		{
			var postData = $scope.model;
			postData.returnModel = (type=="keep");

			$http.post($init.data.resource_url, postData).success(function()
			{
				if(type == 'close')
					$scope.closeModal();
			});
		}
	}
}])


///////////////
;//end module//
///////////////

