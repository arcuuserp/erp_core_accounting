var erpApp = angular.module('erpApp', [
	 'ui.router'
	,'ace.stateConfigProvider'
	,'ngCookies'
	,'pascalprecht.translate'
	,'infinite-scroll'
	,'oc.lazyLoad'
	,'oc.modal'
	,'kendo.directives'
	,'ace.directives.custom.kendo'
	,'ace.directives'
	,'ace.common.filters'
	,'ace.common.services'
]);

erpApp.config(['$translateProvider','i18n_FILES','PREFERRED_LANGUAGE',
	function (  $translateProvider , i18n_FILES , PREFERRED_LANGUAGE ) {

	var langFiles = [{
			prefix: '/modules/core/i18n/common.',
			suffix: '.json'
		}];

	if(i18n_FILES)
	{
		angular.forEach(i18n_FILES, function(values, module_key) {
			for(var i=0; i<values.length; i++)
			{
				this.push({ prefix: '/modules/'+module_key+'/i18n/'+values[i]+'.', suffix: '.json' });
			}
		}, langFiles);
	}

    $translateProvider.useStaticFilesLoader({files: langFiles})
	    .preferredLanguage(PREFERRED_LANGUAGE)
	    .fallbackLanguage('sq-AL');

	//$translateProvider.useLocalStorage();
	$translateProvider.useMissingTranslationHandlerLog();

	//KENDO-UI LANGUAGE
	kendo.culture(PREFERRED_LANGUAGE);

}]);

erpApp.factory('errorInterceptor', ['$q', '$rootScope', '$location',
    function ($q, $rootScope, $location) {
        return {
            /*request: function (config) {
                return config || $q.when(config);
            },
            requestError: function(request){
                return $q.reject(request);
            },*/
            response: function (response)
            {
	            if (response.status == 226)//pdf-report
	            {
	                //$rootScope.dialogManager('pdf', 'Print PDF', response.data);
	            }

                return response || $q.when(response);
            },
            responseError: function (response)
            {
	            if (response.status == 401)
	            {
	                delete sessionStorage.authenticated;
	                $location.path('/');
	                Flash.show(response.data.flash);
	                //$rootScope.dialogManager('error', 'Restricted Area', 'Please login');
	            }
	            else if(response.status == 403)//Restricted Area
	                $rootScope.dialogManager('SERVER.RESTRICTED_MESSAGE', 'SERVER.RESTRICTED_AREA', 'info');
	            else if(response.status == 406)
	                $rootScope.dialogManager(response.data, 'SERVER.NOT_ACCEPTABLE', 'warning');
	            else if(response.status == 412)//ERROR WITH CUSTOM MESSAGE WHERE THE USER NEEDS TO TAKE ACTION
	                $rootScope.dialogManager(response.data, 'SERVER.WARNING', 'warning');
	            else if(response.status == 418)
	                $rootScope.dialogManager('SERVER.TOKEN_MISSMATCH', 'SERVER.ERROR', 'danger');
	            else// if(response.status == 500)
	                $rootScope.dialogManager('SERVER.ERROR_MESSAGE', 'SERVER.ERROR', 'danger');

                return $q.reject(response);
            }
        };
}]);


erpApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('errorInterceptor');
}]);

// It's very handy to add references to $state and $stateParams to the $rootScope
// so that you can access them from any scope within your applications. For example,
// <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
// to active whenever 'contacts.list' or one of its decendents is active.
erpApp.run( ['$rootScope','$state','$stateParams','$http','CSRF_TOKEN','$ocModal','$location','$translate',
	function ($rootScope , $state , $stateParams , $http , CSRF_TOKEN , $ocModal , $location , $translate) {

		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;

		$http.defaults.headers.common['csrf_token'] = CSRF_TOKEN;

	    $rootScope.dialogManager = function(message, title, type)
	    {
		    if(type == 'pdf')
		    {
		        $ocModal.open('partials/dialogs/PDFViewerPopup.html', 'pdfPopupCtrl', message, {
						    size: 'lg',
						    keyboard: false,
						    backdrop: 'static',
						    windowClass: 'popup'
					    });
		    }
		    else if(type == 'login')
		    {
		        $ocModal.open('partials/dialogs/loginAuthPopup.html', 'loginAuthPopupCtrl', {}, {
						    size: 'md',
						    keyboard: true,
						    backdrop: true,
						    windowClass: 'popup'
					    });
		    }
		    else
		    {
			    var translateVars = [title];
			    if(angular.isArray(message))
			    {
				    translateVars = message;
					translateVars.unshift(title);
			    }
			    else if(typeof(message) === 'object' && !!message)
			    {
				    if(message['msg'])
					    translateVars.push(message['msg']);
				    else
					    translateVars.push('SERVER.ERROR_MESSAGE');
			    }
			    else
			        translateVars.push(message);

				$translate(translateVars).then(function (translations) {
					var messageValue;
					title = translations[translateVars[0]];
					if(translateVars.length > 2)
					{
						messageValue = [];
						//SKIP TITLE (i=1)
						for(var i=1; i<translateVars.length; i++)
						{
							messageValue.push(translations[translateVars[i]]);
						}
					}
					else
						messageValue = translations[translateVars[1]];

					//OPEN POPUP WINDOW
			        $ocModal.alert(messageValue, title, type);
				});
		    }
	    };

		$rootScope.currentUser = function() {
			return sessionStorage.firstname;
		};

		$rootScope.goBack = function () {
			window.history.back();
		};

		$rootScope.$on('onModuleCloseEvent', function(event, id, modal)
		{
			if($state.previous.state['name'] == "")
			{
				$state.previous = {state: $state.get('home'), params: {}};
			}

			$state.go($state.previous.state.name, $state.previous.params);
		});

		$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

			if( $ocModal.getOpenedModals().length > 0 &&
				$ocModal.getOpenedModals().indexOf(fromState))
			{
				//event.preventDefault();
				$ocModal.close(fromState, false);
			}

			$state.previous = {state: fromState, params: fromParams};
		});


		$rootScope.changeLanguage = function (langKey) {
			//console.log(langKey);
		    $translate.use(langKey);
			console.log($translate('HEADLINE'));
			$translate('NAMESPACE.PARAGRAPH').then(function (value) {
				console.log(value);
			});
		};
	}
]);


erpApp.controller('HomeCtrl', ['$scope', '$http', '$rootScope', '$state', function($scope, $http, $rootScope, $state) {

	//for later implementation as needed
	$scope.popup = function()
	{
		//dialogs.notify("Under maintainance", "Under maintainance, please come back to it later.",{size: 'sm'});
	};

	$scope.openState = function()
	{
		$state.go("test");
	};

	$(document).ready(function() {
		$("#recentAndCreateMenus").kendoMenu();
	});

}]);

erpApp.controller('OcModelCustomPopupController', ['$scope', '$init', function($scope, $init) {
    console.log($scope.param1, $init.param1);
}]);

erpApp.controller('BodyCtrl', ['$scope', '$http', '$rootScope', '$timeout', function($scope, $http, $rootScope, $timeout) {

	$scope.menu_left_datasource = [
		{text: 'Sistemi', value: 'core', view: '/modules/core/menu_left.html'},
		{text: 'Kontabiliteti', value: 'accounting', view: '/modules/accounting/menu_left.html'},
		{text: 'Magazina', value: 'warehouse', view: '/modules/warehouse/menu_left.html'},
		{text: 'Billing', value: 'billing', view: '/modules/accounting/menu_left.html'}
	];
	$scope.menu_left_selected = $scope.menu_left_datasource[1];
	$scope.menuLeftDropDownOptions = {
        dataTextField: "text",
        dataValueField: "value",
        dataSource: $scope.menu_left_datasource,
        select: onMenuLeftDropDownSelect,
		index: 1
    };

    function onMenuLeftDropDownSelect(e) {
	    var dataItem = this.dataItem(e.item.index());
	    $scope.menu_left_selected = dataItem;
    };


	$scope.panelBarOptions = {
		/*animation: {
			open : { effects: "fadeIn" }
		},*/
		expandMode: "single",//multiple
		select: function(e) {
			if ($(e.item).is(".k-state-active")) {
				var that = this;
				$timeout(function(){ that.collapse(e.item); }, 100);
			}
		},
		collapse: function (e) {
			$(".k-state-selected", e.item).removeClass("k-state-selected k-state-focused");
		}
	};


	$scope.toggleMenu = function()
	{
		var mainMenu = $("#main-sidebar");
		var mainView = $("#main-content-view");
		var toggleBtn = $("#main-sidebar-toggle-btn > i");
		if(mainMenu.hasClass('toggled-main-sidebar'))
		{
			mainMenu.removeClass('toggled-main-sidebar');
			mainView.removeClass('toggled-main-sidebar');
			toggleBtn.removeClass('fa-angle-double-right')
					 .addClass('fa-angle-double-left');
		}
		else
		{
			mainMenu.addClass('toggled-main-sidebar');
			mainView.addClass('toggled-main-sidebar');
			toggleBtn.removeClass('fa-angle-double-left')
					 .addClass('fa-angle-double-right');
		}

		//REFRESH THE SPLITTER CONTAINER TO ADOPT TO THE NEW SISE
		try {
			$('.section-splitter-container').data("kendoSplitter").resize(true);
		}
		catch(err) {
			//console.log(err);
		}
	};

}]);
