//Module for users management
angular.module('warehouse.itemtypes', [])

.controller('itemtypeCTRL', ['$scope', '$http', '$init', '$state', function($scope, $http, $init, $state) {

}])

.controller('itemtypesFormCTRL', ['$scope', '$http', '$init', function($scope, $http, $init){

	
	$scope.itemtypes=$init.data.itemtypes;
	$scope.itemkinds=$init.data.itemkinds;

	$scope.model = $init.data.model;
	if(!$init.data.model)
	{
		$scope.model = {active: 1};
	}


	$scope.save = function(type)
	{
		if ($scope.validator.validate())
		{
			var postData = $scope.model;
			postData.returnModel = (type=="keep");

			$http.post($init.data.resource_url, postData).success(function()
			{
				if(type == 'close')
					$scope.closeModal();
			});
		}
	}
}])



///////////////
;//end module//
///////////////

