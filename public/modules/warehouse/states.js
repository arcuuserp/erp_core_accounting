
erpApp.config(['$stateProvider','$locationProvider','$urlRouterProvider','$ocLazyLoadProvider','StateConfigProvider',
	function (  $sp , $locationProvider , $urlRouterProvider , $ocLazyLoadProvider , StateConfigProvider ) {

	StateConfigProvider.preconfig.set({statesReference: $sp, module:'warehouse', debug: false});//, override:'deka'});
	var route = StateConfigProvider.stateRoutes;

	$sp
	//.state(route.get('itemgroups', 'create'))
	;

	route.resource('warehouses',        {create: {size:'lg'}, edit: {size:'lg'}});
	route.resource('itemtypes');
	route.resource('itemkinds');
	route.resource('items',				{create: {size:'full'}, edit: {size:'full'}});
	route.resource('itemgroups',		{create: {size:'full'}, edit: {size:'full'}});
	route.resource('itemaccountsschemas',	{create: {size:'full'}, edit: {size:'full'}});
	
	

		
}]);
