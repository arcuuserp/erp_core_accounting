//Module for users management
angular.module('warehouse.itemaccountsschemas', [])

.controller('itemaccountsschemasCTRL', ['$scope', '$http', '$init', '$state', '$translate', '$aceStorage', function($scope, $http, $init, $state, $translate, $aceStorage) {
  	
  	//$aceStorage.clear();

/*  	id	id_itemkind	field_code	language_var	description	active
1	1	inventory	ACCOUNT_SCHEMAS.INVENTORY	\N	1
2	1	purchase	ACCOUNT_SCHEMAS.PURCHASE	\N	1
3	1	cost	ACCOUNT_SCHEMAS.COST	\N	1
4	1	sales	ACCOUNT_SCHEMAS.SALES	\N	1
5	1	purchase_discount	ACCOUNT_SCHEMAS.PURCHASE_DISCOUNT	\N	1
6	1	sales_discount	ACCOUNT_SCHEMAS.SALES_DISCOUNT	\N	1
7	2	cost	ACCOUNT_SCHEMAS.COST	\N	1
8	2	sales	ACCOUNT_SCHEMAS.SALES	\N	1
9	2	purchase_discount	ACCOUNT_SCHEMAS.PURCHASE_DISCOUNT	\N	1
10	2	sales_discount	ACCOUNT_SCHEMAS.SALES_DISCOUNT	\N	1
11	4	purchase	ACCOUNT_SCHEMAS.PURCHASE	\N	1
12	4	cost	ACCOUNT_SCHEMAS.COST	\N	1
13	4	sales	ACCOUNT_SCHEMAS.SALES	\N	1
14	4	purchase_discount	ACCOUNT_SCHEMAS.PURCHASE_DISCOUNT	\N	1
15	4	sales_discount	ACCOUNT_SCHEMAS.SALES_DISCOUNT	\N	1
16	4	actives	ACCOUNT_SCHEMAS.ACTIVES	\N	1
17	4	depriciation_expenditures	ACCOUNT_SCHEMAS.DEPRICIATION_EXPENDITURES	\N	1
18	4	accumulated_depriciation	ACCOUNT_SCHEMAS.ACCUMULATED_DEPRICIATION	\N	1*/
  	$scope.lang = {};
	$translate(['ACCOUNT_SCHEMAS.INVENTORY', 'ACCOUNT_SCHEMAS.PURCHASE', 'ACCOUNT_SCHEMAS.COST', 'ACCOUNT_SCHEMAS.SALES', 'ACCOUNT_SCHEMAS.PURCHASE_DISCOUNT',
		'ACCOUNT_SCHEMAS.SALES_DISCOUNT', 'ACCOUNT_SCHEMAS.ACTIVES', 'ACCOUNT_SCHEMAS.DEPRICIATION_EXPENDITURES', 'ACCOUNT_SCHEMAS.ACCUMULATED_DEPRICIATION'
		]).then(function (translations) {
		$scope.lang = translations;

		$scope.accountsGridOptionsLoad();
	});

	$scope.itemkinds = $aceStorage.kendoDataSource('/warehouse/itemkinds/load', $init.data['itemkinds_version']);
	$scope.schemas_data = [];
	$scope.selectedItem = {};
	$scope.schemas_structure = $init.data['schemas_structure']


	$scope.selectedItemkind = {};
	$scope.itemkindSchema = function (e) 
    {
		if (e.sender.select() == -1) {
            e.sender.value("");
			$scope.selectedItemkind = {};
			return;
        }

		$scope.selectedItemkind = {id: e.sender.dataItem()['id'], name: e.sender.dataItem()['name']};//not by reference
		//console.log("te",$scope.selectedItemkind);
	
		if($scope.selectedItemkind['id'])
		{
			var postData = {id_itemkind: $scope.selectedItemkind['id']};
			$http.post('/warehouse/itemaccountsschemas/load', postData).success( function (data) {
				
				var schemas = data;
				var newSchema ={id:'new' ,name:'Krijo nje skeme te re', accounts:[]};
				var choseSchema ={ id:'select' , name:'Zgjidh nje skeme Llogarish' };
				schemas.push(newSchema);
				schemas.unshift(choseSchema);
				$scope.schemaCombo.dataSource.data(schemas);
				//console.log('test2',$scope.schemaCombo);

			});
		}


	};

	$scope.itemfieldSchema = function (e) 
    {
    	var selectedSchema= e.sender.dataItem();
		
        if(e.sender.dataItem()['id'] != 'new' ){
        	var grid_data = [];
        	var structureLength = $scope.schemas_structure.length; 
        	var accountsLength = selectedSchema['accounts'].length; 
        	for(var i=0; i<structureLength; i++)
        	{
	        	if($scope.selectedItemkind ['id'] == $scope.schemas_structure[i].id_itemkind )
	        	{
	        		for (var j=0; j<accountsLength; j++)
	        		{
	        			if ($scope.schemas_structure[i].field_code == selectedSchema['accounts'][j].field_code) 
	        			{
							var d = new Date();
	        				grid_data.push({id: d.getTime()+'_'+Math.random(), 
	        					label: $scope.lang[$scope.schemas_structure[i].language_var],
	        					field: $scope.schemas_structure[i].field_code,
	        					id_account: selectedSchema['accounts'][j].id_account}); 
	        				break ;
	        			}
	        		}
	        	}
        	}
        	$scope.grid.dataSource.data(grid_data);       
        }
	};

	/********************************************************
	* GRID OPTIONS
	*******************************************************/

	$scope.accComboBoxDataSource = $aceStorage.kendoDataSource('/accounting/accounts/load', $init.data['accounts_version']);
    $scope.subjectComboBoxEditor = function (container, options) {
        $('<input data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoMultiFilterComboBox({
		        dataSource: $scope.accComboBoxDataSource,
		        headerTemplate: '<a style="display: block; width: 100%; background-color: #ddd;" href="#">Add New</a>',
		        multiFilterFields: [
				        { field: "code", operator: "startswith"},
				        { field: "name", operator: "contains"}
			        ],
		        filter: 'startswith',
		        suggest: true,
                dataValueField: "id",
                dataTextField: "code_name",
		        open: function() { $scope.gridDropdownPopupClosed = false; },
		        close: function() { $scope.gridDropdownPopupClosed = true; },
                change: function () {
	                if (this.value() && this.select() === -1) {
		                this.value("");
			            options.model.set('subject', null);
		            }
                }
            });
    };


	$scope.accountsGridOptionsLoad = function()
	{
		$scope.gridOptions = {
			columns: [
				{field: 'row', title: '#', width: '30px', attributes: { "class": "row-col" }},
				{field: 'description', title: $scope.lang['INPUTS.DESCRIPTION'], width: '130px'},
				{field: 'subject', title: $scope.lang['ACC_COMMON.SUBJECT_ACCOUNT'], width: '280px',
						template: function(item) {
							if(item.subject==null) return "";
							return item.subject['code_name'];
						},
						editor: function (container, options) {
					        $scope.subjectComboBoxEditor(container, options);
					    }
				}
			],
			dataSource: {
				//autoSync: true,
				data: [],
				schema: {
					model: {
						id: "id",
				        fields: {
	                        row: {editable: false},
					        description: {type: "string"},
					        subject: {}
				        }
					}
				}
			},
			editable: {
				mode: "inline",
	            confirmation: false
			},
			mobile: true,
			resizable: true,
			scrollable: false
		};
	};

	$scope.save = function()
	{
		if ($scope.validator.validate())
		{
			$http.post($init.data.resource_url, $scope.model).success(function()
			{
				$state.reload();
			});
		}
	};

}])

.controller('itemaccountsschemasCTRL', ['$scope', '$http', '$init','$state', function($scope, $http, $init,$state){

	
	$scope.id_itemkind = $init.data.id_itemkind;
	
	$scope.model = $init.data.model;
	if(!$init.data.model)
	{
		$scope.model = {active: 1, id_currency: 2};
    }

	

	$scope.save = function()
	{
		if ($scope.validator.validate())
		{
			$http.post($init.data.resource_url, $scope.model).success(function()
			{
				$state.reload();
			});
		}
	};

	$scope.refresh = function()
	{
		$state.reload();
	};

}])	


///////////////
;//end module//
///////////////
