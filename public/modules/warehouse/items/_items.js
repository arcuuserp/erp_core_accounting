//Module for users management
angular.module('warehouse.items', [])


.controller('itemsCTRL', ['$scope', '$http', '$init', '$state', function($scope, $http, $init, $state) {

	$scope.treeData = new kendo.data.HierarchicalDataSource({ data: $init.data.tree_data});
	
	$(window).on("resize", function() {
		kendo.resize($(".k-grid-content"));
	});

	$scope.toggleSplitter = function()
	{
		var toggleBtn = $(".splitter-toggle-btn > i");
		if(toggleBtn.hasClass('fa-angle-double-left'))
		{
			$scope.splitter.collapse(".k-pane:first");
			toggleBtn.removeClass('fa-angle-double-left')
					 .addClass('fa-angle-double-right');
		}
		else
		{
			$scope.splitter.expand(".k-pane:first");
			toggleBtn.removeClass('fa-angle-double-right')
					 .addClass('fa-angle-double-left');
		}
	};

	$scope.selectedItem = {};
	$scope.treeOptions = {
		dataSource: $scope.treeData,
		dataTextField:'name',
		change: function(e){ 
			var item = this.dataItem(this.select());

			$scope.selectedItem = angular.copy(item);//not by reference
			$scope.loadItemTabData();
		}
	};

	$scope.loadedOnce = false;
	$scope.loadItemTabData = function(e)
	{
		if($scope.tabnavigator.select().index()==0)//DETAILED GRID
		{	
			if($scope.selectedItem['type'])
			{
				$scope.grid.dataSource.filter({
				    logic: "and", filters: [{field:"id_"+$scope.selectedItem.type, operator:"eq", value: $scope.selectedItem.id}]
			    });
			}
		}
		else //ITEM PRICE LEVELS
		{	
			if($scope.selectedItem['type'])
			{
				$scope.grid2.dataSource.filter({
				    logic: "and", filters: [{field:"id_"+$scope.selectedItem.type, operator:"eq", value: $scope.selectedItem.id}]
			    });
			}
			else if($scope.loadedOnce == false)
			{
				$scope.loadedOnce = true;
				$scope.grid2.dataSource.read();
			}
		}
	};
	
	//grid
	function getGridHeight(innerWidth)
	{
		var mainContentViewHeight = $('#main-content-view').height() - 85;
		if(innerWidth <= 767)
			mainContentViewHeight -= 40;
		return mainContentViewHeight;
	}

	function getGrid2Height(innerWidth)
	{
		var mainContentViewHeight = $('#main-content-view').height() - 140;
		if(innerWidth <= 767)
			mainContentViewHeight -= 40;
		
		return mainContentViewHeight;
	}

	$(window).on("resize", function(e)
	{
		$scope.grid.wrapper.height(getGridHeight(e.currentTarget.innerWidth));
		$scope.grid.resize(true);
		
		$scope.grid2.wrapper.height(getGrid2Height(e.currentTarget.innerWidth));
		$scope.grid2.resize(true);
	});

	$scope.gridResultTotal = 0;
	$scope.gridOptions = {
		dataSource: {
			schema: {
				data: function (result) {
					return result.data;
				},
				total: function (result) {
					if(result.total)
						$scope.gridResultTotal = result.total;

					return $scope.gridResultTotal;
				},
				model: {
			        fields: {
				        opening_balance: {
					        type: "number", parse: function (data) { return data; }
				        }
			        }
				}
			},
			pageSize: 30,
			serverPaging: true,
			serverFiltering: true,
			serverSorting: true,
			transport: {
				read: {
					url: $init.data.resource_url+'/grid'
				}
			}
		},
		columns: [
			{field: 'code', title: 'Kodi', width: '90px',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			},
			{field: 'name', title: 'Emri', width: '180px',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			},
			{field: 'actual_quantity', title: 'Sasia Gjendje', width: '135px',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			},
			{field: 'unit', title: 'Njesia', width: '95px',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			},
			{field: 'cost', title: 'Kosto', width: '115px', 
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			},
			{field: 'purchase_account', title: 'Ll.Blerje', width: '170px',
					filterable: { cell: { showOperators: false, operator: "eq", dataSource: {}} }
			},
			{field: 'sale_account', title: 'Ll.Shitje', width: '105px',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			},
			{field: 'cost_account', title: 'Ll.Kosto', width: '180px',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			},
			{field: 'account', title: 'Ll.Inventari', width: '140px',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			},
			{field: 'barcode', title: 'Barkodi', width: '135px',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			},
			{field: 'opening_date', title: 'Date Celje', width: '180px',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			}
		],
		scrollable: {
			virtual: true
		},
		mobile: true,
		resizable: true,
		sortable: {
            mode: "multiple",
            allowUnsort: true
        },
		reorderable: true,
		filterable: {
            mode: "menu, row"
        },
		selectable: "row",
		change: onChange
	};

	function onChange(arg) {
		var data = this.dataItem(this.select());
		$state.go($init.data.resource_url+'Edit', {id: data.id});
	}

	$scope.price_levels_count = 10;
	if($init.data.price_levels_count)
		$scope.price_levels_count = parseInt( $init.data.price_levels_count);
	
	$scope.modelFields = { 	code: { editable: false },
				       		name: { editable: false },
				       		unit: { editable: false }};

	$scope.gridColumns = [
			{field: 'code', title: 'Kodi', width: '95px',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			},
			{field: 'name', title: 'Emri', width: '180px',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			},
			{field: 'unit', title: 'Njesia', width: '108px',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			}
		];

	for(var i=1; i<=$scope.price_levels_count; i++) {
		$scope.gridColumns.push(
			{field: 'level_'+i, title: 'Niveli '+i, width: '135px',
					filterable: { cell: { showOperators: false, operator: "contains", delay: 707, minLength: 3} }
			});

		$scope.modelFields['level_'+i] = {type: 'number'};
	};
				
	$scope.gridOptions2 = {
		dataSource: {
			schema: {
				data: function (result) {
					return result.data;
				},
				total: function (result) {
					if(result.total)
						$scope.gridResultTotal = result.total;

					return $scope.gridResultTotal;
				},
				model: {
			        id: "id",
			        fields: $scope.modelFields
				}
			},
			pageSize: 30,
			serverPaging: true,
			serverFiltering: true,
			serverSorting: true,
			autoBind: false,
			transport: {
				read: {
					url: $init.data.resource_url+'/grid2'
				},				
				update: {
					url: '/warehouse/itempricelevels',
            		type: "post"
				}
			},
			batch: true
		},
		columns: $scope.gridColumns,
		editable: true,
		scrollable: {
			virtual: true
		},
		mobile: true,
		resizable: true,
		sortable: {
            mode: "multiple",
            allowUnsort: true
        },
		filterable: {
            mode: "menu, row"
        }
	};


	//grid
	$scope.$on("kendoWidgetCreated", function(event, widget){

		if (widget === $scope.grid)
		{
			$scope.grid.wrapper.height(getGridHeight( $(window).width() ));
			$scope.grid.resize(true);
			$("#grid tr.k-alt").removeClass("k-alt");
		}
		if (widget === $scope.grid2)
		{
			$scope.grid2.wrapper.height(getGrid2Height( $(window).width() ));
			$scope.grid2.resize(true);
			$("#grid tr.k-alt").removeClass("k-alt");
		}
		if(widget === $scope.tabnavigator)
		{
			$scope.tabnavigator.bind("show", $scope.loadItemTabData);
		}
	});

	
	$scope.save = function(type)
	{
		$scope.grid2.saveChanges();
	}
	


}])


.controller('itemsFormCTRL', ['$scope', '$http', '$init', '$aceStorage', function($scope, $http, $init, $aceStorage){


	$scope.currencies = $aceStorage.kendoDataSource('/core/currencies/load', $init.data['currencies_version']);
	$scope.home_id_currency = $init.data['home_id_currency'];
	$scope.itemkinds = $aceStorage.kendoDataSource('/warehouse/itemkinds/load', $init.data['itemkinds_version']);
	$scope.itemtypes = $aceStorage.kendoDataSource('/warehouse/itemtypes/load', $init.data['itemtypes_version']);
	$scope.itemgroups = $aceStorage.kendoDataSource('/warehouse/itemgroups/load', $init.data['itemgroups_version']);
	$scope.taxes = $aceStorage.kendoDataSource('/accounting/taxes/load', $init.data['taxes_version']);
	
	$scope.model = $init.data.model;
	if(!$init.data.model)
	{
		$scope.model = {id_currency: $scope.home_id_currency, opening_exchange_rate: 1, id_itemkind: 1};
    }

    $scope.itemgroupChange = function(e)
	{
		return;
	};

    $scope.itemtypeChange = function(e)
	{	
		
		$scope.itemgroups.filter({
	        logic: "and", filters: [{field: 'id_itemtype', operator:"eq", value: $scope.model.id_itemtype}]
	    });
	};

	$scope.itemkindChange = function(e)
	{
		$scope.itemtypes.filter({
	        logic: "and", filters: [{field: 'id_itemkind', operator:"eq", value: $scope.model.id_itemkind}]
	    });

	    var view = $scope.itemtypes.view();
		if(view[0])
		{
			$scope.model.id_itemtype = view[0].id;
		}
		$scope.itemtypeChange();
	};
	
	$scope.itemkindChange();


    $scope.currencyChange = function(e)
	{
		if($scope.model.id_currency == $init.data['home_id_currency'])
			$scope.model.opening_exchange_rate = 1;
		else
			$scope.model.opening_exchange_rate = $aceStorage.get('last_exchange_rate_'+$scope.model.id_currency, 1);
	};

	$scope.updateExchangeRate = function(value)
	{
		$aceStorage.set('last_exchange_rate_'+$scope.model.id_currency, (value)?value:$scope.model.opening_exchange_rate);
	};
	
	console.log( $scope.model);
	$scope.displayNameComboCustomValue = "";
	$scope.displayNameComboOptions = {
		dataSource: { data: [] },
		change: function () {
			if (this.value() && this.select() === -1) {
				$scope.displayNameComboCustomValue = this.value();
				$scope.bildDispalyName();
			}
		}
	}

    $scope.bildDispalyName = function () 
    {
		var displayListSuggestions = [];
		if ($scope.model['contact_firstname'] && $scope.model.contact_firstname.trim() != "")
		{
			if($scope.model['contact_lastname'] && $scope.model.contact_lastname.trim() !="")
			{
				displayListSuggestions.push($scope.model.contact_firstname+" "+$scope.model.contact_lastname);
				displayListSuggestions.push($scope.model.contact_lastname+" "+$scope.model.contact_firstname);
			}
			displayListSuggestions.push($scope.model.contact_firstname);
		}
		else if($scope.model['contact_lastname'] && $scope.model.contact_lastname.trim() !="")
		{
			displayListSuggestions.push($scope.model.contact_lastname);			
		}

		if ($scope.model['company_name'] && $scope.model.company_name.trim() !="")
		{
			displayListSuggestions.push($scope.model.company_name);
		}

		console.log(displayListSuggestions);
		var dataSource = new kendo.data.DataSource({ data: displayListSuggestions});		
		$scope.display_name_combo.setDataSource(dataSource);
		$scope.display_name_combo.select(0);
    } ;

    $scope.testDisplay = function()
    {
    	console.log($scope.model);
    }

	$scope.save = function(type)
	{
		if ($scope.validator.validate())
		{
			var postData = $scope.model;
			postData.returnModel = (type=="keep");

			$http.post($init.data.resource_url, postData).success(function()
			{
				if(type == 'close')
					$scope.closeModal();
			});
		}
	}
}])

///////////////
;//end module//
///////////////


