//Module for users management
angular.module('warehouse.itemgroups', [])

.controller('itemgroupsCTRL', ['$scope', '$http', '$init', '$state','$translate', function($scope, $http, $init, $state, $translate) {

	$scope.treeData = new kendo.data.HierarchicalDataSource({ data: $init.data.tree_data});
	
	$(window).on("resize", function() {
		kendo.resize($(".k-grid-content"));
	});

	$scope.toggleSplitter = function()
	{
		var toggleBtn = $(".splitter-toggle-btn > i");
		if(toggleBtn.hasClass('fa-angle-double-left'))
		{
			$scope.splitter.collapse(".k-pane:first");
			toggleBtn.removeClass('fa-angle-double-left')
					 .addClass('fa-angle-double-right');
		}
		else
		{
			$scope.splitter.expand(".k-pane:first");
			toggleBtn.removeClass('fa-angle-double-right')
					 .addClass('fa-angle-double-left');
		}
	};

	$scope.selectedItem = {};
	$scope.treeOptions = {
		dataSource: $scope.treeData,
		dataTextField:'name',
		change: function(e){ 

			var item = this.dataItem(this.select());

			$scope.selectedItem = angular.copy(item);//not by reference

			if (item.type == 'itemkind' || item.type == 'itemgroup')
			{
				;//show btn
			}
		}
	};


	//on btn click
	$scope.createNewGroup = function()
	{
		var selectedNode = $scope.tree.select();
		var itemtypeID = $scope.selectedItem.id;

		if ($scope.selectedItem.type == 'itemgroup')
		{
			selectedNode = $scope.tree.parent(selectedNode);
			itemtypeID = $scope.selectedItem.id_itemtype;
		}
		console.log('itemtypeID', itemtypeID);
		$scope.tree.select($scope.tree.append({id: -1, name: '+ Grupi i ri', id_itemtype: itemtypeID}, selectedNode));

		//edit mode disable tree
		$("div.overlay").fadeToggle("fast");
	};

	
	$scope.accountinginventorymethod_data = [{id:1, name:"FIFO"}, {id:2, name:"KM"}];

	$scope.model = $init.data.model;
	if(!$init.data.model)
	{
		$scope.model = {};
    }

	$scope.save = function()
	{
		if ($scope.validator.validate())
		{
			var postdata = {name: $scope.selectedItem.name,
				description: $scope.selectedItem.description,
				id_itemtype: $scope.selectedItem.id_itemtype,
				id_accountinginventorymethod: $scope.selectedItem.id_accountinginventorymethod,
				accountinginventorymethod_name: $scope.accountinginventorymethod.dataItem().name,
				id_item_accounts_schema: $scope.selectedItem.id_item_accounts_schema};

			if($scope.selectedItem.id != -1)
				postdata.id = $scope.selectedItem.id;

			console.log('postdata', postdata);
			$http.post($init.data.resource_url,postdata).success(function()
			{
				$state.reload();
			});
		}
	};
	
	$scope.refresh = function()
	{
		$state.reload();
	};


	
	

}])

.controller('itemgroupsFormCTRL', ['$scope', '$http', '$state', '$init', function($scope, $http, $state, $init) {

 		

}])

///////////////
;//end module//
///////////////
