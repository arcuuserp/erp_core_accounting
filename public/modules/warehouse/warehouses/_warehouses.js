//Module for users management
angular.module('warehouse.warehouses', [])

.controller('warehousesCTRL', ['$scope', '$http', '$init', '$state', '$translate', function($scope, $http, $init, $state, $translate) {

	$scope.$on("kendoWidgetCreated", function(event, widget)
	{
		if (widget === $scope.grid)
		{
			$scope.grid.wrapper.height(getGridHeight( $(window).width() ));
			$scope.grid.resize(true);
			$("#grid tr.k-alt").removeClass("k-alt");
			/*$scope.grid.element.on('dblclick', function (e) {console.log(e) });*/
		}
		if(widget === $scope.tabnavigator)
		{
			$scope.tabnavigator.bind("show", $scope.loadItemTabData);
		}
		//if(widget.options.name === "Splitter"){//widget.element.data("kendoSplitter").resize(true);}
	});


	$scope.selectedItem = {};
	$scope.mainListCodeField = "code";
	$scope.mainListNameField = "name";

	$scope.getSelectedItem = function(item)
	{
		$scope.selectedItem = angular.copy(item);//not by reference
		$scope.loadItemTabData();
	};

	$scope.loadItemTabData = function(e)
	{
		if($scope.tabnavigator.select().index()==0)//details tab
		{
			if(!$scope.selectedItem.hasOwnProperty('created_at'))//if no details loaded
			{
				$http.get($init.data.resource_url+'/'+$scope.selectedItem.id).success(function(result)
				{
					angular.extend($scope.selectedItem, result);
				});
			}
		}
		else //GRIDS TAB
		{
			if($scope.selectedItem['docs_grid_loaded'] !== true)
			{
				var filterField = "id_warehouse";
				var currFilterObj = $scope.grid.dataSource.filter();
				var currentFilters = currFilterObj ? currFilterObj.filters : [];
				if (currentFilters && currentFilters.length > 0) {
			        for (var i = 0; i < currentFilters.length; i++) {
			            if (currentFilters[i].field == filterField) {
			                currentFilters.splice(i, 1);
			                break;
			            }
			        }
			    }

				currentFilters.push({field:filterField, operator:"eq", value:$scope.selectedItem.id});
				$scope.grid.dataSource.filter({
			        logic: "and", filters: currentFilters
			    });
				$scope.selectedItem['docs_grid_loaded'] = true;
			}
		}
	};

	$scope.toggleSplitter = function()
	{
		var toggleBtn = $(".splitter-toggle-btn > i");
		if(toggleBtn.hasClass('fa-angle-double-left'))
		{
			$scope.splitter.collapse(".k-pane:first");
			toggleBtn.removeClass('fa-angle-double-left')
					 .addClass('fa-angle-double-right');
		}
		else
		{
			$scope.splitter.expand(".k-pane:first");
			toggleBtn.removeClass('fa-angle-double-right')
					 .addClass('fa-angle-double-left');
		}
	};

	$scope.mainListData = [];
	$scope.mainListOpt = {
		wait: false,
		filter: null,
		sort: [],
		nextPage: 1,
		total: 0
	};

	$scope.mainListDataSource = new kendo.data.DataSource({
		serverPaging: true,
		serverFiltering: true,
		serverSorting: true,
		pageSize: 40,
		transport: {
			read: {
				url: $init.data.resource_url+'/browse'
			}
		},
		schema: {
			data: function (result) {
				$scope.mainListOpt.nextPage = $scope.mainListDataSource.page() + 1;
				var n = result['data'].length;
				for(var i = 0; i < n; i++) {
					$scope.mainListData.push(result['data'][i]);
				}
				$scope.mainListOpt.wait = false;
				$scope.$apply();
				return result.data;
			},
			total: function (result) {
				if(result.total)
					$scope.mainListOpt.total = result.total;

				return $scope.mainListOpt.total;
			}
		}
	});
	
	// read data from the remote service
	$scope.mainListDataSource.read();


	$('#mainSearchBox').keypress(function (e) {
		if(e.which == 13) {
			$scope.searchMainList();
		}
	});

	$scope.searchMainList = function(clear)
	{
		var searchBox = $('#mainSearchBox');
		if(clear===true)
		{
			searchBox.val('');
			$scope.mainListOpt['sort'] = [];
		}

		var term = searchBox.val();
		$scope.mainListOpt.nextPage = 1;
		if(term.trim()!="")
		{
			$scope.mainListOpt['filter'] = {
				logic:"or",
				filters:[
					{field: $scope.mainListCodeField, operator:"eq", value:term},   //search code
					{field: $scope.mainListNameField, operator:"contains", value:term}  //search name
				]
			};
		}
		else
		{
			$scope.mainListOpt['filter'] = null;
		}
		$scope.mainListData = [];
		$scope.mainListRequest();
	};

	$scope.sortMainList = function(fieldName)
	{
		$scope.mainListOpt.nextPage = 1;
		if(fieldName)
		{
			if($scope.mainListOpt['sort'].length==1 && $scope.mainListOpt['sort'][0]['field'] == fieldName)
			{
				if($scope.mainListOpt['sort'][0]['dir'] == "asc")
					$scope.mainListOpt['sort'][0]['dir'] = "desc";
				else
					$scope.mainListOpt['sort'][0]['dir'] = "asc";
			}
			else
			{
				$scope.mainListOpt['sort'] = [{
					field: fieldName,
		            dir: "asc"
				}];
			}
		}
		else
		{
			$scope.mainListOpt['sort'] = [];
		}

		$scope.mainListData = [];
		$scope.mainListRequest();
	};

	$scope.mainListLoadMore = function() {
		if($scope.mainListOpt.wait==false && $scope.mainListOpt.nextPage <= $scope.mainListDataSource.totalPages())
		{
			$scope.mainListRequest();
		}
	};

	$scope.mainListRequest = function()
	{
		$scope.mainListOpt.wait = true;
		var dsQuery = {
			page: $scope.mainListOpt.nextPage,
			pageSize: 40,
			filter: $scope.mainListOpt.filter,
			sort: $scope.mainListOpt.sort
		};

		$scope.mainListDataSource.query(dsQuery);
	};

	/********************************************************
	* GRID OPTIONS
	*******************************************************/
	function getGridHeight(innerWidth)
	{
		var mainContentViewHeight = $('#main-content-view').height() - 100;
		if(innerWidth <= 767)
			mainContentViewHeight += 155;
		return mainContentViewHeight;
	}

	$(window).on("resize", function(e)
	{
		$scope.grid.wrapper.height(getGridHeight(e.currentTarget.innerWidth));
		$scope.grid.resize(true);
	});

	$scope.filterInit = function (event) {
		var filterType = event.sender.dataSource.options.schema.model.fields[event.field].type;
		if (filterType == "date" || filterType == "number")
		{
			var container = event.container;
			var beginOperator = container.find("[data-role=dropdownlist]:eq(0)").data("kendoDropDownList");
			var endOperator = container.find("[data-role=dropdownlist]:eq(2)").data("kendoDropDownList");

			container.data("kendoPopup").bind("open", function (e)
			{
				if(filterType == "date")
				{
					beginOperator.value("lte");
					beginOperator.trigger("change");
			        endOperator.value("gte");
					endOperator.trigger("change");
				}
				else if(filterType == "number")
				{
					beginOperator.value("gte");
					beginOperator.trigger("change");
					endOperator.value("lte");
					endOperator.trigger("change");
				}
			});
		}
	};

	$scope.gridResultTotal = 0;
	$scope.gridOptions = {
		selectable: "row",
		change: onChange,
		scrollable: { virtual: true },
		mobile: true,
		resizable: true,
		sortable: {
            mode: "multiple",
            allowUnsort: true
        },
		reorderable: true,
		filterable: { mode: "menu, row" },
		autoBind: false,
		dataSource: {
			serverPaging: true,
			serverFiltering: true,
			serverSorting: true,
			pageSize: 40,
			transport: {
				read: {
					url: "/accounting/docstransactions/grid"
				}
			},
			schema: {
				data: function (result) {
					return result.data;
				},
				total: function (result) {
					if(result.total)
						$scope.gridResultTotal = result.total;

					return $scope.gridResultTotal;
				},
				model: {
			        fields: {
				        document_date: { type: "date" },
				        total_with_vat: {
					        type: "number",
					        parse: function (data) { return data; }
				        }
			        }
				}
			}
		},
		columns: [
			{field: 'id_documentgroup', title: 'Tipi i Dokumentit', width: '15%',
					template: function(item){ return getDocumentTypeLang(item.subject); },
					filterable: {
						ui: docTypeFilterFunc,
		                cell: {
			                showOperators: false,
			                template: function(args) {
				                docTypeFilterFunc(args.element);
			                }
		                }
		            }
			},
			{field: 'document_date', title: 'Data Krijimit', width: '15%', format: "{0:d}", parseFormat: "yyyy-MM-dd",
					filterable: {
						ui: dateFilterFunc,
						cell: {
							showOperators: false, operator: "lte", /*, enabled: false */
							template: function(args){
								dateFilterFunc(args.element);
							}
						}
					}

			},
			{field: 'description', title: 'Pershkrimi', width: '25%',
					filterable: { cell: { showOperators: false, operator: "contains", dataSource: {}} }
			},
			{field: 'document_number', title: 'Numer Dokumenti', width: '15%',
					filterable: { cell: { showOperators: false, operator: "contains", dataSource: {}} }
			},
			{field: 'currency', title: 'Monedha', width: '10%',
					filterable: {
						cell: {
							showOperators: false, operator: "eq", dataSource: {}
							/*,template: function(input){
                                //input.width(100);
                            }*/
						} }
			},
			{field: 'total_with_vat', title: 'Shuma', width: '20%', attributes: { style:"text-align:right;" },
					template: function(item) { return kendo.toString(parseFloat(item.total_with_vat), "n"); },
					filterable: { cell: { showOperators: false, operator: "gte"} }
			}
		]
	};

	function getDocumentTypeLang(docType){
		var documentTypeLabels = {  saleinvoiceheader: 'Fature Shitje',
							        purchaseinvoiceheader: 'Fatura Blerje'};

		return documentTypeLabels[docType];
	}


	function dateFilterFunc(element) {
		 element.kendoDatePicker({
			 parseFormats: Utils.dateParseFormats()
		 });
    }

	function docTypeFilterFunc(element) {
        element.kendoDropDownList({
            dataSource: new kendo.data.DataSource({ data: [
                { label: "Fature Shitje", data: 2 },
				{ label: "Mandat Arketimi", data: 3 },
				{ label: "Flete Dalje Magazine", data: 4 },
				{ label: "Urdher Shitje", data: 1 },
				{ label: "Oferta te Dhena", data: 1 },
				{ label: "Zbritje nga Shitjet", data: 1 },
				{ label: "Kthim Fature Blerje", data: 1 }
            ] }),
            optionLabel: "- Te Gjitha",
            dataTextField: "label",
            dataValueField: "data",
	        valuePrimitive: true,
	        height: 400
        });

		//set list width
		element.data("kendoDropDownList").list.width(200);
    }

	function onChange(arg) {
		var data = this.dataItem(this.select());
		$state.go('core/currenciesEdit', {id: data.id});
	}


}])

.controller('warehousesFormCTRL', ['$scope', '$http', '$init', function($scope, $http, $init){

	
	$scope.units=$init.data.units;

	$scope.model = $init.data.model;
	if(!$init.data.model)
	{
		$scope.model = {active: 1};
    }

	$scope.save = function(type)
	{
		if ($scope.validator.validate())
		{
			var postData = $scope.model;
			postData.returnModel = (type=="keep");

			$http.post($init.data.resource_url, postData).success(function()
			{
				if(type == 'close')
					$scope.closeModal();
			});
		}
	}
}])

///////////////
;//end module//
///////////////

