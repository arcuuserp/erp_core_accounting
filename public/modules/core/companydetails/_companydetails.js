//Module for users management
angular.module('core.companydetails', [])

.controller('companydetailsFormCTRL', ['$scope', '$http', '$init','$state', function($scope, $http, $init,$state){

	$scope.industrycategory = $init.data.industrycategory;
	$scope.taxpayerstatus = $init.data.taxpayerstatus;
	$scope.organisationforms = $init.data.organisationforms;

	$scope.model = $init.data.model;
	if(!$init.data.model)
	{
		$scope.model = {active: 1, id_currency: 2};
    }

	$scope.save = function()
	{
		if ($scope.validator.validate())
		{
			$http.post($init.data.resource_url, $scope.model).success(function()
			{
				$state.reload();
			});
		}
	};

	$scope.refresh = function()
	{
		$state.reload();
	};

}])	

///////////////
;//end module//
///////////////

