//Module for users management
angular.module('core.taxes', [])

.controller('taxesCTRL', ['$scope', '$http', '$init', '$state', function($scope, $http, $init, $state) {

	$(window).on("resize", function() {
		kendo.resize($(".k-grid-content"));
	});

	function getGridHeight(innerWidth)
	{
		var mainContentViewHeight = $('#main-content-view').height() - 85;
		if(innerWidth <= 767)
		{
			mainContentViewHeight += 55;
		}

		return mainContentViewHeight;
	}

	$(window).on("resize", function(e)
	{
		$scope.grid.wrapper.height(getGridHeight(e.currentTarget.innerWidth));
		$scope.grid.resize(true);
	});

	$scope.gridResultTotal = 0;
		$scope.gridOptions = {
		columns: [
			{field: 'name', title: 'Emri', width: '30%',
					filterable: { cell: { showOperators: false, operator: "contains"} }
				    //template: function(item){ return "<a ui-sref='core/currenciesEdit({id:"+ item.id +"})'>"+item.name+"</a>"; }
			},
			{field: 'percentage', title: 'Tipi', width: '20%',
					template: function(item){ return (item.percentage==1 ? "<b>%</b>":"<i>Fikse</i>"); },
					filterable: {
						ui: statusFilterFunc2,
		                cell: {
			                showOperators: false,
			                template: function(args) {
				                statusFilterFunc2(args.element);
			                }
		                    /*dataSource: new kendo.data.DataSource({ data: [
		                        { label: "Aktive", data: 1 },
		                        { label: "Jo Aktive", data: 0 }
		                    ] }),
			                dataTextField: "data"*/
		                }
		            }
				    //template: kendo.template('#= active==1 ? "Aktive" : "Jo Aktive" #')
			},
			{field: 'tax_rate', title: 'Vlera', width: '30%',
					filterable: { cell: { showOperators: false, operator: "contains"} }
			},
			{field: 'active', title: 'Status', width: '20%',
					template: function(item){ return (item.active==1 ? "<b>Aktive</b>":"<i>Jo Aktive</i>"); },
					filterable: {
						ui: statusFilterFunc,
		                cell: {
			                showOperators: false,
			                template: function(args) {
				                statusFilterFunc(args.element);
			                }
		                    /*dataSource: new kendo.data.DataSource({ data: [
		                        { label: "Aktive", data: 1 },
		                        { label: "Jo Aktive", data: 0 }
		                    ] }),
			                dataTextField: "data"*/
		                }
		            }
				    //template: kendo.template('#= active==1 ? "Aktive" : "Jo Aktive" #')
			}
		],
		dataSource: {
			schema: {
				data: function (result) {
					return result.data;
				},
				total: function (result) {
					if(result.total)
						$scope.gridResultTotal = result.total;

					return $scope.gridResultTotal;
				},
				model: {
			        fields: {
				        updated_at: { type: "date" }
			        }
				}
			},
			pageSize: 30,
			serverPaging: true,
			serverFiltering: true,
			serverSorting: true,
			transport: {
				read: {
					url: "/core/taxes/grid",
					data: {testParam: 'test value'}
				}
			}
		},
		/*toolbar: [
			{ name: "create" },
			{ name: "save" },
			{ name: "cancel" }
		],
		toolbar: ["excel"],
        excel: {
            allPages: true
        },*/
		//toolbar: kendo.template($("#template").html()),
		//pageable: true,
		scrollable: {
			virtual: true
		},
		mobile: true,
		resizable: true,
		sortable: {
            mode: "multiple",
            allowUnsort: true
        },
		reorderable: true,
		filterable: {
            mode: "menu, row"
        },
		selectable: "row",
		change: onChange//,
		//dataBound: onDataBound,
		//dataBinding: onDataBinding
	};

	function statusFilterFunc(element) {
        element.kendoDropDownList({
            dataSource: new kendo.data.DataSource({ data: [
                { label: "Aktive", data: 1 },
                { label: "Jo Aktive", data: 0 }
            ] }),
            dataTextField: "label",
            dataValueField: "data",
	        valuePrimitive: true,
            optionLabel: "-- Te Gjitha --"
        });
    }

    	function statusFilterFunc2(element) {
        element.kendoDropDownList({
            dataSource: new kendo.data.DataSource({ data: [
                { label: "%", data: 1 },
                { label: "Fikse", data: 0 }
            ] }),
            dataTextField: "label",
            dataValueField: "data",
	        valuePrimitive: true,
            optionLabel: "-- Te Gjitha --"
        });
    }

	function onChange(arg) {
		var data = this.dataItem(this.select());
		//var data2 = this.dataSource.data();
		//console.log(data2);
		$state.go('core/taxesEdit', {id: data.id});
	}

	/*$scope.$on("kendoWidgetCreated", function(event, widget){
		if (widget === $scope.grid) {
			$scope.grid.element.on('dblclick', function (e) {
				console.log(e)
			});
		}
	});*/


	$scope.$on("kendoWidgetCreated", function(event, widget){

		if (widget === $scope.grid)
		{
			$scope.grid.wrapper.height(getGridHeight( $(window).width() ));
			$scope.grid.resize(true);
			$("#grid tr.k-alt").removeClass("k-alt");
			$scope.grid.element.on('dblclick', function (e) {
				console.log(e)
			});
			/*
			var dropDown = $scope.grid.element.find("#category").kendoDropDownList({
		        dataTextField: "CategoryName",
		        dataValueField: "CategoryID",
		        autoBind: false,
		        optionLabel: "All",
		        dataSource: {
		            type: "odata",
		            severFiltering: true,
		            transport: {
		                read: "http://demos.telerik.com/kendo-ui/service/Northwind.svc/Categories"
		            }
		        },
		        change: function() {
		            var value = this.value();
		            if (value) {
		                $scope.grid.dataSource.filter({ field: "CategoryID", operator: "eq", value: parseInt(value) });
		            } else {
		                $scope.grid.dataSource.filter({});
		            }
		        }
		    });*/
		}
	});

}])

.controller('taxesFormCTRL', ['$scope', '$http', '$init', function($scope, $http, $init){

	$scope.model = $init.data.model;
	if(!$init.data.model)
	{
		$scope.model = {active: 1,percentage: 1};
    }

	$scope.save = function(type)
	{
		if ($scope.validator.validate())
		{
			var postData = $scope.model;
			postData.returnModel = (type=="keep");

			$http.post($init.data.resource_url, postData).success(function()
			{
				if(type == 'close')
					$scope.closeModal();
			});
		}
	}

	$scope.delete = function()
	{
		$http.delete($init.data.resource_url+'/'+$scope.model.id).success(function()
		{
			$scope.closeModal();
		});
	};

}])

///////////////
;//end module//
///////////////

