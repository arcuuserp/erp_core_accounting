//Module for users management
angular.module('core.access', [])

.controller('LoginCtrl', ['$scope','$http','$rootScope','$sanitize','$location',
function($scope, $http, $rootScope, $sanitize, $location){

    //temp to be removed
    //$scope.email = "admin@mail.com";
    $scope.username = "admin";
    $scope.password = "test";

	$http.get('login-check').success($scope.onSuccess);

    $scope.login = function(){
	    var postData = {
		    //'email': $sanitize($scope.email),
		    'username': $sanitize($scope.username),
		    'password': $sanitize($scope.password),
		    'remember': $sanitize($scope.remember)
	    };
	    $http.post('login', postData).success($scope.onSuccess);
    }

	$scope.onSuccess = function(data)
	{
		sessionStorage.firstname = data.firstname;
		$rootScope.currentUser = function() {
			return sessionStorage.firstname;
		};
		$location.path('/home');
		Flash.clear();
		sessionStorage.authenticated = true;
	}
}])

.controller('LogoutCtrl',function($http, $location, Flash) {
	$http.get('logout').success(function(response)
		{
			delete sessionStorage.authenticated;
			delete sessionStorage.firstname;
			Flash.show(response.flash);
			$location.path('/');
		}
	);
})

.controller('UsersListCtrl', ['$scope', 'result', function($scope, result) {

	$scope.users = result.data;

}])

.controller('UsersEditCtrl', ['$scope', '$http', '$location', 'result', 'dialogs', function($scope, $http, $location, result, dialogs){
	
	$scope.roles = result.data.rolesData;
    if(result.data['userData'] != null)
    {
        $scope.user = result.data.userData;
        var rolesLength = result.data.rolesData.length;
        for(var i=0; i<rolesLength; i++)
        {
	        if(result.data.userData.id_role == result.data.rolesData[i].id)
	        {
		        $scope.selected_role = result.data.rolesData[i];
	        }
        }
    }
    else
    {
        $scope.selected_role = result.data.rolesData[0];
    }

    $scope.deleteRecord = function(user)
    {
        var dlg = dialogs.confirm('Confirm Deletion', 'Are you sure you want to delete this user?', {size: 'md'});
        dlg.result.then(function()
        {
            $http.post('delete-user', {id:$scope.user.id}).success(function()
                {
                    $location.path('/users');
                }
            );
        });
    }

    $scope.saveUser = function(user)
    {
        //console.log(user);
	    user['id_role'] = $scope.selected_role.id;
        $http.post('save-user', user).success(function()
            {
	            $location.path('/users');
            }
        );
    }

	$scope.changePassword = function()
	{
		var dlg = dialogs.create('partials/dialogs/newPasswordAuthPopup.html', 'PasswordAuthPopupCtrl', {pwdTypeLabel: 'New'}, {
			size: 'md',
			keyboard: true,
			backdrop: true,
			windowClass: 'popup'
		});

		dlg.result.then(
			function (password) {
				var postData = {new_password: password, user_id: $scope.user.id};
				console.log(postData);
				$http.post('change-password', postData).success(function()
					{
						//alert(data.msg);
						$location.path('/users');
					}
				);
			});
	}


}])

.controller('EditUserProfileCtrl', ['$scope', '$http', '$location', 'result', 'dialogs', function($scope, $http, $location, result, dialogs){

	$scope.user = result.data;

	$scope.changePassword = function(user)
	{
		var dlg = dialogs.create('partials/dialogs/newPasswordAuthPopup.html', 'PasswordAuthPopupCtrl', {pwdTypeLabel: 'New'}, {
			size: 'md',
			keyboard: true,
			backdrop: true,
			windowClass: 'popup'
		});

		dlg.result.then(
			function (password) {
				var postData = {new_password: password, user_id: $scope.user.id};
				console.log(postData);
				$http.post('change-password', postData).success(function()
					{
						//alert(data.msg);
						$location.path('/logout');
					}
				);
			});
	}

}])