//Module for users management
angular.module('core.unit', [])

.controller('unitCTRL', ['$scope', '$http', '$init', '$state', function($scope, $http, $init, $state) {

}])

.controller('unitCreateCTRL', ['$scope', '$http', '$init', function($scope, $http, $init){

	$scope.model = {active: 1};

	$scope.save = function(type)
	{
		if ($scope.validator.validate())
		{
			var postData = $scope.model;
			postData.returnModel = (type=="keep");

			$http.post($init.data.resource_url, postData).success(function()
			{
				if(type == 'close')
					$scope.closeModal();
			});
		}
	}
}])

.controller('unitEditCTRL', ['$scope', '$http', '$init', function($scope, $http, $init){

	$scope.model = $init.data.model;

	$scope.save = function(type)
	{
		if ($scope.validator.validate())
		{
			var postData = $scope.model;
			postData.returnModel = (type=="keep");

			$http.post($init.data.resource_url, postData).success(function()
			{
				if(type == 'close')
					$scope.closeModal();
			});
		}
	}
}])

.controller('unitShowCTRL', ['$scope', '$ocModal', '$rootScope', '$state', function($scope, $ocModal, $rootScope, $state) {

	$scope.validate = function(event) {
		event.preventDefault();

		if ($scope.validator.validate()) {
			$scope.validationMessage = "Hooray! Your tickets has been booked!";
			$scope.validationClass = "valid";
		} else {
			$scope.validationMessage = "Oops! There is invalid data in the form.";
			$scope.validationClass = "invalid";
		}
	};

	$scope.close = function()
	{
		//$state.go($state.previous.state.name, $state.previous.params);
		//$rootScope.closePoup();
		$ocModal.closeSelf();
	};

}])

///////////////
;//end module//
///////////////

