//Module for users management
angular.module('core.settings', [])


.controller('settingsEditCTRL', ['$scope', '$http', '$init', function($scope, $http, $init){

	$scope.model = $init.data;

	$scope.save = function(type)
	{
		if ($scope.validator.validate())
		{
			$http.post($init.data.resource_url, $scope.model).success(function()
			{
				$scope.closeModal();
			});
		}
	}
}])

.controller('settingsGeneralsCTRL', ['$scope', '$http', '$state', '$init', function($scope, $http, $state, $init) {

	$scope.model = {doc_date_print: ($init.data.doc_date_print == "1"),
					doc_time_print: ($init.data.doc_time_print == "1"),
					save_doc_before_print: ($init.data.save_doc_before_print == "1")};
	$scope.save = function()
	{
		$scope.model.doc_date_print = ($scope.model.doc_date_print)?1:0;
		$scope.model.doc_time_print = ($scope.model.doc_time_print)?1:0;
		$scope.model.save_doc_before_print = ($scope.model.save_doc_before_print)?1:0;

		$http.post($init.data.resource_url, $scope.model).success(function()
		{
			$state.reload();
		});
	};

	$scope.refresh = function()
	{
		$state.reload();
	};

}])

.controller('settingsAccountingCTRL', ['$scope', '$http', '$state', '$init', '$translate', function($scope, $http, $state, $init, $translate) {
	
	$scope.model = $init.data;
	var fiscalYear = $scope.model.fiscal_year_start_date.split('-');
	$scope.model.fiscal_year_start_day = fiscalYear[0];
	$scope.model.fiscal_year_start_month = fiscalYear[1];

	$translate(['MONTHS.JANUARY','MONTHS.FEBRUARY','MONTHS.MARCH','MONTHS.APRIL','MONTHS.MAY','MONTHS.JUNE',
				'MONTHS.JULY','MONTHS.AUGUST','MONTHS.SEPTEMBER','MONTHS.OCTOBER','MONTHS.NOVEMBER','MONTHS.DECEMBER'
				]).then(function (t) 
	{
		$scope.months = [{id:'01', name: t['MONTHS.JANUARY'], maxDays: 31},
						{id: '02', name: t['MONTHS.FEBRUARY'], maxDays: 28},
						{id: '03', name: t['MONTHS.MARCH'], maxDays: 31},
						{id: '04', name: t['MONTHS.APRIL'], maxDays: 30},
						{id: '05', name: t['MONTHS.MAY'], maxDays: 31},
						{id: '06', name: t['MONTHS.JUNE'], maxDays: 30},
						{id: '07', name: t['MONTHS.JULY'], maxDays: 31},
						{id: '08', name: t['MONTHS.AUGUST'], maxDays: 31},
						{id: '09', name: t['MONTHS.SEPTEMBER'], maxDays: 30},
						{id: '10', name: t['MONTHS.OCTOBER'], maxDays: 31},
						{id: '11', name: t['MONTHS.NOVEMBER'], maxDays: 30},
						{id: '12', name: t['MONTHS.DECEMBER'], maxDays: 31}];					
	});


	$scope.save = function()
	{	
		if ($scope.validator.validate())
		{
			var maxDays = $scope.fiscalMonthDropdown.dataItem().maxDays;
			if(parseInt($scope.model.fiscal_year_start_day) > maxDays)
			{
				$scope.model.fiscal_year_start_day = maxDays;
			}

			$scope.model.fiscal_year_start_date = $scope.model.fiscal_year_start_day+'-'+$scope.model.fiscal_year_start_month;
			console.log($scope.model);

			$http.post($init.data.resource_url, $scope.model).success(function()
			{
				$state.reload();
			});
		}
	};

	$scope.refresh = function()
	{
		$state.reload();
	};

}])

.controller('settingsProfitlossCTRL', ['$scope', '$http', '$state', '$init', function($scope, $http, $state, $init) {

	$scope.model = $init.data;

	$scope.save = function()
	{
		if ($scope.validator.validate())
		{
			$http.post($init.data.resource_url, $scope.model).success(function()
			{
				$state.reload();
			});
		}
	};

	$scope.refresh = function()
	{
		$state.reload();
	};

}])

.controller('settingsSuppliersCTRL', ['$scope', '$http', '$state', '$init', function($scope, $http, $state, $init) {

	$scope.model = $init.data;
	$scope.model.alert_recipts_to_be_paid = ($init.data.alert_recipts_to_be_paid == "1");

	$scope.save = function()
	{

		if ($scope.validator.validate())
		{
			$scope.model.alert_recipts_to_be_paid = ($scope.model.alert_recipts_to_be_paid)?1:0;
			$http.post($init.data.resource_url, $scope.model).success(function()
			{
				$state.reload();
			});
		}
	};

	$scope.refresh = function()
	{
		$state.reload();
	};

}])
.controller('settingsInitialcreationCTRL', ['$scope', '$http', '$state', '$init', function($scope, $http, $state, $init) {

	$scope.model = $init.data;

	$scope.save = function()
	{
		if ($scope.validator.validate())
		{
			$http.post($init.data.resource_url, $scope.model).success(function()
			{
				$state.reload();
			});
		}
	};

	$scope.refresh = function()
	{
		$state.reload();
	};


}])
.controller('settingsAssetsCTRL', ['$scope', '$http', '$state', '$init', function($scope, $http, $state, $init) {

	$scope.model = $init.data;

	$scope.save = function()
	{
		if ($scope.validator.validate())
		{
			$http.post($init.data.resource_url, $scope.model).success(function()
			{
				$state.reload();
			});
		}
	};

	$scope.refresh = function()
	{
		$state.reload();
	};

}])
.controller('settingsCustomersCTRL', ['$scope', '$http', '$state', '$init', function($scope, $http, $state, $init) {

	$scope.model = $init.data;
	$scope.model.sale_maturation_days = ($init.data.sale_maturation_days == "1");

	$scope.save = function()
	{

		if ($scope.validator.validate())
		{
			$scope.model.sale_maturation_days = ($scope.model.sale_maturation_days)?1:0;
			$http.post($init.data.resource_url, $scope.model).success(function()
			{
				$state.reload();
			});
		}
	};

	$scope.refresh = function()
	{
		$state.reload();
	};

}])
.controller('settingsTaxesCTRL', ['$scope', '$http', '$state', '$init', function($scope, $http, $state, $init) {

	$scope.model = $init.data;

	$scope.save = function()
	{
		if ($scope.validator.validate())
		{
			$http.post($init.data.resource_url, $scope.model).success(function()
			{
				$state.reload();
			});
		}
	};

	$scope.refresh = function()
	{
		$state.reload();
	};

}])

.controller('settingsSecurityCTRL', ['$scope', '$http', '$state', '$init', function($scope, $http, $state, $init) {

	$scope.model = $init.data;
	$scope.model.password_reset_obligatory = ($init.data.password_reset_obligatory == "1");
	$scope.model.different_new_password = ($init.data.different_new_password == "1");

	$scope.save = function()
	{

		if ($scope.validator.validate())
		{
			$scope.model.password_reset_obligatory = ($scope.model.password_reset_obligatory)?1:0;
			$scope.model.different_new_password = ($scope.model.different_new_password)?1:0;
			$http.post($init.data.resource_url, $scope.model).success(function()
			{
				$state.reload();
			});
		}
	};

	$scope.refresh = function()
	{
		$state.reload();
	};

}])

.controller('settingsWarehousesCTRL', ['$scope', '$http', '$state', '$init', function($scope, $http, $state, $init) {

	$scope.model = $init.data;
	$scope.save = function()
	{

		if ($scope.validator.validate())
		{
			$http.post($init.data.resource_url, $scope.model).success(function()
			{
				//$state.reload();
			});
		}
	};

	$scope.refresh = function()
	{
		$state.reload();
	};

}])
///////////////
;//end module//
///////////////
