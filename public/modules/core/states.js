
erpApp.config(['$stateProvider','$locationProvider','$urlRouterProvider','$ocLazyLoadProvider','StateConfigProvider',
	function (  $sp , $locationProvider , $urlRouterProvider , $ocLazyLoadProvider , StateConfigProvider ) {

		StateConfigProvider.preconfig.set({statesReference: $sp, module:'core', debug: false});//, override:'deka'});
		var route = StateConfigProvider.stateRoutes;

		$sp
	        .state("home", {
				url: "/",
				templateUrl: "modules/core/home.html",
				controller: "HomeCtrl"
			})
			.state("main", {
				url: "/main",
				templateUrl: "modules/core/home.html",
				controller: "HomeCtrl"
			})
			.state('login', {
				url: "/login",
	            templateUrl: "modules/core/access/login.html",
	            controller: "LoginCtrl",
	            resolve: {
	                store: function ($ocLazyLoad) {
	                    return $ocLazyLoad.load(
	                        {
	                            name: "user",
	                            files: ["user/user.js"]
	                        }
	                    )
	                }
	            }
	        })
			.state('logout', {
				url: "/logout",
	            templateUrl: "modules/core/access/logout.html",
	            controller: "LogoutCtrl",
	            resolve: {
	                store: function ($ocLazyLoad) {
	                    return $ocLazyLoad.load(
	                        {
	                            name: "core.access",
	                            files: ["modules/core/access/_access.js"]
	                        }
	                    )
	                }
	            }
	        })
			.state('user', {
	            templateUrl: "user/user.html",
	            controller: "UserCtrl",
	            resolve: {
	                store: function ($ocLazyLoad) {
	                    return $ocLazyLoad.load(
	                        {
	                            name: "user",
	                            files: ["user/user.js"]
	                        }
	                    )
	                }
	            }
	        })
		    .state(route.get('companydetails', 'create'))
	        .state('core/settingsGenerals', route.get('settings', 'show', 'code', null, null, null, 'modules/core/settings/generalsForm.html', 'settingsGeneralsCTRL', '/core/settings/11/{code}'))
	        .state('core/settingsAccounting', route.get('settings', 'show', 'code', null, null, null, 'modules/core/settings/accountingForm.html', 'settingsAccountingCTRL', '/core/settings/12/{code}'))
	        .state('core/settingsProfitloss', route.get('settings', 'show', 'code', null, null, null, 'modules/core/settings/profitlossForm.html', 'settingsProfitlossCTRL', '/core/settings/13/{code}'))
	        .state('core/settingsInitialcreation', route.get('settings', 'show', 'code', null, null, null, 'modules/core/settings/initialcreationForm.html', 'settingsInitialcreationCTRL', '/core/settings/14/{code}'))
	        .state('core/settingsSuppliers', route.get('settings', 'show', 'code', null, null, null, 'modules/core/settings/suppliersForm.html', 'settingsSuppliersCTRL', '/core/settings/15/{code}'))
	        .state('core/settingsAssets', route.get('settings', 'show', 'code', null, null, null, 'modules/core/settings/assetsForm.html', 'settingsAssetsCTRL', '/core/settings/16/{code}'))
	        .state('core/settingsCustomers', route.get('settings', 'show', 'code', null, null, null, 'modules/core/settings/customersForm.html', 'settingsCustomersCTRL', '/core/settings/17/{code}'))
	        .state('core/settingsTaxes', route.get('settings', 'show', 'code', null, null, null, 'modules/core/settings/taxesForm.html', 'settingsTaxesCTRL', '/core/settings/18/{code}'))
	        .state('core/settingsSecurity', route.get('settings', 'show', 'code', null, null, null, 'modules/core/settings/securityForm.html', 'settingsSecurityCTRL', '/core/settings/19/{code}'))
	        .state('core/settingsWarehouses', route.get('settings', 'show', 'code', null, null, null, 'modules/core/settings/warehousesForm.html', 'settingsWarehousesCTRL', '/core/settings/20/{code}'))
	        
		
		;//END of stateProvider

		//RESOURCES PLACED AFTER THE INDIVIDUAL STATE DECLARATIONS
		route.resource('currencies');
		route.resource('roles');
		route.resource('reports');
		route.resource('users');
		route.resource('industrycategories');
		route.resource('unit');
		route.resource('taxes');
		
	    // Without server side support html5 must be disabled.
	    //$locationProvider.html5Mode(false);

	    // We configure ocLazyLoad to use the lib script.js as the async loader
	    $ocLazyLoadProvider.config({
	      debug: false,
	      events: true
	      /*,modules: [{
	        name: 'gridModule',
	        files: [
	          'js/gridModule.js'
	        ]
	      }]*/
	    });


		/////////////////////////////
		// Redirects and Otherwise //
		/////////////////////////////

		// Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
		// The `when` method says if the url is ever the 1st param, then redirect to the 2nd param
		// Here we are just setting up some convenience urls.
		$urlRouterProvider
			//.when('/c?id', '/contacts/:id')
			//.when('/user/:id', '/contacts/:id')
			// If the url is ever invalid, e.g. '/asdf', then redirect to '/' aka the home state
			.otherwise('/');

		//$locationProvider.hashPrefix('('+sessionStorage.user_name+')');
		//$urlMatcherFactoryProvider.strictMode(false)
		//$locationProvider.html5Mode(true);
}]);
