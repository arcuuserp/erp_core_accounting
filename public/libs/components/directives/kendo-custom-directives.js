var aceDirectives = angular.module('ace.directives.custom.kendo', []);


aceDirectives.directive("aceDatePicker", ['$parse', function($parse){
    return {
	    restrict: 'A',
		replace: false,         // replace original markup with template
		transclude: false,      // do not copy original HTML content
	    scope: false,			//false=parents scope OR true=isolated scope
        link: function (scope, element, attributes) {

	        var options = {
		        parseFormats: Utils.dateParseFormats(),
                change: function () {
	                var selectedValue = this.value();
                    scope.$apply(function () {
	                    if(attributes.aceModel)
	                    {
							var model = $parse(attributes.aceModel);
		                    model.assign(scope, Date.convertToMysql(selectedValue));
	                    }
                    });
                }
            };

			if(attributes.aceFormat)
				options['format'] = attributes.aceFormat;

			if(attributes.aceParseFormat)
				options['parseFormats'] = [attributes.aceParseFormat];

			if(attributes.aceMax)
				options['max'] = Date.createFromMysql(attributes.aceMax);
			if(attributes.aceMin)
				options['min'] = Date.createFromMysql(attributes.aceMin);

	        var defaultTodaysDate;
			if(!attributes.showToday || (attributes.showToday && attributes.showToday !== 'false'))
			{
				options['value'] = defaultTodaysDate = new Date();
			}

	        var widget = element.kendoDatePicker(options).data("kendoDatePicker");

	        if(attributes.aceModel)
	        {
				scope.$watch(attributes.aceModel, function(variable) {
					if(variable)
					{
						widget.value(Date.createFromMysql(variable));
					}
					else if(defaultTodaysDate != null)
					{
						var model = $parse(attributes.aceModel);
		                model.assign(scope, Date.convertToMysql(defaultTodaysDate));
					}
				});
	        }
	        if(attributes.ngDisabled)
	        {
				scope.$watch(attributes.ngDisabled, function(variable) {
					if(variable === true || variable === false)
						widget.enable(!variable);
				});
	        }
	        //directive name added to scope
	        if(attributes.aceDatePicker)
	            scope[attributes.aceDatePicker] = widget;
        }
    };
}]);


aceDirectives.directive("aceDropDownList", ['$parse', '$aceStorage', function($parse, $aceStorage){
    return {
	    restrict: 'A',          //E=element C=class A=attribute
		replace: false,         // replace original markup with template
		transclude: false,      // do not copy original HTML content
	    scope: false,			//false=parents scope OR true=isolated scope
		/*{              		// set up directive's isolated scope
			getValue: "@",		// name var passed by value (string, one-way)
			bindVar:  "=",		// amount var passed by reference (two-way)
			callFunc: "&"		// save action
		},*/
        link: function (scope, element, attributes) {

			if(attributes.onChange)
                var changeHandler = $parse(attributes.onChange);

	        var dataValueField = attributes.aceDataValueField ? attributes.aceDataValueField : "id";
	        var dataTextField = attributes.aceDataTextField ? attributes.aceDataTextField : "name";
	        var options = {
		        delay: 0,
		        //optionLabel: "Select Name",
                dataValueField: dataValueField,
                dataTextField: dataTextField,
                change: function (e) {

                    var selectedItem = this.dataItem();
                    scope.$apply(function () {
	                    if(attributes.aceModel)
	                    {
		                    var selectedValue = (selectedItem && selectedItem[dataValueField])?selectedItem[dataValueField]:null;
							var model = $parse(attributes.aceModel);
		                    model.assign(scope, selectedValue);
	                    }/*
	                    if(attributes.aceDataItem)
	                    {
		                    var model = $parse(attributes.aceDataItem);
		                    model.assign(scope, selectedItem);
	                    }*/
                    });

					if(attributes.onChange)
					{
		                changeHandler(scope, {event: e});
						scope.$apply();
					}
                }
            };
	        if(attributes.aceLoadUrl)
	        {
		        options['autoBind'] = false;
		        if(attributes.aceCacheData && attributes.aceCacheData=='false')
		        {
			        options['dataSource'] = {
	                            transport: {
	                                read: {
	                                    url: attributes.aceLoadUrl
	                                },
		                            cache: true
	                            },
								schema: {
									data: function (result) {
										return result.data;
									}
								}
	                        };

			        if(attributes.aceServerFiltering && attributes.aceServerFiltering=='true')
			        {
			            options['filter'] = "contains";
				        options['dataSource']['serverFiltering'] = true;
		                options['delay'] = 707;
		                options['minLength'] = 2;
			        }
		        }
				else if(!attributes.aceCacheVersion)
		        {
					options['dataSource'] = $aceStorage.kendoDataSource(attributes.aceLoadUrl);
			    }
	        }

			if(attributes.aceAutoBind)
				options['autoBind'] = (attributes.aceAutoBind=='true');
			if(attributes.aceFilter)
	            options['filter'] = attributes.aceFilter;
	        if(attributes.aceMinLength)
	            options['minLength'] = attributes.aceMinLength;
	        if(attributes.aceSelectedIndex)
	            options['index'] = attributes.aceSelectedIndex;
			if(attributes.aceTemplate)
			{
				options['template'] = attributes.aceTemplate;
				options['valueTemplate'] = attributes.aceTemplate;
			}

	        var widget = element.kendoDropDownList(options).data("kendoDropDownList");

	        if(attributes.aceDatasource)
	        {
				scope.$watch(attributes.aceDatasource, function(variable) {
					if(variable)
						widget.setDataSource(variable);
				});
	        }
	        if(attributes.aceCacheVersion)
	        {
				var unRegisterWatch = scope.$watch(attributes.aceCacheVersion, function(variable) {
					if(variable)
					{
	                    var ds = $aceStorage.kendoDataSource(attributes.aceLoadUrl, variable);
						widget.setDataSource(ds);
						unRegisterWatch();
					}
				});
	        }
	        if(attributes.aceModel)
	        {
				scope.$watch(attributes.aceModel, function(variable) {
					if(variable)
						widget.value(variable);
				});
	        }

	        if(attributes.ngDisabled)
	        {
				scope.$watch(attributes.ngDisabled, function(variable) {
					if(variable === true || variable === false)
						widget.enable(!variable);
				});
	        }

	        //directive name added to scope
	        if(attributes.aceDropDownList)
	            scope[attributes.aceDropDownList] = widget;
        }
    };
}]);


aceDirectives.directive("aceComboBox", ['$parse', '$aceStorage', function($parse, $aceStorage){
    return {
	    restrict: 'A',
		replace: false,
		transclude: false,
	    scope: false,
        link: function (scope, element, attributes) {

			if(attributes.onChange)
                var changeHandler = $parse(attributes.onChange);

	        var dataValueField = attributes.aceDataValueField ? attributes.aceDataValueField : "id";
	        var dataTextField = attributes.aceDataTextField ? attributes.aceDataTextField : "name";
	        var options = {
		        delay: 0,
		        //optionLabel: " ",
		        filter: 'contains',
		        suggest: true,
                dataValueField: dataValueField,
                dataTextField: dataTextField,
                change: function (e) {
                    var selectedItem = this.dataItem();
					//custom data not allowed					
					if (this.value() && this.select() === -1) {
						this.value("");
						selectedItem = null;
					}
					
                    scope.$apply(function () {
	                    if(attributes.aceModel)
	                    {
							var selectedValue = (selectedItem && selectedItem[dataValueField])?selectedItem[dataValueField]:null;
							var model = $parse(attributes.aceModel);
		                    model.assign(scope, selectedValue);
	                    }/*
	                    if(attributes.aceDataItem)
	                    {
		                    var model = $parse(attributes.aceDataItem);
		                    model.assign(scope, selectedItem);
	                    }*/
                    });

					if(attributes.onChange)
					{
		                changeHandler(scope, {event: e});
						scope.$apply();
					}
                }
            };

	        if(attributes.aceLoadUrl)
	        {
		        options['autoBind'] = false;
		        if(attributes.aceCacheData && attributes.aceCacheData=='false')
		        {
			        options['dataSource'] = {
	                            transport: {
	                                read: {
	                                    url: attributes.aceLoadUrl
	                                },
		                            cache: true
	                            },
								schema: {
									data: function (result) {
										return result.data;
									}
								}
	                        };

			        if(attributes.aceServerFiltering && attributes.aceServerFiltering=='true')
			        {
				        options['dataSource']['serverFiltering'] = true;
		                options['delay'] = 707;
		                options['minLength'] = 2;
			        }
		        }
				else if(!attributes.aceCacheVersion)
		        {
					options['dataSource'] = $aceStorage.kendoDataSource(attributes.aceLoadUrl);
			    }
	        }
			if(attributes.aceAutoBind)
				options['autoBind'] = (attributes.aceAutoBind=='true');
			if(attributes.aceFilter)
	            options['filter'] = attributes.aceFilter;
	        if(attributes.aceMinLength)
	            options['minLength'] = attributes.aceMinLength;
	        if(attributes.aceSelectedIndex)
	            options['index'] = attributes.aceSelectedIndex;
			if(attributes.aceTemplate)
				options['template'] = attributes.aceTemplate;

	        var widget;
	        if(attributes.aceMultiFilters)
			{
				if(attributes.aceMultiFilters=='code_name')
				{
					if(!attributes.aceDataTextField)
						options['dataTextField'] = 'code_name';

					//if(!attributes.aceTemplate)
					//	options['template'] = '#= data.code # - #= data.name #';

					options['multiFilterFields'] = [
				        { field: "code", operator: "startswith"},
				        { field: "name", operator: "contains"}
			        ];
				}
				else
				{
					//example:
					//ace-multi-filters="code:startswith, name:contains"

					var multiFilterFields = [];
					var fields = attributes.aceMultiFilters.split(',');
					for(var i=0; i < fields.length; i++)
			        {
				        var fieldParts =  fields[i].split(':');
			            multiFilterFields.push({ field: fieldParts[0].trim(), operator: fieldParts[1].trim()});
			        }
					options['multiFilterFields'] = multiFilterFields;
				}

		        widget = element.kendoMultiFilterComboBox(options).data("kendoMultiFilterComboBox");
			}
	        else
	        {
		        widget = element.kendoComboBox(options).data("kendoComboBox");
	        }

	        if(attributes.aceDatasource)
	        {
				scope.$watch(attributes.aceDatasource, function(variable) {
					if(variable)
						widget.setDataSource(variable);
				});
	        }
	        if(attributes.aceCacheVersion)
	        {
				var unRegisterWatch = scope.$watch(attributes.aceCacheVersion, function(variable) {
					if(variable)
					{
	                    var ds = $aceStorage.kendoDataSource(attributes.aceLoadUrl, variable);
						widget.setDataSource(ds);
						unRegisterWatch();
					}
				});
	        }
	        if(attributes.aceModel)
	        {
				scope.$watch(attributes.aceModel, function(variable) {
					if(variable)
						widget.value(variable);
				});
	        }

	        if(attributes.ngDisabled)
	        {
				scope.$watch(attributes.ngDisabled, function(variable) {
					if(variable === true || variable === false)
						widget.enable(!variable);
				});
	        }

	        //directive name added to scope
	        if(attributes.aceComboBox)
	            scope[attributes.aceComboBox] = widget;
        }
    };
}]);


aceDirectives.directive("aceOverlay", ['$parse', function($parse){
    return {
	    restrict: 'A',
		replace: false,         // replace original markup with template
		transclude: false,      // do not copy original HTML content
	    scope: false,			//false=parents scope OR true=isolated scope
        link: function (scope, element, attributes) {
	        if(!attributes.noOverlayClass || attributes.noOverlayClass == "false")
	            element.addClass('overlay-container');

	        if(attributes.aceOverlay)
	        {
				scope.$watch(attributes.aceOverlay, function(variable) {
					if(variable === true)
						element.find('.overlay').show();
					else
						element.find('.overlay').hide();
				});

		        var overlayDiv = angular.element('<div class="overlay"></div>');
				element.append(overlayDiv);//$compile(overlayDiv)(scope);
	        }
		}
    };
}]);


aceDirectives.directive('aceTooltipValidator', ['$location', function($location){
    return {
        restrict: 'A',//E=element C=class A=attribute

		scope: false,			//false=parents scope OR true=isolated scope
		/*{              		// set up directive's isolated scope
			getValue: "@",		// name var passed by value (string, one-way)
			bindVar:  "=",		// amount var passed by reference (two-way)
			callFunc: "&"		// save action
		},*/
		replace: false,         // replace original markup with template
		transclude: false,      // do not copy original HTML content

		//controller: [ "$scope", function ($scope) { …  }],

		compile: function(element, attributes)
		{
			var tooltip = element.kendoTooltip({
				filter: ".k-invalid",
				content: function(e) {
					var errorMessage = element.find("[data-for=" + e.target.attr("name") + "]");
					return '<span class="k-icon k-warning"> </span>' + errorMessage.text();
				},
				position: "right",
				show: function(e) {
					this.popup.element.addClass("form-validation-hint");
				}
			});
			
			var linkFunction = function(scope, element, attributes) {
				// bind element to data in $scope
					
				scope.validator = element.kendoValidator({
					 messages: {
						required: "Required",
						pattern: "{0} is not valid",
						min: "{0} should be greater than or equal to {1}",
						max: "{0} should be smaller than or equal to {1}",
						step: "{0} is not valid",
						email: "{0} is not valid email",
						url: "{0} is not valid URL",
						date: "{0} is not valid date"
						// overrides the built-in message for the email rule
						// with a custom function that returns the actual message
						/*,email: function(input) {
							 return getMessage(input);
						}*/
					},
					rules: {
						custom: function(input) {
							if (input.is("[name=username]")) {
								return input.val() === "Tom";
							}
							return true;
						}
					}
				}).data("kendoValidator");
			};

			return linkFunction;
		}
    }
}]);