
/**********************************************
*	GENERAL HELPER FUNCTIONS
**********************************************/
var Utils = Utils || {};

Utils.cultures = {'en-US': {DATE_PARSE_FORMATS: ["MMddyy", "MMdd"]},
				  'sq-AL': {DATE_PARSE_FORMATS: ["ddMMyy", "ddMM"]}};

(function (kendo) {

	Utils.dateParseFormats = function () {
		return Utils.cultures[kendo.culture().name]['DATE_PARSE_FORMATS'];
	};

	Utils.cultureDecimalReplace = function (value) {
		return value.toString().replace('.', kendo.culture().numberFormat['.']);
	};

	/**********************************************
	*	KENDO COMPONENTS EXTENSIONS
	**********************************************/
	kendo.ui.plugin(kendo.ui.ComboBox.extend({
	        options: {
		        name: "MultiFilterComboBox"
	        },
	        _filterSource: function() {
		        var that = this;
		        var fields = that.options['multiFilterFields'];
		        var filterObj = {logic: "or", filters: []};
		        for(var i=0; i < fields.length; i++)
		        {
		            filterObj.filters.push({ field: fields[i].field, operator: fields[i].operator, value: that.text() });
		        }

		        that.dataSource.filter(filterObj);
	        }
	    })
	);

})(window.kendo);


(function ($, angular, window) {

	Utils.isSet = function (value) {
		return (angular.isDefined(value) && value!=null && value != '');
	};

	Utils.jasperHostUrl = function (value) {
		return 'http://'+window.location.hostname+':8080/jasperserver';
	};

})(jQuery, angular, window);


Utils.currencyName = function(id, list)
{
	if(list)
	{
		var n = list.length;
		for(var i=0; i<n; i++)
		{
			if(list[i]['id'] == id)
				return list[i]['name'];
		}
	}

	return '';
};

Utils.redirectRequest = function(url, data, target)
{
	var postInput = ">";
	if(data != undefined)
	{
		postInput = ' method="post"> ';
		for (var key in data){
			postInput += '<input type="hidden" name="'+key+'" value="'+data[key]+'" />';
		}
	}

	var formTarget = '';//'target="_self"'
	if(target != undefined)
	{
		formTarget = 'target="'+target+'"';
	}

	var form = $('<form action="'+url+'" '+formTarget+' '+postInput+'</form>');
	$('body').append(form);
	$(form).submit();
};


/**********************************************
*	DATE CUSTOM HELPER FUNCTIONS 
**********************************************/
var Date = Date || {};

Date.createFromMysql = function(mysql_string, includeTime)
{
    if(typeof mysql_string === 'string')
    {
        var t = mysql_string.split(/[- :]/);

        //when t[3], t[4] and t[5] are missing they defaults to zero
        return new Date(t[0], t[1] - 1, t[2], t[3] || 0, t[4] || 0, t[5] || 0);
    }

    return null;
};

Date.convertToMysql = function(date, includeTime)
{
	var mysql_date = "";
    if(date)
    {
		mysql_date = date.getUTCFullYear() + '-' +
					('00' + (date.getUTCMonth()+1)).slice(-2) + '-' +
					('00' + date.getDate()).slice(-2);//not used getUTCDate because of the timezone

	    if(includeTime===true)
	    {
			mysql_date += ' '+('00' + date.getUTCHours()).slice(-2) + ':' +
							('00' + date.getUTCMinutes()).slice(-2) + ':' +
							('00' + date.getUTCSeconds()).slice(-2);
	    }
	    else
	        mysql_date += ' 00:00:00';
    }

    return mysql_date;
};


Date.mysqlDateToISO = function(mysqlDate)
{
    if(typeof mysqlDate === 'string')
    {
	    return mysqlDate.replace(/(.+) (.+)/, "$1T$2Z");
    }

    return null;
};

Date.convertMysqlDate = function(mysqlDate, includeTime)
{
    if(typeof mysqlDate === 'string')
    {
        var dt;
	    if(includeTime===true)
	    {
		    dt = mysqlDate.split(/[- :]/);
			return dt[2]+'/'+dt[1]+'/'+dt[0]+' '+(dt[3] || 0)+':'+(dt[4] || 0);//no seconds
	    }
	    else
	    {
		    dt = mysqlDate.split(/[- ]/)
	        return dt[2]+'/'+dt[1]+'/'+dt[0];
	    }
    }

    return "";
};


/**********************************************
*	ACCOUNTING HELPER FUNCTIONS 
**********************************************/
var AccHelpers = AccHelpers || {};

AccHelpers.docTypes = function(){
	
	return;
};
 