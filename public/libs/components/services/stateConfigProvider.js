//Module for services management
angular.module('ace.stateConfigProvider', [])
.provider('StateConfig', function () {

	this.$get = function() {
		return this;
	};

	this.preconfig = function () {
		var debug = false;
		var stateProviderReference = null;
		var module = 'core';
		var override = '';
		var rootPath = 'modules/';//path to modules or to override folder

		return {
			set: function (values) {
				if (angular.isDefined(values.module) && values.module != '')
					module = values.module;
				if (angular.isDefined(values.override) && values.override != '') {
					override = values.override;
					rootPath = 'overrides/' + values.override + '/';
				}
				if(angular.isDefined(values.statesReference))
					stateProviderReference = values.statesReference;

				debug = (values['debug'] === true);
			},
			getStateProvider: function () {
				return stateProviderReference;
			},
			getModule: function () {
				return module;
			},
			getRootPath: function () {
				return rootPath;
			},
			getOverride: function (extension) {
				if (override != "")
					return override + extension;
				return override;
			},
			debugMode: function () {
				return debug;
			}
		};
	}();

	/**
	 * Function Wrapper that holds the methods that generate state config objects
	 *
	 * @method stateFactory
	 * @param {Object} c Pre-Configuration object values
	 * @return {Object} Returns functions
	 */
	this.stateRoutes = function (c) {
		/**
		 * Creates a RESTful-like routes/states for one resource/model having multiple actions
		 * such as: index, show, create, edit, browse etc.
		 *
		 * @method resource
		 * @param {String} model                Resource Name of the Model
		 * @param {Array} customActions         A list of custom actions set manually
		 * @param {String} section              The Name of a section that might be different from the Model name
		 * @param {String} module               A module name that might be different from the one set in the preconfig object
		 * @return {Object}                     Returns functions
		 */
		var restfulResource = function (model, customActions, section, module) {
			var stateProvider = c.getStateProvider();
			if (stateProvider != null && angular.isDefined(stateProvider))
			{
				var actions = { 'index':  {params:null, size:null},
								'create': {params:null, size:'md'},
								'edit':   {params:'id', size:'md'}};//empty string means "index"

				if (typeof(customActions) === 'object' && !!customActions) {

					if(angular.isArray(customActions))
					{
						actions = {};
						var reportsObj = {};
						if(isSet(section) && section=='reports')
							reportsObj = {view: 'split_action', ctrl: 'no_controller', lazyload: 'no_lazyload'};
						for(var i=0; i<customActions.length; i++)
						{
							actions[customActions[i]] = reportsObj;
						}
					}
					else
					{
						for(var action in customActions){
					        if(actions[action]){
					            angular.extend(actions[action], customActions[action]);
					        }else{
					           actions[action] = customActions[action];//angular.extend({}, actions['index'], customActions[action]);
					        }
					    }
					}
				}

				angular.forEach(actions, function(value, action_key)
				{
					this.state(getAutoStateResolve(
						model,
						action_key,
						value['params'],
						(value['size']?{size: value['size']}:null),
						section,
						module,
						value['view'],
						value['ctrl'],
						null,
						value['lazyload']
					));
				}, stateProvider);

			} else
				console.error("No reference to the $stateProvider on StateFactory->restfulResource");
		};

		/**
		 * Creates a function that allows to override the automatically created state config data
		 * by providing the first parameter as an object with manual values
		 *
		 * @method custom
		 * @param {Object} manualValues An object with manual values to override the automatically created state config data
		 * @param {String} model            Resource Name of the Model
		 * @param {String} action           The action performed on an object
		 * @param {String} urlParam         Parameter name such as "id"
		 * @param {String|Object} popup     if string with the word "popup" creates a default popup else an object with popup settings
		 * @param {String} section          The Name of a section that might be different from the Model name
		 * @param {String} module           A module name that might be different from the one set in the preconfig object
		 * @param {String} templateUrl      An override of the one generated automatically
		 * @param {String} controller       An override of the one generated automatically
		 * @param {String} routeUrl         An override of the one generated automatically
		 * @param {Object} lazyLoadFiles    An override of the one generated automatically
		 * @param {Object} additionalResolve  An override or extension of the  "resolve" object including initial-data
		 * @return {Object}                   Returns a state object
		 */
		var manualStateResolve = function (manualValues, model, action, urlParam, popup, section, module, templateUrl, controller, routeUrl, lazyLoadFiles, additionalResolve) {
			var stateObj = getAutoStateResolve(model, action, urlParam, popup, section, module, templateUrl, controller, routeUrl, lazyLoadFiles, additionalResolve);

			if (typeof(manualValues) === 'object' && !!manualValues) {
				angular.extend(stateObj, manualValues);
			}
			else {
				console.error("The first parameter passed to StateConfigProvider is expected to be of type object");
			}

			return stateObj;
		};

		/**
		 * The main function the generates the state config object
		 *
		 * @method get
		 * @param {String} model            Resource Name of the Model
		 * @param {String} action           The action performed on an object
		 * @param {String} urlParams        Parameter name such as "id"
		 * @param {String|Object} popup     if string with the word "popup" creates a default popup else an object with popup settings
		 * @param {String} section          The Name of a section that might be different from the Model name
		 * @param {String} module           A module name that might be different from the one set in the preconfig object
		 * @param {String} templateUrl      An override of the one generated automatically
		 * @param {String} controller       An override of the one generated automatically
		 * @param {String} routeUrl         An override of the one generated automatically
		 * @param {Object} lazyLoadFiles    An override of the one generated automatically
		 * @param {Object} additionalResolve  An override or extension of the  "resolve" object including initial-data
		 * @return {Object}                   Returns a state object
		 */

		var getAutoStateResolve = function (model, action, urlParams, popup, section, module, templateUrl, controller, routeUrl, lazyLoadFiles, additionalResolve) {
			section = isSet(section) ? section+"/" : "";
			module = isSet(module) ? module : c.getModule();

			var sectionPath = c.getRootPath() + module + '/' + section + model;
			var actionType = configAction('client', module, model, action, urlParams, section);
			var angularModuleName = c.getOverride('.') + module + '.' + model;
			var stateObj = {};//return value

			//state name can be overridden by specifying it directly in the state declaration
			stateObj.name = module+'/'+section+model;

			if(isSet(action))
			{
				if(action == "index")
					action = '';

				if(templateUrl == 'split_action')//from the model name by a slash not camel notation
					stateObj.name += '/' + action;
				else
					stateObj.name += uppercaseFirst(action);//invoicesCreate
			}

			stateObj.url = isSet(routeUrl) ? routeUrl : '/'+actionType.url;
			if(isSet(templateUrl))
			{
				if(templateUrl == 'split_action')
					stateObj.templateUrl = sectionPath + '/' + action + '.html';
				else
					stateObj.templateUrl = templateUrl;
			}
			else
				stateObj.templateUrl = sectionPath + '/' + model + actionType.default + '.html';// accounting/invoices/invoicesForm.html

			if(isSet(controller))
			{
				if(controller != 'no_controller')
					stateObj.controller = controller;
			}
			else
				stateObj.controller = model + actionType.default + 'CTRL';//invoicesFormCTRL

			stateObj.resolve = {
				$init: ['$http', '$stateParams', function ($http, $stateParams) {

					var urlParamValue;
					if(angular.isArray(urlParams))
					{
						urlParamValue = [];
						for(var i=0; i< urlParams.length; i++){
							urlParamValue.push($stateParams[urlParams[i]]);
						}
					}
					else
					{
						urlParamValue = $stateParams[urlParams];
					}

					var urlConfig = configAction('server', module, model, action, urlParamValue, section);
					return $http.get(urlConfig.url).success(function (data) {
						data.resource_url = c.getOverride('/') + module + '/' + model;
						return data;
					})
				}]
			};

			if(lazyLoadFiles != "no_lazyload")
			{
				stateObj.resolve['lazyLoad'] = ['$ocLazyLoad', function ($ocLazyLoad) {
					if (isSet(lazyLoadFiles) && typeof(lazyLoadFiles) === 'object' && !!lazyLoadFiles) {
						return $ocLazyLoad.load(lazyLoadFiles);
					}
					else {
						return $ocLazyLoad.load({
							name: angularModuleName,// + 'Module',//ex: accounting.invoicesModule
							files: [sectionPath + '/_' + model + '.js']// accounting/invoices/_invoices.js
						});
					}
				}];
			}

			/*
			 $init: ['$scope', '$http', '$stateParams', function ($scope, $http, $stateParams) {
			 $scope.resource_url = c.getOverride('/') + module + '/' + model;
			 return $http.get( configAction('server', module, model, action, $stateParams[urlParam]).url)
			 .success(function (data) {
			 $scope.data = data;
			 }
			 );
			 }]
			 */
			if (typeof(additionalResolve) === 'object' && !!additionalResolve) {
				angular.extend(stateObj.resolve, additionalResolve);
			}


			/**
			 * $ocModal => https://github.com/ocombe/ocModal
			 * parmaters:
			 *   id
			 *   url -> template url
			 *   controller
			 *   cls -> css classes or effect classes
			 *   *size: 'full',
			 *   *backdrop: 'static',
			 *   init -> initialData on resolve
			 *   isolate
			 *   closeOnEsc
			 *   dispatchCloseEvent (boolean)
			 * methods
			 *   onOpen
			 *   onClose
			 *
			 */
			//string or object
			if (isSet(popup) || (typeof(popup) === 'object' && !!popup))
			{
				var popupConfig = {
					id: stateObj.name,
					url: stateObj.templateUrl,
					controller: stateObj.controller,
					onClose: function()
					{
						//console.log("on close");
					},
					cls: 'slide-down',
					size: 'full',
					backdrop: 'static',
					isolate: true,
					init: {}
				};

				if (typeof(popup) === 'object' && !!popup)
					angular.extend(popupConfig, popup);

				var popupStateObj = { name: stateObj.name,
									  url: stateObj.url,
									  resolve: stateObj.resolve};

				if (typeof(stateObj.resolve.$init) === 'object' && !!stateObj.resolve.$init) {
					popupStateObj.onEnter = ['$ocModal', '$init', function ($ocModal, $init) {
						popupConfig.init = $init;
						$ocModal.open(popupConfig);
					}];
				}
				else {
					popupStateObj.onEnter = ['$ocModal', function ($ocModal) {
						$ocModal.open(popupConfig);
					}];
				}
				popupStateObj.popup = true;

				if(c.debugMode() === true) {//print the created state object to console
					popupStateObj.debug_popup_conf = popupConfig;
					console.debug(popupStateObj);
				}

				return popupStateObj;
			}

			if(c.debugMode() === true)//print the created state object to console
				console.debug(stateObj);

			return stateObj;
		};

		var configAction = function (urlType, module, model, action, urlParams, section) {
			/*
			 '/hello/' - Matches only if the path is exactly '/hello/'. There is no special treatment for trailing slashes, and patterns have to match the entire path, not just a prefix.
			 '/user/:id' - Matches '/user/bob' or '/user/1234!!!' or even '/user/' but not '/user' or '/user/bob/details'. The second path segment will be captured as the parameter 'id'.
			 */
			var urlParamValue = "";

			if (isSet(urlParams)) {
				if(angular.isArray(urlParams))
				{
					for(var i=0; i< urlParams.length; i++){
						var param = urlParams[i];
						if (urlType == 'client') {
							param = ':' + param;
						}
						urlParamValue += '/' + param;
					}
				}
				else //single param as string
				{
					if (urlType == 'client') {
						urlParams = ':' + urlParams;
					}
					urlParamValue = '/' + urlParams;
				}
			}

			var urlValue = '';
			var defaultType = '';

			//got to list on action='index' or if there is (no action and no params)
			//if there is a parameter on empty action it goes into edit action
			if (!angular.isDefined(action) || action == '' || action == "index") {
				urlValue = model;//+'/index';//no need to declare the index
			}
			else if (action == "create") {
				urlValue = model + '/' + action + urlParamValue;
				defaultType = 'Form';
			}
			else if (action == "edit") {
				if (!isSet(urlParamValue))
					console.warn("States: Missing url parameter for [action='edit'] on resource => " + model.toUpperCase());

				urlValue = model + urlParamValue + '/edit';
				defaultType = 'Form';
			}
			else if (action == "show") {
				if (!isSet(urlParamValue))
					console.warn("States: Missing url parameter for [action='show'] on resource => " + model.toUpperCase());

				urlValue = model + urlParamValue+'/';
				defaultType = 'Show';
			}
			else {
				urlValue = model + '/' + action + urlParamValue;
				defaultType = uppercaseFirst(action);
			}

			return {url: c.getOverride('/') + module + '/' + section + urlValue, default: defaultType};
		};

		var isSet = function (value) {
			return (value!=null && angular.isDefined(value) && value != '');
		};

		var uppercaseFirst = function (text) {
			return text.charAt(0).toUpperCase() + text.substring(1);
		};

		return {
			resource: restfulResource,
			custom: manualStateResolve,
			get: getAutoStateResolve
		}
	}(this.preconfig)

});
