//Module for services management
angular.module('ace.common.services', [])

.factory('$aceStorage', ['$window', function($window) {

	var setSimple = function(key, value, replaceSlashes) {
		if(replaceSlashes)
			key = key.replace(/\//g, '_');
		$window.localStorage[key] = value;
	};

	var getSimple = function(key, defaultValue, replaceSlashes) {
		if(replaceSlashes)
			key = key.replace(/\//g, '_');
		return $window.localStorage[key] || defaultValue;
	};

	var valueExists = function(key, replaceSlashes) {
		if(replaceSlashes)
			key = key.replace(/\//g, '_');
		return !($window.localStorage[key] === null);
	};

	var	clear = function(key, replaceSlashes) {
		if(key)
		{
			if(replaceSlashes)
				key = key.replace(/\//g, '_');
			$window.localStorage.removeItem(key);
		}
		else //CLEAR ALL
		{
			$window.localStorage.clear();
		}
	};

	var	setVersion = function(key, value) {
		setSimple(key+'_cache_version', value, true);
	};

	var getVersion = function(key, replaceSlashes) {
		return getSimple(key+'_cache_version', 0, true);
	};

	var setObject = function(key, value) {
		setSimple(key, JSON.stringify(value), true);
	};

	var getObject = function(key) {
		return JSON.parse(getSimple(key, '{}', true));
	};

	var	getDataSource = function(url, cache_version, returnOptionsConfig) {

		var options;

		if(cache_version!=null && cache_version == getVersion(url, 0) && valueExists(url, true))
		{
			options = {data: getObject(url)};
		}
		else
		{
			options = {
				transport: {
					read: {
						url: url,
						data: {cache_version: getVersion(url, 0)},
	                    cache: true
					}
				},
				schema: {
					data: function (result) {
						if(result['use_cache'])
						{
							return getObject(url);
						}
						else
						{
							setVersion(url, result.cache_version);
							setObject(url, result.data);
						}

						return result.data;
					}
				}
			};
		}

		if(returnOptionsConfig)
			return options;
		else
			return new kendo.data.DataSource(options);
	};

	return {
		set: setSimple,
		get: getSimple,
		exists: valueExists,
		clear: clear,
		setVersion: setVersion,
		getVersion: getVersion,
		setObject: setObject,
		getObject: getObject,
		kendoDataSource: getDataSource
	}
}])

.factory('$jasperReports', ['$window', '$translate', function($window, $translate) {

	var runReport = function (urlCustomConfig, params, callbackSuccess)
	{
		//host: http://localhost:8080
		var urlConfig = { host: Utils.jasperHostUrl(),
						  action: "/rest_v2/reportExecutions",
						  report: "",
						  format: "html",
						  ignorePagination: false,
						  pages: '1'
						};

		if (typeof(urlCustomConfig) === 'object' && !!urlCustomConfig)
			angular.extend(urlConfig, urlCustomConfig);
		else
			urlConfig['report'] = urlCustomConfig;

		var executionProperties = { reportUnitUri: urlConfig['report'],
									outputFormat: urlConfig['format'],
							        ignorePagination: urlConfig['ignorePagination'],
							        pages: urlConfig['pages']};

		if(params){
			//http://stackoverflow.com/questions/25025822/need-example-of-passing-jasper-reports-parameters-for-rest-v2-api-using-json
			var parameters = [];
			angular.forEach(params, function(value, key) {
				this.push({name: key, value: [value]});
			}, parameters);

			executionProperties['parameters'] = {reportParameter: parameters};
		}

		return $.ajax({
			url: urlConfig['host']+urlConfig['action'],
			method: 'POST',
			xhrFields: {
				withCredentials: true
			},
			data: JSON.stringify(executionProperties),
			contentType: "application/json; charset=utf-8",//send format
			dataType: "json",//return format
			success: function(data) {
				if(data.totalPages > 0){
					//GET http://localhost:8080/jasperserver/rest_v2/reportExecutions/912382875_1366638024956_2/exports/html/outputResource
					getReport({
								  action: '/rest_v2/reportExecutions/',
								  report: data.requestId+'/exports/'+data.exports[0]['id']+'/outputResource',
							      format: ""
								},
							  null, 0,
							  function( result )
							  {
								  callbackSuccess({html: result.html, page: 1, totalPages: data.totalPages});
							  }
							);
				}
				else
				{
					$translate('REPORTS.EMPTY_RESULT').then(function (noResultText) {
						var html ="<h3>"+noResultText+"</h3>";
						callbackSuccess({html: html, totalPages: 0});
					});
				}
			}
		});
	};

	var getReport = function (urlCustomConfig, params, page, callbackSuccess)
	{
		//host: http://localhost:8080
		var urlConfig = { host: Utils.jasperHostUrl(),
						  action: "/rest_v2/reports",
						  report: '',
						  format: ".html"
						};

		if (typeof(urlCustomConfig) === 'object' && !!urlCustomConfig)
			angular.extend(urlConfig, urlCustomConfig);
		else
			urlConfig['report'] = urlCustomConfig;

		var urlPath = urlConfig['host']+urlConfig['action']+urlConfig['report']+urlConfig['format'];
		var pageParam = "";
		var parameters = "";

		if(page > 0)
			pageParam = '?page='+page;

		if(params){
			if(pageParam == "")
				pageParam = "?";
			else
				pageParam += "&";

			parameters = $.param(params);
		}

		urlPath = urlPath+pageParam+parameters;

		return $.ajax({
			url: urlPath,
			xhrFields: {
				withCredentials: true
			},
			success: function(html) {
				callbackSuccess({html: html, page: page});
			}
		});
	};

	return {
		run: runReport,
		get: getReport
	}
}]);
