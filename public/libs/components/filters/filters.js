angular.module('ace.common.filters', [])

.filter('mysqlDateToISO', function() {
	return function(mysqlDate) {
		return Date.mysqlDateToISO(mysqlDate);
	};
})

.filter('kendoDate', function() {
	return function(mysqlDate, format) {
		if(mysqlDate){
			return kendo.toString(kendo.parseDate(mysqlDate, "yyyy-MM-dd"), (format)?format:'d');
		}
		return "";
	};
})

.filter('kendoNumber', function() {
	return function(value, format) {
		if(!isNaN(value)){
			return kendo.toString(value, (format)?format:'c');
		}
		return "";
	};
})

/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
.filter('propsFilter', function() {
	return function(items, props) {
		var out = [];

		if (angular.isArray(items)) {
			items.forEach(function(item) {
				var itemMatches = false;

				var keys = Object.keys(props);
				for (var i = 0; i < keys.length; i++) {
					var prop = keys[i];
					var text = props[prop].toLowerCase();
					if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
						itemMatches = true;
						break;
					}
				}

				if (itemMatches) {
					out.push(item);
				}
			});
		} else {
			// Let the output be the input untouched
			out = items;
		}

		return out;
	}
})
