/*
* Kendo UI v2014.3.1119 (http://www.telerik.com/kendo-ui)
* Copyright 2014 Telerik AD. All rights reserved.
*
* Kendo UI commercial licenses may be obtained at
* http://www.telerik.com/purchase/license-agreement/kendo-ui-complete
* If you do not own a commercial license, this file shall be governed by the trial license terms.
*/
(function(f, define){
    define([], f);
})(function(){

/* FlatColorPicker messages */

if (kendo.ui.FlatColorPicker) {
kendo.ui.FlatColorPicker.prototype.options.messages =
$.extend(true, kendo.ui.FlatColorPicker.prototype.options.messages,{
  "apply": "Apliko",
  "cancel": "Anullo"
});
}

/* ColorPicker messages */

if (kendo.ui.ColorPicker) {
kendo.ui.ColorPicker.prototype.options.messages =
$.extend(true, kendo.ui.ColorPicker.prototype.options.messages,{
  "Apliko": "Apliko",
  "Anullo": "Anullo"
});
}

/* ColumnMenu messages */

if (kendo.ui.ColumnMenu) {
kendo.ui.ColumnMenu.prototype.options.messages =
$.extend(true, kendo.ui.ColumnMenu.prototype.options.messages,{
  "sortAscending": "Rendit Mbare",
  "sortDescending": "Rendit Mbrapsht",
  "filter": "Filtro",
  "columns": "Kolonat",
  "done": "Kryer",
  "settings": "Konfigurim Kolonash",
  "lock": "Blloko",
  "unlock": "Ç`blloko"
});
}

/* Editor messages */

if (kendo.ui.Editor) {
kendo.ui.Editor.prototype.options.messages =
$.extend(true, kendo.ui.Editor.prototype.options.messages,{
  "bold": "Bold",
  "italic": "Italic",
  "underline": "Underline",
  "strikethrough": "Strikethrough",
  "superscript": "Superscript",
  "subscript": "Subscript",
  "justifyCenter": "Center text",
  "justifyLeft": "Align text left",
  "justifyRight": "Align text right",
  "justifyFull": "Justify",
  "insertUnorderedList": "Insert unordered list",
  "insertOrderedList": "Insert ordered list",
  "indent": "Indent",
  "outdent": "Outdent",
  "createLink": "Insert hyperlink",
  "unlink": "Remove hyperlink",
  "insertImage": "Insert image",
  "insertFile": "Insert file",
  "insertHtml": "Insert HTML",
  "viewHtml": "View HTML",
  "fontName": "Select font family",
  "fontNameInherit": "(inherited font)",
  "fontSize": "Select font size",
  "fontSizeInherit": "(inherited size)",
  "formatBlock": "Format",
  "formatting": "Format",
  "foreColor": "Color",
  "backColor": "Background color",
  "style": "Styles",
  "emptyFolder": "Empty Folder",
  "uploadFile": "Upload",
  "orderBy": "Arrange by:",
  "orderBySize": "Size",
  "orderByName": "Name",
  "invalidFileType": "The selected file \"{0}\" is not valid. Supported file types are {1}.",
  "deleteFile": 'Are you sure you want to delete "{0}"?',
  "overwriteFile": 'A file with name "{0}" already exists in the current directory. Do you want to overwrite it?',
  "directoryNotFound": "A directory with this name was not found.",
  "imageWebAddress": "Web address",
  "imageAltText": "Alternate text",
  "imageWidth": "Width (px)",
  "imageHeight": "Height (px)",
  "fileWebAddress": "Web address",
  "fileTitle": "Title",
  "linkWebAddress": "Web address",
  "linkText": "Text",
  "linkToolTip": "ToolTip",
  "linkOpenInNewWindow": "Open link in new window",
  "dialogUpdate": "Update",
  "dialogInsert": "Insert",
  "dialogButtonSeparator": "or",
  "dialogCancel": "Anullo",
  "createTable": "Create table",
  "addColumnLeft": "Add column on the left",
  "addColumnRight": "Add column on the right",
  "addRowAbove": "Add row above",
  "addRowBelow": "Add row below",
  "deleteRow": "Delete row",
  "deleteColumn": "Delete column"
});
}

/* FileBrowser messages */

if (kendo.ui.FileBrowser) {
kendo.ui.FileBrowser.prototype.options.messages =
$.extend(true, kendo.ui.FileBrowser.prototype.options.messages,{
  "uploadFile": "Ngarko",
  "orderBy": "Rendit sipas",
  "orderByName": "Emri",
  "orderBySize": "Madhesia",
  "directoryNotFound": "Nuk gjendet nje dosje me kete emer.",
  "emptyFolder": "Dosje Bosh",
  "deleteFile": 'A jeni i sigurte per fshirjen e "{0}"?',
  "invalidFileType": "Dokumenti i zgjedhur \"{0}\" nuk eshte i vlefshem. Tipet e lejuar jane: {1}.",
  "overwriteFile": "Nje dokument me kete emer \"{0}\" egziston tashme ne kete dosje. A deshironi ta mbishkruani?",
  "dropFilesHere": "terheq/lesho dokumente ketu per ti ngarkuar",
  "search": "Kerko"
});
}

/* FilterCell messages */

if (kendo.ui.FilterCell) {
kendo.ui.FilterCell.prototype.options.messages =
$.extend(true, kendo.ui.FilterCell.prototype.options.messages,{
  "isTrue": "e vertete",
  "isFalse": "e gabuar",
  "filter": "Filtro",
  "clear": "Pastro",
  "operator": "Operatori"
});
}

/* FilterMenu messages */

if (kendo.ui.FilterMenu) {
kendo.ui.FilterMenu.prototype.options.messages =
$.extend(true, kendo.ui.FilterMenu.prototype.options.messages,{
  "info": "Shfaq sipas kushteve:",
  "isTrue": "e vertete",
  "isFalse": "e gabuar",
  "filter": "Filtro",
  "clear": "Pastro",
  "and": "Dhe",
  "or": "Ose",
  "selectValue": "- Zgjedh -",
  "operator": "Operatori",
  "value": "Vlera",
  "Anullo": "Anullo"
});
}

/* FilterMenu operator messages */

if (kendo.ui.FilterMenu) {
kendo.ui.FilterMenu.prototype.options.operators =
$.extend(true, kendo.ui.FilterMenu.prototype.options.operators,{
  "string": {
    "eq": "E njejte me",
    "neq": "E ndryshme nga",
    "startswith": "Fillon me",
    "contains": "Permban",
    "doesnotcontain": "Nuk permban",
    "endswith": "Perfundon me"
  },
  "number": {
    "eq": "E barabarte me",
    "neq": "E ndryshme nga",
    "gte": "Fillon nga",
    "gt": "Me i madh se",
    "lte": "Deri ne",
    "lt": "Me i vogel se"
  },
  "date": {
    "eq": "E njejte me",
    "neq": "E ndryshme nga",
    "gte": "Fillon nga data",
    "gt": "Me vone se",
    "lte": "Deri me date",
    "lt": "Me heret se"
  },
  "enums": {
    "eq": "E barabarte me",
    "neq": "E ndryshme nga"
  }
});
}

/* Gantt messages */

if (kendo.ui.Gantt) {
kendo.ui.Gantt.prototype.options.messages =
$.extend(true, kendo.ui.Gantt.prototype.options.messages,{
  "views": {
    "day": "Dite",
    "week": "Jave",
    "month": "Muaj"
  },
  "actions": {
    "append": "Shto Detyre",
    "addChild": "Shto Femije",
    "insertBefore": "Shto Siper",
    "insertAfter": "Shto Poshte"
  }
});
}

/* Grid messages */

if (kendo.ui.Grid) {
kendo.ui.Grid.prototype.options.messages =
$.extend(true, kendo.ui.Grid.prototype.options.messages,{
  "commands": {
    "cancel": "Anullo Ndryshimet",
    "canceledit": "Anullo",
    "create": "Shto te ri",
    "destroy": "Fshij",
    "edit": "Modifiko",
    "save": "Ruaj Ndryshimet",
    "select": "Zgjedh",
    "update": "Ndrysho"
  },
  "editable": {
    "cancelDelete": "Anullo",
    "confirmation": "A jeni i sigurt per fshirjen e rreshtit?",
    "confirmDelete": "Fshij"
  }
});
}

/* Groupable messages */

if (kendo.ui.Groupable) {
kendo.ui.Groupable.prototype.options.messages =
$.extend(true, kendo.ui.Groupable.prototype.options.messages,{
  "empty": "Kap/terheq nje krye kolone dhe leshoje ketu qe te grupohet sipas asaj kolone"
});
}

/* NumericTextBox messages */

if (kendo.ui.NumericTextBox) {
kendo.ui.NumericTextBox.prototype.options =
$.extend(true, kendo.ui.NumericTextBox.prototype.options,{
  "upArrowText": "Rritje",
  "downArrowText": "Zbritje"
});
}

/* Pager messages */

if (kendo.ui.Pager) {
kendo.ui.Pager.prototype.options.messages =
$.extend(true, kendo.ui.Pager.prototype.options.messages,{
  "display": "{0} - {1} nga {2} rreshta",
  "empty": "Nuk ka asnje element per shfaqje",
  "page": "Faqe",
  "of": "nga {0}",
  "itemsPerPage": "rreshta per faqe",
  "first": "Shko tek faqja e pare",
  "previous": "Shko tek faqja e meparshme",
  "next": "Shko tek faqja tjeter",
  "last": "Shko tek faqja e fundit",
  "refresh": "Rifresko",
  "morePages": "Me shume faqe"
});
}

/* PivotGrid messages */

if (kendo.ui.PivotGrid) {
kendo.ui.PivotGrid.prototype.options.messages =
$.extend(true, kendo.ui.PivotGrid.prototype.options.messages,{
  "measureFields": "Terheq Fusha te Dhenash Ketu",
  "columnFields": "Lesho Fusha Kolone Ketu",
  "rowFields": "Lesho Fusha Rreshti Ketu"
});
}

/* PivotFieldMenu messages */

if (kendo.ui.PivotFieldMenu) {
kendo.ui.PivotFieldMenu.prototype.options.messages =
$.extend(true, kendo.ui.PivotFieldMenu.prototype.options.messages,{
  "info": "Shfaq sipas kushteve:",
  "filterFields": "Filtrim Fushe",
  "filter": "Filtro",
  "include": "Pershij Fushat...",
  "title": "Fushat per tu perfshire",
  "clear": "Pastro",
  "ok": "Ok",
  "Anullo": "Anullo",
  "operators": {
	"eq": "E njejte me",
    "neq": "E ndryshme nga",
    "startswith": "Fillon me",
    "contains": "Permban",
    "doesnotcontain": "Nuk permban",
    "endswith": "Perfundon me"
  }
});
}

/* RecurrenceEditor messages */

if (kendo.ui.RecurrenceEditor) {
kendo.ui.RecurrenceEditor.prototype.options.messages =
$.extend(true, kendo.ui.RecurrenceEditor.prototype.options.messages,{
  "frequencies": {
    "never": "Kurre",
    "hourly": "Per Ore",
    "daily": "Ditore",
    "weekly": "Javore",
    "monthly": "Mujore",
    "yearly": "Vjetore"
  },
  "hourly": {
    "repeatEvery": "Repeat every: ",
    "interval": " hour(s)"
  },
  "daily": {
    "repeatEvery": "Perserit çdo: ",
    "interval": " dite"
  },
  "weekly": {
    "interval": " jave",
    "repeatEvery": "Perserit çdo: ",
    "repeatOn": "Perserit sipas: "
  },
  "monthly": {
    "repeatEvery": "Perserit çdo: ",
    "repeatOn": "Perserit sipas: ",
    "interval": " muaj",
    "day": "Dita "
  },
  "yearly": {
    "repeatEvery": "Perserit çdo: ",
    "repeatOn": "Perserit sipas: ",
    "interval": " vite",
    "of": " nga "
  },
  "end": {
    "label": "Fundi:",
    "mobileLabel": "Perfundon",
    "never": "Kurre",
    "after": "Pas ",
    "occurrence": " gjetje",
    "on": "Ne "
  },
  "offsetPositions": {
    "first": "i pari",
    "second": "i dyti",
    "third": "i treti",
    "fourth": "i katerti",
    "last": "i fundit"
  },
  "weekdays": {
    "day": "dita",
    "weekday": "fundjave",
    "weekend": "dite fundjave"
  }
});
}

/* Scheduler messages */

if (kendo.ui.Scheduler) {
kendo.ui.Scheduler.prototype.options.messages =
$.extend(true, kendo.ui.Scheduler.prototype.options.messages,{
  "allDay": "all day",
  "date": "Date",
  "event": "Event",
  "time": "Time",
  "showFullDay": "Show full day",
  "showWorkDay": "Show business hours",
  "today": "Today",
  "save": "Save",
  "Anullo": "Anullo",
  "destroy": "Delete",
  "deleteWindowTitle": "Delete event",
  "ariaSlotLabel": "Selected from {0:t} to {1:t}",
  "ariaEventLabel": "{0} on {1:D} at {2:t}",
  "confirmation": "Are you sure you want to delete this event?",
  "views": {
    "day": "Day",
    "week": "Week",
    "workWeek": "Work Week",
    "agenda": "Agenda",
    "month": "Month"
  },
  "recurrenceMessages": {
    "deleteWindowTitle": "Delete Recurring Item",
    "deleteWindowOccurrence": "Delete current occurrence",
    "deleteWindowSeries": "Delete the series",
    "editWindowTitle": "Edit Recurring Item",
    "editWindowOccurrence": "Edit current occurrence",
    "editWindowSeries": "Edit the series",
    "deleteRecurring": "Do you want to delete only this event occurrence or the whole series?",
    "editRecurring": "Do you want to edit only this event occurrence or the whole series?"
  },
  "editor": {
    "title": "Title",
    "start": "Start",
    "end": "End",
    "allDayEvent": "All day event",
    "description": "Description",
    "repeat": "Repeat",
    "timezone": " ",
    "startTimezone": "Start timezone",
    "endTimezone": "End timezone",
    "separateTimezones": "Use separate start and end time zones",
    "timezoneEditorTitle": "Timezones",
    "timezoneEditorButton": "Time zone",
    "timezoneTitle": "Time zones",
    "noTimezone": "No timezone",
    "editorTitle": "Event"
  }
});
}

/* Slider messages */

if (kendo.ui.Slider) {
kendo.ui.Slider.prototype.options =
$.extend(true, kendo.ui.Slider.prototype.options,{
  "increaseButtonTitle": "Rritje",
  "decreaseButtonTitle": "Zbritje"
});
}

/* TreeView messages */

if (kendo.ui.TreeView) {
kendo.ui.TreeView.prototype.options.messages =
$.extend(true, kendo.ui.TreeView.prototype.options.messages,{
  "loading": "Loading...",
  "requestFailed": "Problem serveri.",
  "retry": "Riprovo"
});
}

/* Upload messages */

if (kendo.ui.Upload) {
kendo.ui.Upload.prototype.options.localization=
$.extend(true, kendo.ui.Upload.prototype.options.localization,{
  "select": "Zgjedh dokument...",
  "Anullo": "Anullo",
  "retry": "Riprovo",
  "remove": "Fshij",
  "uploadSelectedFiles": "Ngarko dukumente",
  "dropFilesHere": "terheq dhe lesho dokumenta ketu per ngarkim",
  "statusUploading": "duke ngarkuar",
  "statusUploaded": "ngarkuar",
  "statusWarning": "me probleme",
  "statusFailed": "deshtoi",
  "headerStatusUploading": "Duke ngarkuar...",
  "headerStatusUploaded": "Kryer"
});
}

/* Validator messages */

if (kendo.ui.Validator) {
kendo.ui.Validator.prototype.options.messages =
$.extend(true, kendo.ui.Validator.prototype.options.messages,{
  "required": "{0} e domosdoshme",
  "pattern": "{0} e pa vlefshme",
  "min": "{0} duhet te jete me e madhe ose e barabarte me {1}",
  "max": "{0} duhet te jete me e vogel ose e barabarte me {1}",
  "step": "{0} i pa vlefshem",
  "email": "{0} email i pa sakte",
  "url": "{0} adrese URL e pa sakte",
  "date": "{0} date e pa vlefshme"
});
}


return window.kendo;

}, typeof define == 'function' && define.amd ? define : function(_, f){ f(); });