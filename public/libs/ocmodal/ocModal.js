/**
 * ocModal - An angularJS modal directive / service
 * @version v0.1.10
 * @link https://github.com/ocombe/ocModal
 * @license MIT
 * @author Olivier Combe <olivier.combe@gmail.com>
 */
(function() {
	'use strict';

	var ocModal = angular.module('oc.modal', []);

	ocModal.factory('$ocModal', ['$rootScope', '$controller', '$location', '$timeout', '$compile', '$sniffer', '$q', function($rootScope, $controller, $location, $timeout, $compile, $sniffer, $q) {
		var $body = angular.element(document.body),
			$dialogsWrapper = angular.element('<div role="dialog" tabindex="-1" class="modal"><div class="modal-backdrop" style="display: none;"></div></div>'),
			$modalWrapper = angular.element(
				'<div class="modal-wrapper"></div>'
			),
			modals = {},
			openedModals = [],
			baseOverflow,
			windowSizes = {sm: 'small', md: 'medium', lg: 'large', full: 'fullscreen'};

		// include the modal in DOM at start for animations
		$modalWrapper.css('display', 'none');
		$modalWrapper.append($dialogsWrapper);
		$body.append($modalWrapper);
		$dialogsWrapper.on('click', function(e) {
			if(angular.element(e.target).hasClass('modal-backdrop')) { // only if clicked on backdrop
				$rootScope.$apply(function() {
					if(self.params.backdrop != 'static') 
						self.closeOnEsc();
				});
			}
			e.stopPropagation();
		});

		var parseMaxTime = function parseMaxTime(str) {
			var total = 0, values = angular.isString(str) ? str.split(/\s*,\s*/) : [];
			angular.forEach(values, function(value) {
				total = Math.max(parseFloat(value) || 0, total);
			});
			return total;
		}

		var getAnimDuration = function getDuration($element) {
			var duration = 0;
			if(($sniffer.transitions || $sniffer.animations)) {
				//one day all browsers will have these properties
				var w3cAnimationProp = 'animation';
				var w3cTransitionProp = 'transition';

				//but some still use vendor-prefixed styles
				var vendorAnimationProp = $sniffer.vendorPrefix + 'Animation';
				var vendorTransitionProp = $sniffer.vendorPrefix + 'Transition';

				var durationKey = 'Duration',
					delayKey = 'Delay',
					animationIterationCountKey = 'IterationCount';

				//we want all the styles defined before and after
				var ELEMENT_NODE = 1;
				angular.forEach($element, function(element) {
					if(element.nodeType == ELEMENT_NODE) {
						var elementStyles = window.getComputedStyle(element) || {};

						var transitionDelay = Math.max(parseMaxTime(elementStyles[w3cTransitionProp + delayKey]),
							parseMaxTime(elementStyles[vendorTransitionProp + delayKey]));

						var animationDelay = Math.max(parseMaxTime(elementStyles[w3cAnimationProp + delayKey]),
							parseMaxTime(elementStyles[vendorAnimationProp + delayKey]));

						var transitionDuration = Math.max(parseMaxTime(elementStyles[w3cTransitionProp + durationKey]),
							parseMaxTime(elementStyles[vendorTransitionProp + durationKey]));

						var animationDuration = Math.max(parseMaxTime(elementStyles[w3cAnimationProp + durationKey]),
							parseMaxTime(elementStyles[vendorAnimationProp + durationKey]));

						if(animationDuration > 0) {
							animationDuration *= Math.max(parseInt(elementStyles[w3cAnimationProp + animationIterationCountKey]) || 0,
								parseInt(elementStyles[vendorAnimationProp + animationIterationCountKey]) || 0, 1);
						}

						duration = Math.max(animationDelay + animationDuration, transitionDelay + transitionDuration, duration);
					}
				});
			}

			return duration * 1000;
		}

		angular.element(document).on('keyup', function(e) {
			if (e.keyCode == 27 && openedModals.length > 0) {
				e.stopPropagation();
				$rootScope.$apply(function() {
					self.closeOnEsc(openedModals[openedModals.length - 1]);
				});
			}
		});

		var self = {
			waitingForOpen: false,

			getOpenedModals: function() {
				return openedModals;
			},

			register: function(params) {
				modals[params.id || '_default'] = params;
			},

			remove: function(id) {
				delete modals[id || '_default'];
			},

			open: function(opt) {
				if(typeof opt === 'string') {
					if(opt.match('<')) { // if html code
						opt = {
							template: opt
						}
					} else {
						opt = {
							url: opt
						}
					}
				}
				var modal = modals[opt.id || '_default'];
				if(!modal) {
					$dialogsWrapper.append($compile('<div oc-modal="'+(opt.id ? opt.id : '_default')+'"></div>')($rootScope));
					$timeout(function() { // let the ng-include detect that it's now empty
						self.open(opt);
					});
					return;
				} else if(modal && openedModals.indexOf(opt.id || '_default') !== -1) { // if modal already opened
					if(self.waitingForOpen) {
						return;
					}
					self.waitingForOpen = true;
					self.close(opt.id).then(function() {
						self.open(opt);
					})
					return;
				}
				// ok let's open the modal
				if(!self.waitingForOpen) {
					if(openedModals.length === 0) { // if no modal opened
						baseOverflow = document.body.style.overflow;
						document.body.style.overflow = 'hidden';
						$modalWrapper.css('display', 'block');
					} else {
						for(var i = 0, len = openedModals.length; i < len; i++) {
							var $e = modals[openedModals[i]].$element;
							modals[openedModals[i]].baseZIndex = $e.css('z-index');
							$e.css('z-index', '-1');
							$e.addClass('no-backdrop');
						}
					}
				}
				self.waitingForOpen = false;
				openedModals.push(opt.id || '_default');
				modal.params = opt;
				self.params = opt;
				modal.$scope.customClass = modal.params.cls;

				if(windowSizes[opt.size])
					modal.$scope.sizeClass = 'modal-'+windowSizes[opt.size];
				else
					modal.$scope.sizeClass = 'modal-fullscreen';

				// timeout for animations (if any)
				$rootScope.$digest();
				$body[0].offsetWidth; // force paint to be sure the element is in the page
				$timeout(function() {
					modal.$scope.modalShow = true;
				}, 100);

				if(typeof modal.params.onOpen === 'function') {
					modal.params.onOpen();
				}

				var off = modal.$scope.$on('$includeContentLoaded', function(event) { // on view load

					//BOOTSTRAP BUG-FIX FOR DROPDOWNS
					$('.modal-dialog .dropdown-toggle').dropdownOnPopup();

					/*
					$('.dropdown-toggle').mouseover(function() {
				        $('.dropdown-menu').show();
				    });

				    $('.dropdown-toggle').mouseout(function() {
				        var t = setTimeout(function() {
				            $('.dropdown-menu').hide();
				        }, 100);

				        $('.dropdown-menu').on('mouseenter', function() {
				            $('.dropdown-menu').show();
				            clearTimeout(t);
				        }).on('mouseleave', function() {
				            $('.dropdown-menu').hide();
				        })
				    });*/


					if(modal.params.init && !modal.params.isolate) {
						angular.extend(event.targetScope, modal.params.init);
					}
					if(typeof modal.params.controller === 'string') {
						$controller(modal.params.controller, {$scope: event.targetScope, $init: modal.params.init, $ocModalParams: modal.params}); // inject controller
					}
					off();
				});

				if(modal.params.template) {
					modal.$scope.modalTemplate = modal.params.template; // load the view
				} else if(modal.params.url) {
					modal.$scope.modalUrl = modal.params.url; // load the view
				} else {
					throw "You need to define a template or an url";
					return;
				}

				if(typeof callback === 'function') {
					modal.$scope.callbacksList.push(callback);
				}
			},

			closeOnEsc: function(id) {
				if(modals[id || openedModals[openedModals.length -1]].params.closeOnEsc !== false) {
					return self.close(id);
				}
			},

			closeSelf: function() {
				return self.close(self.params.id);
			},

			alert: function(message, title, type, opts) {
				var icon;
				switch(type)
				{
					case 'danger'://#c12e2a
						icon = 'fa-exclamation-triangle';
						break;
					case 'warning'://#eb9316
						icon = 'fa-exclamation';
						break;
					case 'success'://#419641
						icon = 'fa-thumbs-up ';
						break;
					default://info #5cb85c
						type = 'info';
						icon = 'fa-info-circle';
						break;
				}
				
				var msgContent;
				if(angular.isArray(message) && message.length > 1)
				{
					msgContent = "<ul>";
					for(var i=0; i < message.length; i++)
					{
						msgContent += "<li>"+message[i]+"</li>";
					}
					
					msgContent += "</ul>";
				}
				else
					msgContent = message;
				
				var tml = '<div class="modal-header '+type+'-message-alert"><button type="button" class="modal-header-btn close-modal" data-dismiss="modal" aria-hidden="true" ng-click="closeModal()"><i class="fa fa-times"></i></button><button type="button" ng-click="closeModal()" class="modal-header-btn recent-btn"><i class="fa '+icon+'"></i></button>'+
								'<span class="modal-title">'+title+'</span></div><div class="modal-body message-alert-popup">'+
								msgContent+'</div><div class="modal-footer '+type+'-message-alert" style="text-align: center;"><button type="button" class="btn btn-'+type+'" data-dismiss="modal" ng-click="closeModal()" translate="FORM.OK" style="min-width: 100px;"></button></div>';
							console.log(tml);
				var options = {template: tml, cls: 'fade-in alert', size: 'md'};
				if(opts)
					angular.extend(options, opts);
				self.open(options);
			},
			confirm: function(message, title, opts) {
				var options = {template: '<div>{{param1}}</div>',
					controller: 'OcModelCustomAlertController',
					init: {
						window_title: title,
						message_content: message
					}};
				angular.extend(options, opts);
				self.open(options);
			},

			close: function(id, dispatchCloseEvent) {
				var args,
					deferred = $q.defer();
				if(typeof id === 'string' && openedModals.indexOf(id) !== -1) {
					args = Array.prototype.slice.call(arguments, 1);
				} else {
					args = arguments;
				}
				if(typeof id === 'undefined' || openedModals.indexOf(id) === -1) {
					id = openedModals[openedModals.length -1];
				}
				var modal = modals[id || openedModals[openedModals.length -1]];
				if(modal && modal.$scope.modalShow === true) { // if the modal is opened
					var animDuration = getAnimDuration(angular.element(modal.$element[0].querySelector('.modal-content')));
					$timeout(function() {
						modal.$scope.modalShow = false;

						$timeout(function() {
							modal.$scope.$destroy();
							modal.$element.remove(); // destroy the modal

							modal.callbacksList = []; // forget all callbacks
							openedModals.splice(openedModals.indexOf(id || openedModals[openedModals.length -1]), 1);
							if(openedModals.length === 0) { // if no modal left opened
								if(!self.waitingForOpen) { // in case the current modal is closed because another opened with the same id (avoid backdrop flickering in firefox)
									document.body.style.overflow = baseOverflow; // restore the body overflow
									$modalWrapper.css('display', 'none');
								}
							} else {
								var topModal = modals[openedModals[openedModals.length - 1]];
								topModal.$element.css('z-index', topModal.baseZIndex);
								topModal.$element.removeClass('no-backdrop');
							}
							if(typeof modal.params.onClose === 'function') {
								if(dispatchCloseEvent !== false && (modal.params.dispatchCloseEvent !== false))
									$rootScope.$broadcast('onModuleCloseEvent', id, modal);
								modal.params.onClose.apply(undefined, args);
							}

							deferred.resolve();
						}, animDuration);
					});
				} else {
					deferred.resolve();
				}
				return deferred.promise;
			}
		};

		return self;
	}]);

	ocModal.directive('ocModal', ['$ocModal', '$compile', '$timeout', function($ocModal, $compile, $timeout) {
		return {
			restrict: 'AE',
			replace: true,
			scope: true,
			template:
			'<div class="modal-dialog {{sizeClass}}">' +
				'<div class="modal-backdrop"></div>' +
				'<div class="modal-content {{customClass}}" ng-class="{opened: modalShow}" ng-if="modalTemplate"></div>' +
				'<div class="modal-content {{customClass}}" ng-class="{opened: modalShow}" ng-include="modalUrl"></div>' +
			'</div>',

			link: function link($scope, $element, $attrs) {
				var id = $attrs.ocModal,
					$templateWrapper;

				$scope.closeModal = function() {
					var args = Array.prototype.slice.call(arguments);
					args.unshift(id);
					$ocModal.close.apply(undefined, args);
				};

				$ocModal.register({
					id: id,
					$scope: $scope,
					$element: $element
				});

				$element.on('$destroy', function() {
					$ocModal.remove(id);
				});

				$scope.$watch('modalTemplate', function(newVal, oldVal) {
					if(typeof newVal !== 'undefined') {
						if(!$templateWrapper) {
							$templateWrapper = angular.element($element.children()[1]);
						}
						$templateWrapper.append($compile(newVal)($scope));
						$scope.$emit('$includeContentLoaded');
					}
				});
			}
		}
	}]);

	ocModal.directive('ocModalOpen', ['$ocModal', function($ocModal) {
		return {
			restrict: 'A',
			require: '?modal',
			link: function($scope, $element, $attrs) {
				$element.on('click touchstart', function(e) {
					e.preventDefault();
					e.stopPropagation();
					var newScope = $scope.$new();
					var params = newScope.$eval($attrs.ocModalOpen);
					if(params) {
						if(typeof params === "number") {
							params = { url: $attrs.ocModalOpen };
						} else if(typeof params === "string") {
							params = { url: params };
						}
						if(!params.url) {
							throw "You need to set the modal url";
						}
						$scope.$apply(function() {
							$ocModal.open(params);
						});
					}
				});
			}
		};
	}]);

	ocModal.directive('ocModalClose', ['$ocModal', function($ocModal) {
		return {
			restrict: 'A',
			require: '?modal',
			link: function($scope, $element, $attrs) {
				$element.on('click touchstart', function(e) {
					e.preventDefault();
					e.stopPropagation();
					$scope.$apply(function() {
						if($attrs.ocModalClose) {
							var params = $scope.$new().$eval($attrs.ocModalClose);
						}
						$ocModal.close(params);
					});
				});
			}
		};
	}]);



})();


/* ========================================================================
 * Bootstrap: dropdown.js v3.3.0
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // DROPDOWN CLASS DEFINITION
  // =========================

  var backdrop = '.dropdown-backdrop'
  var toggle   = '[data-toggle="dropdownonpopup"]'
  var DropdownOnPopup = function (element) {
    $(element).on('click.bs.dropdownonpopup', this.toggle)
  }

  DropdownOnPopup.VERSION = '3.3.0'

  DropdownOnPopup.prototype.toggle = function (e) {
    var $this = $(this)

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    clearMenus()

    if (!isActive) {
      if (!$parent.closest('.navbar-nav').length) {
        // if mobile we use a backdrop because click events don't delegate
        $('<div class="dropdown-backdrop"/>').insertAfter($(this)).on('click', clearMenus)
      }

      var relatedTarget = { relatedTarget: this }
      $parent.trigger(e = $.Event('show.bs.dropdownonpopup', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this
        .trigger('focus')
        .attr('aria-expanded', 'true')

      $parent
        .toggleClass('open')
        .trigger('shown.bs.dropdownonpopup', relatedTarget)
    }

    return false
  }

  DropdownOnPopup.prototype.keydown = function (e) {
    if (!/(38|40|27|32)/.test(e.which)) return

    var $this = $(this)

    e.preventDefault()
    e.stopPropagation()

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    if ((!isActive && e.which != 27) || (isActive && e.which == 27)) {
      if (e.which == 27) $parent.find(toggle).trigger('focus')
      return $this.trigger('click')
    }

    var desc = ' li:not(.divider):visible a'
    var $items = $parent.find('[role="menu"]' + desc + ', [role="listbox"]' + desc)

    if (!$items.length) return

    var index = $items.index(e.target)

    if (e.which == 38 && index > 0)                 index--                        // up
    if (e.which == 40 && index < $items.length - 1) index++                        // down
    if (!~index)                                      index = 0

    $items.eq(index).trigger('focus')
  }

  function clearMenus(e) {
    if (e && e.which === 3) return
    $(backdrop).remove()
    $(toggle).each(function () {
      var $this         = $(this)
      var $parent       = getParent($this)
      var relatedTarget = { relatedTarget: this }

      if (!$parent.hasClass('open')) return

      $parent.trigger(e = $.Event('hide.bs.dropdownonpopup', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this.attr('aria-expanded', 'false')
      $parent.removeClass('open').trigger('hidden.bs.dropdownonpopup', relatedTarget)
    })
  }

  function getParent($this) {
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = selector && $(selector)

    return $parent && $parent.length ? $parent : $this.parent()
  }


  // DROPDOWN PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.dropdownonpopup')

      if (!data) $this.data('bs.dropdownonpopup', (data = new DropdownOnPopup(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.dropdownOnPopup

  $.fn.dropdownOnPopup             = Plugin
  $.fn.dropdownOnPopup.Constructor = DropdownOnPopup


  // DROPDOWN NO CONFLICT
  // ====================

  $.fn.dropdownOnPopup.noConflict = function () {
    $.fn.dropdownOnPopup = old
    return this
  }


  // APPLY TO STANDARD DROPDOWN ELEMENTS
  // ===================================

  $(document)
    .on('click.bs.dropdownonpopup.data-api', clearMenus)
    .on('click.bs.dropdownonpopup.data-api', '.dropdownonpopup form', function (e) { e.stopPropagation() })
    .on('click.bs.dropdownonpopup.data-api', toggle, DropdownOnPopup.prototype.toggle)
    .on('keydown.bs.dropdownonpopup.data-api', toggle, DropdownOnPopup.prototype.keydown)
    .on('keydown.bs.dropdownonpopup.data-api', '[role="menu"]', DropdownOnPopup.prototype.keydown)
    .on('keydown.bs.dropdownonpopup.data-api', '[role="listbox"]', DropdownOnPopup.prototype.keydown)

}(jQuery);

/* Modernizr 2.8.2 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-flexbox-flexboxlegacy-cssclasses-testprop-testallprops-domprefixes
 */
(function(e,t,n){function x(e){f.cssText=e}function T(e,t){return x(prefixes.join(e+";")+(t||""))}function N(e,t){return typeof e===t}function C(e,t){return!!~(""+e).indexOf(t)}function k(e,t){for(var r in e){var i=e[r];if(!C(i,"-")&&f[i]!==n){return t=="pfx"?i:true}}return false}function L(e,t,r){for(var i in e){var s=t[e[i]];if(s!==n){if(r===false)return e[i];if(N(s,"function")){return s.bind(r||t)}return s}}return false}function A(e,t,n){var r=e.charAt(0).toUpperCase()+e.slice(1),i=(e+" "+p.join(r+" ")+r).split(" ");if(N(t,"string")||N(t,"undefined")){return k(i,t)}else{i=(e+" "+d.join(r+" ")+r).split(" ");return L(i,t,n)}}var r="2.8.2",i={},s=true,o=t.documentElement,u="modernizr",a=t.createElement(u),f=a.style,l,c={}.toString,h="Webkit Moz O ms",p=h.split(" "),d=h.toLowerCase().split(" "),v={},m={},g={},y=[],b=y.slice,w,E={}.hasOwnProperty,S;if(!N(E,"undefined")&&!N(E.call,"undefined")){S=function(e,t){return E.call(e,t)}}else{S=function(e,t){return t in e&&N(e.constructor.prototype[t],"undefined")}}if(!Function.prototype.bind){Function.prototype.bind=function(t){var n=this;if(typeof n!="function"){throw new TypeError}var r=b.call(arguments,1),i=function(){if(this instanceof i){var e=function(){};e.prototype=n.prototype;var s=new e;var o=n.apply(s,r.concat(b.call(arguments)));if(Object(o)===o){return o}return s}else{return n.apply(t,r.concat(b.call(arguments)))}};return i}}v["flexbox"]=function(){return A("flexWrap")};v["csstransforms"]=function(){return!!A("transform")};for(var O in v){if(S(v,O)){w=O.toLowerCase();i[w]=v[O]();y.push((i[w]?"":"no-")+w)}}i.addTest=function(e,t){if(typeof e=="object"){for(var r in e){if(S(e,r)){i.addTest(r,e[r])}}}else{e=e.toLowerCase();if(i[e]!==n){return i}t=typeof t=="function"?t():t;if(typeof s!=="undefined"&&s){o.className+=" "+(t?"":"no-")+e}i[e]=t}return i};x("");a=l=null;(function(e,t){function c(e,t){var n=e.createElement("p"),r=e.getElementsByTagName("head")[0]||e.documentElement;n.innerHTML="x<style>"+t+"</style>";return r.insertBefore(n.lastChild,r.firstChild)}function h(){var e=y.elements;return typeof e=="string"?e.split(" "):e}function p(e){var t=f[e[u]];if(!t){t={};a++;e[u]=a;f[a]=t}return t}function d(e,n,r){if(!n){n=t}if(l){return n.createElement(e)}if(!r){r=p(n)}var o;if(r.cache[e]){o=r.cache[e].cloneNode()}else if(s.test(e)){o=(r.cache[e]=r.createElem(e)).cloneNode()}else{o=r.createElem(e)}return o.canHaveChildren&&!i.test(e)&&!o.tagUrn?r.frag.appendChild(o):o}function v(e,n){if(!e){e=t}if(l){return e.createDocumentFragment()}n=n||p(e);var r=n.frag.cloneNode(),i=0,s=h(),o=s.length;for(;i<o;i++){r.createElement(s[i])}return r}function m(e,t){if(!t.cache){t.cache={};t.createElem=e.createElement;t.createFrag=e.createDocumentFragment;t.frag=t.createFrag()}e.createElement=function(n){if(!y.shivMethods){return t.createElem(n)}return d(n,e,t)};e.createDocumentFragment=Function("h,f","return function(){"+"var n=f.cloneNode(),c=n.createElement;"+"h.shivMethods&&("+h().join().replace(/[\w\-]+/g,function(e){t.createElem(e);t.frag.createElement(e);return'c("'+e+'")'})+");return n}")(y,t.frag)}function g(e){if(!e){e=t}var n=p(e);if(y.shivCSS&&!o&&!n.hasCSS){n.hasCSS=!!c(e,"article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}"+"mark{background:#FF0;color:#000}"+"template{display:none}")}if(!l){m(e,n)}return e}var n="3.7.0";var r=e.html5||{};var i=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i;var s=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i;var o;var u="_html5shiv";var a=0;var f={};var l;(function(){try{var e=t.createElement("a");e.innerHTML="<xyz></xyz>";o="hidden"in e;l=e.childNodes.length==1||function(){t.createElement("a");var e=t.createDocumentFragment();return typeof e.cloneNode=="undefined"||typeof e.createDocumentFragment=="undefined"||typeof e.createElement=="undefined"}()}catch(n){o=true;l=true}})();var y={elements:r.elements||"abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",version:n,shivCSS:r.shivCSS!==false,supportsUnknownElements:l,shivMethods:r.shivMethods!==false,type:"default",shivDocument:g,createElement:d,createDocumentFragment:v};e.html5=y;g(t)})(this,t);i._version=r;i._domPrefixes=d;i._cssomPrefixes=p;i.testProp=function(e){return k([e])};i.testAllProps=A;o.className=o.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(s?" js "+y.join(" "):"");return i})(this,this.document)