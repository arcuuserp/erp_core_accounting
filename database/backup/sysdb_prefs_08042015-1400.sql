/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.39 : Database - sysdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sysdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sysdb`;

/*Table structure for table `preference` */

DROP TABLE IF EXISTS `preference`;

CREATE TABLE `preference` (
  `preference_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preferencename` varchar(200) DEFAULT NULL,
  `value` varchar(2000) DEFAULT NULL,
  `id_preferencegroup` int(10) unsigned DEFAULT NULL,
  `friendly_name` varchar(2000) DEFAULT NULL,
  `preference_help` tinyint(1) DEFAULT NULL,
  `trigger` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`preference_id`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=latin1;

/*Data for the table `preference` */

insert  into `preference`(`preference_id`,`preferencename`,`value`,`id_preferencegroup`,`friendly_name`,`preference_help`,`trigger`) values (4,'alert_past_entry_prior_to','10',2,'Lajmero nese regjistrimi i perket nje periudhe te kaluar me pare se',NULL,NULL),(5,'alert_future_entry_later_than','10',2,'Lajmero nese regjistrimi i perket nje periudhe te me vonshme me pas se',NULL,NULL),(7,'doc_date_print','0',1,'Printo daten ne dokumente',NULL,NULL),(8,'doc_time_print','0',1,'Printo oren ne dokumente',NULL,NULL),(9,'save_doc_before_print','0',1,'Ruaj dokumentin perpara se te printohet',NULL,NULL),(12,'alert_recipts_to_be_paid','1',3,'Lajmero, ',NULL,NULL),(13,'recipts_to_be_paid_in_days','3',3,'faturat duhen paguar brenda ',NULL,NULL),(15,'acc_purchase_discount','393',3,'Llogari zbritje nga blerjet',NULL,NULL),(16,'acc_purchase_discount_label','77 - TÃƒÂ« ardhura tÃƒÂ« tjera jo korente',3,NULL,NULL,NULL),(17,'sale_maturation_days','1',4,'Fatura e shitjes ka ',NULL,NULL),(18,'sale_maturation_days_value','3',4,'dite maturimi',NULL,NULL),(20,'acc_sale_discount','333',4,'Llogari zbritje nga shitjet',NULL,NULL),(21,'acc_sale_discount_label','67 - Shpenzime te tjera jo korente*',4,NULL,NULL,NULL),(22,'active_stock','1',5,'Inventari eshte aktiv',NULL,NULL),(28,'income_tax_rate','5',6,'Norma e tatim fitimit eshte',NULL,NULL),(29,'sales_tax','0',6,'Aplikon shoqeria tatim mbi shitjet?',NULL,NULL),(30,'vat_rate','1000',6,'Norma e TVSH eshte ',NULL,NULL),(32,'cash_flow_acc_classification',NULL,8,'Kliko me poshte per te bere klasifikimin e llogarive ne pasqyren Cash Flow',NULL,NULL),(33,'custom_vat_rate','50',6,NULL,NULL,NULL),(34,'password_strength','40',11,'Fortesia e Password-it ',NULL,NULL),(35,'password_reset_time','250.00',11,'Password-i duhet ndryshuar cdo ',NULL,NULL),(36,'password_reset_obligatory','0',11,'Ndryshimi i password-it eshte i detyrueshem.',NULL,NULL),(37,'fiscal_year_start_date','01-01',2,'Data e fillimit te vitit fiskal',NULL,NULL),(39,'different_new_password','0',11,'Password-i i ri duhet te jete i ndryshem nga ai i meparshmi.',NULL,NULL),(40,'acc_sales_payable_vat','138',4,'Llogari TVSH e Pagueshme',NULL,NULL),(41,'acc_sales_payable_vat_label','4457 - Shteti Ã¢â‚¬â€œ TVSH e pagueshme',4,NULL,NULL,NULL),(42,'acc_purchase_receivable_vat','137',3,'Llogari TVSH e Arketueshme',NULL,NULL),(43,'acc_purchase_receivable_vat_label','4456 - Shteti Ã¢â‚¬â€œ TVSH e zbritshme',3,NULL,NULL,NULL),(44,'acc_payment_discount','15',3,'Llogari zbritje ne pagese',NULL,NULL),(45,'acc_payment_discount_label','2 - Aktivet afatgjatÃƒÂ«',3,NULL,NULL,NULL),(46,'acc_payment_overdue_interest','359',3,'Llogari kamatvonese ne pagese',NULL,NULL),(47,'acc_payment_overdue_interest_label','7 - TÃƒÂ« ardhurat',3,NULL,NULL,NULL),(48,'acc_receipts_discount','73',4,'Llogari zbritje ne arketim',NULL,NULL),(49,'acc_receipts_discount_label','3 - InventarÃƒÂ«t',4,NULL,NULL,NULL),(50,'acc_receipts_overdue_interest','397',4,'Llogari kamatvonese ne arketim',NULL,NULL),(51,'acc_receipts_overdue_interest_label','8 - Llogarite e posacme',4,NULL,NULL,NULL),(52,'acc_exchange_loss','331',2,'Llogari per humbje nga kursi i kembimit',NULL,NULL),(53,'acc_exchange_loss_label','669 - Humbje nga kÃ«mbimet dhe perkthimet valutore',2,NULL,NULL,NULL),(54,'acc_exchange_gain','391',2,'Llogari per fitim nga kursi i kembimit',NULL,NULL),(55,'acc_exchange_gain_label','769 - Fitim nga kembimet valutore',2,NULL,NULL,NULL),(56,'acc_advanced_reciept','120',2,'Llogari per parapagime te dhena',NULL,NULL),(57,'acc_advanced_reciept_label','418 - Parapagime te dhena',2,NULL,NULL,NULL),(58,'acc_advanced_payment','114',2,'Llogari per parapagime te marra',NULL,NULL),(59,'acc_advanced_payment_label','409 - Parapagime te marra',2,NULL,NULL,NULL),(60,'month_working_hours','174',14,'Ore normale pune ne muaj',NULL,NULL),(61,'retirement_age_male','65',14,'Mosha e pensionit per meshkujt',NULL,NULL),(62,'retirement_age_female','55',14,'Mosha e pensionit per femrat',NULL,NULL),(63,'month_working_days','23',14,'Dite normale pune ne muaj',NULL,NULL),(64,'working_day_monday','1',14,'E Hene',NULL,NULL),(65,'working_day_tuesday','1',14,'E Marte',NULL,NULL),(66,'working_day_wednesday','1',14,'E Merkure',NULL,NULL),(67,'working_day_thursday','1',14,'E Enjte',NULL,NULL),(68,'working_day_friday','1',14,'E Premte',NULL,NULL),(69,'working_day_saturday','',14,'E Shtune',NULL,NULL),(70,'working_day_sunday','',14,'E Diel',NULL,NULL),(179,'acc_profit','14',15,'Llogari per rezultatin e vitit',NULL,NULL),(180,'acc_profit_label','109 - Rezultati i ushtrimit',15,NULL,NULL,NULL),(181,'acc_accumulated_earning','13',15,'Llogari Fitim/Humbje e pashperndare',NULL,NULL),(182,'acc_accumulated_earning_label','108 - Fitimi/Humbja e pashpërn',15,NULL,NULL,NULL),(183,'acc_asset_gain','395',16,'Fitim nga shitja e aktiveve',NULL,NULL),(184,'acc_asset_gain_label','772 - Të ardhura nga shitja e ',16,NULL,NULL,NULL),(185,'acc_asset_loss','334',16,'Vlera kontabel neto e aktiveve te nxjerra',NULL,NULL),(186,'acc_asset_loss_label','672 - Vlera kontabel e aktive ',16,NULL,NULL,NULL),(187,'acc_initial_creation','398',17,'Llogari per celjen fillestare',NULL,NULL),(188,'acc_initial_creation_label','890 - Bilanci i Çeljes',17,NULL,NULL,NULL),(189,'acc_vat_on_purchase','137',6,'TVSH ne blerje',NULL,NULL),(190,'acc_vat_on_purchase_label','4456 - Shteti – TVSH e zbritsh',6,NULL,NULL,NULL),(191,'acc_vat_on_sale','138',6,'TVSH ne shitje',NULL,NULL),(192,'acc_vat_on_sale_label','4457 - Shteti – TVSH e paguesh',6,NULL,NULL,NULL);

/*Table structure for table `preferencegroup` */

DROP TABLE IF EXISTS `preferencegroup`;

CREATE TABLE `preferencegroup` (
  `preferencegroup_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prefgroup_name` varchar(50) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `id_module` tinyint(3) unsigned DEFAULT NULL,
  `prefgroup_order` smallint(5) unsigned DEFAULT NULL,
  `id_preferencegroup` int(10) unsigned NOT NULL DEFAULT '0',
  `unique_name` varchar(30) DEFAULT NULL,
  `custom_ctrlaction` varchar(200) NOT NULL DEFAULT '',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`preferencegroup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `preferencegroup` */

insert  into `preferencegroup`(`preferencegroup_id`,`prefgroup_name`,`description`,`id_module`,`prefgroup_order`,`id_preferencegroup`,`unique_name`,`custom_ctrlaction`,`deleted`) values (1,'Te pergjithshme','Preferencat e kompanise per Sistemin.',4,1,0,'general','',0),(2,'Kontabiliteti','Preferencat e kompanise per Kontabilitetin.',4,2,0,'accounting','',0),(3,'Blerjet dhe Furnitoret','Preferencat e kompanise per Blerjet dhe Furnitoret.',4,3,0,'purchasesAndSuppliers','',0),(4,'Shitjet dhe Klientet','Preferencat e kompanise per Shitjet dhe Klientet.',4,4,0,'salesAndClients','',0),(6,'Tatimet','Preferencat e kompanise per Tatimet.',4,6,0,'taxes','',0),(7,'Monedhat','Preferencat e kompanise per Monedhat.',4,7,0,'currency','',0),(8,'Raportet','Preferencat e kompanise per Raportet.',4,8,0,'reports','',0),(9,'Qendra Kosto','Konfigurimi i Qendrave te Kostos',4,9,0,'costcenters','',0),(10,'Termat Tregtare','Termat Tregtar',4,10,0,'commercialterms','',0),(11,'Siguria','Siguria e sistemit.',4,11,0,'security','',0),(12,'Te Dhenat e Ndermarrjes','Informacion mbi ndermarrjes',4,1,0,'companyInfo','companyCRUD/showCompanyPreferences',0),(13,'Nr. Serial i Dokumentave','Numri ose seriali qe i vendoset dokumentave kur krijohen',4,13,0,'documentGroupSerial','prdocumentsCRUD/showDocGroupPreferences',0),(14,'Burime njerezore','Preferencat e kompanise per Burimet Njerezore',1,13,0,'humanresources','',1),(15,'Fitim/Humbje','Llogarite ne lidhje me fitimin dhe humbjen',4,14,2,'profitloss','',0),(16,'Kontabiliteti i aktiveve','Kontabiliteti i aktiveve',4,15,2,'assetAccounting','',0),(17,'Celje fillestare','Llogarite per celjen fillestare',4,16,2,'initialCreation','',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
