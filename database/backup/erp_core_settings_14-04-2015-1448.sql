/*
SQLyog Ultimate v11.11 (32 bit)
MySQL - 5.6.21 : Database - erp_core
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`erp_core` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `erp_core`;

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `value` varchar(2000) DEFAULT NULL,
  `id_settingsgroup` int(10) unsigned DEFAULT NULL,
  `lang_var` varchar(2000) DEFAULT NULL COMMENT 'translate variable code i18n',
  `trigger` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=193 DEFAULT CHARSET=latin1;

/*Data for the table `settings` */

insert  into `settings`(`id`,`name`,`value`,`id_settingsgroup`,`lang_var`,`trigger`) values (4,'alert_past_entry_prior_to','10',2,'Lajmero nese regjistrimi i perket nje periudhe te kaluar me pare se',NULL),(5,'alert_future_entry_later_than','10',2,'Lajmero nese regjistrimi i perket nje periudhe te me vonshme me pas se',NULL),(7,'doc_date_print','0',1,'Printo daten ne dokumente',NULL),(8,'doc_time_print','0',1,'Printo oren ne dokumente',NULL),(9,'save_doc_before_print','0',1,'Ruaj dokumentin perpara se te printohet',NULL),(12,'alert_recipts_to_be_paid','1',3,'Lajmero, ',NULL),(13,'recipts_to_be_paid_in_days','3',3,'faturat duhen paguar brenda ',NULL),(15,'acc_purchase_discount','393',3,'Llogari zbritje nga blerjet',NULL),(16,'acc_purchase_discount_label','77 - TÃƒÂ« ardhura tÃƒÂ« tjera jo korente',3,NULL,NULL),(17,'sale_maturation_days','1',4,'Fatura e shitjes ka ',NULL),(18,'sale_maturation_days_value','3',4,'dite maturimi',NULL),(20,'acc_sale_discount','333',4,'Llogari zbritje nga shitjet',NULL),(21,'acc_sale_discount_label','67 - Shpenzime te tjera jo korente*',4,NULL,NULL),(22,'active_stock','1',5,'Inventari eshte aktiv',NULL),(28,'income_tax_rate','5',6,'Norma e tatim fitimit eshte',NULL),(29,'sales_tax','0',6,'Aplikon shoqeria tatim mbi shitjet?',NULL),(30,'vat_rate','1000',6,'Norma e TVSH eshte ',NULL),(32,'cash_flow_acc_classification',NULL,8,'Kliko me poshte per te bere klasifikimin e llogarive ne pasqyren Cash Flow',NULL),(33,'custom_vat_rate','50',6,NULL,NULL),(34,'password_strength','40',11,'Fortesia e Password-it ',NULL),(35,'password_reset_time','250.00',11,'Password-i duhet ndryshuar cdo ',NULL),(36,'password_reset_obligatory','0',11,'Ndryshimi i password-it eshte i detyrueshem.',NULL),(37,'fiscal_year_start_date','01-01',2,'Data e fillimit te vitit fiskal',NULL),(39,'different_new_password','0',11,'Password-i i ri duhet te jete i ndryshem nga ai i meparshmi.',NULL),(40,'acc_sales_payable_vat','138',4,'Llogari TVSH e Pagueshme',NULL),(41,'acc_sales_payable_vat_label','4457 - Shteti Ã¢â‚¬â€œ TVSH e pagueshme',4,NULL,NULL),(42,'acc_purchase_receivable_vat','137',3,'Llogari TVSH e Arketueshme',NULL),(43,'acc_purchase_receivable_vat_label','4456 - Shteti Ã¢â‚¬â€œ TVSH e zbritshme',3,NULL,NULL),(44,'acc_payment_discount','15',3,'Llogari zbritje ne pagese',NULL),(45,'acc_payment_discount_label','2 - Aktivet afatgjatÃƒÂ«',3,NULL,NULL),(46,'acc_payment_overdue_interest','359',3,'Llogari kamatvonese ne pagese',NULL),(47,'acc_payment_overdue_interest_label','7 - TÃƒÂ« ardhurat',3,NULL,NULL),(48,'acc_receipts_discount','73',4,'Llogari zbritje ne arketim',NULL),(49,'acc_receipts_discount_label','3 - InventarÃƒÂ«t',4,NULL,NULL),(50,'acc_receipts_overdue_interest','397',4,'Llogari kamatvonese ne arketim',NULL),(51,'acc_receipts_overdue_interest_label','8 - Llogarite e posacme',4,NULL,NULL),(52,'acc_exchange_loss','331',2,'Llogari per humbje nga kursi i kembimit',NULL),(53,'acc_exchange_loss_label','669 - Humbje nga kÃ«mbimet dhe perkthimet valutore',2,NULL,NULL),(54,'acc_exchange_gain','391',2,'Llogari per fitim nga kursi i kembimit',NULL),(55,'acc_exchange_gain_label','769 - Fitim nga kembimet valutore',2,NULL,NULL),(56,'acc_advanced_reciept','120',2,'Llogari per parapagime te dhena',NULL),(57,'acc_advanced_reciept_label','418 - Parapagime te dhena',2,NULL,NULL),(58,'acc_advanced_payment','114',2,'Llogari per parapagime te marra',NULL),(59,'acc_advanced_payment_label','409 - Parapagime te marra',2,NULL,NULL),(60,'month_working_hours','174',14,'Ore normale pune ne muaj',NULL),(61,'retirement_age_male','65',14,'Mosha e pensionit per meshkujt',NULL),(62,'retirement_age_female','55',14,'Mosha e pensionit per femrat',NULL),(63,'month_working_days','23',14,'Dite normale pune ne muaj',NULL),(64,'working_day_monday','1',14,'E Hene',NULL),(65,'working_day_tuesday','1',14,'E Marte',NULL),(66,'working_day_wednesday','1',14,'E Merkure',NULL),(67,'working_day_thursday','1',14,'E Enjte',NULL),(68,'working_day_friday','1',14,'E Premte',NULL),(69,'working_day_saturday','',14,'E Shtune',NULL),(70,'working_day_sunday','',14,'E Diel',NULL),(179,'acc_profit','14',15,'Llogari per rezultatin e vitit',NULL),(180,'acc_profit_label','109 - Rezultati i ushtrimit',15,NULL,NULL),(181,'acc_accumulated_earning','13',15,'Llogari Fitim/Humbje e pashperndare',NULL),(182,'acc_accumulated_earning_label','108 - Fitimi/Humbja e pashpërn',15,NULL,NULL),(183,'acc_asset_gain','395',16,'Fitim nga shitja e aktiveve',NULL),(184,'acc_asset_gain_label','772 - Të ardhura nga shitja e ',16,NULL,NULL),(185,'acc_asset_loss','334',16,'Vlera kontabel neto e aktiveve te nxjerra',NULL),(186,'acc_asset_loss_label','672 - Vlera kontabel e aktive ',16,NULL,NULL),(187,'acc_initial_creation','398',17,'Llogari per celjen fillestare',NULL),(188,'acc_initial_creation_label','890 - Bilanci i Çeljes',17,NULL,NULL),(189,'acc_vat_on_purchase','137',6,'TVSH ne blerje',NULL),(190,'acc_vat_on_purchase_label','4456 - Shteti – TVSH e zbritsh',6,NULL,NULL),(191,'acc_vat_on_sale','138',6,'TVSH ne shitje',NULL),(192,'acc_vat_on_sale_label','4457 - Shteti – TVSH e paguesh',6,NULL,NULL);

/*Table structure for table `settingsgroup` */

DROP TABLE IF EXISTS `settingsgroup`;

CREATE TABLE `settingsgroup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `id_module` tinyint(3) unsigned DEFAULT NULL,
  `order` smallint(5) unsigned DEFAULT NULL,
  `id_settingsgroup` int(10) unsigned NOT NULL DEFAULT '0',
  `code` varchar(30) DEFAULT NULL,
  `custom_ctrlaction` varchar(200) NOT NULL DEFAULT '',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `settingsgroup` */

insert  into `settingsgroup`(`id`,`name`,`description`,`id_module`,`order`,`id_settingsgroup`,`code`,`custom_ctrlaction`,`deleted`) values (1,'Te pergjithshme','Preferencat e kompanise per Sistemin.',4,1,0,'generals','',0),(2,'Kontabiliteti','Preferencat e kompanise per Kontabilitetin.',4,2,0,'accounting','',0),(3,'Blerjet dhe Furnitoret','Preferencat e kompanise per Blerjet dhe Furnitoret.',4,3,0,'suppliers','',0),(4,'Shitjet dhe Klientet','Preferencat e kompanise per Shitjet dhe Klientet.',4,4,0,'customers','',0),(6,'Tatimet','Preferencat e kompanise per Tatimet.',4,6,0,'taxes','',0),(7,'Monedhat','Preferencat e kompanise per Monedhat.',4,7,0,'currency','',0),(8,'Raportet','Preferencat e kompanise per Raportet.',4,8,0,'reports','',0),(9,'Qendra Kosto','Konfigurimi i Qendrave te Kostos',4,9,0,'costcenters','',0),(10,'Termat Tregtare','Termat Tregtar',4,10,0,'commercialterms','',0),(11,'Siguria','Siguria e sistemit.',4,11,0,'security','',0),(12,'Te Dhenat e Ndermarrjes','Informacion mbi ndermarrjes',4,1,0,'companydetails','companyCRUD/showCompanyPreferences',0),(13,'Nr. Serial i Dokumentave','Numri ose seriali qe i vendoset dokumentave kur krijohen',4,13,0,'documentnumbers','prdocumentsCRUD/showDocGroupPreferences',0),(15,'Fitim/Humbje','Llogarite ne lidhje me fitimin dhe humbjen',4,14,2,'profitloss','',0),(16,'Kontabiliteti i aktiveve','Kontabiliteti i aktiveve',4,15,2,'assets','',0),(17,'Celje fillestare','Llogarite per celjen fillestare',4,16,2,'initialcreation','',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
