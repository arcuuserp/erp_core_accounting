/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.39 : Database - erp_accounting
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`erp_accounting` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `erp_accounting`;

/*Table structure for table `payment_rate` */

DROP TABLE IF EXISTS `payment_rate`;

CREATE TABLE `payment_rate` (
  `payment_rate_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_rate_agreement` bigint(20) NOT NULL,
  `serial_number` varchar(400) NOT NULL,
  `document_number` varchar(400) NOT NULL,
  `document_date` datetime NOT NULL,
  `rate_date` datetime NOT NULL,
  `rate_date_al` varchar(10) NOT NULL,
  `due_date` datetime NOT NULL,
  `due_date_al` varchar(10) NOT NULL,
  `id_commercialterms` int(11) NOT NULL,
  `rate_amount` decimal(12,2) NOT NULL,
  `rate_amount_for_interest_calc` decimal(12,2) NOT NULL,
  `id_customer` bigint(20) unsigned NOT NULL,
  `id_currency` int(11) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `exchange_rate` decimal(10,2) NOT NULL,
  PRIMARY KEY (`payment_rate_id`),
  KEY `id_rate_agreement` (`id_rate_agreement`),
  KEY `id_customer` (`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payment_rate` */

/*Table structure for table `payment_rate_term_calc` */

DROP TABLE IF EXISTS `payment_rate_term_calc`;

CREATE TABLE `payment_rate_term_calc` (
  `payment_rate_term_calc_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_payment_rate` bigint(20) unsigned NOT NULL,
  `id_commercialterms` int(10) unsigned NOT NULL,
  `id_customer` bigint(20) unsigned NOT NULL,
  `total` decimal(12,2) NOT NULL,
  `calculation_date` datetime NOT NULL,
  `max_interestamt_reached` tinyint(1) NOT NULL,
  `home_cur_total` decimal(12,2) NOT NULL,
  `posted_to_gl` tinyint(1) NOT NULL,
  PRIMARY KEY (`payment_rate_term_calc_id`),
  KEY `id_payment_rate` (`id_payment_rate`,`id_commercialterms`),
  KEY `id_customer` (`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payment_rate_term_calc` */

/*Table structure for table `rate_agreement` */

DROP TABLE IF EXISTS `rate_agreement`;

CREATE TABLE `rate_agreement` (
  `rate_agreement_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `system_date` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  `agreement_date` datetime NOT NULL,
  `rate_payments_start_date` datetime NOT NULL,
  `id_customer` bigint(20) NOT NULL,
  `total_amount` decimal(12,2) NOT NULL,
  `id_itemfrequency` int(11) NOT NULL,
  `total_rates` int(11) NOT NULL,
  `id_commercialterms` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `exchange_rate` decimal(10,2) NOT NULL,
  PRIMARY KEY (`rate_agreement_id`),
  KEY `id_customer` (`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rate_agreement` */

/*Table structure for table `rate_agreement_invoices` */

DROP TABLE IF EXISTS `rate_agreement_invoices`;

CREATE TABLE `rate_agreement_invoices` (
  `rate_agreement_invoices_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_rate_agreement` bigint(20) NOT NULL,
  `id_saleinvoiceheader` varchar(50) NOT NULL,
  PRIMARY KEY (`rate_agreement_invoices_id`),
  KEY `id_rate_agreement` (`id_rate_agreement`,`id_saleinvoiceheader`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rate_agreement_invoices` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
