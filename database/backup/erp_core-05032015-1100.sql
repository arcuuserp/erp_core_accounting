/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.39 : Database - erp_core
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`erp_core` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `erp_core`;

/*Table structure for table `administrativeregions` */

DROP TABLE IF EXISTS `administrativeregions`;

CREATE TABLE `administrativeregions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(400) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

/*Data for the table `administrativeregions` */

insert  into `administrativeregions`(`id`,`name`,`deleted`) values (1,'Berat',0),(2,'Bulqize',0),(3,'Delvine',0),(4,'Devoll',0),(5,'Diber',0),(6,'Durres',0),(7,'Elbasan',0),(8,'Fier',0),(9,'Gjirokaster',0),(10,'Gramsh',0),(11,'Has',0),(12,'Kavaje',0),(13,'Kolonje',0),(14,'Korce',0),(15,'Kruje',0),(16,'Kucove',0),(17,'Kukes',0),(18,'Kurbin',0),(19,'Lezh',0),(20,'Librazhd',0),(21,'Lushnje',0),(22,'Malesi e Madhe',0),(23,'Mallakaster',0),(24,'Mat',0),(25,'Mirdite',0),(26,'Peqin',0),(27,'Permet',0),(28,'Pogradec',0),(29,'Puke',0),(30,'Sarande',0),(31,'Shkoder',0),(32,'Skrapar',0),(33,'Tepelene',0),(34,'Tirane',0),(35,'Tropoje',0),(36,'Vlore',0);

/*Table structure for table `bankaccounts` */

DROP TABLE IF EXISTS `bankaccounts`;

CREATE TABLE `bankaccounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subject_id` bigint(20) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `bank_name` varchar(200) DEFAULT NULL,
  `account_number` varchar(200) DEFAULT NULL,
  `iban` varchar(200) DEFAULT NULL,
  `id_currency` tinyint(4) DEFAULT NULL,
  `currency` varchar(20) NOT NULL,
  `default` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `bankaccounts` */

insert  into `bankaccounts`(`id`,`subject_id`,`subject`,`bank_name`,`account_number`,`iban`,`id_currency`,`currency`,`default`,`deleted`) values (4,2,'supplier','BKT','222','IBAN',3,'EUR',1,0),(5,896,'supplier','reifeissen','777','888',2,'ALL',1,0),(9,3,'supplier','re','1234','234',2,'ALL',1,0),(15,4,'supplier','bkty','1232','12312',1,'USD',1,0),(19,5,'supplier','reifeissen','123123','123123',2,'ALL',1,0),(20,2,'customer','bkt','123','1231',2,'ALL',1,0),(21,1,'company','Reifadflsajfd','32432','21234234',2,'ALL',1,0),(22,1,'company','Banka','234234234','234324324',2,'ALL',0,0),(23,1,'company','Reiffaisen','3434545','290424092',6,'AUD',0,0),(24,138,'supplier','BKT','403139982','IBAN',2,'ALL',1,0),(25,138,'supplier','Tirana ','Nr.Llogarise','0100-301249-101CB',2,'ALL',0,0),(26,138,'supplier','Raiffeisen','Nr.Llogarise','AL 66 2025 2003 00000000 0187 5913',2,'ALL',0,0);

/*Table structure for table `company` */

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(400) DEFAULT NULL,
  `company_nipt` varchar(400) DEFAULT NULL,
  `company_address` varchar(400) DEFAULT NULL,
  `phonenumber` varchar(400) DEFAULT NULL,
  `fax` varchar(400) DEFAULT NULL,
  `email` varchar(400) DEFAULT NULL,
  `id_industrycategory` bigint(20) DEFAULT NULL,
  `id_organisationform` int(11) DEFAULT NULL,
  `id_taxpayerstatus` tinyint(4) DEFAULT NULL,
  `date_activity_start` date DEFAULT NULL,
  `id_administrativeregion` tinyint(4) DEFAULT NULL,
  `activity_detail` varchar(400) DEFAULT NULL,
  `id_currency` tinyint(4) NOT NULL DEFAULT '0',
  `currency` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`,`id_currency`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `company` */

insert  into `company`(`id`,`company_name`,`company_nipt`,`company_address`,`phonenumber`,`fax`,`email`,`id_industrycategory`,`id_organisationform`,`id_taxpayerstatus`,`date_activity_start`,`id_administrativeregion`,`activity_detail`,`id_currency`,`currency`) values (1,'Ujësjellës Kanalizime Sh.a. Durrës','J61819502V','Durres','+35552222222 ','+35552222222 ','info@ukdurres.com ',10,4,2,'2011-01-01',6,'Furnizimi me Uje te pijshem ',2,'ALL');

/*Table structure for table `currencies` */

DROP TABLE IF EXISTS `currencies`;

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `monedha_01` (`id`),
  KEY `monedha_02` (`name`,`deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=288 DEFAULT CHARSET=utf8;

/*Data for the table `currencies` */

insert  into `currencies`(`id`,`name`,`description`,`created_at`,`updated_at`,`active`,`deleted`) values (1,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(2,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(3,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(4,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(5,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(6,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(7,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(8,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(9,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(10,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(11,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(12,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(13,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(14,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(15,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(16,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(17,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(18,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(19,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(20,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(21,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(22,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(23,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(24,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(25,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(26,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(27,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(28,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(29,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(30,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(31,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(32,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(33,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(34,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(35,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(36,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(37,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(38,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(39,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(40,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(41,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(42,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(43,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(44,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(45,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(46,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(47,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(48,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(49,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(50,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(51,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(52,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(53,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(54,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(55,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(56,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(57,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(58,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(59,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(60,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(61,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(62,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(63,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(64,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(65,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(66,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(67,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(68,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(69,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(70,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(71,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(72,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(73,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(74,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(75,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(76,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(77,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(78,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(79,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(80,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(81,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(82,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(83,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(84,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(85,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(86,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(87,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(88,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(89,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(90,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(91,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(92,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(93,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(94,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(95,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(96,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(97,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(98,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(99,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(100,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(101,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(102,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(103,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(104,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(105,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(106,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(107,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(108,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(109,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(110,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(111,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(112,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(113,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(114,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(115,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(116,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(117,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(118,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(119,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(120,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(121,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(122,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(123,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(124,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(125,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(126,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(127,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(128,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(129,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(130,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(131,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(132,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(133,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(134,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(135,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(136,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(137,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(138,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(139,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(140,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(141,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(142,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(143,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(144,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(145,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(146,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(147,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(148,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(149,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(150,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(151,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(152,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(153,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(154,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(155,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(156,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(157,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(158,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(159,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(160,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(161,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(162,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(163,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(164,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(165,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(166,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(167,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(168,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(169,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(170,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(171,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(172,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(173,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(174,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(175,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(176,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(177,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(178,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(179,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(180,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(181,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(182,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(183,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(184,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(185,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(186,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(187,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(188,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(189,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(190,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(191,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(192,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(193,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(194,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(195,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(196,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(197,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(198,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(199,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(200,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(201,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(202,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(203,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(204,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(205,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(206,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(207,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(208,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(209,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(210,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(211,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(212,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(213,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(214,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(215,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(216,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(217,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(218,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(219,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(220,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(221,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(222,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(223,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(224,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(225,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(226,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(227,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(228,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(229,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(230,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(231,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(232,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(233,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(234,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(235,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(236,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(237,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(238,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(239,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(240,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(241,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(242,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(243,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(244,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(245,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(246,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(247,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(248,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(249,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(250,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(251,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(252,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(253,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(254,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(255,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(256,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(257,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(258,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(259,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(260,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(261,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(262,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(263,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(264,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(265,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(266,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(267,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(268,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(269,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(270,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(271,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(272,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(273,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(274,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(275,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(276,'USD','Dollari amerikan',NULL,'2012-02-01 18:40:26',1,0),(277,'ALL','Leku shqiptar',NULL,'2012-02-01 18:40:26',1,0),(278,'EUR','Monedha e përbashkët europiane',NULL,'2012-02-01 18:40:26',1,0),(279,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,1),(280,'CAD','Dollari kanadez',NULL,'2012-02-01 18:40:26',0,0),(281,'AUD','Dollari australian',NULL,'2012-02-01 18:40:26',0,0),(282,'YEN','Jeni japonez',NULL,'2012-02-01 18:40:26',0,0),(283,'GBP','Paundi britanik',NULL,'2012-02-01 18:40:26',0,0),(284,'SEK','Korona suedeze',NULL,'2012-02-01 18:40:26',0,0),(285,'CHF','Franga zviceriane',NULL,'2012-02-01 18:40:26',0,0),(286,'TRY','Lira turke',NULL,'2012-02-01 18:40:26',0,0),(287,'aff','Letter when we complete a new application - corporate','2015-02-18 16:45:45','2015-02-18 16:45:45',1,0);

/*Table structure for table `currencies_good` */

DROP TABLE IF EXISTS `currencies_good`;

CREATE TABLE `currencies_good` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `monedha_01` (`id`) USING BTREE,
  KEY `monedha_02` (`name`,`deleted`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Data for the table `currencies_good` */

insert  into `currencies_good`(`id`,`name`,`description`,`created_at`,`updated_at`,`active`,`deleted`) values (1,'USD','Dollari amerikan','2015-02-19 13:15:18','2012-02-01 18:40:26',1,0),(2,'ALL','Leku shqiptar','2015-02-19 13:15:18','2015-02-23 09:35:25',1,0),(3,'EUR','Monedha e përbashkët europiane','2015-02-19 13:15:18','2012-02-01 18:40:26',1,0),(4,'CAD','Dollari kanadez','2015-02-19 13:15:18','2012-02-01 18:40:26',0,1),(5,'CAD','Dollari kanadez','2015-02-19 13:15:18','2012-02-01 18:40:26',0,0),(6,'AUD','Dollari australian','2015-02-19 13:15:18','2012-02-01 18:40:26',0,0),(7,'YEN','Jeni japonez','2015-02-19 13:15:18','2012-02-01 18:40:26',0,0),(8,'GBP','Paundi britanik','2015-02-19 13:15:18','2012-02-01 18:40:26',0,0),(9,'SEK','Korona suedeze','2015-02-19 13:15:18','2012-02-01 18:40:26',0,0),(10,'CHF','Franga zviceriane','2015-02-19 13:15:18','2012-02-01 18:40:26',0,0),(11,'TRY','Lira turke','2015-02-19 13:15:18','2015-02-20 14:51:10',0,0),(20,'TST3','Test','2015-02-19 13:15:18','2015-02-19 19:14:58',1,0),(21,'TEST','Some test','2015-02-19 13:16:15','2015-02-19 13:16:15',1,0),(22,'ACE','adfsadfsadfs saldfj aslkfjs adlfk','2015-02-19 19:14:28','2015-02-19 19:14:28',1,0),(23,'asdfsaf','asdfsaf','2015-02-19 19:22:43','2015-02-19 19:22:43',1,0),(24,'asdf','asdf','2015-02-19 20:26:32','2015-02-19 20:24:36',1,0),(25,'asdf','asdf','2015-02-19 20:26:52','2015-02-19 20:26:52',1,0);

/*Table structure for table `exchangeratecategories` */

DROP TABLE IF EXISTS `exchangeratecategories`;

CREATE TABLE `exchangeratecategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoryname` varchar(200) NOT NULL,
  `description` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `exchangeratecategories` */

insert  into `exchangeratecategories`(`id`,`categoryname`,`description`) values (1,'Arka','Kursi per veprimet me arken'),(2,'Banka','Kursi per veprimet me banken'),(3,'Klientet','Kursi per veprimet me klientet'),(4,'Furnitoret','Kursi per veprimet me furnitoret'),(5,'Inventari','Kursi per celjen e artikujve'),(6,'Per Kontabilitetin','Kursi per te gjitha veprimet kontabel');

/*Table structure for table `exchangerates` */

DROP TABLE IF EXISTS `exchangerates`;

CREATE TABLE `exchangerates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_exchangeratecategories` int(10) unsigned NOT NULL,
  `categoryname` varchar(200) NOT NULL,
  `idrelative_currency` int(10) unsigned NOT NULL,
  `relative_currency` varchar(200) NOT NULL,
  `idbase_currency` int(10) unsigned NOT NULL,
  `base_currency` varchar(200) NOT NULL,
  `buy_rate` decimal(20,4) NOT NULL,
  `sell_rate` decimal(20,4) NOT NULL,
  `rate_date` datetime NOT NULL,
  `modifieddate` datetime NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `kurskembimi_01` (`id`) USING BTREE,
  KEY `kurskembimi_02` (`idrelative_currency`,`idbase_currency`,`rate_date`,`deleted`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

/*Data for the table `exchangerates` */

insert  into `exchangerates`(`id`,`id_exchangeratecategories`,`categoryname`,`idrelative_currency`,`relative_currency`,`idbase_currency`,`base_currency`,`buy_rate`,`sell_rate`,`rate_date`,`modifieddate`,`deleted`) values (1,1,'Arka',3,'EUR',2,'ALL',139.0000,140.0000,'2013-04-05 00:00:00','2013-04-05 20:15:11',0),(2,3,'Klientet',3,'EUR',2,'ALL',138.0000,140.0000,'2013-04-05 00:00:00','2013-04-05 20:15:29',0),(3,4,'Furnitoret',3,'EUR',2,'ALL',138.0000,140.0000,'2013-04-05 00:00:00','2013-04-05 20:15:46',0),(4,5,'Inventari',3,'EUR',2,'ALL',138.0000,140.0000,'2013-04-05 00:00:00','2013-04-05 20:16:04',0),(5,5,'Inventari',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-06 17:08:51','0000-00-00 00:00:00',0),(6,5,'Inventari',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-06 17:09:45','0000-00-00 00:00:00',0),(7,5,'Inventari',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-06 17:10:13','0000-00-00 00:00:00',0),(8,5,'Inventari',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-06 18:02:04','0000-00-00 00:00:00',0),(9,5,'Inventari',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-06 18:12:01','0000-00-00 00:00:00',0),(10,5,'Inventari',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-06 18:17:58','0000-00-00 00:00:00',0),(11,5,'Inventari',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-06 19:26:18','0000-00-00 00:00:00',0),(12,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-06 19:35:32','0000-00-00 00:00:00',0),(13,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-06 19:35:33','0000-00-00 00:00:00',0),(14,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-06 19:36:57','0000-00-00 00:00:00',0),(15,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-07 16:35:50','0000-00-00 00:00:00',0),(16,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-07 16:35:52','0000-00-00 00:00:00',0),(17,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-07 16:35:53','0000-00-00 00:00:00',0),(18,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-07 16:36:20','0000-00-00 00:00:00',0),(19,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-07 16:40:56','0000-00-00 00:00:00',0),(20,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-07 16:40:56','0000-00-00 00:00:00',0),(21,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-07 16:41:08','0000-00-00 00:00:00',0),(22,1,'Arka',3,'EUR',2,'ALL',122.0000,122.0000,'2013-04-07 17:11:22','0000-00-00 00:00:00',0),(23,1,'Arka',3,'EUR',2,'ALL',122.0000,122.0000,'2013-04-07 17:11:22','0000-00-00 00:00:00',0),(24,1,'Arka',3,'EUR',2,'ALL',122.0000,122.0000,'2013-04-07 17:11:32','0000-00-00 00:00:00',0),(25,1,'Arka',1,'USD',2,'ALL',122.0000,122.0000,'2013-04-07 17:13:22','0000-00-00 00:00:00',0),(26,1,'Arka',1,'USD',2,'ALL',122.0000,122.0000,'2013-04-07 17:13:24','0000-00-00 00:00:00',0),(27,1,'Arka',1,'USD',2,'ALL',122.0000,122.0000,'2013-04-07 17:13:25','0000-00-00 00:00:00',0),(28,1,'Arka',1,'USD',2,'ALL',122.0000,122.0000,'2013-04-07 17:14:08','0000-00-00 00:00:00',0),(29,1,'Arka',1,'USD',2,'ALL',122.0000,122.0000,'2013-04-07 17:14:09','0000-00-00 00:00:00',0),(30,1,'Arka',3,'EUR',2,'ALL',122.0000,122.0000,'2013-04-07 17:14:22','0000-00-00 00:00:00',0),(31,1,'Arka',3,'EUR',2,'ALL',122.0000,122.0000,'2013-04-07 17:14:22','0000-00-00 00:00:00',0),(32,1,'Arka',3,'EUR',2,'ALL',122.0000,122.0000,'2013-04-07 17:15:39','0000-00-00 00:00:00',0),(33,1,'Arka',3,'EUR',2,'ALL',122.0000,122.0000,'2013-04-07 17:15:39','0000-00-00 00:00:00',0),(34,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-07 17:18:38','0000-00-00 00:00:00',0),(35,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-07 17:18:38','0000-00-00 00:00:00',0),(36,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-09 09:26:13','0000-00-00 00:00:00',0),(37,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-09 09:26:13','0000-00-00 00:00:00',0),(38,1,'Arka',3,'EUR',2,'ALL',122.0000,122.0000,'2013-04-09 10:17:30','0000-00-00 00:00:00',0),(39,1,'Arka',3,'EUR',2,'ALL',122.0000,122.0000,'2013-04-09 10:17:30','0000-00-00 00:00:00',0),(40,1,'Arka',3,'EUR',2,'ALL',122.0000,122.0000,'2013-04-09 10:23:41','0000-00-00 00:00:00',0),(41,1,'Arka',3,'EUR',2,'ALL',122.0000,122.0000,'2013-04-09 10:23:41','0000-00-00 00:00:00',0),(42,1,'Arka',3,'EUR',2,'ALL',134.0000,134.0000,'2013-04-09 14:40:56','0000-00-00 00:00:00',0),(43,1,'Arka',3,'EUR',2,'ALL',134.0000,134.0000,'2013-04-09 14:40:56','0000-00-00 00:00:00',0),(44,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-09 15:17:56','0000-00-00 00:00:00',0),(45,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-09 15:17:56','0000-00-00 00:00:00',0),(46,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-09 15:20:19','0000-00-00 00:00:00',0),(47,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-09 15:20:19','0000-00-00 00:00:00',0),(48,1,'Arka',3,'EUR',2,'ALL',142.0000,142.0000,'2013-04-09 15:20:30','0000-00-00 00:00:00',0),(49,1,'Arka',3,'EUR',2,'ALL',133.0000,133.0000,'2013-04-10 11:37:30','0000-00-00 00:00:00',0),(50,1,'Arka',3,'EUR',2,'ALL',133.0000,133.0000,'2013-04-10 11:37:30','0000-00-00 00:00:00',0),(51,1,'Arka',3,'EUR',2,'ALL',144.0000,144.0000,'2013-04-10 11:38:09','0000-00-00 00:00:00',0),(52,1,'Arka',3,'EUR',2,'ALL',144.0000,144.0000,'2013-04-10 11:38:09','0000-00-00 00:00:00',0),(53,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-10 11:41:28','0000-00-00 00:00:00',0),(54,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-10 11:41:28','0000-00-00 00:00:00',0),(55,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-10 14:46:27','0000-00-00 00:00:00',0),(56,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-10 14:46:27','0000-00-00 00:00:00',0),(57,1,'Arka',3,'EUR',2,'ALL',111.0000,111.0000,'2013-04-10 14:53:55','0000-00-00 00:00:00',0),(58,1,'Arka',3,'EUR',2,'ALL',111.0000,111.0000,'2013-04-10 14:53:55','0000-00-00 00:00:00',0),(59,1,'Arka',3,'EUR',2,'ALL',133.0000,133.0000,'2013-04-10 15:03:50','0000-00-00 00:00:00',0),(60,1,'Arka',3,'EUR',2,'ALL',133.0000,133.0000,'2013-04-10 15:03:50','0000-00-00 00:00:00',0),(61,1,'Arka',3,'EUR',2,'ALL',144.0000,144.0000,'2013-04-10 15:13:32','0000-00-00 00:00:00',0),(62,1,'Arka',3,'EUR',2,'ALL',144.0000,144.0000,'2013-04-10 15:13:32','0000-00-00 00:00:00',0),(63,1,'Arka',3,'EUR',2,'ALL',133.0000,133.0000,'2013-04-10 15:42:28','0000-00-00 00:00:00',0),(64,1,'Arka',3,'EUR',2,'ALL',133.0000,133.0000,'2013-04-10 15:42:28','0000-00-00 00:00:00',0),(65,1,'Arka',3,'EUR',2,'ALL',144.0000,144.0000,'2013-04-10 15:50:57','0000-00-00 00:00:00',0),(66,1,'Arka',3,'EUR',2,'ALL',144.0000,144.0000,'2013-04-10 15:50:57','0000-00-00 00:00:00',0),(67,1,'Arka',3,'EUR',2,'ALL',144.0000,144.0000,'2013-04-10 16:05:43','0000-00-00 00:00:00',0),(68,1,'Arka',3,'EUR',2,'ALL',144.0000,144.0000,'2013-04-10 16:05:44','0000-00-00 00:00:00',0),(69,1,'Arka',3,'EUR',2,'ALL',144.0000,144.0000,'2013-04-10 18:25:58','0000-00-00 00:00:00',0),(70,1,'Arka',3,'EUR',2,'ALL',144.0000,144.0000,'2013-04-10 18:25:58','0000-00-00 00:00:00',0),(71,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-10 18:26:08','0000-00-00 00:00:00',0),(72,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-10 18:26:43','0000-00-00 00:00:00',0),(73,1,'Arka',3,'EUR',2,'ALL',143.0000,143.0000,'2013-04-10 18:28:41','0000-00-00 00:00:00',0),(74,1,'Arka',3,'EUR',2,'ALL',143.0000,143.0000,'2013-04-10 18:28:42','0000-00-00 00:00:00',0),(75,1,'Arka',3,'EUR',2,'ALL',142.0000,142.0000,'2013-04-10 18:33:17','0000-00-00 00:00:00',0),(76,1,'Arka',3,'EUR',2,'ALL',142.0000,142.0000,'2013-04-10 18:33:17','0000-00-00 00:00:00',0),(77,1,'Arka',3,'EUR',2,'ALL',142.0000,142.0000,'2013-04-10 18:33:34','0000-00-00 00:00:00',0),(78,1,'Arka',3,'EUR',2,'ALL',144.0000,144.0000,'2013-04-10 18:34:46','0000-00-00 00:00:00',0),(79,1,'Arka',3,'EUR',2,'ALL',144.0000,144.0000,'2013-04-10 18:34:46','0000-00-00 00:00:00',0),(80,1,'Arka',3,'EUR',2,'ALL',141.0000,141.0000,'2013-04-10 18:36:55','0000-00-00 00:00:00',0),(81,1,'Arka',3,'EUR',2,'ALL',141.0000,141.0000,'2013-04-10 18:36:55','0000-00-00 00:00:00',0),(82,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-10 18:38:45','0000-00-00 00:00:00',0),(83,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-10 18:38:45','0000-00-00 00:00:00',0),(84,1,'Arka',3,'EUR',2,'ALL',141.0000,141.0000,'2013-04-10 18:42:00','0000-00-00 00:00:00',0),(85,1,'Arka',3,'EUR',2,'ALL',141.0000,141.0000,'2013-04-10 18:42:00','0000-00-00 00:00:00',0),(86,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-10 18:46:04','0000-00-00 00:00:00',0),(87,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-10 18:46:04','0000-00-00 00:00:00',0),(88,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-10 18:46:16','0000-00-00 00:00:00',0),(89,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-13 15:01:15','0000-00-00 00:00:00',0),(90,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-13 15:01:16','0000-00-00 00:00:00',0),(91,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-13 15:03:40','0000-00-00 00:00:00',0),(92,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-13 15:03:40','0000-00-00 00:00:00',0),(93,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-13 15:03:55','0000-00-00 00:00:00',0),(94,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-13 15:12:59','0000-00-00 00:00:00',0),(95,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-13 15:12:59','0000-00-00 00:00:00',0),(96,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-13 15:13:29','0000-00-00 00:00:00',0),(97,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-13 15:13:29','0000-00-00 00:00:00',0),(98,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-13 15:13:40','0000-00-00 00:00:00',0),(99,5,'Inventari',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-18 17:27:52','0000-00-00 00:00:00',0),(100,5,'Inventari',3,'EUR',2,'ALL',139.0000,139.0000,'2013-04-18 17:45:44','0000-00-00 00:00:00',0),(101,1,'Arka',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-18 17:54:13','0000-00-00 00:00:00',0),(102,5,'Inventari',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-19 20:12:17','0000-00-00 00:00:00',0),(103,5,'Inventari',3,'EUR',2,'ALL',139.3300,139.3300,'2013-04-25 11:55:18','0000-00-00 00:00:00',0),(104,5,'Inventari',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-25 11:59:23','0000-00-00 00:00:00',0),(105,5,'Inventari',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-25 17:49:01','0000-00-00 00:00:00',0),(106,5,'Inventari',3,'EUR',2,'ALL',140.0000,140.0000,'2013-04-25 22:15:23','0000-00-00 00:00:00',0),(107,5,'Inventari',3,'EUR',2,'ALL',142.0000,142.0000,'2013-04-26 11:32:34','0000-00-00 00:00:00',0),(108,5,'Inventari',3,'EUR',2,'ALL',144.0000,144.0000,'2013-04-26 11:33:36','0000-00-00 00:00:00',0),(109,5,'Inventari',3,'EUR',2,'ALL',134.0000,134.0000,'2013-04-26 14:01:00','0000-00-00 00:00:00',0),(110,5,'Inventari',3,'EUR',2,'ALL',122.0000,122.0000,'2013-04-26 15:03:06','0000-00-00 00:00:00',0),(111,5,'Inventari',3,'EUR',2,'ALL',133.0000,133.0000,'2013-04-26 16:43:45','0000-00-00 00:00:00',0),(112,5,'Inventari',3,'EUR',2,'ALL',144.0000,144.0000,'2013-04-26 17:26:05','0000-00-00 00:00:00',0),(113,1,'Arka',2,'ALL',2,'ALL',1.0000,1.0000,'2013-08-29 09:25:21','0000-00-00 00:00:00',0),(114,1,'Arka',2,'ALL',2,'ALL',1.0000,1.0000,'2013-08-29 09:25:24','0000-00-00 00:00:00',0);

/*Table structure for table `industrycategories` */

DROP TABLE IF EXISTS `industrycategories`;

CREATE TABLE `industrycategories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(400) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `id_industrycategory` bigint(20) DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `industrycategories` */

insert  into `industrycategories`(`id`,`category_name`,`description`,`id_industrycategory`,`modifieddate`,`deleted`) values (1,'Import/Export','Import/Export',5,'2012-03-07 11:52:59',0),(2,'Ndertim','Ndertim',7,'2012-03-07 11:53:17',0),(3,'Fason','Fason',5,'2012-03-07 11:54:02',0),(4,'Software','Software',6,'2012-11-30 20:21:14',0),(5,'Industri e lehte','Industri e lehte',0,'2012-03-07 12:02:04',0),(6,'Teknologji Informacioni','Teknologji Informacioni',0,'2012-03-07 12:02:45',0),(7,'Industri e rende','Industri e rende',0,'2012-03-07 12:03:37',0),(8,'Perpunim mineralesh','Perpunim mineralesh',7,'2012-03-07 12:04:19',0),(9,'Sherbime Publike','Sherbime Publike',0,'2012-08-28 00:00:00',0),(10,'Ujesjelles Kanalizime','Ujesjelles Kanalizime',9,'2012-08-28 00:00:00',0),(11,'Hardware','test',6,'2012-11-30 20:21:28',1),(12,'Hardware','test',6,'2012-11-30 20:23:51',1);

/*Table structure for table `language` */

DROP TABLE IF EXISTS `language`;

CREATE TABLE `language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_code` varchar(10) DEFAULT NULL,
  `language_name` varchar(50) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `language` */

/*Table structure for table `locations` */

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lat` varchar(100) DEFAULT NULL,
  `long` varchar(100) DEFAULT NULL,
  `name` varchar(400) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `locations` */

/*Table structure for table `organisationforms` */

DROP TABLE IF EXISTS `organisationforms`;

CREATE TABLE `organisationforms` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(400) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `organisationforms` */

insert  into `organisationforms`(`id`,`name`,`description`,`deleted`) values (1,'Person fizik','Person fizik',0),(2,'Shoqeri me pergjegjesi te pakufizuar','Shoqeri me pergjegjesi te pakufizuar',0),(3,'Shoqeri me pergjegjesi te kufizuar','Shoqeri me pergjegjesi te kufizuar',0),(4,'Shoqeri anonime','Shoqeri anonime',0),(5,'Shoqeri komandite','Shoqeri komandite',0),(6,'Joint-venture','Joint-venture',0);

/*Table structure for table `proves` */

DROP TABLE IF EXISTS `proves`;

CREATE TABLE `proves` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `proves` */

insert  into `proves`(`id`,`name`,`active`) values (1,'aa',NULL),(2,'a',0),(3,'a',NULL);

/*Table structure for table `rights` */

DROP TABLE IF EXISTS `rights`;

CREATE TABLE `rights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_rightsection` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `route_url` varchar(100) DEFAULT NULL,
  `right_label` varchar(200) DEFAULT NULL,
  `virtual` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'not an actual route used for dispaly',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `rights` */

insert  into `rights`(`id`,`id_rightsection`,`route_url`,`right_label`,`virtual`,`active`) values (3,1,'users-list','Users List',0,1),(4,1,'get-user','Create/Edit User',0,1),(5,1,'delete-user','Delete User',0,1),(7,2,'newapplications-list','New application List',0,1),(8,2,'new-applicant','New Applicant',0,1),(9,2,'get-applicant','Get Applicant',0,1),(10,2,'delete-applicant','Delete Applicant',0,1),(11,2,'get-processnewapp','Get Process New Applicant',0,1),(12,2,'delete-processnewapp','Delete Process New Applicant',0,1),(13,2,'application-allotments-list','Run Allotments',0,1),(14,7,'promoters-list','Promoters List',0,1),(15,7,'get-promoter','Create/Edit Promoter',0,1),(16,7,'delete-promoter','Delete Promoter',0,1),(17,1,'doctemplates-list','Doc-Templates List',0,1),(18,1,'get-doctemplate','Edit Document Template',0,1),(19,1,'parameters-list','Parameters List',0,1),(20,1,'bondseries-list','Bond Series List',0,1),(21,8,'close-bondserie','Close Bond Series',0,1),(22,8,'bonds-list','Bonds List',0,1),(24,1,'permissions','Permissions (Roles, Passwords)',0,1),(25,1,'get-role','Get Role',0,1),(26,1,'save-globalpass','Save Global Password',0,1),(27,3,'investors-list','Investors List',0,1),(28,3,'get-investor','Get Investors',0,1),(29,4,'make-transfer','Make Transfer',0,1),(30,5,'interest-allotmnets-list','Interest Allotment List',0,1),(31,6,'redemption-investors-list','Redemption List',0,1),(32,3,'get-ledgeractivity','Get Ledger Activity',0,1),(33,3,'get-notesdocument','get Ledger Notes Document',0,1),(34,3,'upload-note-docs','Upload Note Documents',0,1),(35,2,'ran-allotments-list','Ran Allotments List',0,1);

/*Table structure for table `rightsections` */

DROP TABLE IF EXISTS `rightsections`;

CREATE TABLE `rightsections` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `section_name` varchar(100) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `id_module` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1=system',
  PRIMARY KEY (`id`),
  KEY `id_module` (`id_module`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `rightsections` */

insert  into `rightsections`(`id`,`section_name`,`description`,`id_module`) values (1,'Admin and Permissions',NULL,1),(2,'Application Centre',NULL,1),(3,'Investor Centre',NULL,1),(4,'Transfer Centre',NULL,1),(5,'Interest Centre',NULL,1),(6,'Redemtion Centre',NULL,1),(7,'Promoters',NULL,1),(8,'Bonds Centre',NULL,1);

/*Table structure for table `rolerights` */

DROP TABLE IF EXISTS `rolerights`;

CREATE TABLE `rolerights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_role` int(10) unsigned NOT NULL,
  `id_right` int(10) unsigned NOT NULL,
  `route_url` varchar(200) NOT NULL,
  `access` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

/*Data for the table `rolerights` */

insert  into `rolerights`(`id`,`id_role`,`id_right`,`route_url`,`access`) values (23,2,3,'users-list',1),(24,2,4,'get-user',1),(25,2,5,'delete-user',1),(26,2,7,'newapplications-list',1),(27,2,14,'promoters-list',1),(28,2,17,'doctemplates-list',1),(29,2,18,'get-doctemplate',1),(30,2,19,'parameters-list',1),(31,2,20,'bondseries-list',1),(32,2,21,'close-bondserie',1),(33,2,22,'bonds-list',1),(34,2,23,'investor-bonds',1),(35,2,27,'investors-list',1),(36,2,30,'interest-allotmnets-list',1),(37,2,31,'redemption-investors-list',1);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `flg_delete` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'can be deleted or not',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`id`,`role_name`,`description`,`flg_delete`,`deleted`) values (1,'Super Admin','a.ka su',0,0),(2,'Admin','a.k.a supervisor',1,0),(3,'Basic User','a.k.a default user',1,0);

/*Table structure for table `taxpayerstatus` */

DROP TABLE IF EXISTS `taxpayerstatus`;

CREATE TABLE `taxpayerstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(400) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `taxpayerstatus` */

insert  into `taxpayerstatus`(`id`,`name`,`description`,`deleted`) values (1,'Pa TVSH','Nuk paguan TVSH',0),(2,'Me TVSH','Paguna TVSH',0);

/*Table structure for table `unitconversions` */

DROP TABLE IF EXISTS `unitconversions`;

CREATE TABLE `unitconversions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idunitentry` bigint(20) DEFAULT NULL,
  `idunitexit` bigint(20) DEFAULT NULL,
  `coefficient` decimal(10,4) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `subject_id` bigint(20) DEFAULT NULL,
  `purchase_default` tinyint(1) NOT NULL,
  `sales_default` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;

/*Data for the table `unitconversions` */

insert  into `unitconversions`(`id`,`idunitentry`,`idunitexit`,`coefficient`,`description`,`deleted`,`subject`,`subject_id`,`purchase_default`,`sales_default`) values (1,9,9,1000.0000,'meter kub - 1000.0000 m3',0,'item',1,1,1),(2,0,9,0.0000,' -  m3',0,'item',17,0,0),(3,0,9,0.0000,' -  m3',0,'item',18,0,0),(4,0,9,0.0000,' -  m3',0,'item',19,0,0),(5,0,10,0.0000,' -  Njesi',0,'item',20,0,0),(6,0,9,0.0000,' -  m3',1,'item',1,0,0),(7,0,9,0.0000,' -  m3',1,'item',2,0,0),(8,0,10,0.0000,' -  Njesi',1,'item',3,0,0),(9,0,10,0.0000,' -  Njesi',0,'item',4,0,0),(10,0,10,0.0000,' -  Njesi',0,'item',5,0,0),(11,0,10,0.0000,' -  Njesi',0,'item',6,0,0),(12,0,9,0.0000,' -  m3',0,'item',7,0,0),(13,0,9,0.0000,' -  m3',0,'item',8,0,0),(14,0,10,0.0000,' -  Njesi',0,'item',9,0,0),(15,0,10,0.0000,' -  Njesi',0,'item',10,0,0),(16,0,10,0.0000,' -  Njesi',0,'item',11,0,0),(17,0,10,0.0000,' -  Njesi',0,'item',12,0,0),(18,0,13,0.0000,' -  m2',0,'item',13,0,0),(19,0,10,0.0000,' -  Njesi',0,'item',14,0,0),(20,0,10,0.0000,' -  Njesi',0,'item',15,0,0),(21,0,9,0.0000,' -  m3',1,'item',16,0,0),(22,0,2,0.0000,' -  Njesi',0,'item',17,0,0),(23,0,2,0.0000,' -  Njesi',1,'item',1724,0,0),(24,0,11,0.0000,' -  LIT',0,'item',1725,0,0),(25,0,11,0.0000,' -  LIT',0,'item',1726,0,0),(26,0,14,0.0000,' -  KG',0,'item',1727,0,0),(27,0,11,0.0000,' -  LIT',0,'item',1728,0,0),(28,0,15,0.0000,' -  COPE',1,'item',1729,0,0),(29,0,15,0.0000,' -  COPE',1,'item',1730,0,0),(30,0,11,0.0000,' -  LIT',0,'item',1731,0,0),(31,0,11,0.0000,' -  LIT',0,'item',1732,0,0),(32,0,10,0.0000,' -  ML',0,'item',1733,0,0),(33,0,15,0.0000,' -  COPE',0,'item',1734,0,0),(34,0,15,0.0000,' -  COPE',0,'item',1735,0,0),(35,0,2,0.0000,' -  Njesi',0,'item',1736,0,0),(36,0,15,0.0000,' -  COPE',0,'item',1737,0,0),(37,0,15,0.0000,' -  COPE',0,'item',1738,0,0),(38,0,14,0.0000,' -  KG',0,'item',1739,0,0),(39,0,15,0.0000,' -  COPE',0,'item',1740,0,0),(40,0,1,0.0000,' -  m3',0,'item',1741,0,0),(41,0,14,0.0000,' -  KG',0,'item',1742,0,0),(42,0,12,0.0000,' -  KV',0,'item',1743,0,0),(43,0,15,0.0000,' -  COPE',0,'item',1744,0,0),(44,0,10,0.0000,' -  ML',0,'item',1745,0,0),(45,0,10,0.0000,' -  ML',0,'item',1746,0,0),(46,0,15,0.0000,' -  COPE',0,'item',1747,0,0),(47,0,15,0.0000,' -  COPE',0,'item',1748,0,0),(48,0,15,0.0000,' -  COPE',0,'item',1749,0,0),(49,0,15,0.0000,' -  COPE',0,'item',1750,0,0),(50,0,15,0.0000,' -  COPE',0,'item',1751,0,0),(51,0,15,0.0000,' -  COPE',0,'item',1752,0,0),(52,0,15,0.0000,' -  COPE',0,'item',1753,0,0),(53,0,15,0.0000,' -  COPE',1,'item',1754,0,0),(54,0,15,0.0000,' -  COPE',0,'item',1755,0,0),(55,0,15,0.0000,' -  COPE',0,'item',1756,0,0),(56,0,13,0.0000,' -  KT',0,'item',1757,0,0),(57,0,11,0.0000,' -  LIT',0,'item',1758,0,0),(58,0,11,0.0000,' -  LIT',0,'item',1759,0,0),(59,0,11,0.0000,' -  LIT',0,'item',1760,0,0),(60,0,11,0.0000,' -  LIT',0,'item',1761,0,0),(61,0,15,0.0000,' -  COPE',0,'item',1762,0,0),(62,0,15,0.0000,' -  COPE',0,'item',1763,0,0),(63,0,15,0.0000,' -  COPE',0,'item',1764,0,0),(64,0,15,0.0000,' -  COPE',0,'item',1765,0,0),(65,0,15,0.0000,' -  COPE',0,'item',1766,0,0),(66,0,15,0.0000,' -  COPE',0,'item',1767,0,0),(67,0,15,0.0000,' -  COPE',0,'item',1768,0,0),(68,0,15,0.0000,' -  COPE',0,'item',1769,0,0),(69,0,15,0.0000,' -  COPE',0,'item',1770,0,0),(70,0,15,0.0000,' -  COPE',0,'item',1771,0,0),(71,0,15,0.0000,' -  COPE',0,'item',1772,0,0),(72,0,11,0.0000,' -  LIT',0,'item',1773,0,0),(73,0,11,0.0000,' -  LIT',0,'item',1774,0,0),(74,0,11,0.0000,' -  LIT',0,'item',1775,0,0),(75,0,11,0.0000,' -  LIT',0,'item',1776,0,0),(76,0,15,0.0000,' -  COPE',0,'item',1777,0,0),(77,0,15,0.0000,' -  COPE',0,'item',1778,0,0),(78,0,15,0.0000,' -  COPE',0,'item',1779,0,0),(79,0,15,0.0000,' -  COPE',0,'item',1784,0,0),(80,0,15,0.0000,' -  COPE',0,'item',1785,0,0),(81,0,15,0.0000,' -  COPE',0,'item',1786,0,0),(82,0,15,0.0000,' -  COPE',0,'item',1787,0,0),(83,0,15,0.0000,' -  COPE',0,'item',1788,0,0),(84,0,15,0.0000,' -  COPE',0,'item',1789,0,0),(85,0,15,0.0000,' -  COPE',0,'item',1790,0,0),(86,0,15,0.0000,' -  COPE',0,'item',1791,0,0),(87,0,15,0.0000,' -  COPE',0,'item',1792,0,0),(88,0,15,0.0000,' -  COPE',0,'item',1793,0,0),(89,0,15,0.0000,' -  COPE',0,'item',1794,0,0),(90,0,15,0.0000,' -  COPE',0,'item',1795,0,0),(91,0,15,0.0000,' -  COPE',0,'item',1796,0,0),(92,0,15,0.0000,' -  COPE',0,'item',1797,0,0),(93,0,11,0.0000,' -  LIT',0,'item',1798,0,0),(94,0,16,0.0000,' -  BID',0,'item',1799,0,0),(95,0,11,0.0000,' -  LIT',0,'item',1800,0,0),(96,0,16,0.0000,' -  BID',0,'item',1801,0,0),(97,0,15,0.0000,' -  COPE',0,'item',1802,0,0),(98,0,15,0.0000,' -  COPE',0,'item',1803,0,0),(99,0,15,0.0000,' -  COPE',0,'item',1804,0,0),(100,0,15,0.0000,' -  COPE',0,'item',1805,0,0),(101,0,15,0.0000,' -  COPE',0,'item',1806,0,0),(102,0,15,0.0000,' -  COPE',0,'item',1807,0,0),(103,0,15,0.0000,' -  COPE',0,'item',1808,0,0),(104,0,15,0.0000,' -  COPE',0,'item',1809,0,0);

/*Table structure for table `unitgroups` */

DROP TABLE IF EXISTS `unitgroups`;

CREATE TABLE `unitgroups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `unitgroups` */

insert  into `unitgroups`(`id`,`name`,`description`,`deleted`) values (1,'Mase','Njesi mase si kg, gr, mgr etj',0),(2,'Gjatesi','Njesi gjatesie si m, cm, mm etj',0);

/*Table structure for table `units` */

DROP TABLE IF EXISTS `units`;

CREATE TABLE `units` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `fullunit` tinyint(1) DEFAULT NULL,
  `id_unitgroup` bigint(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `units` */

insert  into `units`(`id`,`symbol`,`name`,`description`,`fullunit`,`id_unitgroup`,`deleted`) values (1,'m3','meter kub','njesi vellimi',0,1,0),(2,'Njesi','Njesi','Njesi',1,1,0),(3,'m2','meter katror','njesi siperfaqeje',0,1,0),(4,'TON','TON',NULL,0,1,0),(5,'THAS','THAS',NULL,0,1,0),(6,'TB','TB',NULL,0,1,0),(7,'SHIS','SHIS',NULL,0,1,0),(8,'PALE','PALE',NULL,0,1,0),(9,'PAKO','PAKO',NULL,0,1,0),(10,'ML','ML',NULL,0,1,0),(11,'LIT','LIT',NULL,0,1,0),(12,'KV','KV',NULL,0,1,0),(13,'KT','KT',NULL,0,1,0),(14,'KG','KG',NULL,0,1,0),(15,'COPE','COPE',NULL,0,1,0),(16,'BID','BID',NULL,0,1,0);

/*Table structure for table `user_failed_logins` */

DROP TABLE IF EXISTS `user_failed_logins`;

CREATE TABLE `user_failed_logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_users` int(10) unsigned DEFAULT '0',
  `ip_address` char(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `attempted` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usersId` (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `user_failed_logins` */

insert  into `user_failed_logins`(`id`,`id_users`,`ip_address`,`attempted`) values (1,3,'127.0.0.1',1421147999),(2,3,'127.0.0.1',1421148008);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `email_code` varchar(100) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `id_role` tinyint(4) unsigned NOT NULL DEFAULT '2',
  `img_url` varchar(500) DEFAULT NULL,
  `description` varchar(800) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0=not active 1=active 2=Banned',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`email`,`email_code`,`firstname`,`lastname`,`id_role`,`img_url`,`description`,`tel`,`created_at`,`updated_at`,`remember_token`,`status`,`deleted`) values (1,'admin','$2y$10$z7Hnu7F8LIjiB.2Tp5Q6zuN62dNEglRfbqTn/M2ng4RmO3xhNbVHy','admin@mail.com',NULL,'John','Smith',1,NULL,NULL,'564854565','2014-11-13 00:44:53','2015-01-13 11:19:55','o2Vq1gvAW2qGRewQ9YseBNxd4APwWKfT1vlgWMG9ALyn5I6dD2IW9BufapJz',1,0),(2,'mary','$2y$10$z7Hnu7F8LIjiB.2Tp5Q6zuN62dNEglRfbqTn/M2ng4RmO3xhNbVHy','mary_black@yahoo.com',NULL,'Mary','Black',2,NULL,NULL,'+4465959656','2014-11-13 00:44:53','2014-12-13 17:20:55','ycKXHr43P82NBt5AinLAOIu5oXAocshKt4eBavCdzeLKhcBbIjzgSWWIdwr8',1,0),(3,'nick','','nick@info.com',NULL,'nick','black',3,NULL,NULL,'+356221556','2014-11-16 11:28:30','2014-12-19 15:56:25',NULL,1,0),(4,'roger.ixe','','roger@ixe.com',NULL,'roger','bradly',2,NULL,NULL,'+4445515756','2014-11-16 21:51:25','2014-11-20 12:07:38',NULL,1,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
