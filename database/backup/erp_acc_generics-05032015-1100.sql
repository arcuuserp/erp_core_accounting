/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.39 : Database - erp_acc_generics
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`erp_acc_generics` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `erp_acc_generics`;

/*Table structure for table `cash` */

DROP TABLE IF EXISTS `cash`;

CREATE TABLE `cash` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `unit_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0=cash 1=bank',
  `name` varchar(400) DEFAULT NULL,
  `code` varchar(400) DEFAULT NULL,
  `id_account` varchar(400) DEFAULT NULL,
  `account` varchar(400) NOT NULL,
  `id_currency` varchar(400) DEFAULT NULL,
  `currency` varchar(400) DEFAULT NULL,
  `id_financialstatementposition` int(11) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `opening_balance` decimal(20,5) DEFAULT NULL,
  `opening_date` datetime DEFAULT NULL,
  `opening_exchange_rate` decimal(20,5) DEFAULT NULL,
  `opening_balance_home_currency` decimal(20,5) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `cash` */

/*Table structure for table `contacts` */

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_cutomer` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_vendor` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `salutation` varchar(20) DEFAULT 'MR' COMMENT 'Language variable SALUTATION_MR',
  `contact_name` varchar(100) DEFAULT NULL,
  `contact_code` varchar(100) DEFAULT NULL,
  `id_bankaccount` int(10) unsigned NOT NULL,
  `default_bankaccount` varchar(400) DEFAULT NULL,
  `default_bank` varchar(400) NOT NULL,
  `default_bankaccount_iban` varchar(100) DEFAULT NULL,
  `default_itempricelevel` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `id_currency` smallint(6) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `debit_limit_warning` decimal(20,5) DEFAULT NULL,
  `debit_limit_block` decimal(20,5) NOT NULL,
  `individual_balance` decimal(20,5) NOT NULL,
  `credit_limit` bigint(20) DEFAULT NULL,
  `opening_balance_home_currency` decimal(20,5) DEFAULT NULL,
  `opening_balance` decimal(20,5) DEFAULT NULL,
  `opening_date` datetime DEFAULT NULL,
  `opening_exchange_rate` decimal(20,5) DEFAULT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `company_registration_code` varchar(100) DEFAULT NULL COMMENT 'nipt',
  `company_admin_title` varchar(10) DEFAULT NULL,
  `company_admin_namesurname` varchar(100) DEFAULT NULL,
  `id_organisationform` int(11) DEFAULT NULL,
  `company_address` varchar(400) DEFAULT NULL,
  `company_contact_person` varchar(100) DEFAULT NULL,
  `phonenumber` varchar(100) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `id_location` int(11) DEFAULT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `id_industrycategory` int(11) DEFAULT NULL,
  `id_account` int(11) DEFAULT NULL,
  `account` varchar(400) NOT NULL,
  `id_commercialterms` int(11) DEFAULT NULL,
  `id_administrativeregion` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_code` (`contact_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `contacts` */

/*Table structure for table `items` */

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `items` */

/*Table structure for table `tests` */

DROP TABLE IF EXISTS `tests`;

CREATE TABLE `tests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tests` */

insert  into `tests`(`id`,`name`) values (1,'one'),(2,'two'),(3,'three'),(4,'four'),(5,'five');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
