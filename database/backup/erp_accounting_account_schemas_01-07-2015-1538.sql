/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.39 : Database - erp_accounting
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`erp_accounting` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `erp_accounting`;

/*Table structure for table `item_account_schemas` */

DROP TABLE IF EXISTS `item_account_schemas`;

CREATE TABLE `item_account_schemas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_itemkind` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `code` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `item_account_schemas` */

insert  into `item_account_schemas`(`id`,`id_itemkind`,`name`,`code`) values (1,1,'Test 1','001'),(2,1,'Prove 2','002'),(3,2,'Sherbim 1','201'),(4,2,'service 22','222');

/*Table structure for table `item_account_schemas_accounts` */

DROP TABLE IF EXISTS `item_account_schemas_accounts`;

CREATE TABLE `item_account_schemas_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_schema` int(10) unsigned DEFAULT NULL,
  `field_code` varchar(200) DEFAULT NULL,
  `id_account` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `item_account_schemas_accounts` */

insert  into `item_account_schemas_accounts`(`id`,`id_schema`,`field_code`,`id_account`) values (1,1,'sales',22),(2,1,'cost',33);

/*Table structure for table `item_account_schemas_structure` */

DROP TABLE IF EXISTS `item_account_schemas_structure`;

CREATE TABLE `item_account_schemas_structure` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_itemkind` int(11) DEFAULT NULL,
  `field_code` varchar(200) DEFAULT NULL,
  `language_var` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `item_account_schemas_structure` */

insert  into `item_account_schemas_structure`(`id`,`id_itemkind`,`field_code`,`language_var`,`description`,`active`) values (1,1,'inventory','ACCOUNT_SCHEMAS.INVENTORY',NULL,1),(2,1,'purchase','ACCOUNT_SCHEMAS.PURCHASE',NULL,1),(3,1,'cost','ACCOUNT_SCHEMAS.COST',NULL,1),(4,1,'sales','ACCOUNT_SCHEMAS.SALES',NULL,1),(5,1,'purchase_discount','ACCOUNT_SCHEMAS.PURCHASE_DISCOUNT',NULL,1),(6,1,'sales_discount','ACCOUNT_SCHEMAS.SALES_DISCOUNT',NULL,1),(7,2,'cost','ACCOUNT_SCHEMAS.COST',NULL,1),(8,2,'sales','ACCOUNT_SCHEMAS.SALES',NULL,1),(9,2,'purchase_discount','ACCOUNT_SCHEMAS.PURCHASE_DISCOUNT',NULL,1),(10,2,'sales_discount','ACCOUNT_SCHEMAS.SALES_DISCOUNT',NULL,1),(11,4,'purchase','ACCOUNT_SCHEMAS.PURCHASE',NULL,1),(12,4,'cost','ACCOUNT_SCHEMAS.COST',NULL,1),(13,4,'sales','ACCOUNT_SCHEMAS.SALES',NULL,1),(14,4,'purchase_discount','ACCOUNT_SCHEMAS.PURCHASE_DISCOUNT',NULL,1),(15,4,'sales_discount','ACCOUNT_SCHEMAS.SALES_DISCOUNT',NULL,1),(16,4,'actives','ACCOUNT_SCHEMAS.ACTIVES',NULL,1),(17,4,'depriciation_expenditures','ACCOUNT_SCHEMAS.DEPRICIATION_EXPENDITURES',NULL,1),(18,4,'accumulated_depriciation','ACCOUNT_SCHEMAS.ACCUMULATED_DEPRICIATION',NULL,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
