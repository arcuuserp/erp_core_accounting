
USE `erp_accounting2`;

/* Alter table in target */
ALTER TABLE `cashreceipt` 
	ADD COLUMN `user_full_name` varchar(50)  COLLATE latin1_swedish_ci NULL after `id_docstatus` , 
	ADD COLUMN `id_user` tinyint(10) unsigned   NULL after `user_full_name` ;

/* Alter table in target */
ALTER TABLE `cashreceiptbodyaccounts` 
	ADD COLUMN `id_cashreceipt` bigint(20)   NULL first , 
	CHANGE `id` `id` int(11)   NOT NULL auto_increment after `id_cashreceipt` , 
	DROP COLUMN `id_cashreceiptheader` , 
	DROP KEY `id_cashreceiptheader`, ADD KEY `id_cashreceiptheader`(`id_cashreceipt`) ;

/* Alter table in target */
ALTER TABLE `entryadjustmentbody` 
	ADD COLUMN `id_entryadjustment` int(10) unsigned   NOT NULL after `id` , 
	CHANGE `subject` `subject` varchar(200)  COLLATE latin1_swedish_ci NULL after `id_entryadjustment` , 
	DROP COLUMN `id_entryadjustmentheader` , 
	DROP KEY `id_entryadjustmentheader`, ADD KEY `id_entryadjustmentheader`(`id_entryadjustment`) ;

/* Alter table in target */
ALTER TABLE `entrywarehousebody` 
	ADD COLUMN `id_entrywarehouse` bigint(20)   NULL first , 
	CHANGE `id` `id` int(11)   NOT NULL auto_increment after `id_entrywarehouse` , 
	DROP COLUMN `id_entrywarehouseheader` , 
	DROP KEY `id_entrywarehouseheader`, ADD KEY `id_entrywarehouseheader`(`id_entrywarehouse`) ;

/* Alter table in target */
ALTER TABLE `exitwarehousebody` 
	ADD COLUMN `id_exitwarehouse` bigint(20)   NOT NULL first , 
	CHANGE `id` `id` int(11)   NOT NULL auto_increment after `id_exitwarehouse` , 
	DROP COLUMN `id_exitwarehouseheader` , 
	DROP KEY `id_exitwarehouseheader`, ADD KEY `id_exitwarehouseheader`(`id_exitwarehouse`) ;

/* Create table in target */
CREATE TABLE `intervalet`(
	`label_vl` varchar(100) COLLATE latin1_swedish_ci NULL  , 
	`label_p` varchar(100) COLLATE latin1_swedish_ci NULL  , 
	`lower` tinyint(4) NULL  , 
	`upper` tinyint(4) NULL  
) ENGINE=InnoDB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Create table in target */
CREATE TABLE `intervalet_od`(
	`label_vl` varchar(100) COLLATE latin1_swedish_ci NULL  , 
	`label_p` varchar(100) COLLATE latin1_swedish_ci NULL  , 
	`lower` tinyint(4) NULL  , 
	`upper` tinyint(4) NULL  
) ENGINE=InnoDB DEFAULT CHARSET='latin1' COLLATE='latin1_swedish_ci';


/* Alter table in target */
ALTER TABLE `p_payment` 
	ADD COLUMN `id_cashpayment` bigint(20)   NOT NULL after `id` , 
	CHANGE `id_currency` `id_currency` tinyint(4)   NOT NULL after `id_cashpayment` , 
	DROP COLUMN `id_cashpaymentheader` , 
	DROP KEY `id_cashpaymentheader`, ADD KEY `id_cashpaymentheader`(`id_cashpayment`) ;

/* Alter table in target */
ALTER TABLE `payment_rate` 
	ADD COLUMN `id` bigint(20)   NOT NULL auto_increment first , 
	CHANGE `id_rate_agreement` `id_rate_agreement` bigint(20)   NOT NULL after `id` , 
	DROP COLUMN `payment_rate_id` , 
	DROP KEY `PRIMARY`, ADD PRIMARY KEY(`id`) ;

/* Alter table in target */
ALTER TABLE `payment_rate_term_calc` 
	ADD COLUMN `id` bigint(20) unsigned   NOT NULL auto_increment first , 
	CHANGE `id_payment_rate` `id_payment_rate` bigint(20) unsigned   NOT NULL after `id` , 
	DROP COLUMN `payment_rate_term_calc_id` , 
	DROP KEY `PRIMARY`, ADD PRIMARY KEY(`id`) ;

/* Alter table in target */
ALTER TABLE `proposedofferbody` 
	ADD COLUMN `id_proposedoffer` int(10) unsigned   NOT NULL first , 
	CHANGE `id` `id` int(11)   NOT NULL auto_increment after `id_proposedoffer` , 
	DROP COLUMN `id_proposedofferheader` ;

/* Alter table in target */
ALTER TABLE `purchasediscountinvoice` 
	ADD COLUMN `id_purchaseinvoice` int(10) unsigned   NOT NULL after `id_exitwarehouseheader` , 
	CHANGE `id_supplier` `id_supplier` int(11)   NOT NULL after `id_purchaseinvoice` , 
	DROP COLUMN `id_purchaseinvoiceheader` ;

/* Alter table in target */
ALTER TABLE `purchasediscountinvoicebody` 
	ADD COLUMN `id_purchasediscountinvoice` bigint(20)   NULL after `id` , 
	CHANGE `id_item` `id_item` int(11)   NULL after `id_purchasediscountinvoice` , 
	DROP COLUMN `id_purchasediscountinvoiceheader` , 
	DROP KEY `id_purchasediscountinvoiceheader`, ADD KEY `id_purchasediscountinvoiceheader`(`id_purchasediscountinvoice`) , 
	DROP KEY `id_returninvoiceheader`, ADD KEY `id_returninvoiceheader`(`id_purchasediscountinvoice`) ;

/* Alter table in target */
ALTER TABLE `purchasediscountinvoicebodyaccounts` 
	ADD COLUMN `id_purchasediscountinvoice` bigint(20)   NULL after `id` , 
	CHANGE `id_account` `id_account` int(11)   NULL after `id_purchasediscountinvoice` , 
	DROP COLUMN `id_purchasediscountinvoiceheader` , 
	DROP KEY `id_purchasediscountinvoiceheader`, ADD KEY `id_purchasediscountinvoiceheader`(`id_purchasediscountinvoice`) ;

/* Alter table in target */
ALTER TABLE `purchaseinvoicebody` 
	ADD COLUMN `id_purchaseinvoice` bigint(20)   NULL first , 
	CHANGE `id` `id` int(11)   NOT NULL auto_increment after `id_purchaseinvoice` , 
	DROP COLUMN `id_purchaseinvoiceheader` , 
	DROP KEY `id_purchaseinvoiceheader`, ADD KEY `id_purchaseinvoiceheader`(`id_purchaseinvoice`) ;

/* Alter table in target */
ALTER TABLE `purchaseorderbody` 
	ADD COLUMN `id_purchaseorder` int(11)   NULL first , 
	CHANGE `id` `id` int(11)   NOT NULL auto_increment after `id_purchaseorder` , 
	DROP COLUMN `id_purchaseorderheader` ;

/* Alter table in target */
ALTER TABLE `r_payment` 
	ADD COLUMN `id_cashreceipt` bigint(20)   NOT NULL after `id` , 
	CHANGE `id_currency` `id_currency` tinyint(4)   NOT NULL after `id_cashreceipt` , 
	DROP COLUMN `id_cashreceiptheader` , 
	DROP KEY `id_cashreceiptheader`, ADD KEY `id_cashreceiptheader`(`id_cashreceipt`) ;

/* Alter table in target */
ALTER TABLE `receivedofferbody` 
	ADD COLUMN `id_receivedoffer` int(10) unsigned   NOT NULL first , 
	CHANGE `id` `id` int(11)   NOT NULL auto_increment after `id_receivedoffer` , 
	DROP COLUMN `id_receivedofferheader` ;

/* Alter table in target */
ALTER TABLE `requestofferbody` 
	ADD COLUMN `id_requestoffer` int(11)   NULL first , 
	CHANGE `id` `id` int(11)   NOT NULL auto_increment after `id_requestoffer` , 
	DROP COLUMN `id_requestofferheader` ;

/* Alter table in target */
ALTER TABLE `salesdiscountinvoice` 
	ADD COLUMN `id_saleinvoice` int(10) unsigned   NOT NULL after `id_entrywarehouseheader` , 
	CHANGE `id_customer` `id_customer` int(11)   NOT NULL after `id_saleinvoice` , 
	DROP COLUMN `id_saleinvoiceheader` ;

/* Alter table in target */
ALTER TABLE `salesdiscountinvoicebody` 
	ADD COLUMN `id_salesdiscountinvoice` bigint(20)   NULL after `id` , 
	CHANGE `id_item` `id_item` int(11)   NULL after `id_salesdiscountinvoice` , 
	DROP COLUMN `id_salesdiscountinvoiceheader` , 
	DROP KEY `id_salesdiscountinvoiceheader`, ADD KEY `id_salesdiscountinvoiceheader`(`id_salesdiscountinvoice`) ;

/* Alter table in target */
ALTER TABLE `salesinvoice` 
	ADD COLUMN `id_salesorder` bigint(20)   NULL after `id` , 
	ADD COLUMN `id_proposedoffer` bigint(20)   NULL after `id_salesorder` , 
	CHANGE `idwarehouse_exit` `idwarehouse_exit` bigint(20)   NOT NULL after `id_proposedoffer` , 
	ADD COLUMN `billingperiod_month_human` varchar(40)  COLLATE latin1_swedish_ci NULL after `paid_status` , 
	ADD COLUMN `total_interest_overdue` decimal(20,5)   NULL after `billingperiod_month_human` , 
	DROP COLUMN `id_salesorderheader` , 
	DROP COLUMN `id_proposedofferheader` ;

/* Alter table in target */
ALTER TABLE `salesinvoicebody` 
	ADD COLUMN `id_saleinvoice` bigint(20)   NULL first , 
	CHANGE `id` `id` bigint(20)   NOT NULL auto_increment after `id_saleinvoice` , 
	DROP COLUMN `id_saleinvoiceheader` , 
	DROP KEY `id_saleinvoiceheader`, ADD KEY `id_saleinvoiceheader`(`id_saleinvoice`) ;

/* Alter table in target */
ALTER TABLE `salesinvoicebodyaccounts` 
	ADD COLUMN `id_saleinvoice` bigint(20)   NULL first , 
	CHANGE `id` `id` int(11)   NOT NULL auto_increment after `id_saleinvoice` , 
	DROP COLUMN `id_saleinvoiceheader` , 
	DROP KEY `id_saleinvoiceheader`, ADD KEY `id_saleinvoiceheader`(`id_saleinvoice`) ;

/* Alter table in target */
ALTER TABLE `salesorderbody` 
	ADD COLUMN `id_salesorder` int(11)   NULL first , 
	CHANGE `id` `id` int(11)   NOT NULL auto_increment after `id_salesorder` , 
	DROP COLUMN `id_salesorderheader` ;